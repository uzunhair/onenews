v2.9.0_09_07
+ Уменьшен шрифт заголовка в списке вывода Таблица
+ Убрано дублирование заголовка Комментирования в объявлениях
+ Дополнение к документации - Пользователи
+ Убран дубль шапки в подписчиках Канала
+ Исправлен вывод аватарки подписчиков Канала
+ Добавлено обтекание полей в типах контента
+ Внесены поправки в Chosen
+ Доработан компонент видео
+ Исправлен вывод мультисписка в фильтре
+ Изменен исходный код стилей default_list_tiles.tpl.php, default_list_wookmark.tpl.php



v2.9.0_09_06
+ Исправлен вывод фотографии в default_list.tpl.php

v2.9.0_09_05
+ Исправлено отображение виджета Календарь событий
+ Исправлен виджет вывода событий
+ Исправлен вывод событий в списке и записи
+ Поправлен вывод оценки объекта
+ Разделены классы карты компонента и виджета
+ Переделан виджет Список новостей
+ Исправлено отображение уведомление пустой категории Карты
+ Исправлено отображение info_bar в объявлениях
+ Добавлено визуальное разграничение уведомлений в модальном окне
+ Исправлен вывод активного пункт даты в Афише событий

v2.9.0_09_04
+ Добавлена новая позиция для меню действий breadcrumbs, просто поставьте виджет на эту позицию и у вас появится меню как в дефолтном шаблоне, что позволит вам освободить сайдбар
+ Исправлена ошибка отображение Chosen плагина
+ Добавлена заглушка фото в виджет Карусель контента
+ Исправлено отображение картинки в категории iVideo
+ Доработана проверка наличия фото в списке записей Maps
+ Переделан показ доп. ссылок в заголовке виджета


v2.5.1_09_3

+ Изменен внешний вид фильтра.
 
+ Добавлена возможность вывода иконок в меню.

+ Исправлены ошибки.
```
#!html

<!-- apple-touch-icon -->
<link rel="apple-touch-icon" sizes="57x57" href="/upload/favicons/apple-touch-icon-57x57.png?v=kPPe5p8yqB">
<link rel="apple-touch-icon" sizes="60x60" href="/upload/favicons/apple-touch-icon-60x60.png?v=kPPe5p8yqB">
<link rel="apple-touch-icon" sizes="72x72" href="/upload/favicons/apple-touch-icon-72x72.png?v=kPPe5p8yqB">
<link rel="apple-touch-icon" sizes="76x76" href="/upload/favicons/apple-touch-icon-76x76.png?v=kPPe5p8yqB">
<link rel="apple-touch-icon" sizes="114x114" href="/upload/favicons/apple-touch-icon-114x114.png?v=kPPe5p8yqB">
<link rel="apple-touch-icon" sizes="120x120" href="/upload/favicons/apple-touch-icon-120x120.png?v=kPPe5p8yqB">
<link rel="apple-touch-icon" sizes="144x144" href="/upload/favicons/apple-touch-icon-144x144.png?v=kPPe5p8yqB">
<link rel="apple-touch-icon" sizes="152x152" href="/upload/favicons/apple-touch-icon-152x152.png?v=kPPe5p8yqB">
<link rel="apple-touch-icon" sizes="180x180" href="/upload/favicons/apple-touch-icon-180x180.png?v=kPPe5p8yqB">
<link rel="icon" type="image/png" href="/upload/favicons/favicon-32x32.png?v=kPPe5p8yqB" sizes="32x32">
<link rel="icon" type="image/png" href="/upload/favicons/android-chrome-192x192.png?v=kPPe5p8yqB" sizes="192x192">
<link rel="icon" type="image/png" href="/upload/favicons/favicon-96x96.png?v=kPPe5p8yqB" sizes="96x96">
<link rel="icon" type="image/png" href="/upload/favicons/favicon-16x16.png?v=kPPe5p8yqB" sizes="16x16">
<link rel="manifest" href="/upload/favicons/manifest.json?v=kPPe5p8yqB">
<link rel="mask-icon" href="/upload/favicons/safari-pinned-tab.svg?v=kPPe5p8yqB" color="#ffffff">
<link rel="shortcut icon" href="/upload/favicons/favicon.ico?v=kPPe5p8yqB">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/upload/favicons/mstile-144x144.png?v=kPPe5p8yqB">
<meta name="theme-color" content="#ffffff">
<meta content="/upload/favicons/browserconfig.xml" name="msapplication-config">
```