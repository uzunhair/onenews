module.exports = {
    path: {
        build: {
            templates: 'templates/',
            upload: 'upload/',
            system: 'system/',
            css: 'templates/OneNews/css/',
            js: 'templates/OneNews/js/',
        },
        src: {
            templates: 'src/templates/**/*.*',
            upload: 'src/upload/**/*.*',
            system: 'src/system/**/*.*',
            css: 'src/templates/OneNews/css/**/*.*',
            js: 'src/templates/OneNews/js/**/*.*',
        },
        watch: {
            templates: 'src/templates/**/*.*',
            upload: 'src/upload/**/*.*',
            system: 'src/system/**/*.*',
            css: 'src/templates/OneNews/css/**/*.*',
            js: 'src/templates/OneNews/js/**/*.*',
        },
        clean: {
            templates: 'templates/OneNews/',
        }
    }
};