var gulp = require('gulp'),
    config = require('./config.js'),
    plugin = require('gulp-load-plugins')();

gulp.task('js:dev', function (cb) {
    gulp.src(config.path.src.js)
        .pipe(plugin.plumber())
        .pipe(gulp.dest(config.path.build.js));
    cb();
});

gulp.task('js:prod', function (cb) {
    gulp.src(config.path.src.js)
        .pipe(plugin.uglify())
        .pipe(plugin.plumber())
        .pipe(gulp.dest(config.path.build.js));
    cb();
});