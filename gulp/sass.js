var gulp = require('gulp'),
    config = require('./config.js'),
    plugin = require('gulp-load-plugins')();

gulp.task('css:dev', function (cb) {
    gulp.src(config.path.src.css)
        .pipe(plugin.plumber())
        .pipe(plugin.sourcemaps.init({largeFile: true}))
        .pipe(plugin.sass().on('error', plugin.sass.logError))
        .pipe(plugin.sassUnicode())
        .pipe(plugin.autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(plugin.pxtorem())
        .pipe(plugin.sourcemaps.write(''))
        .pipe(gulp.dest(config.path.build.css));
    cb();
});

gulp.task('css:prod', function (cb) {
  gulp.src(config.path.src.css)
    .pipe(plugin.plumber())
    .pipe(plugin.sass().on('error', plugin.sass.logError))
    .pipe(plugin.sassUnicode())
    .pipe(plugin.cssnano({zindex: false}))
    .pipe(plugin.autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(plugin.pxtorem())
    .pipe(plugin.rename({suffix: '.min'}))
    .pipe(plugin.duration('style.min:build time'))
    .pipe(gulp.dest(config.path.build.css));
  cb();
});