var gulp = require('gulp');
var config = require('./config.js');

gulp.task('templates', function(cb) {
    gulp.src(config.path.src.templates)
      .pipe(gulp.dest(config.path.build.templates));
    cb();
});

gulp.task('system', function(cb) {
    gulp.src(config.path.src.system)
      .pipe(gulp.dest(config.path.build.system));
    cb();
});

gulp.task('upload', function(cb) {
    gulp.src(config.path.src.upload)
      .pipe(gulp.dest(config.path.build.upload));
    cb();
});