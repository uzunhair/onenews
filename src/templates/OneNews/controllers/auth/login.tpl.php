<?php
    $this->setPageTitle(LANG_LOG_IN);
    $this->addBreadcrumb(LANG_LOG_IN);
    $is_ajax = $this->controller->request->isAjax();
?>

<?php if($is_ajax && $captcha_html){ ?>
    <script>window.location.href='<?php echo $this->href_to('login'); ?>';</script>
    <?php return; ?>
<?php } ?>

<?php if($is_ajax){ ?>
    <div style="padding: 20px">
<?php } ?>

<div class="panel panel-default login_layout" style="margin: 0px auto; max-width: 470px;">
<div class="panel-heading">
<?php echo LANG_LOG_IN_ACCOUNT; ?>
</div>
    <div class="panel-body">
    <?php if (!$is_ajax){ ?>
<div class="alert alert-info" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
 <?php echo LANG_PLEASE_LOGIN; ?>
</div>
    <?php } ?>
            <form class="form-horizontal"  action="<?php echo $this->href_to('login'); ?>" method="POST">
	<?php if ($back_url){ echo html_input('hidden', 'back', $back_url); } ?>
                <div class="login_form">
  <div class="form-group">
	<label class="col-sm-2 control-label"><?php echo LANG_EMAIL; ?>:</label>
    <div class="col-sm-10">
	  <div class="input-group">
		<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
		<?php echo html_input('text', 'login_email', '', array('id'=>'login_email', 'required'=>true, 'autofocus'=>true)); ?>
	  </div>
    </div>
  </div>
    <div class="form-group">
	<label class="col-sm-2 control-label"><?php echo LANG_PASSWORD; ?>:</label>
    <div class="col-sm-10">
	  <div class="input-group">
		<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
		<?php echo html_input('password', 'login_password', '', array('required'=>true)); ?>
	  </div>
    </div>
  </div>
  <div class="form-group <?php if (!$captcha_html) {echo 'margin-b0';}?>">
    <div class="col-sm-10 col-sm-offset-2">
      <?php echo $captcha_html; ?>
    </div>
  </div> 
   <div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
	  <div class="input-group">
		<label class="checkbox-inline padding-r5" for="remember" style="margin-top: -10px;">
			<input type="checkbox" id="remember" name="remember" value="1" /> <?php echo LANG_REMEMBER_ME; ?>
		</label>
		<a href="<?php echo $this->href_to('restore'); ?>"><?php echo LANG_FORGOT_PASS; ?></a>
	  </div>
    </div>
  </div>  
   <div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
	  <div class="input-group">
		<?php echo html_submit((LANG_LOG_IN), 'submit'); ?>

		<div class="margin-t5">
    <?php echo LANG_NO_ACCOUNT; ?> <a href="<?php echo $this->href_to('register'); ?>"><?php echo LANG_REGISTRATION; ?></a>
</div>
	  </div>
    </div>
  </div> 
               
                    


                </div>

            </form>
        <?php /*

        <td class="center_cell" valign="top">
            <div>или</div>
        </td>

        <td class="right_cell" valign="top">

            <h3><?php echo LANG_LOG_IN_OPENID; ?></h3>

            <p>В разработке</p>

        </td>
         *
         *
         */ ?>

    </div>
</div>
<?php if($is_ajax){ ?>
    </div>
<?php } ?>
