<?php // Шаблон одной записи на стене // ?>

<?php
    $count = 0;
    if (empty($max_entries)){ $max_entries = false; }
    if (empty($page)){ $page = false; }
?>

<?php foreach($entries as $entry){ ?>

<?php

    $count++;

    $is_can_add = !empty($permissions['reply']) && $entry['parent_id'] == 0;
    $is_can_edit = ($entry['user']['id']==$user->id) || $user->is_admin;
    $is_can_delete = ($entry['user']['id']==$user->id) || $permissions['delete'];

    if (empty($entry['replies_count'])){ $entry['replies_count'] = 0; }

    $is_hidden = ($max_entries && ($count > $max_entries) && ($page == 1));

?>

<div id="entry_<?php echo $entry['id']; ?>" class="entry media"<?php if($is_hidden){ ?> style="display:none"<?php } ?> data-replies="<?php echo $entry['replies_count']; ?>">

        <div class="pull-left avatar-wheel">
            <a href="<?php echo href_to('users', $entry['user']['id']); ?>">
                <?php echo html_avatar_image($entry['user']['avatar'], ($entry['parent_id'] ? 'micro' : 'micro'), $entry['user']['nickname']); ?>
            </a>
        </div>
        <div class="content comment-media-body">
            
            <div class="comment-head">
            <div class="clearfix">
                <div class="pull-left">
                    <a class="entry_user" href="<?php echo href_to('users', $entry['user']['id']); ?>">
                        <?php echo $entry['user']['nickname']; ?>
                    </a>
                    <span class="entry_date text-muted padding-l5">
                        <?php echo html(string_date_age_max($entry['date_pub'], true)); ?>
                    </span>
                    <a  class="padding-l5" href="?wid=<?php echo $entry['id']; ?>" title="<?php echo LANG_WALL_ENTRY_ANCHOR; ?>">#</a>
                </div>

                <div class="anchor pull-right">
    <div class="links<?php if ($entry['replies_count']){ ?> has_replies<?php } ?>">
        <?php if ($entry['replies_count']){ ?>
            <a href="#wall-replies" class="get_replies" onclick="return icms.wall.replies(<?php echo $entry['id']; ?>)">
                <?php echo html_spellcount($entry['replies_count'], LANG_REPLY_SPELLCOUNT); ?>
            </a>
        <?php } ?>
        <?php if ($is_can_add){ ?>
            <a href="#wall-reply" class="reply" onclick="return icms.wall.add(<?php echo $entry['id']; ?>)" data-toggle="tooltip" data-placement="left" title="<?php echo LANG_REPLY; ?>"><span class="glyphicon glyphicon-share-alt"></span> 
            </a>
        <?php } ?>
        <?php if ($is_can_edit){ ?>
            <a href="#wall-edit" class="edit" onclick="return icms.wall.edit(<?php echo $entry['id']; ?>)" data-toggle="tooltip" data-placement="left" title="<?php echo LANG_EDIT; ?>">
            <span aria-hidden="true" class="glyphicon glyphicon-pencil"></span>
            </a>
        <?php } ?>
        <?php if ($is_can_delete){ ?>
            <a href="#wall-delete" class="delete" onclick="return icms.wall.remove(<?php echo $entry['id']; ?>)" data-toggle="tooltip"  data-placement="left" title="<?php echo LANG_DELETE; ?>">
                <span class="glyphicon glyphicon-remove"></span>
            </a>
        <?php } ?>
    </div>
                </div>
            </div>
        </div>
            <div class="comment-text img-responsive-div">
                <?php echo $entry['content_html']; ?>
            </div>
        </div>


    <div class="replies_loading loading"><?php echo LANG_LOADING; ?></div>
    <?php if (!$entry['parent_id']) { ?>
        <div class="replies"></div>
    <?php } ?>
</div>

<?php if ($max_entries && ($count == $max_entries) && (sizeof($entries) > $count) && ($page == 1)){ ?>
    <div class="show_more text-center margin-t10">
        <a href="#wall-more" onclick="return icms.wall.more()" class="btn btn-default"><?php echo LANG_SHOW_ALL; ?></a>
    </div>
<?php } ?>

<?php } ?>
