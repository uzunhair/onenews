<?php if ($photos){ ?>
    <?php $disable_owner = isset($disable_owner) ? true : false; ?>
    <?php foreach($photos as $photo){ ?>

        <?php
            $is_photo_owner = ($is_owner || $photo['user_id'] == $user->id) && !$disable_owner;
            $photo_url = $photo['slug'] ? href_to('photos', $photo['slug'].'.html') : '#';
            $photo['title'] = $photo_url=='#' ? LANG_PHOTOS_NO_PUB : $photo['title'];
        ?>

        <figure class="photo photo-<?php echo $photo['id']; ?> <?php if ($is_photo_owner) { ?> is_my_photo<?php } ?> <?php echo (($photo_url=='#') ? 'unpublished' : ''); ?>" data-w="<?php echo $photo['sizes'][$preset_small]['width']; ?>" data-h="<?php echo $photo['sizes'][$preset_small]['height']; ?>" itemscope itemtype="http://schema.org/ImageObject">
            <a data-src="<?php echo html_image_src($photo['image'], 'big', true, false); ?>" class="is_selector" data-toggle="tooltip" data-placement="bottom"
               title="Открыть в лайтбоксе"
               data-sub-html='<div class="fb-comments" id="<?php html($photo['id']); ?>" data-ti="<?php html($photo['id']); ?>" data-ui="<?php html($photo['user_id']); ?>"></div><h4><?php html(string_short($photo['title'], 20)); ?></h4>'><span
                    class="glyphicon glyphicon-search"></span></a>
            <a class="photo-a"  itemprop="contentUrl"  href="<?php echo $photo_url; ?>#<?php echo ($photo['id']); ?>" data-toggle="tooltip" data-placement="bottom" title="Перейти на страницу">
                <img src="<?php echo html_image_src($photo['image'], $preset_small, true, false); ?>" title="<?php html($photo['title']); ?>" alt="<?php html($photo['title']); ?>" itemprop="thumbnail" />
            </a>
            <figcaption class="relative" itemprop="caption description">
                <div class="info<?php if ($is_photo_owner) { ?> info-3<?php } ?>">
                    <div class="info-title" title="<?php html($photo['title']); ?>" itemprop="name">
                        <?php html($photo['title']); ?>
                    </div>

                    <ul class="info-list">
                        <?php if ($is_photo_owner) { ?>
                            <li>
                                <a title="<?php echo LANG_DELETE; ?>" class="delete" href="#" data-id="<?php echo $photo['id']; ?>">
                                    <span class="glyphicon glyphicon-minus-sign"></span>
                                </a>
                            </li>
                        <?php } ?>
                        <li class="hits-count" title="<?php echo LANG_HITS; ?>">
                            <span class="glyphicon glyphicon-eye-open"></span>
                            <?php echo $photo['hits_count']; ?>
                        </li>
                        <li title="<?php echo LANG_RATING; ?>" class="rating <?php echo html_signed_class($photo['rating']); ?>">
                            <span aria-hidden="true" class="glyphicon glyphicon-star"></span>
                            <?php echo html_signed_num($photo['rating']); ?>
                        </li>
                        <li class="comments" title="<?php echo LANG_COMMENTS; ?>">
                            <span class="glyphicon glyphicon-comment"></span>
                            <span><?php echo $photo['comments']; ?></span>
                        </li>
                    </ul>
                    <meta itemprop="height" content="<?php echo $photo['sizes'][$preset_small]['height']; ?> px">
                    <meta itemprop="width" content="<?php echo $photo['sizes'][$preset_small]['width']; ?> px">
                </div>
            </figcaption>
        </figure>

    <?php } ?>

    <script type="text/javascript">
        <?php if(isset($has_next) || isset($page) || empty($disable_flex)){ ?>
            <?php if(isset($has_next)){ ?>
                <?php if($has_next){ ?>
                    icms.photos.has_next = true;
                <?php } else { ?>
                    icms.photos.has_next = false;
                <?php } ?>
            <?php } ?>
            <?php if(isset($page)){ ?>
                icms.photos.page = <?php echo $page; ?>;
            <?php } ?>
            <?php if(empty($disable_flex)){ ?>
                icms.photos.flexImagesInit('<?php echo (isset($photo_wrap_id) ? '#'.$photo_wrap_id : ''); ?>');
            <?php } ?>
        <?php } ?>
        <?php if(!empty($item['photos_url_params'])){ ?>
            $(function(){
                $('.photo_page_link').each(function (){
                    $(this).attr('href', $(this).attr('href')+'?<?php echo $item['photos_url_params']; ?>');
                });
            });
        <?php } ?>
    </script>
<?php }
