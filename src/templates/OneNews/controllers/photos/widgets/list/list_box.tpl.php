<?php
$this->addCSS($this->getStylesFileName('photos'));
$this->addJS($this->getJavascriptFileName('photos'));
$this->addJS($this->getJavascriptFileName('jquery-flex-images'));
$photo_wrap_id = 'widget-photos-'.$widget->id;
?>
<div class="album-photos-wrap" id="<?php echo $photo_wrap_id; ?>"<?php if ($is_owner) { ?> data-delete-url="<?php echo href_to('photos', 'delete'); ?>"<?php } ?>>
    <?php foreach($photos as $photo){ ?>
        <?php
            $is_photo_owner = $is_owner || $photo['user_id'] == $user->id;
            $photo_url = $photo['slug'] ? href_to('photos', $photo['slug'].'.html') : '#';
            $photo['title'] = $photo_url=='#' ? LANG_PHOTOS_NO_PUB : $photo['title'];
        ?>
        <div class="item photo photo-<?php echo $photo['id']; ?>" data-w="<?php echo $photo['sizes'][$preset_small]['width']; ?>" data-h="<?php echo $photo['sizes'][$preset_small]['height']; ?>">
            <a href="<?php echo $photo_url; ?>#<?php html($photo['id']); ?>" title="<?php html($photo['title']); ?>">
                <img src="<?php echo html_image_src($photo['image'], $preset_small, true, false); ?>" class="img-responsive" alt="<?php html($photo['title']); ?>">
            </a>
        </div>
    <?php } ?>
</div>

<script type="text/javascript">
    $('#<?php echo $photo_wrap_id; ?>').flexImages({rowHeight: <?php if (!isset($height_photo)) { echo $row_height; } else { echo $height_photo;} ; ?>});
</script>

