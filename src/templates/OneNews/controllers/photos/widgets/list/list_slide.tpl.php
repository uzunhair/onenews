<?php if ($photos){ ?>
	<div class="owl-carousel sl_photo sl_id_<?php echo $widget->id; ?>">
		<?php foreach($photos as $photo) { ?>
            <?php
                $photo_url = $photo['slug'] ? href_to('photos', $photo['slug'].'.html') : '#';
                $photo['title'] = $photo_url=='#' ? LANG_PHOTOS_NO_PUB : $photo['title'];
            ?>
			<div class="item" data-hash="img<?php echo $photo['id']; ?>">
				<div class="sl_ph_img">
					<a href="<?php echo $photo_url; ?>">
						<?php echo html_image($photo['image'], 'big'); ?>
					</a>
				</div>
                <div class="sl_info">
                    <div class="sl_desc">
                        <div class="sl_desc_hover">
                            <a href="<?php echo $photo_url; ?>" class="sl_a"><?php html($photo['title']); ?></a>
                            <ul class="list-inline sl_rating">
                                <li>
                                    <a data-toggle="tooltip" title="<?php html($photo['user_nickname']); ?>"
                                       href="/users/<?php html($photo['user_id']); ?>">
                                            <span aria-hidden="true"
                                                  class="glyphicon glyphicon-user sl_user_ico"></span>
                                    </a>
                                </li>
                                <li data-toggle="tooltip" title="Рейтинг">
                                    <span aria-hidden="true" class="glyphicon glyphicon-star"></span>
                                    <?php html($photo['rating']); ?>
                                </li>
                                <li data-toggle="tooltip" title="Комментарии">
                                    <span aria-hidden="true" class="glyphicon glyphicon-comment"></span>
                                    <?php html($photo['comments']); ?>
                                </li>
                                <li data-toggle="tooltip" title="Дата публикации">
                                    <span aria-hidden="true" class="glyphicon glyphicon-calendar"></span>
                                    <?php html(string_date_age_max($photo['date_pub'], true)); ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
			</div>
		<?php } ?>
	</div>
    <div class="owl-carousel sl-fff">
        <?php foreach ($photos as $photo) { ?>
            <div class="item">
                <a href="#img<?php echo $photo['id']; ?>" class="secondary url">
                    <?php echo html_image($photo['image'], 'small'); ?>
                </a>
            </div>
        <?php } ?>
    </div>

<?php $this->addJS("templates/{$this->name}/theme_js/imagesloaded.pkgd.min.js"); ?>
<?php $this->addJS("templates/{$this->name}/theme_js/owl.carousel.min.js"); ?>
<?php $this->addCSS("templates/{$this->name}/theme_css/owl.carousel.css"); ?>

    <script>
        $(document).ready(function () {
            $('.sl-fff').owlCarousel({
                loop:true,
                margin:10,
                nav:false,
                responsive:{
                    0:{
                        items:5
                    },
                    480:{
                        items:6
                    },
                    768:{
                        items:8
                    }
                }
            });

            $('.sl_photo').imagesLoaded(function () {
                $(".sl_id_<?php echo $widget->id; ?>").owlCarousel({
                    items: 1,
                    loop: false,
                    autoplay: false,
                    smartSpeed: 500,
                    autoplayHoverPause: true,
                    URLhashListener: true,
                    autoHeight: true,
                    startPosition: 'URLHash'
                });
            });
        });
    </script>


<?php } ?>