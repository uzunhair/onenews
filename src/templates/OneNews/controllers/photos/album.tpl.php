<?php $this->addJS( $this->getJavascriptFileName('photos') ); ?>
<?php $this->addJS( $this->getJavascriptFileName('jquery-flex-images') ); ?>
<?php $this->addCSS("templates/{$this->name}/addon/lightGallery/css/lightgallery.min.css"); ?>
<div class="position-r">
    <div class="album-photos-wrap clearfix" id="album-photos-list"<?php if ($is_owner || !empty($item['is_public'])) { ?> data-delete-url="<?php echo $this->href_to('delete'); ?>"<?php } ?> itemscope="" itemtype="http://schema.org/ImageGallery">
    <?php echo $this->renderChild('photos', array(
        'photos'   => $photos,
        'item'     => $item,
        'is_owner' => $is_owner,
        'user'     => $user,
        'has_next' => $has_next,
        'preset_small' => $preset_small,
        'page'     => $page
    )); ?>
    </div>


<?php if($photos && ($has_next || (!$has_next && $page > 1))){ ?>
<a class="show-more btn btn-default display-b text-center margin-b15" href="<?php echo $item['base_url'].((strpos($item['base_url'], '?') !== false) ? '&' : '?').'photo_page='.($has_next ? ($page+1) : 1); ?>" onclick="return icms.photos.showMore(this);" data-url="<?php echo href_to('photos', 'more', array($item_type, $item['id'])); ?>" data-url-params="<?php html(json_encode($item['url_params'])); ?>" data-first-page-url="<?php echo $item['base_url']; ?>">
    <span data-to-first="<?php echo LANG_RETURN_TO_FIRST; ?>">
        <?php if($has_next){ echo LANG_SHOW_MORE; } else { echo LANG_RETURN_TO_FIRST; } ?>
    </span>
    <div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
</a>

<script type="text/javascript">
    icms.photos.initial_page = <?php echo $page; ?>;
    icms.photos.init = true;
    icms.photos.mode = 'album';
</script>

<?php } ?>

    <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
    <script src="/templates/OneNews/addon/lightGallery/js/lightgallery.js"></script>
    <script src="/templates/OneNews/addon/lightGallery/js/lg-autoplay.min.js"></script>
    <script src="/templates/OneNews/addon/lightGallery/js/lg-zoom.min.js"></script>
    <script src="/templates/OneNews/addon/lightGallery/js/lg-hash.min.js"></script>
    <script src="/templates/OneNews/addon/lightGallery/js/lg-pager.min.js"></script>
    <script src="/templates/OneNews/addon/lightGallery/lib/jquery.mousewheel.min.js"></script>

<script>
    $(document).ready(function() {
        $("#album-photos-list").lightGallery({
            addClass: 'fb-comments',
            selector: '.is_selector',
            appendSubHtmlTo: '.lg-item',
            mode: 'lg-fade',
            download: false,
            enableDrag: false,
            enableSwipe: false,
            loop:false
        });
        $("#album-photos-list").on('onAfterSlide.lg', function(event, prevIndex, index) {
            if (!$('.lg-outer .lg-item').eq(index).attr('data-load')) {
                try {
                    $('.lg-outer .lg-item').eq(index).attr('data-load', 'loaded');
                    var ti = $('.lg-outer .lg-item').eq(index).find(".fb-comments").attr('data-ti');
                    var ui = $('.lg-outer .lg-item').eq(index).find(".fb-comments").attr('data-ui');
                    $.post('/comments/photos', {ti:ti, ui:ui}, function(r){
                        if (r == null || typeof(r) == 'undefined' || r.error){
                            Lobibox.notify('error',{size:'mini',msg:'Не удалось получить список комментарии'});
                            return;
                        }
                        $('.lg-outer .lg-item').eq(index).find(".fb-comments").html(r.comments);
                        $('#comment_blicks').slimScroll({height: '84%'});
                        history.pushState(null, null, '/photos/view/'+ti);
                    }, "json");
                } catch (err) {
                    Lobibox.notify('error',{size:'mini',msg:err.message});
                }
            }
        });
    });
    function get_ajax_comments(id) {
        var count = $('#'+id+'.fb-comments .ajax_comm_list').length;
        var all = parseInt($('#'+id+'.fb-comments h2').attr('data-all'));
        if(all <= 10){return;}
        $.post('/comments/load', {ti:id, count:count, all:all}, function(r){
            if (r == null || typeof(r) == 'undefined' || r.error){
                Lobibox.notify('error',{size:'mini',msg:'Не удалось получить список комментарии'});
                return;
            }
            if(!r.more){$('#'+id+'.fb-comments .com_more').removeAttr("onclick").addClass('disabled');}
            $(r.comments).insertAfter('#'+id+'.fb-comments .ajax_comm_list:last').hide().fadeIn(800);
        }, "json");
    }
    function vote_rating(id) {
        $.post('/rating/vote', {
            direction: 'up',
            controller: 'photos',
            subject: 'photo',
            id: id
        }, function(result){
            $('.fb-comments h2 .rat_'+id).addClass('disabled');
            if (result.success){Lobibox.notify('success',{size:'mini',msg:'Спасибо за ваш голос'});}
        }, "json");
    }
    function modal_add_comment(id) {
        var content = $('.content_'+id).val();
        var csrf_token = $('.is_'+id+' .input').val();
        if(!content || content == ' ' || content == null || typeof(content) == 'undefined'){
            $('.content_'+id).css('border-color', 'red');
        } else {
            $('.content_'+id).attr('disabled', 'disabled');
            $('.is_'+id+' .btn').removeAttr("onclick").addClass('disabled');
            $('.is_'+id+' .btn i.fa').removeClass('fa-plus').addClass('fa-spin fa-refresh');
            $.post('/comments/add', {
                csrf_token: csrf_token,
                ti: id,
                content: content,
            }, function(result){console.log(result);
                if(result.error){
                    Lobibox.notify('error',{size:'mini',msg:result.message});
                } else {
                    $('#'+id+'.fb-comments .ajax_comm_list.no_comm').remove();
                    $(result.html).prependTo('#'+id+' #comment_blicks');
                    $('.content_'+id).val(result.message);
                    $('.is_'+id+' .btn i.fa').attr('class','fa fa-plus');
                    Lobibox.notify('success',{size:'mini',msg:result.message});
                }
            }, "json");
        }
    }
</script>

<script type="text/javascript">
    <?php echo $this->getLangJS('LANG_PHOTOS_DELETE_PHOTO_CONFIRM'); ?>
    icms.photos.row_height = '<?php echo $row_height; ?>';
    $(function(){
        icms.photos.initAlbum();
    });
</script>
</div>