<?php

    $this->addJS($this->getJavascriptFileName('photos'));
    $this->addJS($this->getJavascriptFileName('screenfull'));

    $this->setPageTitle($photo['title']);
    $this->setPageDescription($photo['content'] ? string_get_meta_description($photo['content']) : ($photo['title'].' — '.$album['title']));

    if ($ctype['options']['list_on']) {
        $this->addBreadcrumb($ctype['title'], href_to($ctype['name']));
    }
    if (isset($album['category'])) {
        foreach ($album['category']['path'] as $c) {
            $this->addBreadcrumb($c['title'], href_to($ctype['name'], $c['slug']));
        }
    }
    if ($ctype['options']['item_on']) {
        $this->addBreadcrumb($album['title'], href_to($ctype['name'], $album['slug']) . '.html');
    }
    $this->addBreadcrumb($photo['title']);

    if ($is_can_edit) {
        $this->addToolButton(array(
            'class' => 'edit',
            'title' => LANG_PHOTOS_EDIT_PHOTO,
            'href'  => $this->href_to('edit', $photo['id'])
        ));
    }

    if ($is_can_delete) {
        $this->addToolButton(array(
            'class'   => 'delete',
            'title'   => LANG_PHOTOS_DELETE_PHOTO,
            'href'    => 'javascript:icms.photos.delete()',
            'onclick' => "if(!confirm('" . LANG_PHOTOS_DELETE_PHOTO_CONFIRM . "')){ return false; }"
        ));
    }

?>
<?php if ($this->options['photo_go']=='photo_go_true') { ?>
    <span id="<?php html($photo['id']); ?>"></span>
<?php } ?>
<div id="album-photo-item" class="content_item clearfix" data-item-delete-url="<?php if ($is_can_delete){ echo $this->href_to('delete'); } ?>" data-id="<?php echo $photo['id']; ?>" itemscope itemtype="http://schema.org/ImageObject">

<div class="photo-item text-center">
    <div class="photo-image-div">
        <div class="inside">
            <div class="inside_wrap orientation_<?php echo $photo['orientation']; ?>" id="fullscreen_cont">
                <div id="photo_container" <?php if($full_size_img){?>data-full-size-img="<?php echo $full_size_img; ?>"<?php } ?>>
                    <?php echo $this->renderChild('view_photo_container', array(
                        'photos_url_params' => $photos_url_params,
                        'photo'      => $photo,
                        'preset'     => $preset,
                        'prev_photo' => $prev_photo,
                        'next_photo' => $next_photo
                    )); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="photo-nav">
    <?php $this->addJS("templates/{$this->name}/theme_js/owl.carousel.min.js"); ?>
    <?php $this->addCSS("templates/{$this->name}/theme_css/owl.carousel.css"); ?>
    <div class="owl-carousel">
        <?php $index = 0; ?>
        <?php foreach($photos as $thumb) { ?>
            <div data-hash="<?php echo ($thumb['id']); ?>" class="photo-nav-item <?php if ($thumb['id'] == $photo['id']) { ?> photo-nav-active<?php } ?>">
                <a href="<?php echo $this->href_to('view', $thumb['slug']); ?>#<?php echo ($thumb['id']); ?>" title="<?php echo $thumb['title']; ?>">
                    <img class="owl-lazy" data-src="<?php echo html_image_src($thumb['image'], 'normal', true); ?>" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ1MUQ2OTQ0RDcyMjExRTQ4NUI2RTY3NDE3MjgxNTAyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ1MUQ2OTQ1RDcyMjExRTQ4NUI2RTY3NDE3MjgxNTAyIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RDUxRDY5NDJENzIyMTFFNDg1QjZFNjc0MTcyODE1MDIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RDUxRDY5NDNENzIyMTFFNDg1QjZFNjc0MTcyODE1MDIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6luD2XAAAAEElEQVR42mL+//8/A0CAAQAJCwMBR2lONwAAAABJRU5ErkJggg==" alt="<?php echo ($thumb['title']); ?>">
                </a>
            </div>
            <?php if ($thumb['id'] == $photo['id']) { $active_index = $index; } else { $index++; } ?>
        <?php } ?>
    </div>  
    <script>
        $(document).ready(function(){
            $('.owl-carousel').owlCarousel({
                items:4,
                loop:true,
                center:true,
                lazyLoad:true,
                navText: false,
                autoplayHoverPause:true,
                nav:true,
                navClass: [ 'photo-owl-prev', 'photo-owl-next' ],
                startPosition: 'URLHash',
                responsive:{
                    0:{
                        margin:10
                    },
                    992:{
                        items:4,
                        margin:15
                    },
                    1200:{
                        items:5,
                        margin:15
                    }
                }
            });
        });
    </script>

    </div>
</div>
    <div class="photo-right">
        <div class="photo_author">
            <div class="info_bar margin-null">
                <li class="album_user" title="<?php echo LANG_AUTHOR ?>">
                    <a href="<?php echo href_to('users', $photo['user']['id']); ?>">
                        <?php echo html_avatar_image($photo['user']['avatar'], 'micro', $photo['user']['nickname']); ?>
                    </a>
                </li>
                <li class="album_user_link">
                    <a href="<?php echo href_to('users', $photo['user']['id']); ?>" title="<?php echo LANG_AUTHOR ?>">
                        <span class="glyphicon glyphicon-user"></span>
                        <?php echo $photo['user']['nickname']; ?>
                    </a>
                </li>
                <li class="album_date text-muted" title="<?php echo LANG_DATE_PUB; ?>">
                    <span class="glyphicon glyphicon-calendar"></span>
                    <?php echo html_date_time($photo['date_pub']); ?>
                </li>
                <?php if (!empty($photo['rating_widget'])) { ?>
                    <li class="bar_item bi_rating">
                            <?php echo $photo['rating_widget']; ?>
                        </li>
                <?php } ?>
                <li class="share">
                    <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"
                            charset="utf-8"></script>
                    <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                    <div class="ya-share2"
                         data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp"
                         data-size="s"></div>
                </li>
            </div>
        </div>


        <?php if (!empty($photo['content'])){ ?>
            <div class="photo_content" itemprop="description">
                <?php echo $photo['content']; ?>
            </div>
        <?php } ?>

        <?php if (!empty($downloads)){ ?>
            <div class="download_menu">
                <span id="download-button" class="download-button btn btn-danger btn-lg"><i class="glyphicon glyphicon-download-alt"></i> <?php echo LANG_DOWNLOAD; ?></span>
                <div id="bubble" class="dropdown-menu">
                        <?php foreach ($downloads as $download) { ?>
                            <div class="item <?php echo $download['preset']; ?>_download_preset <?php echo (!$download['link'] ? 'disable_download' : ''); ?>">
                                <div>
                                    <label class="margin-b0"><input <?php echo ($download['select'] ? 'checked=""' : ''); ?> type="radio" name="download" <?php echo (!$download['link'] ? 'disabled=""' : ''); ?> value="<?php echo $download['link']; ?>"> <?php echo $download['name']; ?> </label>
                                </div>
                                <div>
                                    <?php echo $download['size']; ?>
                                </div>
                            </div>
                        <?php } ?>
                    <a class="download-button process_download btn btn-primary btn-sm" href=""><?php echo LANG_DOWNLOAD; ?></a>
                </div>
            </div>
        <?php } ?>
        <div class="photo-info">
            <?php if ($photo['exif'] || $photo['camera']){ ?>
                <div class="exif_wrap">
                    <?php if ($photo['camera']){ ?>
                        <a href="<?php html(href_to('photos', 'camera-'.urlencode($photo['camera']))); ?>">
                            <span class="glyphicon glyphicon-camera"></span>
                            <?php html($photo['camera']); ?>
                        </a>
                    <?php } ?>
                    <?php if ($photo['exif']){ ?>
                        <div class="exif_info">
                            <?php foreach ($photo['exif'] as $name => $value) { ?>
                                <span title="<?php echo string_lang('lang_exif_'.$name); ?>"><?php html($value); ?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
    
            <dl class="photo_details">
                <?php foreach ($photo_details as $detail) { ?>
                    <dt><?php echo $detail['name']; ?></dt>
                    <dd>
                        <?php if(isset($detail['link'])){ ?>
                            <a href="<?php echo $detail['link']; ?>" title="<?php html($detail['value']); ?>">
                                <?php echo $detail['value']; ?>
                            </a>
                        <?php } else { ?>
                            <?php echo $detail['value']; ?>
                        <?php } ?>
                    </dd>
                <?php } ?>
            </dl>
        </div>
        <meta itemprop="height" content="<?php echo $photo['sizes'][$preset]['height']; ?> px">
        <meta itemprop="width" content="<?php echo $photo['sizes'][$preset]['width']; ?> px">
    </div>
</div>

<?php if ($hooks_html) { echo html_each($hooks_html); } ?>

<?php if (!empty($photo['comments_widget'])){ ?>
    <?php echo $photo['comments_widget']; ?>
<?php } ?>

<script>
    icms.photos.init = true;
    icms.photos.mode = 'photo';
</script>
