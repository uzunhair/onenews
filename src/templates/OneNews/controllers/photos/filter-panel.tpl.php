<div class="photo_filter">
    <form action="<?php echo $item['base_url']; ?>" method="get">
        <div class="navbar navbar-default navbar-static-top">
            <ul class="nav navbar-nav">
                <li class="dropdown <?php echo !isset($item['filter_selected']['ordering']) ?'': 'box_menu_select'; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="<?php echo LANG_SORTING; ?>">
                        <?php echo $item['filter_panel']['ordering'][$item['filter_values']['ordering']]; ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach($item['filter_panel']['ordering'] as $value => $name){ ?>
                            <?php $url_params = $item['url_params']; $url_params['ordering'] = $value; ?>
                            <li<?php if($item['filter_values']['ordering'] == $value){ ?> class="active" <?php } ?>>
                                <a href="<?php echo $page_url.'?'.http_build_query($url_params); ?>">
                                    <?php echo $name; ?>
                                    <?php if($item['filter_values']['ordering'] == $value){ ?>
                                        <input type="hidden" name="ordering" value="<?php echo $value; ?>">
                                        <i class="check">&larr;</i>
                                    <?php } ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <li class="dropdown <?php echo !isset($item['filter_selected']['orderto']) ?'': 'box_menu_select'; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="<?php echo LANG_PHOTOS_SORT_ORDERTO; ?>">
                        <?php echo $item['filter_panel']['orderto'][$item['filter_values']['orderto']]; ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach($item['filter_panel']['orderto'] as $value => $name){ ?>
                            <?php $url_params = $item['url_params']; $url_params['orderto'] = $value; ?>
                            <li<?php if($item['filter_values']['orderto'] == $value){ ?> class="active" <?php } ?>>
                                <a href="<?php echo $page_url.'?'.http_build_query($url_params); ?>">
                                    <?php echo $name; ?>
                                    <?php if($item['filter_values']['orderto'] == $value){ ?>
                                        <input type="hidden" name="orderto" value="<?php echo $value; ?>">
                                        <i class="check">&larr;</i>
                                    <?php } ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>

                <?php if($item['filter_panel']['types']){ ?>
                    <li class="dropdown <?php echo !isset($item['filter_selected']['types']) ?'': 'box_menu_select'; ?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <?php echo $item['filter_panel']['types'][$item['filter_values']['types']]; ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php foreach($item['filter_panel']['types'] as $value => $name){ ?>
                                <?php $url_params = $item['url_params']; $url_params['types'] = $value; ?>
                                <li<?php if($item['filter_values']['types'] == $value){ ?> class="active" <?php } ?>>
                                    <a href="<?php echo $page_url.'?'.http_build_query($url_params); ?>">
                                        <?php echo $name; ?>
                                        <?php if($item['filter_values']['types'] == $value){ ?>
                                            <input type="hidden" name="types" value="<?php echo $value; ?>">
                                            <i class="check">&larr;</i>
                                        <?php } ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
                <li class="dropdown <?php echo !isset($item['filter_selected']['orientation']) ? '' : 'box_menu_select'; ?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <?php echo $item['filter_panel']['orientation'][$item['filter_values']['orientation']]; ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach ($item['filter_panel']['orientation'] as $value => $name) { ?>
                            <?php $url_params = $item['url_params'];
                            $url_params['orientation'] = $value; ?>
                            <li<?php if($item['filter_values']['orientation'] == $value){ ?> class="active" <?php } ?>>
                                <a href="<?php echo $page_url.'?'.http_build_query($url_params); ?>">
                                    <?php echo $name; ?>
                                    <?php if ($item['filter_values']['orientation'] == $value) { ?>
                                        <input type="hidden" name="orientation" value="<?php echo $value; ?>">
                                        <i class="check">&larr;</i>
                                    <?php } ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <?php if($item['filter_values']['width'] || $item['filter_values']['height']){ ?>
                            <span class="box_menu box_menu_select"><?php echo LANG_PHOTOS_MORE_THAN; ?> <?php html($item['filter_values']['width']); ?> x <?php html($item['filter_values']['height']); ?></span>
                        <?php } else { ?>
                            <span class="box_menu"><?php echo LANG_PHOTOS_SIZE; ?></span>
                        <?php } ?>
                        <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu">
                        <div class="size_search_params">
                            <div class="title"><?php echo LANG_PHOTOS_MORE_THAN; ?></div>
                            <div class="divider"></div>
                            <div class="field">
                                <label><?php echo LANG_PHOTOS_SIZE_W; ?></label>
                                <input type="text" name="width" value="<?php html($item['filter_values']['width']); ?>"
                                       placeholder="px" class="input form-control">
                            </div>
                            <div class="field">
                                <label><?php echo LANG_PHOTOS_SIZE_H; ?></label>
                                <input type="text" name="height"
                                       value="<?php html($item['filter_values']['height']); ?>" placeholder="px"
                                       class="input form-control">
                            </div>
                            <div class="buttons">
                                <input type="submit" class="button btn btn-default btn-sm"
                                       value="<?php echo LANG_FIND; ?>">
                            </div>
                        </div>
                    </div>
                </li>
                <?php if($item['filter_selected']) { ?>
                    <li class="active">
                        <a title="<?php echo LANG_PHOTOS_CLEAR_FILTER; ?>" class="box_menu clear_filter" href="<?php echo $page_url; ?>">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </form>
</div>