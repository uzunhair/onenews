<?php
    if(!empty($this->controller->options['is_filter'])) {
        $this->renderAsset('ui/filter-panel', array(
            'css_prefix' => 'groups',
            'page_url'   => $page_url,
            'fields'     => $fields,
            'filters'    => $filters
        ));
    }
?>

<?php if ($groups){ ?>

<?php $this->addJS("templates/{$this->name}/theme_js/jquery.dotdotdot.js"); ?>
        <script type="text/javascript">
            $(function() {
                $('.height-max-60').dotdotdot({
                    watch: 'window'
                });
            });
        </script>
    <?php
        $index_first = $page * $perpage - $perpage + 1;
        $index = 0;
    ?>

    <div class="groups-list striped-list">

        <?php foreach($groups as $group){ ?>

        <div class="media item">

            <?php if ($fields['logo']['is_in_list'] && $group['logo']){ ?>
                <div class="group-left">
                    <a href="<?php echo href_to('groups', $group['slug']); ?>" class="group-logo"
                       style="background-image:url('/upload/<?php echo html_image_src($group['logo'], 'big'); ?>')">
                    </a>
                </div>
            <?php } ?>
            <div class="group-item2">
                <div class="media-body title  media-middle">
                    <?php if ($fields['title']['is_in_list']){ ?>
                        <a href="<?php echo href_to('groups', $group['slug']); ?>"><?php html($group['title']); ?></a>
                        <?php if ($group['is_closed']) { ?>
                            <span class="is_closed" title="<?php html(LANG_GROUP_IS_CLOSED_ICON); ?>"></span>
                        <?php } ?>
                    <?php } ?>
                    <?php if (!empty($group['fields'])) { ?>
                        <div class="height-max-60 fields">
                            <?php foreach($group['fields'] as $field){ ?>
                                <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">
                                    <?php if ($field['label_pos'] != 'none'){ ?>
                                        <div class="title_<?php echo $field['label_pos']; ?>">
                                            <?php echo $field['title'] . ($field['label_pos'] == 'left' ? ': ' : ''); ?>
                                        </div>
                                    <?php } ?>
                                    <div class="value">
                                        <?php echo $field['html']; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="media-right  media-middle">
                    <div class="group-right-width">
                    <?php if ($dataset_name == 'rating') { ?>
                        <div class="position" style="font-size:34px;">
                            <?php $position = $index_first + $index; ?>
                            
                                <?php echo $position; ?>
                           
                        </div>
                    <?php } ?>

                    <?php if ($dataset_name == 'popular') { ?>
                    <div class="group-count">
                        <?php echo $group['members_count'] ? html_spellcount_tag($group['members_count'], LANG_GROUPS_MEMBERS_SPELLCOUNT) : '&mdash;'; ?>
                    </div>
                    <?php } elseif ($dataset_name == 'rating') { ?>

                        <span class="rate_value rating font-s14" title="<?php echo LANG_RATING; ?>"><span aria-hidden="true" class="glyphicon glyphicon-signal"></span> <?php echo $group['rating']; ?></span>

                    <?php } else { ?>

                        <?php echo html_date($group['date_pub']); ?>

                    <?php } ?>
                    </div>
                </div>          
            </div>
        </div>


            <?php $index++; ?>

        <?php } ?>

    </div>

    <?php if ($perpage < $total) { ?>
        <?php echo html_pagebar($page, $perpage, $total, $page_url, $filters); ?>
    <?php } ?>

<?php } ?>
