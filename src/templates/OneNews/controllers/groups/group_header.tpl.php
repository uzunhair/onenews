<?php

    $user = cmsUser::getInstance();

    $this->addMenuItems('group_tabs', $this->controller->getGroupTabs($group));
    $this->addMenuItems('controller_actions_menu', $this->controller->getToolButtons($group));

    $this->setPagePatternTitle($group);
    $this->setPagePatternDescription($group);

?>

<?php if(!empty($group['fields']['cover']['is_in_item']) && $group['cover']){ ?>
    <div style="background-image: url(<?php echo html_image_src($group['cover'], $group['fields']['cover']['handler']->getOption('size_full'), true); ?>);" id="group_head">
        <div class="gwrapper">
            <div class="group_counts">
                <div>
                    <strong><?php echo LANG_RATING; ?>:</strong>
                    <span><?php echo $group['rating']; ?></span>
                </div>
                <div>
                    <strong><?php echo LANG_GROUP_INFO_CREATED_DATE; ?>:</strong>
                    <span><?php echo string_date_age_max($group['date_pub'], true); ?></span>
                </div>
                <div>
                    <strong><?php echo LANG_GROUP_INFO_OWNER; ?>:</strong>
                    <span><a href="<?php echo href_to('users', $group['owner_id']); ?>"><?php html($group['owner_nickname']); ?></a></span>
                </div>
            </div>
            <div class="share">
                <script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
                <script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
                <div class="ya-share2" data-title="<?php html($group['title']); ?><?php if (!empty($group['sub_title'])) { ?> / <?php html($group['sub_title']); ?><?php } ?>" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,viber,whatsapp" data-description="<?php html(string_short($group['description'], 200)); ?>"></div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="group_title_bar">
    <div class="d-flex flex-1 align-items-center">
        <?php if (!empty($group['fields']['logo']['is_in_item']) && $group['logo']) { ?>
            <div class="logo group-bar-left">
                <?php echo html_image($group['logo'], $group['fields']['logo']['handler']->getOption('size_full'), $group['title']); ?>
            </div>
        <?php } ?>
        <div class="group-bar-body">
            <h1 id="group_profile_title">
                <?php if (!empty($group['fields']['title']['is_in_item'])) { ?>
                    <?php if (!empty($this->controller->options['tag_h1'])) { ?>
                        <?php echo string_replace_keys_values_extended($this->controller->options['tag_h1'], $group); ?>
                    <?php } else { ?>
                        <?php html($group['title']); ?>
                    <?php } ?>
                    <?php if (!empty($group['sub_title'])) { ?>
                        <span>/ <?php html($group['sub_title']); ?></span>
                    <?php } ?>
                <?php } ?>
                <?php if ($group['is_closed']) { ?>
                    <span class="is_closed" title="<?php html(LANG_GROUP_IS_CLOSED_ICON); ?>"></span>
                <?php } ?>
            </h1>
        </div>
        <div class="group-bar-right">
            <div class="group-bar-rating">
                <span class="glyphicon glyphicon-signal"></span>
                <?php echo $group['rating']; ?>
            </div>
        </div>
    </div>
    <div class="group-bar-count hidden-xs d-flex align-items-center">
        <div><?php echo html_spellcount_tag($group['members_count'], LANG_GROUPS_MEMBERS_SPELLCOUNT); ?></div>
    </div>
</div>

<?php if (!$group['is_closed'] || ($group['access']['is_member'] || $user->is_admin)){ ?>
    <div id="group_profile_tabs">
        <div class="tabs-menu d-flex">
            <?php $this->menu('group_tabs', true, 'nav nav-tabs nav-tabs-group', 6); ?>
            <ul class="nav nav-tabs" role="tablist">
                <?php $this->actionsToolbar(LANG_GROUPS_MENU); ?>
            </ul>
        </div>
    </div>
<?php } ?>
