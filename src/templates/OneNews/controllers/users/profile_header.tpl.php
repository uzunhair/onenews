<?php $this->addJS("templates/{$this->name}/js/users.js"); ?>

<?php $user = cmsUser::getInstance(); ?>
<div id="user_profile_title">
<div class="user-profile-status media" <?php if (!empty($bg_img)){ ?>style="position:absolute;"<?php } ?>>
    <div class="media-body media-middle">
    <?php if ($this->controller->options['is_status']) { ?>
        <span class="status" <?php if (!$profile['status']){ ?>style="display:none"<?php } ?>>
            <span class="text status-text">
                <?php if ($profile['status']) { ?>
                    <?php html($profile['status']['content']); ?>
                <?php } ?>
            </span>
            <?php if ($user->is_logged){ ?>
                <?php if ($this->controller->options['is_wall']){ ?>
                <span class="reply">
                    <?php if (empty($profile['status']['replies_count'])) { ?>
                        <a class="status-icon" href="<?php echo $this->href_to($profile['id']) . "?wid={$profile['status']['wall_entry_id']}&reply=1"; ?>" data-toggle="tooltip" title="<?php echo LANG_REPLY; ?>">
                            <span class="glyphicon glyphicon-comment"></span>
                        </a>
                    <?php } else { ?>
                        <a class="status-icon" href="<?php echo $this->href_to($profile['id']) . "?wid={$profile['status']['wall_entry_id']}"; ?>"  data-toggle="tooltip" title="<?php echo html_spellcount($profile['status']['replies_count'], LANG_REPLY_SPELLCOUNT); ?>">
                            <span class="glyphicon glyphicon-comment"></span>
                        </a>
                    <?php } ?>
                </span>
                <?php if ($profile['id'] == $user->id) { ?>
                    <span class="delete">
                        <a class="status-icon" href="#delete-status" onclick="return icms.users.deleteStatus(this)" data-url="<?php echo $this->href_to('status_delete', $profile['id']); ?>" data-toggle="tooltip" title="<?php echo LANG_DELETE; ?>">
                        <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </span>
                <?php } ?>
                <?php } ?>
            <?php } ?>
        </span>
    <?php } ?> 

    <?php if ($this->controller->options['is_status'] && $profile['id'] == $user->id) { ?>
        <script>
            <?php echo $this->getLangJS('LANG_REPLY', 'LANG_USERS_DELETE_STATUS_CONFIRM');?>
        </script>

    <!-- Button trigger modal -->
    <a class="status-icon" data-toggle="modal" data-target="#myModalstatus" data-toggle="tooltip" title="редактировать статус">
      <span class="glyphicon glyphicon-pencil"></span>
    </a>

    <!-- Modal -->
    <div class="modal fade" id="myModalstatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
        <div id="user_status_widget">
            <?php
                echo html_input('text', 'status', '', array(
                    'maxlength' => 140,
                    'placeholder' => sprintf(LANG_USERS_WHAT_HAPPENED, $profile['nickname']),
                    'data-url' => $this->href_to('status'),
                    'data-user-id' => $profile['id']
                ));
            ?>
        </div>
          </div>
        </div>
      </div>
    </div>

    <?php } ?>
</div>
    <?php if (!$profile['is_deleted']){ ?>
        <div id="user_profile_rates" class="rates media-right media-middle"
             data-url="<?php echo $this->href_to('karma_vote', $profile['id']); ?>"
             data-log-url="<?php echo $this->href_to('karma_log', $profile['id']); ?>"
             data-is-comment="<?php echo $this->controller->options['is_karma_comments']; ?>">
            <div class="karma block">
                <?php if ($profile['is_can_vote_karma']){ ?>
                    <a href="#vote-up" onclick="return icms.users.karmaUp()" class="thumb thumb_up" title="<?php echo LANG_KARMA_UP; ?>">
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                    </a>
                <?php } ?>
                <span class="value <?php echo html_signed_class($profile['karma']); ?>" title="<?php echo LANG_KARMA; ?>">
                    <?php echo html_signed_num($profile['karma']); ?>
                </span>
                <?php if ($profile['is_can_vote_karma']){ ?>
                    <a href="#vote-down" onclick="return icms.users.karmaDown()" class="thumb thumb_down" title="<?php echo LANG_KARMA_DOWN; ?>">
                        <span class="glyphicon glyphicon-thumbs-down"></span>
                    </a>
                <?php } ?>
            </div>
            <?php if ($this->controller->options['is_karma_comments']) { ?>
                <script><?php echo $this->getLangJS('LANG_USERS_KARMA_COMMENT'); ?></script>
            <?php } ?>
        </div>
    <?php } ?>
</div>
</div>

<div class="user-profile-navbar">
    <div class="profile-avatar media-left media-middle">
        <a href="<?php echo $this->href_to($profile['id']); ?>">
            <?php echo html_avatar_image($profile['avatar'], 'small', $profile['nickname']); ?>
        </a>
    </div>
<h1 class="profile-h1 media-left media-middle">
        <a href="<?php echo $this->href_to($profile['id']); ?>"><?php html($profile['nickname']); ?></a>
        <?php if ($profile['is_locked']){ ?>
            <span class="is_locked"><?php echo LANG_USERS_LOCKED_NOTICE_PUBLIC; ?></span>
        <?php } ?>
</h1>

<div class="rating-block media-left media-middle">
    <div class="rating-block-border">
        <div class="rating-glyphicon glyphicon-star" title="<?php echo LANG_RATING; ?>">
            <span class="rating-text"><?php echo $profile['rating']; ?></span>
        </div>
        <div class="rating-glyphicon glyphicon-signal" title="<?php echo LANG_KARMA; ?>">
            <span class="rating-text">
                <?php echo html_signed_num($profile['karma']); ?>
            </span>
        </div>
    </div>
</div>

<?php if (!isset($is_can_view) || $is_can_view){ ?>

    <?php if (empty($tabs)){ $tabs = $this->controller->getProfileMenu($profile); } ?>

	<?php if (count($tabs)>1){ ?>
	
		<?php $this->addMenuItems('profile_tabs', $tabs); ?>

		<div id="user_profile_tabs" class="media-body">
			<div class="tabs-menu">
				<?php $this->menu('profile_tabs', true, 'nav nav-profile-tabs flex', $this->controller->options['max_tabs']); ?>
			</div>
		</div>

	<?php } ?>
<?php } ?>
</div>