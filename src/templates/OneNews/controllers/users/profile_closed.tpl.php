<?php

    $this->addJS('templates/OneNews/js/jquery-ui.js');
    $this->addCSS('templates/OneNews/css/jquery-ui.css');

    $this->setPageTitle($profile['nickname']);

    $this->addBreadcrumb(LANG_USERS, href_to('users'));
    $this->addBreadcrumb($profile['nickname']);

    if (is_array($tool_buttons)){
        foreach($tool_buttons as $button){
            $this->addToolButton($button);
        }
    }

?>



<div id="user_profile" class="row">

    <div id="profile-left-col" class="padding-b15">
        <div id="profile-avatar" <?php if ($profile['avatar']) { ?>class="profile-avatar-yes" style="background-image:url('<?php echo html_image_src($profile['avatar'], 'normal', true); ?>')" <?php } ?>>
            <?php echo html_avatar_image($profile['avatar'], 'normal', $profile['nickname']); ?>
        </div>
    </div>

    <div id="profile-right-col">
        <p class="text-primary"><strong> <?php html($profile['nickname']); ?></strong></p>
        <p><?php echo LANG_USERS_PROFILE_IS_HIDDEN; ?></p>
        <p>
            <strong><?php echo LANG_USERS_PROFILE_REGDATE; ?>:</strong>
            <?php echo string_date_age_max($profile['date_reg'], true); ?>
        </p>

        <p>
            <strong><?php echo LANG_USERS_PROFILE_LOGDATE; ?>:</strong>
            <?php echo $profile['is_online'] ? '<span class="online">'.LANG_ONLINE.'</span>' : string_date_age_max($profile['date_log'], true); ?>
        </p>

    </div>

</div>