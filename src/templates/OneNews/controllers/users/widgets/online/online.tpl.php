<?php if ($profiles){ ?>

    <div class="wd_online_list">
    <?php if ($is_avatars) { ?>
        
        <?php foreach($profiles as $profile) { ?>
        <?php $url = href_to('users', $profile['id']); ?>
            <a data-w="32" data-h="32" class="item item-avatar" href="<?php echo $url; ?>" data-toggle="tooltip" title="<?php html($profile['nickname']); ?>">
                <?php echo html_avatar_image($profile['avatar'], 'micro', $profile['nickname']); ?>
            </a>
        <?php } ?>
        <?php $this->addJS("templates/{$this->name}/addon/jQuery-flexImages/jquery.flex-images.min.js"); ?>
        <script>
            $('.wd_online_list').flexImages({container: '.item-avatar', rowHeight: 32});
        </script>
    <?php } else { ?>
        <?php foreach($profiles as $profile) { ?>
        <?php $url = href_to('users', $profile['id']); ?>
        <ul class="list-inline margin-b0">
            <li> 
                <span class="glyphicon glyphicon-user"></span> 
                <a class="item item-name" href="<?php echo $url; ?>">
                    <?php html($profile['nickname']); ?>
                </a>
            </li>
        </ul>
        <?php } ?>
    <?php } ?>
    </div>

<?php } ?>
