<?php if ($profiles){ ?>
    <?php if ($style=='list'){ ?>
        <div id="wd-user-list" class="clearfix">
            <?php $size = $style == 'list' ? 'micro' : 'small'; ?>
            <?php foreach($profiles as $profile) { ?>
                <?php $url = href_to('users', $profile['id']); ?>
                <div class="media">
                    <div class="media-left">
                        <a href="<?php echo $url; ?>" title="<?php html($profile['nickname']); ?>"><?php echo html_avatar_image($profile['avatar'], $size, $profile['nickname']); ?></a>
                    </div>
                    <div class="media-body media-middle">
                           <a href="<?php echo $url; ?>"><?php html($profile['nickname']); ?></a>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div id="wd-user-list" class="clearfix wd_pf_list flex-images">
            <?php $size = $style == 'list' ? 'micro' : 'small'; ?>
            <?php foreach($profiles as $profile) { ?>
                <?php $url = href_to('users', $profile['id']); ?>
                <div class="wd_pf_item" data-w="64" data-h="64">
                    <a href="<?php echo $url; ?>" title="<?php html($profile['nickname']); ?>">
                        <?php echo html_avatar_image($profile['avatar'], $size, $profile['nickname']); ?>
                    </a>
                </div>
            <?php } ?>
            <?php $this->addJS("templates/{$this->name}/addon/jQuery-flexImages/jquery.flex-images.min.js"); ?>
            <script>
                $('#wd-user-list').flexImages({container: '.wd_pf_item', rowHeight: 64});
            </script>
        </div>
    <?php } ?>
<?php } ?> 
