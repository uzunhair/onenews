<ul class="nav navbar-nav navbar-user">
	<li>
		<a href="<?php echo href_to('users', $user->id); ?>" data-toggle="tooltip"  title="<?php html($user->nickname); ?>" data-placement="bottom" class="menu-avatar-a">
			<span class="menu-avatar-span">
				<?php echo html_avatar_image($user->avatar, 'micro', $user->nickname); ?>
		 	</span>
			<span class="navbar-user-info">
				<span class="navbar-user-name">
					<?php html($user->nickname); ?>
				</span>
				<br>
				<span class="navbar-user-email">
					<?php html($user->email); ?>
				</span>
			</span>
		</a>
	</li>
</ul>
<?php $this->menu( $widget->options['menu'], $widget->options['is_detect'], 'nav navbar-nav', $widget->options['max_items'] ); ?>


