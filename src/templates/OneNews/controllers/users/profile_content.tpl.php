<?php

    $this->addBreadcrumb(LANG_USERS, href_to('users'));
    $this->addBreadcrumb($profile['nickname'], href_to_profile($profile));
    $this->addBreadcrumb($list_header, href_to_profile($profile, array('content', $ctype['name'])));

    if ($folders && $folder_id && isset($folders[$folder_id])){

        $this->addBreadcrumb($folders[$folder_id]['title']);

        $this->setPageTitle($list_header, $folders[$folder_id]['title'], $profile['nickname']);
        $this->setPageDescription($profile['nickname'].' — '.$list_header.' '.$folders[$folder_id]['title']);

    } else {

        $this->setPageTitle($list_header, $profile['nickname']);
        $this->setPageDescription($profile['nickname'].' — '.$list_header);

    }

    if (cmsUser::isAllowed($ctype['name'], 'add')) {

        $this->addToolButton(array(
            'class' => 'add',
            'title' => sprintf(LANG_CONTENT_ADD_ITEM, $ctype['labels']['create']),
            'href'  => href_to($ctype['name'], 'add').(($folder_id  && is_numeric($folder_id) && ($user->id == $profile['id'] || $user->is_admin)) ? '?folder_id='.$folder_id : ''),
        ));

    }

    if ($folder_id  && is_numeric($folder_id) && ($user->id == $profile['id'] || $user->is_admin)){

        $this->addToolButton(array(
            'class' => 'folder_edit',
            'title' => LANG_EDIT_FOLDER,
            'href'  => href_to($ctype['name'], 'editfolder', $folder_id),
        ));

        $this->addToolButton(array(
            'class' => 'folder_delete',
            'title' => LANG_DELETE_FOLDER,
            'href'  => href_to($ctype['name'], 'delfolder', $folder_id),
            'onclick' => "if(!confirm('".LANG_DELETE_FOLDER_CONFIRM."')){ return false; }"
        ));

    }

    if ($user->is_admin){
        $this->addToolButton(array(
            'class' => 'page_gear',
            'title' => sprintf(LANG_CONTENT_TYPE_SETTINGS, mb_strtolower($ctype['title'])),
            'href'  => href_to('admin', 'ctypes', array('edit', $ctype['id']))
        ));
    }

?>

<div id="user_profile_header" class="clearfix">
    <div id="user_profile_title">

    <?php if (!empty($ctype['options']['is_rss']) && $this->controller->isControllerEnabled('rss')){ ?>
        <div class="content_list_rss_icon">
            <a href="<?php echo href_to('rss', 'feed', $ctype['name']) . '?user='.$profile['id']; ?>">RSS</a>
        </div>
    <?php } ?>

    </div>

    <div class="user-profile-navbar">

        <div class="profile-avatar media-left media-middle">
            <a href="<?php echo href_to_profile($profile); ?>">
                <?php echo html_avatar_image($profile['avatar'], 'normal', $profile['nickname']); ?>
            </a>
        </div>
        
        <h1 class="profile-h1 media-left media-middle">
            <a href="<?php echo href_to_profile($profile); ?>">
                <?php html($profile['nickname']); ?>
            </a>
            <?php if ($profile['is_locked']){ ?>
                <span class="is_locked"><?php echo LANG_USERS_LOCKED_NOTICE_PUBLIC; ?></span>
            <?php } ?>
        </h1>

        <div class="rating-block media-left media-middle">
            <div class="rating-block-border padding-t10 padding-b10">
                <?php echo $list_header; ?>
            </div>
        </div>

        <?php if (!empty($datasets)){
            $this->renderAsset('ui/datasets-panel', array(
                'datasets'        => $datasets,
                'dataset_name'    => $dataset,
                'current_dataset' => $current_dataset,
                'base_ds_url'     => $base_ds_url
            ));
        } ?>

    <?php if ($folders){ ?>
        <div class="media-body">
            <ul class="nav nav-profile-tabs flex">
                <?php if (!empty($ctype['options']['is_rss'])){ ?>
                    <li class="hidden-xs pull-right">
                        <a href="<?php echo href_to('rss', 'feed', $ctype['name']) . $rss_query; ?>">RSS</a>
                    </li>
                <?php } ?>

                <?php foreach($folders as $folder){ ?>
                    <?php
                        $is_selected = $folder['id'] == $folder_id;

                        $url = $folder['id'] ?
                                href_to_profile($profile, array('content', $ctype['name'], $folder['id'])) :
                                href_to_profile($profile, array('content', $ctype['name']));
                    ?>
                    <li <?php if ($is_selected){ ?>class="active"<?php } ?>>
                        <?php if ($is_selected){ $current_folder = $folder; ?>
                            <a href="#"><?php echo $folder['title']; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $url; ?>"><?php echo $folder['title']; ?></a>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
    </div>

</div>

<div id="user_content_list">
    <?php echo $html; ?>
</div>

<?php $hooks_html = cmsEventsManager::hookAll("content_{$ctype['name']}_items_html", array('user_view', $ctype, $profile, (!empty($current_folder) ? $current_folder : array()))); ?>
<?php if ($hooks_html) { ?>
    <div class="sub_items_list">
        <?php echo html_each($hooks_html); ?>
    </div>
<?php } ?>