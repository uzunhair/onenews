<?php

    $this->addMenuItems('profile_tabs', $this->controller->getProfileEditMenu($profile));

?>

<h1 class="cont_title"><?php echo LANG_USERS_EDIT_PROFILE; ?></h1>

<div id="user_profile_tabs_edit">
    <div class="tabs-menu">
        <?php $this->menu('profile_tabs', true, 'nav nav-tabs', 6); ?>
    </div>
</div>
