<h1 class="cont_head"><?php echo $page_title; ?></h1>

<div id="moderation_content_pills">
    <?php $this->menu('moderation_content_types', true, 'nav nav-tabs'); ?>
</div>

<div id="moderation_content_list"><?php echo $list_html; ?></div>