<?php

    $this->setPageTitle(LANG_SEARCH_TITLE);

    $this->addBreadcrumb(LANG_SEARCH_TITLE, $this->href_to(''));
    if($query){
        $this->addBreadcrumb($query);
    }

    $content_menu = array();

    $uri_query = http_build_query(array(
        'q'    => $query,
        'type' => $type,
        'date' => $date
    ), '', '&');

    $this->addJS('templates/default/js/video/all.js');
    $this->addCSS('templates/default/controllers/video/styles.css');
    $this->addCSS('templates/default/css/font-awesome/css/font-awesome.min.css');

    if ($results){

        foreach($results as $result){

            $content_menu[] = array(
                'title'    => $result['title'],
                'url'      => $this->href_to('index', array($result['name'])) . '?' . $uri_query,
                'url_mask' => $this->href_to('index', array($result['name'])),
                'counter'  => $result['count']
            );

            if($result['items']){
                $search_data = $result;
            }

        }

        $content_menu[0]['url'] = href_to('search') . '?' . $uri_query;
        $content_menu[0]['url_mask'] = href_to('search');

        $this->addMenuItems('results_tabs', $content_menu);

        $this->setPageTitle($query, $target_title, mb_strtolower(LANG_SEARCH_TITLE));

    }

?>

<h1><?php echo LANG_SEARCH_TITLE; ?></h1>

<div id="search_form">
    <form action="<?php echo href_to('search'); ?>" method="get" class="form-inline">
        <div class="form-group margin-r5"><?php echo html_input('text', 'q', $query, array('placeholder'=>LANG_SEARCH_QUERY_INPUT)); ?></div>
        <div class="form-group margin-r5"><?php echo html_select('type', array(
            'words' => LANG_SEARCH_TYPE_WORDS,
            'exact' => LANG_SEARCH_TYPE_EXACT,
        ), $type); ?></div>
        <div class="form-group margin-r5"><?php echo html_select('date', array(
            'all' => LANG_SEARCH_DATES_ALL,
            'w' => LANG_SEARCH_DATES_W,
            'm' => LANG_SEARCH_DATES_M,
            'y' => LANG_SEARCH_DATES_Y,
        ), $date); ?></div>
        <div class="form-group margin-r5"><?php echo html_submit(LANG_FIND); ?></div>
    </form>
</div>

<?php if ($query && empty($search_data)){ ?>
    <p id="search_no_results"><?php echo LANG_SEARCH_NO_RESULTS; ?></p>
<?php } ?>

<?php if (!empty($search_data)){ ?>

    <div id="search_results_pills">
        <?php $this->menu('results_tabs', true, 'nav nav-tabs'); ?>
    </div>

    <div id="search_results_list" class="video_list content_list">
        <?php foreach($search_data['items'] as $item){ ?>

            <div class="full video_list_item content_list_item">
                <div class="float_left video_tumb_block">
                    <a target="_blank" class="background_image" href="<?php echo $item['url']; ?>" <?php if (!empty($item['image'])){ ?>style="background-image: url(<?php echo html_image_src($item['photo'], 'normal', true); ?>)"<?php } ?>>
                        <i class="fa fa-play-circle-o"></i>
                        <?php echo $item['image']; ?>
                    </a>
                    <?php if($item['duration']){ ?>
                        <div class="duration"><i class="fa fa-clock-o"></i> <span title="<?php echo LANG_VIDEO_DURATION; ?>"><?php echo duration_format($item['duration']); ?></span></div>
                    <?php } ?>
                </div>
                <div class="overflow video_text_block">
                    <div class="float_left video_text">
                        <h3 class="nowrap_text">
                            <a target="_blank" href="<?php echo $item['url']; ?>"><span><?php echo $item['title']; ?></span></a><?php if($item['is_hd']) { ?> <span class="is_hd" title="<?php echo LANG_VIDEO_HD; ?>">HD</span><?php } ?>
                        </h3>
                        <div class="video_content">
                            <?php foreach($item['fields'] as $field=>$value){ ?>

                                <div class="field f_<?php echo $field; ?>">
                                    <div class="value">
                                        <?php echo string_short($value, 410); ?>
                                    </div>
                                </div>

                            <?php } ?>
                            <div class="info_bar">
                                <?php if ($item['ctype']['is_rating']){ ?>
                                    <div class="bar_item">
                                        <i class="fa fa-star"></i> <?php echo $item['rating']; ?>
                                    </div>
                                <?php } ?>
                                <div class="bar_item bi_date_pub">
                                    <i class="fa fa-calendar"></i> <?php echo html_date_time($item['date_pub']); ?>
                                </div>
                                <?php if (!empty($item['ctype']['options']['hits_on'])){ ?>
                                    <div class="bar_item" title="<?php echo LANG_HITS; ?>">
                                        <i class="fa fa-eye"></i> <?php echo $item['hits_count']; ?>
                                    </div>
                                <?php } ?>
                                <?php if ($item['ctype']['is_comments'] && $item['is_comments_on']){ ?>
                                    <div class="bar_item">
                                        <i class="fa fa-comments"></i> <?php echo intval($item['comments']); ?>
                                    </div>
                                <?php } ?>
                                <?php if($user->is_logged) { ?>
                                    <div class="bar_item addto_playlist" data-href="<?php echo href_to('video', 'list_playlists', array($item['id'])); ?>"><i class="fa fa-plus-circle"></i> <?php echo LANG_VIDEO_ADDTO; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>
    </div>
    <?php if ($search_data['count'] > $perpage){ ?>
        <?php echo html_pagebar($page, $perpage, $search_data['count'], $page_url, $uri_query); ?>
    <?php } ?>

<?php }