<form action="<?php echo href_to('search'); ?>" method="get" class="navbar-form navbar-right navbar-form-sm">
		<div class="input-group">
		<?php echo html_input('text', 'q', '', array('placeholder'=>LANG_WD_SEARCH_QUERY_INPUT, 'class'=>'input-sm')); ?>
          <span class="input-group-btn">
            <button type="submit" name="submit" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-search"></span></button>
          </span>
        </div>
</form>