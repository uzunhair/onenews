<?php $user = cmsUser::getInstance(); ?>

<?php if($channel['background_img'] ) { ?>
<div class="user-channel-header"
     style="background-image: url(<?php echo $fields['background_img']['is_in_item'] ? html_image_src($channel['background_img'], $fields['background_img']['handler']->getOption('size_full'), true) : ''; ?>);"></div>
<?php } ?>
    <div class="user-channel-title clearfix">
        <div class="user-channel-info">
            <?php if(!empty($channel['photo'])) { ?>
            <div class="media-left">
                <div class="channel_logo">
                    <?php echo html_image($channel['photo'], $fields['photo']['handler']->getOption('size_full'), $channel['title']); ?>
                </div>
            </div>
            <?php } ?>
            <div class="media-body">
                <h1 class="user-channel-h1">
                    <?php if ($channel['parent_id']){ ?>
                        <div class="parent_title">
                            <a href="<?php echo href_to($channel['parent_url']); ?>"><?php html($channel['parent_title']); ?></a> &rarr;
                        </div>
                    <?php } ?>
                    <?php html($channel['title']); ?>
                    <?php if ($channel['is_private']) { ?>
                        <span class="is_private" title="<?php html(LANG_PRIVACY_PRIVATE); ?>"></span>
                    <?php } ?>
                </h1>
                <div class="channel_counts">
                    <div class="channel_movie_count"><i class="fa fa-film"></i> <?php echo html_spellcount($channel['movie_count'], LANG_MOVIE_1, LANG_MOVIE_2, LANG_MOVIE_10); ?></div>
                    <div><i class="fa fa-check-square-o"></i> <?php echo html_spellcount($channel['subscribers_count'], LANG_CHANNEL_SUBSCRIBERS1, LANG_CHANNEL_SUBSCRIBERS2, LANG_CHANNEL_SUBSCRIBERS10); ?></div>
                </div>
            </div>
        </div>
        <div class="channel_description">
        <?php if(!empty($channel['teaser'])) { ?>
            <?php  echo $channel['teaser']; ?>
        <?php } ?>
        </div>
        <!--noindex-->
        <div class="subscribers-right">
            <?php if($user->is_logged) { ?>
                <?php if($channel['user_id'] == $user->id) { ?>
                    <a href="<?php echo ($channel['subscribers_count'] ? href_to('channels', $channel['slug'], 'subscribers') : ''); ?>" class="unsubscribe subscribers-link"><i class="fa fa-check-square-o"></i> <?php echo LANG_ONE_NEWS_THEME_VIDEO_SUBSCRIBERS; ?></a>
                <?php } else { ?>
                    <a href="#subscribe" class="subscriber subscribers-link" data-link0="<?php echo href_to('video', 'subscribe', array($channel['id'], 1)); ?>" data-link1="<?php echo href_to('video', 'subscribe', array($channel['id'], 0)); ?>" data-text0="<?php echo LANG_VIDEO_SUBSCRIBE; ?>" data-text1="<?php echo LANG_VIDEO_UNSUBSCRIBE; ?>" data-issubscribe="<?php echo $channel['is_subscribe']; ?>"><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> <span></span></a>
                <?php } ?>
            <?php } else { ?>
                    <a href="<?php echo href_to('auth', 'login'); ?>" rel="nofollow" class="subscribe ajax-modal subscribers-link"><?php echo LANG_VIDEO_SUBSCRIBE; ?></a>
            <?php } ?>
            <span class="count-subscribers subscribers-link"><?php echo $channel['subscribers_count']; ?></span>
        </div><!--/noindex-->
    </div>