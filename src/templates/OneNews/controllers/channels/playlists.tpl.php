<?php

    $page_title = LANG_VIDEO_PLAYLISTS.' '.$channel['title'];

    $this->setPageTitle($page_title);
    $this->setPageDescription($page_title);

    if ($channel_ctype['options']['list_on']){
        $this->addBreadcrumb(empty($channel_ctype['labels']['list']) ? $channel_ctype['title'] : $channel_ctype['labels']['list'], href_to($channel_ctype['name']));
    }

    $this->addBreadcrumb($channel['title'], href_to($channel_ctype['name'], $channel['slug'].'.html'));

    $this->addBreadcrumb($page_title);

    $user = cmsUser::getInstance();

	if ($user->is_admin || $channel['user_id'] == $user->id){
		$this->addToolButton(array(
			'class' => 'add',
			'title' => LANG_VIDEO_NEW_PLAYLISTS,
			'href'  => $this->href_to('add_playlist', $channel['id'])
		));
	}

?>

<div class=" white_del margin-b0 padding-b10">
    <?php $this->renderChild('channel_header', array('channel' => $channel, 'fields' => $fields)); ?>
    <div class="channel-body">
        <?php if($sortings){ ?>
        <div class="channels-dropdown">
            <a id="channels-sort"  href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Сортировать
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="channels-sort">
                <?php foreach($sortings as $key => $value) { ?>
                    <li <?php if($key == $sort){ ?>class="active"<?php } ?>>
                        <a href="?sort=<?php echo $key; ?>"><?php echo $value; ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
        <div id="channels-item-menu">
            <div class="tabs-menu">
                <?php $this->menu('channel-menu', true, 'tabbed nav nav-tabs flex'); ?>
            </div>
        </div>
    </div>
</div>

<div class="white_del margin-t0">
    <div class="playlists_list">
        <?php if($playlists) { ?>
            <?php foreach($playlists as $playlist){ ?>
            <div class="video_list_item overflow media">
                <div class="float_left video_tumb_block">
                    <a class="background_image" href="<?php if($playlist['movie_count']) { ?><?php echo href_to('video', $playlist['mslug'].'.html?playlist='.$playlist['id']); ?><?php } else { ?>#<?php } ?>" <?php if (!empty($playlist['tumb_path'])){ ?>style="background-image: url(<?php echo get_video_url('images_small', $playlist['tumb_path']); ?>)"<?php } ?> <?php if(!$playlist['movie_count']) { ?>onclick="return false;"<?php } ?>>
                        <span class="vitems_count">
                            <b><?php if($playlist['movie_count']) { ?><?php echo $playlist['movie_count']; ?><?php } else { ?><?php echo LANG_NO; ?><?php } ?></b> <?php echo html_spellcount_only($playlist['movie_count'], LANG_MOVIE_1, LANG_MOVIE_2, LANG_MOVIE_10); ?>
                            <i class="fa fa-bars"></i>
                        </span>
                        <span class="playall"><span><i class="fa fa-play"></i> <?php echo LANG_VIDEO_PLAYALL; ?></span></span>
                    </a>
                </div>
                <div class="overflow video_text_block">
                    <div class="float_left video_text">
                        <h3>
                            <?php if($playlist['date_update']){ ?>
                                <div class="float-right date_update color_asbestos"><i class="fa fa-clock-o"></i> <?php echo LANG_VIDEO_UPDATED.' '.string_date_age_max($playlist['date_update'], true); ?></div>
                            <?php } ?>
                            <a href="<?php echo $this->href_to($channel['slug'], array('playlists', $playlist['slug'])); ?>"><?php echo $playlist['title']; ?></a>
                            <?php if($playlist['is_access_by_link']){ ?>
                                <span class="color_red" title="<?php echo LANG_VIDEO_ONLY_LINK_PLAYLIST; ?>"><i class="fa fa-lock"></i></span>
                            <?php } else { ?>
                                <span class="color_green" title="<?php echo LANG_VIDEO_PLAYLIST_FORALL; ?>"><i class="fa fa-globe"></i></span>
                            <?php } ?>
                        </h3>
                        <div class="video_content">
                            <?php echo html_clean($playlist['content'], 420); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if ($perpage < $total) { ?>
            <?php echo html_pagebar($page, $perpage, $total); ?>
        <?php } ?>
    <?php } else { ?>
        <div class="not_found"><?php echo LANG_VIDEO_NO_PLAYLISTS; ?></div>
    <?php } ?>
    </div>
</div>