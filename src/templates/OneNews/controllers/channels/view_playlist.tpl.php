<?php

    $this->setPageTitle($playlist['title']);
    $this->setPageDescription($playlist['title'].' — '.$channel['title']);

    if ($video_ctype['options']['list_on']){
        $this->addBreadcrumb($video_ctype['title'], href_to($video_ctype['name']));
    }

    if ($channel_ctype['options']['list_on']){
        $this->addBreadcrumb($channel_ctype['title'], href_to($channel_ctype['name']));
    }

    $this->addBreadcrumb($channel['title'], href_to($channel_ctype['name'], $channel['slug'].'.html'));

    $this->addBreadcrumb($playlist['title']);

    $user = cmsUser::getInstance();

	if ($user->is_admin || $channel['user_id'] == $user->id){
		$this->addToolButton(array(
			'class' => 'edit',
			'title' => LANG_EDIT,
			'href'  => $this->href_to('edit_playlist', $playlist['id'])
		));
		$this->addToolButton(array(
			'class'   => 'delete',
            'confirm' => LANG_VIDEO_REALY_DELETE_PL,
            'title'   => LANG_DELETE,
            'href'    => $this->href_to('delete_playlist', $playlist['id'])
        ));
        $this->addJS('templates/default/js/jquery-ui.js');
        $this->addCSS('templates/default/css/jquery-ui.css');
	}

?>
<div class=" white_del margin-b0 padding-b10">
    <?php $this->renderChild('channel_header', array('channel' => $channel, 'fields' => $fields)); ?>
    <div class="channel-body">
        <div id="channels-item-menu">
            <div class="tabs-menu">
                <?php $this->menu('channel-menu', true, 'tabbed nav nav-tabs flex'); ?>
            </div>
        </div>
    </div>
</div>

<h1><?php echo $playlist['title'].' <span>'.mb_strtolower(LANG_VIDEO_PLAYLIST).'</span>'; ?></h1>

<div id="playlist_wrap" class="overflow channel_trailer content_item">
    <div class="video_tumb_block">
        <a id="playlist_thumb" class="background_image" href="<?php echo href_to('video', $playlist['mslug'].'.html?playlist='.$playlist['id']); ?>" <?php if (!empty($playlist['tumb_path'])){ ?>style="background-image: url(<?php echo get_video_url('images_big', $playlist['tumb_path']); ?>)"<?php } ?>>
            <span class="playall"><span><i class="fa fa-play"></i> <?php echo LANG_VIDEO_PLAYALL; ?></span></span>
        </a>
    </div>
    <div class="video_edit_info">
        <div>
            <h3><?php echo $playlist['title']; ?></h3>
            <div class="playlist_info">
                <span>
                <?php if($playlist['is_access_by_link']){ ?>
                    <span class="color_red" title="<?php echo LANG_VIDEO_ONLY_LINK_PLAYLIST; ?>"><i class="fa fa-lock"></i></span>
                <?php } else { ?>
                    <span class="color_green" title="<?php echo LANG_VIDEO_PLAYLIST_FORALL; ?>"><i class="fa fa-globe"></i></span>
                <?php } ?>
                </span>
                <span>< class="glyphicon glyphicon-film"></span> <?php echo html_spellcount($playlist['movie_count'], LANG_MOVIE_1, LANG_MOVIE_2, LANG_MOVIE_10); ?></span>
                <span>< class="glyphicon glyphicon-eye-open"></span> <?php echo html_spellcount($total_hits, LANG_HITS_1, LANG_HITS_2, LANG_HITS_10); ?></span>
                <span>< class="glyphicon glyphicon-refresh"></span> <?php echo LANG_VIDEO_UPDATED.' '.string_date_age_max($playlist['date_update'], true); ?></span>
                <?php if($playlist['allow_embed']){ ?>
                    <span><a href="#plbox" id="share_playlist" class="color_green" title="<?php echo LANG_VIDEO_SHARE; ?>"><i class="fa fa-code"></i> <?php echo LANG_VIDEO_SHARE; ?></a></span>
                <?php } ?>
            </div>
            <?php echo $playlist['content']; ?>
        </div>
    </div>
</div>

<?php echo $video_html; ?>

<?php if($page == 1){ ?>
<!--noindex-->
<div id="show_more_block" class="hid" data-page="2" data-total_pages="0" data-perpage="<?php echo $perpage; ?>"><span><?php echo LANG_VIDEO_SHOW_MORE; ?></span> <i class="fa fa-spinner fa-pulse" style="display: none;"></i></div>
<!--/noindex-->
    <script type="text/javascript">
    $(function() {
        iVideo.initAjaxPagebar('.sortable_list');
    });
    </script>
<?php } ?>

<?php if($playlist['allow_embed']){ ?>
    <!--noindex-->
    <div id="plbox" class="hid"><div class="overflow" id="share_playlist_wrap">
    <?php $this->renderControllerChild('video', 'share_code', array('movie'=>array(
            'allow_embed' => 1,
            'is_playlist_checked' => 1,
            'is_playlist' => $playlist['id'],
            'share_code'  => provider::getShareCode($playlist['mid']),
            'id'          => $playlist['mid']
    ))); ?>
    </div></div>
    <!--/noindex-->
    <script type="text/javascript">
        $(function(){
            icms.modal.bind('#share_playlist');
        });
    </script>
<?php } ?>

<?php if ($user->is_admin || $channel['user_id'] == $user->id){ ?>
<script type="text/javascript">
    $(function(){
        var field_movie_list = $('.sortable_list');

        is_list = $(field_movie_list).find('.video_list_item').length;
        if(is_list > 0){
            placeholder = '';
        } else {
            placeholder = 'iwrap sort_placeholder_wrap overflow';
        }

        $(field_movie_list).sortable({
            opacity: 0.9, placeholder: placeholder,
            cursor: 'move', delay: 150, helper : 'clone', cancel: 'h3',
            start: function (event, ui){
                $('.sort_placeholder_wrap').html('<div class="player_wrap sort_placeholder"/>');
            },
            update: function(event, ui) {
                current_id = ui.item.data('movie_id');
                after_id   = ui.item.next().data('movie_id');
                before_id  = ui.item.prev().data('movie_id');
                $(this).sortable('disable');
                $.post('<?php echo $this->href_to('save_order', $playlist['id']); ?>',
                    {current_id: current_id,
                     before_id: before_id,
                     after_id: after_id},
                    function(data){
                        $(field_movie_list).sortable('enable');
                        if(data){
                            $('#playlist_thumb').css('background-image', 'url('+data+')');
                        }
                });
            }
        });
    });
</script>
<?php } ?>