<?php

    $page_title = LANG_CHANNEL_SUBSCRIBERS.' '.$channel['title'];

    $this->setPageTitle($page_title);
    $this->setPageDescription($page_title);

    if ($channel_ctype['options']['list_on']){
        $this->addBreadcrumb(empty($channel_ctype['labels']['list']) ? $channel_ctype['title'] : $channel_ctype['labels']['list'], href_to($channel_ctype['name']));
    }

    $this->addBreadcrumb($channel['title'], href_to($channel_ctype['name'], $channel['slug'].'.html'));

    $this->addBreadcrumb($page_title);

?>
<div class="white_del">
    <?php $this->renderChild('channel_header', array('channel' => $channel, 'fields' => $fields)); ?>
    <div class="channel-body">
        <div id="channels-item-menu">
            <div class="tabs-menu">
                <?php $this->menu('channel-menu', true, 'tabbed nav nav-tabs flex'); ?>
            </div>
        </div>

<h2 class="h3 margin-b20"><?php echo LANG_CHANNEL_SUBSCRIBERS.' <span><a href="'.href_to($channel_ctype['name'], $channel['slug'].'.html').'">'.$channel['title'].'</a></span>'; ?> <sup><?php echo $channel['subscribers_count']; ?></sup></h2>

<div id="users_profiles_list" class="striped-list list-32 subscribers_list">

    <?php foreach($subscribers as $profile){ ?>

        <div class="media users-item">
            <div class="media-left">
                <a class="subscribers-image" href="<?php echo href_to('users', $profile['id']); ?>"><?php echo html_avatar_image($profile['avatar'], 'small', $profile['nickname']); ?></a>
            </div>
            <div class="media-body">
            <div class="title">
                <a href="<?php echo href_to('users', $profile['id']); ?>"><?php html($profile['nickname']); ?></a>
                <?php if ($profile['is_online']){ ?>
                    <span class="is_online"><?php echo LANG_ONLINE; ?></span>
                <?php } ?>
                <?php if(isset($user_channels[$profile['id']])){ ?>
                    <div class="user_channel_list overflow">
                    <?php foreach($user_channels[$profile['id']] as $ch){ ?>
                        <div class="latest_channel_movie float_left">
                            <a class="poster_img" href="<?php echo href_to('channels', $ch['slug'].'.html'); ?>" title="<?php html($ch['title']);?>" style="background-image: url(<?php echo html_image_src($ch['photo'], 'normal', true); ?>)">
                                <span>
                                    <?php echo $ch['movie_count']; ?><br>
                                    <?php echo html_spellcount_only($ch['movie_count'], LANG_MOVIE_1, LANG_MOVIE_2, LANG_MOVIE_10); ?>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                    </div>
                <?php } ?>
            </div>

            <div class="actions">
                <?php echo html_spellcount($profile['movie_count'], LANG_MOVIE_1, LANG_MOVIE_2, LANG_MOVIE_10); ?>, <?php echo LANG_CHANNEL_SUBSCRIBE_AGO; ?> <?php echo string_date_age_max($profile['subscribe_date'], true); ?>
            </div>
            </div>
            <div class="media-right media-middle">
                <?php if($is_logged && $user_id != $profile['id']){ ?>
                    <a class="ajax-modal ajaxlink nowrap" href="<?php echo href_to('messages', 'write', $profile['id']); ?>"><?php echo LANG_CHANNEL_SEND_TO_USER; ?></a>
                <?php } ?>
            </div>
        </div>

    <?php } ?>

    <?php if ($perpage < $channel['subscribers_count']) { ?>
        <?php echo html_pagebar($page, $perpage, $channel['subscribers_count']); ?>
    <?php } ?>

</div>
    </div>
</div>