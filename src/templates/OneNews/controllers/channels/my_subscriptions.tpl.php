<?php

    $this->setPageTitle(LANG_YOU_CHANNEL_SUBSCRIBE);

    if ($ctype['options']['list_on']){
        $list_header = empty($ctype['labels']['list']) ? $ctype['title'] : $ctype['labels']['list'];
        $this->addBreadcrumb($list_header, href_to($ctype['name']));
    }
    $this->addBreadcrumb(LANG_YOU_CHANNEL_SUBSCRIBE);

    $this->addMenuItem('mysubscribe-menu', array(
        'title' => LANG_CHANNELS,
        'url'   => $this->href_to('my_subscriptions')
    ));

    $this->addMenuItem('mysubscribe-menu', array(
        'title' => LANG_CHANNELS_NEW_VIDEO,
        'url' => $this->href_to('my_subscriptions', 'videos')
    ));

    $this->addMenuItem('mysubscribe-menu', array(
        'title' => LANG_CHANNELS_VIEWED_VIDEO,
        'url' => $this->href_to('my_subscriptions', 'viewed_videos')
    ));

?>

<h1 class="nowrap_text"><?php echo LANG_YOU_CHANNEL_SUBSCRIBE; ?></h1>

<div class="content_datasets mysubscribe-menu">
    <?php $this->menu('mysubscribe-menu', true, 'pills-menu'); ?>
</div>

<div id="my_subscriptions_list"><?php echo $html; ?></div>
<script type="text/javascript">
iVideo.unSubscribeCallback = function (subscriberObj){
    $(subscriberObj).parents('.video_channels_list').fadeOut();
};
</script>