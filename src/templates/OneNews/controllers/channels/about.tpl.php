<?php

$user = cmsUser::getInstance();

$page_title = LANG_CHANNEL_ABOUT . ' ' . $channel['title'];

$this->setPageTitle($page_title);
$this->setPageDescription($page_title);

if ($channel_ctype['options']['list_on']) {
    $this->addBreadcrumb(empty($channel_ctype['labels']['list']) ? $channel_ctype['title'] : $channel_ctype['labels']['list'], href_to($channel_ctype['name']));
}

$this->addBreadcrumb($channel['title'], href_to($channel_ctype['name'], $channel['slug'] . '.html'));

$this->addBreadcrumb($page_title);

?>
<div class="white_del">
    <h1 class="nowrap_text"><?php echo LANG_CHANNEL_ABOUT . ' <span><a href="' . href_to($channel_ctype['name'], $channel['slug'] . '.html') . '">' . $channel['title'] . '</a></span>'; ?></h1>
    <?php $this->renderChild('channel_header', array('channel' => $channel, 'fields' => $fields)); ?>
    <div class="channel-body">
        <div id="channels-item-menu">
            <div class="tabs-menu">
                <?php $this->menu('channel-menu', true, 'tabbed nav nav-tabs flex'); ?>
            </div>
        </div>

        <div class="content_item channels_item padding-t10">

            <?php
            // удалим поля из списка, которые выводятся вручную в других местах
            unset($fields['photo'], $fields['background_img'], $fields['teaser']);
            foreach ($fields as $name => $field) { ?>

                <?php if (!$field['is_in_item'] || $field['is_system']) {
                    continue;
                } ?>
                <?php if ((empty($channel[$field['name']]) || empty($field['html'])) && $channel[$field['name']] !== '0') {
                    continue;
                } ?>
                <?php if ($field['groups_read'] && !$user->isInGroups($field['groups_read'])) {
                    continue;
                } ?>

                <?php
                if (!isset($field['options']['label_in_item'])) {
                    $label_pos = 'none';
                } else {
                    $label_pos = $field['options']['label_in_item'];
                }
                ?>

                <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">
                    <?php if ($label_pos != 'none') { ?>
                        <div class="title_<?php echo $label_pos; ?>"><?php html($field['title']); ?>:</div>
                    <?php } ?>
                    <div class="value"><?php echo $field['html']; ?></div>
                </div>

            <?php } ?>

            <?php if ($props && array_filter((array)$props_values)) { ?>
                <?php
                $props_fields = cmsCore::getController('content')->getPropsFields($props);
                $props_fieldsets = cmsForm::mapFieldsToFieldsets($props);
                ?>
                <div class="content_item_props <?php echo $channel_ctype['name']; ?>_item_props">
                    <table>
                        <tbody>
                        <?php foreach ($props_fieldsets as $fieldset) { ?>
                            <?php if ($fieldset['title']) { ?>
                                <tr>
                                    <td class="heading" colspan="2"><?php html($fieldset['title']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if ($fieldset['fields']) { ?>
                                <?php foreach ($fieldset['fields'] as $prop) { ?>
                                    <?php if (isset($props_values[$prop['id']])) { ?>
                                        <?php $prop_field = $props_fields[$prop['id']]; ?>
                                        <tr>
                                            <td class="title"><?php html($prop['title']); ?></td>
                                            <td class="value">
                                                <?php echo $prop_field->setItem($channel)->parse($props_values[$prop['id']]); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>


            <?php
            $is_tags = $channel_ctype['is_tags'] &&
                !empty($channel_ctype['options']['is_tags_in_item']) &&
                $channel['tags'];
            $show_bar = $channel_ctype['is_rating'] ||
                $fields['date_pub']['is_in_item'] ||
                $fields['user']['is_in_item'] ||
                !empty($channel_ctype['options']['hits_on']) ||
                !$channel['is_pub'] ||
                $is_tags ||
                !$channel['is_approved'];
            ?>

            <?php if ($show_bar) { ?>
                <ul class="info_bar">
                    <?php if (!empty($channel['rating_widget'])) { ?>
                        <li class="bar_item bi_rating">
                    <?php echo $channel['rating_widget']; ?>
                </li>
                    <?php } ?>
                    <?php if ($fields['date_pub']['is_in_item']) { ?>
                        <li class="bar_item bi_date_pub" title="<?php html($fields['date_pub']['title']); ?>">
                            <i class="fa fa-calendar"></i> <?php echo $fields['date_pub']['html']; ?>
                        </li>
                    <?php } ?>
                    <?php if (!$channel['is_pub']) { ?>
                        <li class="bar_item bi_not_pub">
                            <i class="fa fa-calendar"></i> <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                        </li>
                    <?php } ?>
                    <?php if (!empty($channel_ctype['options']['hits_on'])) { ?>
                        <li class="bar_item" title="<?php echo LANG_HITS; ?>">
                            <i class="fa fa-eye"></i> <?php echo $channel['hits_count']; ?>
                        </li>
                    <?php } ?>
                    <?php if ($fields['user']['is_in_item']) { ?>
                        <li class="bar_item bi_user" title="<?php html($fields['user']['title']); ?>">
                            <i class="fa fa-user"></i> <?php echo $fields['user']['html']; ?>
                        </li>
                        <?php if (!empty($channel['folder_title'])) { ?>
                            <li class="bar_item bi_folder">
                                <i class="fa fa-folder"></i> <a
                                        href="<?php echo href_to('users', $channel['user']['id'], array('content', $channel_ctype['name'], $channel['folder_id'])); ?>"><?php echo $channel['folder_title']; ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                    <?php if (!$channel['is_approved']) { ?>
                        <li class="bar_item bi_not_approved">
                            <i class="fa fa-warning"></i> <?php echo LANG_CONTENT_NOT_APPROVED; ?>
                        </li>
                    <?php } ?>
                    <?php if ($is_tags) { ?>
                        <li class="bar_item">
                            <i class="fa fa-tags"></i> <?php echo html_tags_bar($channel['tags']); ?>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>

        </div>
        <?php
        if ($channel['is_approved']) {

            echo cmsCore::getController('comments', new cmsRequest(array(
                'target_controller' => 'video',
                'target_subject' => 'channels',
                'target_id' => $channel['id']
            ), cmsRequest::CTX_INTERNAL))->getWidget();

        } ?>
        <script type="text/javascript">
            $(function () {
                $('#comments_widget h2').prepend('<i class="fa fa-bullhorn"></i> ');
            });
        </script>
    </div>
</div>