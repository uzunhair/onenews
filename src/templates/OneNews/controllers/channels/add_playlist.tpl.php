<?php

    $this->setPageTitle($title);

    if ($video_ctype['options']['list_on']){
        $this->addBreadcrumb($video_ctype['title'], href_to($video_ctype['name']));
    }

    if ($channel_ctype['options']['list_on']){
        $this->addBreadcrumb($channel_ctype['title'], href_to($channel_ctype['name']));
    }

    $this->addBreadcrumb($channel['title'], href_to($channel_ctype['name'], $channel['slug'].'.html'));

    $this->addBreadcrumb($title);

?>

<?php if($is_ajax) { ?>

<div class="success_parse_form overflow"><div>
    <?php $this->renderForm($form, $playlist, array(
            'action' => $this->href_to('add_playlist', $channel['id']),
            'method' => 'ajax'
        ), $errors); ?>
</div></div>

<?php } else { ?>

    <h1><?php echo $title; ?></h1>

    <?php $this->renderForm($form, $playlist, array(
            'action' => '',
            'submit' => array('title'=>$submit_title),
            'method' => 'post'
        ), $errors); ?>

<?php } ?>

<script type="text/javascript">
    $(function(){
        if((typeof(iParser) == 'object') ){
            iParser.resoreSubmitAjax();
            icms.modal.resize = function(){};
        }
    });
</script>