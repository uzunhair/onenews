<?php if($sortings){ ?>
<div class="channels-dropdown">
    <a id="channels-sort"  href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        Сортировать
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="channels-sort">
        <?php foreach($sortings as $key => $value) { ?>
            <li <?php if($key == $sort){ ?>class="active"<?php } ?>>
                <a href="?sort=<?php echo $key; ?>"><?php echo $value; ?></a>
            </li>
        <?php } ?>
    </ul>
</div>
<?php } ?>
<div id="channels-item-menu">
    <div class="tabs-menu">
        <?php $this->menu('channel-menu', true, 'tabbed nav nav-tabs flex'); ?>
    </div>
</div>

<?php if($trailer) { ?>

    <div class="overflow channel_trailer content_item">
        <div class="trailer-player">
            <?php echo $trailer['player_html']; ?>
        </div>
        <div class="overflow trailer-text">
                <ul class="info_bar trailer-info-bar margin-null">
                     <?php if($trailer['is_hd']) { ?> 
                    <li>
                       <i class="is_hd">HD</i>
                    </li>
                    <?php } ?>
                    <li class="bar_item">
                        <i class="glyphicon glyphicon-calendar"></i> <?php html(string_date_age_max($trailer['date_pub'], true)); ?>
                    </li>
                    <li class="bar_item">
                        <i class="glyphicon glyphicon-time"></i> <?php echo duration_format($trailer['duration']); ?>
                    </li>
                    <?php if ($trailer['comments']){ ?>
                        <li class="bar_item">
                            <i class="fa fa-comments"></i> <a href="<?php echo href_to('video', $trailer['slug'].'.html'); ?>#comments" title="<?php echo LANG_COMMENTS; ?>"><?php echo intval($trailer['comments']); ?></a>
                        </li>
                    <?php } ?>
                    <li class="bar_item" title="<?php echo LANG_HITS; ?>">
                        <i class="glyphicon glyphicon-eye-open"></i> <?php echo $trailer['hits_count']; ?>
                    </li>
                </ul>
                <h3 class="trailer-h nowrap_text">
                    <?php if ($trailer['parent_id']){ ?>
                        <a class="parent_title" href="<?php echo href_to($trailer['parent_url']); ?>"><?php html($trailer['parent_title']); ?></a>
                        &rarr;
                    <?php } ?>
                        <a href="<?php echo href_to_abs($ctype['name'], $trailer['slug'].'.html'); ?>" title="<?php html($trailer['title']); ?>"><?php html($trailer['title']); ?></a>
                </h3>
                <div class="video_content">
                    <?php if(!empty($trailer['content'])) { ?>
                        <p><?php echo html_clean($trailer['content'], 440); ?></p>
                    <?php } ?>
                </div>
        </div>
    </div>

<?php } ?>

<?php echo $html; ?>