<?php if ($items){ ?>

    <ul class="wg_comm media-list">
        <?php foreach($items as $item) { ?>

            <?php $author_url = href_to('users', $item['user']['id']); ?>
            <?php $target_url = href_to($item['target_url']) . "#comment_{$item['id']}"; ?>

            <li class="media">
			<?php
			/**
             *   <?php if ($show_avatars){ ?>
             *    <div class="pull-left">
             *       <a href="<?php echo $author_url; ?>"><?php echo html_avatar_image($item['user']['avatar'], 'micro', $item['user']['nickname']); ?></a>
             *   </div>
             *  <?php } ?>
			*/
			?> 
					<div class="comments-heading">
						<span class="glyphicon glyphicon-user"></span>
                        <?php if ($item['user_id']) { ?>
                            <a class="author" href="<?php echo $author_url; ?>"><?php html($item['user']['nickname']); ?></a>
                        <?php } else { ?>
                            <span class="author"><?php echo $item['author_name']; ?></span>
                        <?php } ?>						
						<span class="text-muted font-s11">
							<?php echo string_date_age_max($item['date_pub'], true); ?>
						</span>
					</div> 
                        <a class="subject" href="<?php echo $target_url; ?>">
                            <?php echo html_strip($item['target_title'], 50); ?>
						</a>
                        <?php if ($item['is_private']) { ?>
                            <span class="is_private" title="<?php html(LANG_PRIVACY_PRIVATE); ?>"></span>
                        <?php } ?>
                    <?php if ($show_text) { ?>
                        <div class="text">
                            <?php echo html_clean($item['content_html'], 50); ?>
                        </div>
                    <?php } ?>
            </li>
        <?php } ?>
    </ul>

<?php } ?>