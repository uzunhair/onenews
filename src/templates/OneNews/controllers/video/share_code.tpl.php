<?php if($movie['allow_embed']) { ?>

    <label style="display: none" class="from_playlist_hide"><?php echo LANG_MOVIE_EMBED_CODE; ?>:</label>
    <label style="display: none" class="from_playlist"><?php echo LANG_ENABLE_EMBED_PLAYLIST; ?>:</label>

    <?php echo html_textarea('', $movie['share_code'], array(
        'readonly'=>true,
        'id'=>'view_embed_code',
        'onclick'=>'$(this).select();',
    ));?>

    <label for="embed-layout-options"><?php echo LANG_VIDEO_SIZE; ?>:</label>
    <select id="embed-layout-options" class="form-control margin-b5">
        <option value="default" data-width="420" data-height="315">420 × 315</option>
        <option value="medium" data-width="480" data-height="360">480 × 360</option>
        <option value="large" data-width="640" data-height="480">640 × 480</option>
        <option value="hd720" data-width="960" data-height="720">960 × 720</option>
        <option value="custom"><?php echo LANG_OTHER_VIDEO_SIZE; ?></option>
    </select>
    <p id="share-embed-customize" class="hid form-inline">
        <input type="text" maxlength="4" id="embed_w" value="420" class="form-control"> × <input type="text" maxlength="4" id="embed_h" value="315" class="form-control"> <a class="ajaxlink size_cancel btn btn-danger" href="#"><?php echo LANG_CANCEL;?></a>
    </p>
    <label class="checkbox-inline"><input type="checkbox" class="embed_params" data-key="autoplay" value="1" /> <?php echo LANG_ENABLE_AUTOPLAY; ?></label>
    <label class="checkbox-inline" <?php if (empty($movie['is_playlist'])){ ?>style="display: none"<?php } ?>> <input data-key="playlist" type="checkbox" class="embed_params" <?php if (!empty($movie['is_playlist_checked'])){ ?>checked="true"<?php } ?> value="<?php if (!empty($movie['is_playlist'])){ echo $movie['is_playlist']; } ?>" /> <?php echo LANG_ENABLE_EMBED_PLAYLIST; ?></label>
    <label style="display: none" class="from_playlist checkbox-inline"> <input type="checkbox" class="embed_params" data-key="mid" id="share_mid" value="<?php echo $movie['id']; ?>" /> <?php echo LANG_ENABLE_EMBED_PLAYLIST_CURRENT; ?></label>

<script type="text/javascript">
    $(function(){
        iCode = {
            code_selector: '#view_embed_code',
            embed_code: '',
            embed_link: '',
            url_params: {},
            buildCode: function (){
                current_code = this.embed_code;
                if(Object.keys(this.url_params).length > 0){
                    var key, use_val, use_key, i = 0, tmp_arr = [];
                    for(key in this.url_params){
                        use_key = escape(key);
                        use_val = escape((this.url_params[key].toString()));
                        use_val = use_val.replace(/%20/g, '+');
                        tmp_arr[i] = use_key + '=' + use_val;
                        i++;
                    }
                    link = this.embed_link+'?'+tmp_arr.join('&');
                } else {
                    link = this.embed_link;
                }
                current_code.attr('src', link);
                $(this.code_selector).html($('<div>').append(current_code.clone()).remove().html());
            },
            init: function (){
                this.embed_code = $($(this.code_selector).text());
                this.embed_link = this.embed_code.attr('src');
                $('.embed_params').on('click', function(){
                    if($(this).prop('checked')){
                        iCode.url_params[$(this).data('key')] = $(this).val();
                        $('.from_'+$(this).data('key')).show();
                        $('.from_'+$(this).data('key')+'_hide').hide();
                    } else {
                        delete iCode.url_params[$(this).data('key')];
                        subkey = $('.from_'+$(this).data('key')).hide().find('input').prop('checked', false).data('key');
                        $('.from_'+$(this).data('key')+'_hide').show();
                        delete iCode.url_params[subkey];
                    }
                    iCode.buildCode();
                }).each(function(indx, element){
                    $(this).triggerHandler('click');
                });
                $('#embed-layout-options').on('change', function(){
                    width  = $('option:selected', this).data('width');
                    height = $('option:selected', this).data('height');
                    if(!width || !height){
                        $(this).hide();
                        $("#share-embed-customize").fadeIn();
                        $("#embed_w").focus();
                        return;
                    }
                    iCode.embed_code.attr('width', width);
                    iCode.embed_code.attr('height', height);
                    iCode.buildCode();
                }).trigger('change');
                $('#embed_w').on('keyup', function (){
                    iCode.embed_code.attr('width', $(this).val());
                    iCode.buildCode();
                });
                $('#embed_h').on('keyup', function (){
                    iCode.embed_code.attr('height', $(this).val());
                    iCode.buildCode();
                });
                $('.size_cancel').on('click', function (){
                    $('#share-embed-customize').hide();
                    $('#embed-layout-options').fadeIn();
                    return false;
                });
            }
        };
        iCode.init();
    });
</script>
<?php } else { ?>
    <p class="negative"><?php echo LANG_EMBED_IS_DISABLE; ?></p>
<?php }