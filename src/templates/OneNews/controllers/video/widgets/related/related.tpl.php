<div class="widget_video overflow">
    <?php foreach ($movies as $movie) { ?>

    <?php
        $movie_link = href_to_abs($ctype['name'], $movie['slug']) . '.html';
        $is_private = $movie['is_private'] && !empty($hide_except_title) && !$user->isFriend($movie['user_id']);
        $poster_url = (!empty($movie['tumb_path']) ? get_video_url('images_small', $movie['tumb_path']) : '');
        if(!isset($movie['custom_img'])){ // проверка для совместимости с 2.3.0
            $movie['custom_img'] = format_custom_images($movie['custom_img_count'], $movie['tumb_path']);
        }
        if ($is_private && function_exists('default_images')) { // проверка для совместимости с 2.3.0
            $poster_url = html_image_src(default_images('private', 'normal'), 'normal', true);
            $movie_link = '#';
        }
    ?>

    <div class="widget_video_wrap<?php if($movie['custom_img'] && !$is_private){ ?> start_rotation<?php } ?>" <?php if($movie['custom_img'] && !$is_private){ ?> data-custom_img="<?php html(json_encode($movie['custom_img'])); ?>"<?php } ?>>
        <a class="background_image <?php if($is_private) { ?>private_player_wrap movie_is_private<?php } ?>" title="<?php html($movie['title']); ?>" href="<?php echo $movie_link; ?>" target="_top" style="background-image: url(<?php html($poster_url); ?>);">
            <i class="fa fa-play-circle-o"></i>
            <strong><span class="nowrap_text"><?php echo $movie['title']; ?></span></strong>
            <?php if($movie['duration']){ ?>
                <div class="duration"><i class="fa fa-clock-o"></i> <span title="<?php echo LANG_VIDEO_DURATION; ?>"><?php echo duration_format($movie['duration']); ?></span></div>
            <?php } ?>
        </a>
    </div>
    <?php } ?>
</div>
<script type="text/javascript">
    <?php echo $this->getLangJS('LANG_PRIVACY_PRIVATE_HINT'); ?>
</script>
<?php if($widget->getOption('autoscroll')){ ?>
    <script type="text/javascript">
        iAutoScroll = {

            win: null,
            sidebar: null,
            p_height: null,
            s_height: null,
            topPadding: 0,
            bottomPadding: 100,

            init: function (){
                this.win      = $(window);
                if(this.win.width() < 980){ return; }
                this.sidebar  = $('#body aside');
                this.p_height = +$('#body section').height();
                this.s_height = +this.sidebar.height();
                this.offset   = this.sidebar.offset();
                if(this.s_height > this.p_height){
                    this.sidebar.css({width: ''});
                    return;
                }
                this.sidebar.css({width: this.sidebar.width()});
                this.run();
            },
            run: function (){
                handler = function (){
                    iAutoScroll.doAutoScroll();
                };
                this.win.off('scroll', handler).on('scroll', handler).trigger('scroll');
            },
            doAutoScroll: function (){
                scroll_top = this.win.scrollTop();
                delta      = this.p_height-this.s_height+this.bottomPadding;
                if ((scroll_top > (this.offset.top-this.topPadding)) && (scroll_top < delta)) {
                    this.sidebar.css({
                        top: this.topPadding+'px',
                        left: this.offset.left+'px',
                        position: 'fixed'
                    });
                } else if(scroll_top >= delta){
                    this.sidebar.css({
                        top: (delta-scroll_top)+'px',
                        left: this.offset.left+'px',
                        position: 'fixed'
                    });
                } else {
                    this.sidebar.css({
                        top: '', position: ''
                    });
                }
            }

        };
    $(function(){
        iAutoScroll.init();
        $(window).on('resize', function (){
            iAutoScroll.init();
        });
    });
    </script>
<?php }
