<?php if($widget->getOption('show_autoplay_set')){
    $this->addJS('templates/'.$this->name.'/js/jquery-cookie.js');
    $widget->title .= ' <span class="wautoplay color_asbestos"><i class="fa fa-square-o"></i> '.LANG_WD_VIDEO_NEXT_AUTOPLAY.'</span>';
} ?>

<div class="widget_video overflow">
    <div class="widget_video_wrap<?php if($next_item['custom_img'] && !$is_private){ ?> start_rotation<?php } ?>" <?php if($next_item['custom_img'] && !$is_private){ ?> data-custom_img="<?php html(json_encode($next_item['custom_img'])); ?>"<?php } ?>>
        <a class="background_image <?php if($is_private) { ?>private_player_wrap<?php } ?>" title="<?php html($next_item['title']); ?>" href="<?php echo href_to('video', $next_item['slug'] . '.html');?>" target="_top" style="background-image: url(<?php html($poster_url); ?>);">
            <i class="fa fa-play-circle-o"></i>
            <strong><span class="nowrap_text"><?php echo $next_item['title']; ?></span></strong>
            <?php if($next_item['duration']){ ?>
                <div class="duration"><i class="fa fa-clock-o"></i> <span title="<?php echo LANG_VIDEO_DURATION; ?>"><?php echo duration_format($next_item['duration']); ?></span></div>
            <?php } ?>
        </a>
    </div>
</div>
<!--noindex-->
<div class="height100 hid" id="next_video">
    <div class="poster_img" style="background-image: url(<?php html($poster_url); ?>);"></div>
    <div class="next-overlay"></div>
    <div class="next-container">
        <div class="next-wrap">
            <div class="next-info">
                <div class="next-pretitle"><?php echo LANG_VIDEO_NEXT; ?></div>
                <div class="next-title"><?php echo $next_item['title']; ?></div>
            </div>
            <div class="next-cancel">
                <a href="#" title="<?php echo LANG_VIDEO_CANCEL_NEXT; ?>"><?php echo LANG_CANCEL; ?></a>
            </div>
            <div class="timer"><div class="percent"><i class="fa fa-play"></i></div><div id="slice"><div class="pie"></div><div class="pie fill hid"></div></div></div>
        </div>
    </div>
</div>
<!--/noindex-->
<?php if($widget->getOption('show_autoplay_set')){ ?>
<script type="text/javascript">
    $(function(){
        var next_html_block = $('#next_video').removeClass('hid');
        next_html_block.detach();
        var default_after_play = ivPlayLists.runAfterPlay;
        var tid, deg = 0;
        $('#player_container').on('click', '.timer', function (){
            clearInterval(tid);
            window.location.href = '<?php echo href_to('video', $next_item['slug'] . '.html?autoplay=1');?>';
            return false;
        });
        $('#player_container').on('click', '.next-cancel a', function (){
            clearInterval(tid);
            ivPlayLists.restoreAfter().after(); tid, deg = 0;
            return false;
        });
        $('.wautoplay').on('click', function (){
            $('.fa', this).toggleClass('fa-square-o fa-check-square-o');
            $(this).toggleClass('color_asbestos color_green');
            if($('.fa', this).hasClass('fa-square-o')){
                ivPlayLists.runAfterPlay = default_after_play;
                $.cookie('icms[widget_next]', 0, { path: '/' });
                clearInterval(tid);
            } else {
                $.cookie('icms[widget_next]', 1, {expires: 7, path: '/'});
                ivPlayLists.runAfterPlay = function (){
                    $('#player_container').html(next_html_block.clone(true));
                    tid = setInterval(function(){
                        if(deg > 180){
                            $('#slice').addClass('gt50');
                            $('.pie.fill').removeClass('hid');
                        }
                        $('#slice .pie').not('.fill').css({
                            '-moz-transform':'rotate('+deg+'deg)',
                            '-webkit-transform':'rotate('+deg+'deg)',
                            '-o-transform':'rotate('+deg+'deg)',
                            'transform':'rotate('+deg+'deg)'
                        });
                        if(deg >= 360){
                            $('.timer').trigger('click');
                            return;
                        }
                        deg += 15;
                    }, 300);
                };
            }
        });
        if($.cookie('icms[widget_next]') == 1){
            $('.wautoplay').trigger('click');
        }
    });
</script>
<?php } ?>