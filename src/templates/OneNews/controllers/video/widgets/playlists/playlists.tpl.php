<?php
    if(isset($_wd_template)){
        $this->addJS('templates/default/js/video/all.js');
        $this->addCSS('templates/default/controllers/video/styles.css');
    }
?>
<div class="playlists_list">
    <?php foreach($playlists as $playlist){ ?>
    <div class="video_list_item overflow">
        <div class="float_left video_tumb_block">
            <a class="background_image" href="<?php if($playlist['movie_count']) { ?><?php echo href_to('video', $playlist['mslug'].'.html?playlist='.$playlist['id']); ?><?php } else { ?>#<?php } ?>" <?php if (!empty($playlist['tumb_path'])){ ?>style="background-image: url(<?php echo get_video_url('images_small', $playlist['tumb_path']); ?>)"<?php } ?> <?php if(!$playlist['movie_count']) { ?>onclick="return false;"<?php } ?>>
                <span class="vitems_count">
                    <b><?php if($playlist['movie_count']) { ?><?php echo $playlist['movie_count']; ?><?php } else { ?><?php echo LANG_NO; ?><?php } ?></b> <?php echo html_spellcount_only($playlist['movie_count'], LANG_MOVIE_1, LANG_MOVIE_2, LANG_MOVIE_10); ?>
                    <i class="fa fa-bars"></i>
                </span>
                <span class="playall"><span><i class="fa fa-play"></i> <?php echo LANG_VIDEO_PLAYALL; ?></span></span>
            </a>
        </div>
        <div class="overflow video_text_block">
            <div class="float_left video_text">
                <h3>
                    <?php if($playlist['date_update']){ ?>
                        <div class="float-right date_update color_asbestos"><i class="fa fa-clock-o"></i> <?php echo LANG_VIDEO_UPDATED.' '.string_date_age_max($playlist['date_update'], true); ?></div>
                    <?php } ?>
                    <a href="<?php echo href_to('channels', $playlist['channel_slug'], array('playlists', $playlist['slug'])); ?>"><?php echo $playlist['title']; ?></a>
                    <?php if($playlist['is_access_by_link']){ ?>
                        <span class="color_red" title="<?php echo LANG_VIDEO_ONLY_LINK_PLAYLIST; ?>"><i class="fa fa-lock"></i></span>
                    <?php } else { ?>
                        <span class="color_green" title="<?php echo LANG_VIDEO_PLAYLIST_FORALL; ?>"><i class="fa fa-globe"></i></span>
                    <?php } ?>
                </h3>
                <div class="video_content">
                    <?php echo html_clean($playlist['content'], 420); ?>
                </div>
                <div class="gradient_line"></div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>