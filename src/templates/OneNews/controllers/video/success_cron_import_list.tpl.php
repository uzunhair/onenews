<?php

    $date = date(cmsConfig::get('date_format'), $time);
    $date_time = $date.' <span class="time">'.date('H:i', $time). '</span>';

    $this->setPageTitle(sprintf(LANG_VIDEO_SUCCESS_CRON_IMPORT_PAGE, $date));

    if ($ctype['options']['list_on']){
        $list_header = empty($ctype['labels']['list']) ? $ctype['title'] : $ctype['labels']['list'];
        $this->addBreadcrumb($list_header, href_to($ctype['name']));
    }
    $this->addBreadcrumb(sprintf(LANG_VIDEO_SUCCESS_CRON_IMPORT_PAGE, $date));

?>

<h1 class="nowrap_text"><?php echo sprintf(LANG_VIDEO_SUCCESS_CRON_IMPORT_PAGE, $date_time); ?></h1>

<div id="related_video_list"><?php echo $html; ?></div>