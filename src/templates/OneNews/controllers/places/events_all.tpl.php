<?php

	$pagetitle = $h1title = $title;

	if ($cat_id > 1){
		$pagetitle = $title . ': ' . $category['title'];
		$h1title = $title . ': <span>' . $category['title'] . '</span>';
	}

	$this->addBreadcrumb($ctype['title'], href_to($ctype['name']));
	$this->addBreadcrumb($title, $this->href_to('events'));

	$this->setPageTitle($pagetitle);

	if (isset($category['path']) && $category['path']){
        foreach($category['path'] as $c){
            $this->addBreadcrumb($c['title'], $this->href_to('events'). '?cat_id=' . $c['id']);
        }
    }

    $base_url = $this->href_to('events');

    $city = $this->controller->options['is_city_filter'] ? cmsUser::sessionGet('maps-city') : false;
	$city_name = $city ? $city['name'] : LANG_PLACES_CITY_SELECT;

?>
<div class="white_del">
<h1>
    <?php echo $h1title; ?>
    <?php if ($this->controller->options['is_city_filter']) { ?>
        <div id="maps-list-city-selector">
            <a class="ajax-modal" href="<?php echo href_to('places', 'city_select'); ?>" title="<?php echo LANG_PLACES_CITY_SELECT; ?>"><?php echo $city_name; ?></a>
        </div>
    <?php } ?>    
</h1>

<div class="content_datasets">
    <ul class="nav nav-filtr">
        <?php if ($day_date){ ?>
            <li class="active"><?php echo html_date($day_date); ?></li>
        <?php } ?>
        <?php $p_counter = 0; ?>
        <?php foreach($periods as $pid=>$title){ ?>
            <?php $p_selected = ($pid == $period || (!$period && $p_counter==0)); ?>
            <li <?php if ($p_selected){ ?>class="active"<?php } ?>>

                <?php $cat_query = ($cat_id > 1) ? 'cat_id=' . $cat_id : ''; ?>
                <?php if ($p_counter > 0) { $url = $base_url . '?period=' . $pid . ($cat_query ? '&' . $cat_query : ''); } ?>
                <?php if ($p_counter == 0) { $url = $base_url . ($cat_query ? '?' . $cat_query : ''); } ?>

                <?php if ($p_selected){ ?>
                    <a href="#"><?php echo $title; ?></a>
                <?php } else { ?>
                    <a href="<?php echo $url; ?>"><?php echo $title; ?></a>
                <?php } ?>

            </li>
            <?php $p_counter++; ?>
        <?php } ?>
    </ul>
</div>

<?php if ($subcats && $ctype['is_cats'] && !empty($ctype['options']['is_show_cats'])){ ?>
    <div class="gui-panel  clearfix content_categories<?php if (count($subcats)>8){ ?> categories_small<?php } ?>">
        <ul class="cont_cat flex">
            <?php foreach($subcats as $c){ ?>
                <li>
                    <?php
                        $url = $this->href_to('events');
                        $url .= '?' . ($period != 'near' ? "period={$period}&" : '') . "cat_id={$c['id']}";
                    ?>
                    <a href="<?php echo $url; ?>"><?php echo $c['title']; ?></a>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>

<div id="maps-news-list" class="maps-entries-list">

	<?php if ($events) { ?>

		<?php $this->renderChild('event', array(
			'events' => $events,
			'ctype' => $ctype,
            'is_item_link' => true
		)); ?>

	<?php } ?>

	<?php if (!$events) { ?>
		<p>
			<?php echo LANG_PLACES_NEWS_NONE; ?>
		</p>
	<?php } ?>

</div>

<?php if($events && ($total > $perpage)) { ?>
	<?php
		$query = ($cat_id > 1) ? array('cat_id'=>$cat_id) : false;
		echo html_pagebar($page, $perpage, $total, false, $query);
	?>
<?php } ?>
</div>