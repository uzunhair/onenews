<?php 
    $embed_url = href_to_abs($this->controller->name, 'embed', $item['id']) .  '{m}';
    $code = sprintf(LANG_PLACES_EMBED_CODE_TEMPLATE, $embed_url);
?>
<div id="maps-embed-code-form">
	
    <form>

        <fieldset>
        
            <legend><?php echo LANG_OPTIONS; ?></legend>
            
            <?php if (count($item['addrs'])>1){ ?>
            <div class="field marker_id">
                <label><?php echo LANG_PLACES_EMBED_TYPE; ?> </label>
                <?php echo html_select('marker_id', $addrs, $marker_id); ?>
            </div>
            <?php } ?>
        
            <div class="field width">
                <label><?php echo LANG_PLACES_EMBED_W; ?></label>
                <?php echo html_input('text', 'width', $default['width']); ?>
            </div>
            
            <div class="field height">
                <label><?php echo LANG_PLACES_EMBED_H; ?></label>
                <?php echo html_input('text', 'height', $default['height']); ?>
            </div>
            
        </fieldset>
        
        <fieldset>
            
            <legend><?php echo LANG_PLACES_EMBED_CODE; ?></legend>
        
            <div class="field">
                <?php echo html_textarea('code', $code); ?>
                <div class="hint"><?php echo LANG_PLACES_EMBED_CODE_HINT; ?></div>
            </div>
            
        </fieldset>
        
        <div class="buttons">
            <input type="button" class="button" value="<?php echo LANG_CLOSE; ?>">
        </div>
    
    </form>
	
</div>

<script>
    var form = $('#maps-embed-code-form');
	$('.button', form).on('click', function(){ icms.modal.close(); });
    $('.input', form).on('keyup', function(){ changeCode(); });
    $('select', form).on('change', function(){ changeCode(); });
    function changeCode(){        
        var code = '<?php echo $code; ?>';
        var w = Number($('.width .input', form).val());
        var h = Number($('.height .input', form).val());
        var m = $('.marker_id select', form).val();
        if (!w) { w = 400; $('.width', form).addClass('field_error'); } else { $('.width', form).removeClass('field_error'); }
        if (!h) { h = 400; $('.height', form).addClass('field_error'); } else { $('.height', form).removeClass('field_error'); }
        if (!m) { m = ''; } else { m = '/'+m; }
        code = code.replace('{w}', w);
        code = code.replace('{h}', h);
        code = code.replace('{m}', m);
        $('textarea', form).val(code);
    }
    changeCode();
</script>
