<?php
    $is_bh = empty($this->controller->options['no_bh']);
    if ($is_bh){
        $hours = array();
        for($h=0; $h<=23; $h++){
            $hour = $h > 9 ? $h : '0'.$h;
            $hours["{$h}:00"] = "{$h}:00";
            $hours["{$h}:30"] = "{$h}:30";
        }
    }
?>
<div id="maps-marker-form">

	<div id="columns">

		<div id="column-form" class="column">
				<?php echo html_input('hidden', 'marker-id', '0', array('id'=>'input-marker-id')); ?>
				<fieldset>
					<legend><?php echo LANG_PLACES_ADDR; ?></legend>
					<div class="field field-link">
						<span id="city-name"><?php echo $city['name']; ?></span>
						<?php if (!$is_one_city){ ?>
							<a id="select-city" href="#select-city" class="ajaxlink"><?php echo LANG_PLACES_ADDR_SELECT_CITY; ?></a>
						<?php } ?>
						<?php echo html_input('hidden', 'addr[city]', $city['name'], array('id'=>'input-addr-city')); ?>
						<?php echo html_input('hidden', 'addr[region]', $city['region'], array('id'=>'input-addr-region')); ?>
						<?php echo html_input('hidden', 'addr[country]', $city['country'], array('id'=>'input-addr-country')); ?>
						<?php echo html_input('hidden', 'addr[city_id]', $city['id'], array('id'=>'input-addr-city-id')); ?>
						<?php echo html_input('hidden', 'addr[city_lat]', nf($city['lat'], 4), array('id'=>'input-addr-city-lat')); ?>
						<?php echo html_input('hidden', 'addr[city_lng]', nf($city['lng'], 4), array('id'=>'input-addr-city-lng')); ?>
					</div>
					<div class="field">
						<?php echo html_input('text', 'addr[street]', '', array('placeholder'=>LANG_PLACES_ADDR_STREET, 'id'=>'input-addr-street')); ?>
					</div>
					<div class="field">
						<?php echo html_input('text', 'addr[house]', '', array('placeholder'=>LANG_PLACES_ADDR_HOUSE, 'id'=>'input-addr-house')); ?>
					</div>
					<div class="field">
						<?php echo html_input('text', 'addr[room]', '', array('placeholder'=>LANG_PLACES_ADDR_ROOM, 'id'=>'input-addr-room')); ?>
					</div>
				</fieldset>
				<fieldset>
					<legend><?php echo LANG_PLACES_COORD; ?></legend>
					<div class="field field-link">
						<a href="#find-coords" id="find-coords" class="ajaxlink"><?php echo LANG_PLACES_COORD_FIND; ?></a>
					</div>
					<div class="field">
						<?php echo html_input('text', 'coord[lat]', '', array('placeholder'=>LANG_PLACES_COORD_LAT, 'id'=>'input-lat')); ?>
					</div>
					<div class="field">
						<?php echo html_input('text', 'coord[lng]', '', array('placeholder'=>LANG_PLACES_COORD_LNG, 'id'=>'input-lng')); ?>
					</div>
				</fieldset>
				<fieldset>
					<legend><?php echo LANG_PLACES_CONTACTS; ?></legend>
					<div class="field">
						<?php echo html_input('text', 'contacts[phone]', '', array('placeholder'=>LANG_PLACES_CONTACTS_PHONE, 'id'=>'input-contacts-phone')); ?>
					</div>
					<div class="field">
						<?php echo html_input('text', 'contacts[url]', '', array('placeholder'=>LANG_PLACES_CONTACTS_URL, 'id'=>'input-contacts-url')); ?>
					</div>
					<div class="field">
						<?php echo html_input('text', 'contacts[skype]', '', array('placeholder'=>LANG_PLACES_CONTACTS_SKYPE, 'id'=>'input-contacts-skype')); ?>
					</div>
					<div class="field">
						<?php echo html_input('text', 'contacts[email]', '', array('placeholder'=>LANG_EMAIL, 'id'=>'input-contacts-email')); ?>
					</div>
				</fieldset>
            <?php if ($is_bh) { ?>
				<fieldset>
					<legend><?php echo LANG_PLACES_BH; ?></legend>
					<div class="field field-link">
						<a href="#bh-edit" id="bh-edit" class="ajaxlink"><?php echo LANG_EDIT; ?></a>
					</div>
				</fieldset>
            <?php } ?>
		</div>

		<div id="column-map" class="column">
			<div id="map-canvas"></div>
			<div id="city-selection">
				<div id="city-search">
					<h2><?php echo LANG_PLACES_CITY_SELECTION; ?></h2>
					<div class="search-form gui-panel padding-0">
						<div class="legend"><?php echo LANG_PLACES_CITY_SEARCH; ?></div>
						<div class="field form-inline">
							<input type="text" id="city-search-name" class="input form-control"> 
							<input type="button" class="button btn btn-primary" value="<?php echo LANG_FIND; ?>" data-url="<?php echo $this->href_to('city_search'); ?>">
						</div>
					</div>
					<div id="city-results">
						<div class="loading" style="display:none"><?php echo LANG_LOADING; ?></div>
						<div class="list maps-city-list"></div>
					</div>
				</div>
				<div id="city-exists">
					<h2><?php echo LANG_PLACES_CITY_SELECTION; ?></h2>
					<div class="list maps-city-list">
						<?php if ($cities) { ?>
							<?php foreach($cities as $city){ ?>
								<div class="item" data-id="<?php echo $city['id']; ?>" id="city-<?php echo $city['id']; ?>" data-lat="<?php echo nf($city['lat'], 4); ?>" data-lng="<?php echo nf($city['lng'], 4); ?>">
									<div class="name"><?php echo $city['name']; ?></div>
									<div class="details">
										<span class="country"><?php echo $city['country']; ?></span>,
										<span class="region"><?php echo $city['region']; ?></span>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<div class="other margin-t10">
						<a href="#other" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> <?php echo LANG_PLACES_CITY_OTHER; ?></a>
					</div>
				</div>
			</div>
            <?php if ($is_bh) { ?>
            <div id="b-hours">
                <h2><?php echo LANG_PLACES_BH; ?></h2>
                <div class="days">
                    <table class="table table-striped">
                        <?php for($d=1; $d<=7; $d++){ ?>
                            <tr data-day="<?php echo $d; ?>" class="day day-<?php echo $d; ?>">
                                <td class="cell-day">
                                    <?php echo constant('LANG_PLACES_DAY_' . $d); ?>
                                </td>
                                <td class="cell-hours">
                                    <div class="wrap form-inline">
                                        <?php echo html_select('bh-day-'.$d.'-start', $hours, '', array('class'=>'bh-start day-'.$d)); ?> &mdash;
                                        <?php echo html_select('bh-day-'.$d.'-end', $hours, '', array('class'=>'bh-end day-'.$d)); ?>
                                    </div>
                                </td>
                                <td class="cell-off">
                                    <label>
                                        <input type="checkbox" class="bh-off day-<?php echo $d; ?>">
                                        <?php echo LANG_PLACES_DAY_OFF; ?>
                                    </label>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            <?php } ?>
		</div>

	</div>

	<div id="buttons">

		<input type="button" class="button-submit" value="<?php echo LANG_SAVE; ?>">
		<input type="button" class="button" value="<?php echo LANG_CANCEL; ?>">

	</div>

</div>
