<?php 
	$this->addJS('templates/default/js/maps/category.js');

	$icon = empty($category['icon']) ? 'default.png' : $category['icon'];
	$path = cmsConfig::get('upload_host') . '/markers';
	$path_pattern = $path . "/%s";
	$list = cmsCore::getFilesList(cmsConfig::get('upload_root') . '/markers', '*.png');
?>

<fieldset>
	<legend><?php echo LANG_PLACES_CATEGORY_MARKER; ?></legend>
	<div id="maps-category-marker" data-criteria-url="<?php echo $this->href_to('get_criteria'); ?>">
		
		<div class="value">
			<div class="icon" style="background-image:url('<?php printf($path_pattern, $icon); ?>')"></div>
			<a href="#select-marker" class="ajaxlink"><?php echo LANG_SELECT; ?></a>
		</div>
		
		<div id="markers-gallery-window">
			<div id="markers-gallery">
				<?php if ($list) { ?>
					<ul>
						<?php foreach($list as $file){ ?>
						<li style="background-image:url('<?php printf($path_pattern, $file); ?>')"><a href="#select" data-icon="<?php echo $file; ?>"></a></li>
						<?php } ?>
					</ul>
				<?php } ?>
			</div>
		</div>
			 
	</div>
</fieldset>
