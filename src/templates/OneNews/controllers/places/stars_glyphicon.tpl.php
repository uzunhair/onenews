<?php 	
	$rating = $value;
	$value *= 10; $step = 0.5 * 10;
	$value = (round($value)%$step === 0) ? round($value) : round(($value+$step/2)/$step)*$step;
	$value = $value / 10;		
	
	if (!isset($show_value)) { $show_value = true; }
?>
<div class="maps-star-rating<?php if (!empty($class)) { ?> <?php echo $class; ?><?php } ?>">
	<div class="stars">
		<?php for($s=1; $s<=5; $s++) { ?>
			<?php 			
				if ($value >= 1) { $class = 'glyphicon-star'; $value -= 1; } else 
				if ($value >= 0.5) { $class = 'glyphicon-star-empty half'; $value -= 0.5; } else 
				{ $class = 'glyphicon-star-empty'; $value -= 0.5; }
			?>
			<span class=" glyphicon <?php echo $class; ?>"></span>
		<?php } ?>
	<?php if ($show_value) { ?>
		<span class="value"><?php echo $rating; ?></span>
	<?php } ?>
	</div>
</div>
