<?php
    $options = $this->controller->options;    
    $is_one_marker = count($markers)>1 ? false: true;
    $zoom = $is_one_marker ? 16 : 6;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php $this->addMainCSS("templates/{$this->name}/controllers/places/styles_embed.css"); ?>
        <?php $this->addMainJS("templates/{$this->name}/js/jquery.js"); ?>
        <?php $this->addMainJS("templates/{$this->name}/js/maps/systems/{$options['system']}.js"); ?>        
        <?php $this->addMainJS("templates/{$this->name}/js/maps/embed.js"); ?>        
        <?php $this->head(); ?>
    </head>
    <body>
        <div id="map-canvas"></div>
        <script>
            icms.mapsEmbed.init('map-canvas', {
                zoom: <?php echo $zoom; ?>,
                min_zoom: 5,
                max_zoom: 18,
                center: [<?php echo nf($center['lat'], 4); ?>, <?php echo nf($center['lng'], 4); ?>],
                map_type: '<?php echo $options['mm_map_type']; ?>',
                map_type_select: false,
                scroll_zoom: true,
                icons_url: '<?php echo cmsConfig::get('upload_host') . '/markers'; ?>',
                balloon_url: '<?php echo href_to('places', 'balloon'); ?>?is_embed=1',
                bounds: <?php echo $bounds_json; ?>, 
                markers: <?php echo $markers_json; ?>});
        </script>
    </body>
<html>
