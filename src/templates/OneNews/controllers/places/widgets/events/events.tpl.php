<?php $this->addCSS( $this->getStylesFilename('places', 'widgets') ); ?>
<?php if ($events){ ?>

    <div class="widget_places_events maps_widget">
        <?php foreach($events as $entry) { ?>
            <?php 
                $url = href_to($ctype_name, 'events', array($entry['item_id'], $entry['id']));
                $is_image = $is_images && !empty($entry['image']);
            ?>
            <div class="item media media-white<?php if ($is_image) { ?> is-image<?php } ?>">
                <?php if ($is_image) { ?>
                    <div class="image media-left">
                        <a href="<?php echo $url; ?>">
                            <?php echo html_image($entry['image']); ?>
                        </a>
                    </div>
                <?php } ?>
                <div class="media-body">
                    <div class="title">
                        <a href="<?php echo $url; ?>"><?php echo $entry['title']; ?></a>
                    </div>
                    <?php if ($is_teaser && $entry['teaser']){ ?>
                        <div class="content"><?php echo $entry['teaser']; ?></div>
                    <?php } ?>
                    <?php if ($is_dates){ ?>
                        <div class="event-dates">
                            <?php echo LANG_WD_PLACES_EVENTS_START; ?>:
                            <span class="text-primary"><?php echo html_date_time($entry['date_start']); ?></span>
                            <?php if ($entry['is_long'] && $entry['date_end']) { ?>
                                <?php echo LANG_WD_PLACES_EVENTS_END; ?>:
                                <span class="text-primary"><?php echo html_date($entry['date_end']); ?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if ($is_date || $is_user || $is_item) { ?>
                        <div class="details">
                            <?php if ($is_item) { ?>
                                <span class="addr">
                                    <a class="text-info" href="<?php echo href_to($ctype_name, $entry['item_slug'].'.html'); ?>"><?php html($entry['item_title']); ?></a>
                                </span>
                            <?php } ?>
                            <?php if ($is_user) { ?>
                                <span class="author">
                                    <a href="<?php echo href_to('users', $entry['user']['id']); ?>"><?php html($entry['user']['nickname']); ?></a>
                                </span>
                            <?php } ?>
                            <?php if ($is_date) { ?>
                                <span class="date text-muted">
                                    <?php echo html_date($entry['date_pub']); ?>
                                </span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>

<?php } ?>
