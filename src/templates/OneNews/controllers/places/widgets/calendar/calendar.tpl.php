<?php
    $this->addCSS($this->getStylesFilename('places', 'widgets'));
    $this->addJS('templates/default/js/maps/calendar.js');
?>
<div class="widget_places_calendar maps_widget">
    <?php if ($is_show_month) { ?>
        <div class="month_name">
            <?php if ($is_nav){ ?><a class="month-nav prev-month" href="#"><i class="glyphicon glyphicon-chevron-left btn btn-link btn-sm"></i></a><?php } ?>
            <span><?php echo $date_text; ?></span>
            <?php if ($is_nav){ ?><a class="month-nav next-month" href="#"><i class="glyphicon glyphicon-chevron-right btn btn-link btn-sm"></i></a><?php } ?>
        </div>
    <?php } ?>
    <div class="days">
        <?php echo $html; ?>
    </div>
</div>

<script>
    $(document).ready(function(){
        icms.mapsCalendar.init({
            month: <?php echo (int)$month; ?>,
            year: <?php echo (int)$year; ?>,
            city_id: <?php echo (int)$city_id; ?>,
            url: '<?php echo href_to('places', 'events_calendar'); ?>',
        });
    });
</script>