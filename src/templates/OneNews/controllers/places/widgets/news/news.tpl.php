<?php $this->addCSS( $this->getStylesFilename('places', 'widgets') ); ?>
<?php if ($entries){ ?>

    <div class="widget_places_news maps_widget">
        <?php foreach($entries as $entry) { ?>
            <?php 
                $url = href_to($ctype_name, 'news', array($entry['item_id'], $entry['id']));
                $is_image = $is_images && !empty($entry['image']);
            ?>
            <div class="item media media-white<?php if ($is_image) { ?> is-image<?php } ?>">
                <?php if ($is_image) { ?>
                    <a class="media-left image-64" href="<?php echo $url; ?>">
                        <?php echo html_image($entry['image']); ?>
                    </a>
                <?php } ?>
                <div class="media-body">
                    <div class="title">
                        <a href="<?php echo $url; ?>"><?php echo $entry['title']; ?></a>
                    </div>
                    <?php if ($is_teaser && $entry['teaser']){ ?>
                        <div class="content"><?php echo $entry['teaser']; ?></div>
                    <?php } ?>
                    <?php if ($is_date || $is_user || $is_item) { ?>
                        <div class="details">
                            <?php if ($is_item) { ?>
                                <span class="addr">
                                    <a href="<?php echo href_to($ctype_name, $entry['item_slug'].'.html'); ?>"><?php html($entry['item_title']); ?></a>
                                </span>
                            <?php } ?>
                            <?php if ($is_user) { ?>
                                <span class="author">
                                    <span class="glyphicon glyphicon-user"></span><a href="<?php echo href_to('users', $entry['user']['id']); ?>"><?php html($entry['user']['nickname']); ?></a>
                                </span>
                            <?php } ?>
                            <?php if ($is_date) { ?>
                                <span class="date text-muted">
                                    <span class="glyphicon glyphicon-calendar"></span><?php echo html_date($entry['date_pub']); ?>
                                </span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>

<?php } ?>
