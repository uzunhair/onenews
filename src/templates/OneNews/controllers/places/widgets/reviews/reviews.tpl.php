<?php $this->addCSS( $this->getStylesFilename('places', 'widgets') ); ?>
<?php if ($reviews){ ?>

    <div class="widget_places_reviews maps_widget">
        <?php foreach($reviews as $review) { ?>
            <?php 
                $url = href_to($ctype_name, 'review', $review['id']); 
                $title = $review['item_title'] . ($is_addr ? ' &mdash; ' . $review['addrs'][$review['marker_id']] : '');
                $content = $strip ? string_short($review['content_html'], $strip) : false;
            ?>
            <div class="item">
                <div class="title">
                    <a href="<?php echo $url; ?>"><?php echo $title; ?></a>
                    <?php if ($is_rating) { ?>
                        <?php $this->renderControllerChild('places', 'stars', array('value' => $review['score_avg'], 'show_value'=>false, 'class'=>'small')); ?>	
                    <?php } ?>
                </div>
                <?php if ($content){ ?>
                    <div class="content"><?php echo $content; ?> ...</div>
                <?php } ?>
                <?php if ($is_date || $is_user) { ?>
                    <div class="details">
                        <?php if ($is_user) { ?>
                            <span class="author">
                                <a href="<?php echo href_to('users', $review['user']['id']); ?>"><?php html($review['user']['nickname']); ?></a>
                            </span>            
                        <?php } ?>
                        <?php if ($is_date) { ?>
                            <span class="date">
                                <?php html(string_date_age_max($review['date_pub'], true)); ?>
                            </span>  
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

<?php } ?>
