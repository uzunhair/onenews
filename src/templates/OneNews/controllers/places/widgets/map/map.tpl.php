<?php

    $this->addCSS($this->getStylesFileName('places'));
    $this->addCSS($this->getStylesFilename('places', 'widgets'));

    $this->addJS('templates/default/js/maps/systems/'.$options['system'].'.js');
	$this->addJS('templates/default/js/maps/list.js');

	$city = $options['is_city_filter'] ? cmsUser::sessionGet('maps-city') : false;
	$city_name = $city ? $city['name'] : LANG_PLACES_CITY_SELECT;
	$city_full = $city ? implode(', ', array($city['country'], $city['region'], $city['name'])) : false;
	$center = empty($city['lat']) ? explode(':', $options['center']) : array($city['lat'], $city['lng']);

    $map_filter = array();
	if (!empty($category_id)) { $map_filter['category_id'] = $category_id;	}
	if (!empty($dataset)) { $map_filter['dataset'] = $dataset;	}
	if ($city) { $map_filter['city_id'] = $city['id']; }

	$map_filter = http_build_query($map_filter);

?>
<div class="widget_places_map maps_widget">

    <div id="maps-map-block" class="maps-map-widget">
        <?php if ($options['is_city_filter']) { ?>
            <div id="maps-city-selector">
                <a class="ajaxlink ajax-modal" href="<?php echo href_to('places', 'city_select'); ?>" title="<?php echo LANG_PLACES_CITY_SELECT; ?>"><?php echo $city_name; ?></a>
            </div>
        <?php } ?>
        <div id="map-canvas" style="width: <?php echo $width; ?>;height: <?php echo $height; ?>"></div>
    </div>

    <script>
        icms.mapsList.init('map-canvas', {
            zoom: <?php echo $options['zoom'] + 1; ?>,
            min_zoom: <?php echo $options['min_zoom'] + 1; ?>,
            max_zoom: <?php echo $options['max_zoom'] + 1; ?>,
            center: [<?php echo nf($center[0], 4); ?>, <?php echo nf($center[1], 4); ?>],
            city_addr: <?php echo $city_full && empty($city['lat']) ? "'{$city_full}'" : 'false'; ?>,
            city_id: <?php echo $city ? $city['id'] : 'false'; ?>,
            map_type: '<?php echo $options['map_type']; ?>',
            map_type_select: <?php echo $options['map_type_select'] ? 'true' : 'false'; ?>,
            scroll_zoom: <?php echo $options['scroll_zoom'] ? 'true' : 'false'; ?>,
            bounds: <?php echo $options['mk_bounds'] ? 'true' : 'false'; ?>,
            filter: '<?php echo $map_filter; ?>',
            load_url: '<?php echo href_to('places', 'markers'); ?>',
            icons_url: '<?php echo cmsConfig::get('upload_host') . '/markers'; ?>',
            balloon_url: '<?php echo href_to('places', 'balloon'); ?>',
            city_save_url: '<?php echo href_to('places', 'city_save'); ?>',
            delay: '<?php echo $options['mk_delay']; ?>'
        });
    </script>

</div>