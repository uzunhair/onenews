<?php $this->addMainCSS("templates/{$this->name}/fonts/fontello/css/fontello.css"); ?>

<?php if ($contacts){ ?>
	<ul class="list-null maps-contacts-list">
		<?php if (!empty($contacts['phone'])) { ?>
			<li class="phone">
			<a href="tel:<?php html($contacts['phone']); ?>" itemprop="telephone">
                <span>
				    <span class="glyphicon glyphicon-earphone"></span>
                    Телефон:
                </span>
				<?php html($contacts['phone']); ?>
			</a>
			</li>
		<?php } ?>
		<?php if (!empty($contacts['skype'])) { ?>
			<li class="skype">
                <a href="skype:<?php html($contacts['skype']); ?>?chat">
                    <span>
                        <span class="fontello icon-skype"></span>
                        Skype:
                    </span>
                    <?php html($contacts['skype']); ?>
                </a>
			</li>
		<?php } ?>								
		<?php if (!empty($contacts['url'])) { ?>
			<?php $href = $contacts['url']; ?>
			<?php $href = strstr($href, 'http://') ? $href : 'http://' . $href; ?>
			<li class="url">
				<a href="<?php echo $href; ?>" target="_blank" itemprop="url">
                    <span>
                        <span class="glyphicon glyphicon-globe"></span>
                        Сайт:
                    </span>
					<?php html($contacts['url']); ?>
				</a>
			</li>
		<?php } ?>
		<?php if (!empty($contacts['email'])) { ?>
			<?php $href = 'mailto:'.$contacts['email']; ?>											
			<li class="email">
				<a href="<?php echo $href; ?>" target="_blank" itemprop="email">
                    <span>
                        <span class="fontello icon-at"></span>
                        E-mail:
                    </span>
					<?php html($contacts['email']); ?>
				</a>
			</li>											
		<?php } ?>											
	</ul>
<?php } ?>
