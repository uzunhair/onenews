<?php 

	$pagetitle = sprintf(LANG_PLACES_REVIEW_VIEW, $review['id']);

	$this->renderChild('item_header', array(
		'item' => $item,
		'ctype' => $ctype,
		'is_only_breadcrumb' => true
	));

	$this->addBreadcrumb(LANG_PLACES_ITEM_TAB_REVIEWS, $this->href_to('reviews', $item['id']));
	$this->addBreadcrumb($pagetitle);
	
	$this->setPageTitle($pagetitle);
	
	$url = href_to_abs($ctype['name'], $item['slug'] . '.html#marker-' . $review['marker_id'] );
	
?>

<h1><?php echo $pagetitle; ?></h1>

<div id="maps-review-view">
	
	<div class="review-target gui-panel">
		<div class="maps-balloon media">
			<?php if (!empty($item['photo'])){ ?>
				<a href="<?php echo $url; ?>" class="media-left">
					<?php echo html_image($item['photo'], 'small'); ?>
				</a>
			<?php } ?>
			<div class="media-body">
				<a href="<?php echo $url; ?>" class="media-heading font-s16">
					<?php html($item['title']); ?>
				</a>
				<div class="maps_list_addrs addrs margin-b5">
					<span class="glyphicon glyphicon-map-marker"></span>  
					<?php html($item['addrs'][$review['marker_id']]); ?>
				</div>
				<?php if ($marker['contacts']){ ?>
					<div class="contacts">							
						<?php $this->renderChild('contacts', array('contacts'=>$marker['contacts'])); ?>							
					</div>		
				<?php } ?>
			</div>
		</div>
	</div>
	
	<?php 
		$this->renderChild('review', array(
			'item' => $item,
			'ctype' => $ctype,
			'reviews' => array($review),
			'criteria' => $criteria, 
			'is_crop' => false,
			'is_answer_link' => false,
			'is_addr' => false,
		));
	?>
	
	<?php if ($is_can_answer) { ?>
		<?php $this->renderForm($form, $review, array(
			'action' => '',
			'method' => 'post'
		), $errors); ?>	
	<?php } ?>
</div>