<div id="maps-city-selector-window">

	<div class="list maps-city-list">
		<?php if ($cities) { ?>
			<?php foreach($cities as $city){ ?>
				<a class="item" href="<?php echo $this->href_to('city_set', $city['id']); ?>">
					<div class="name"><?php echo $city['name']; ?></div>
					<div class="details">
						<span class="country"><?php echo $city['country']; ?></span>, 
						<span class="region"><?php echo $city['region']; ?></span>
					</div>
				</a>
			<?php } ?>
		<?php } ?>
	</div>
		
</div>
