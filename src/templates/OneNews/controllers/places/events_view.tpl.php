<?php

	$user = cmsUser::getInstance();

	$this->renderChild('item_header', array(
		'item' => $item,
		'ctype' => $ctype,
		'is_only_breadcrumb' => true
	));

	$this->addBreadcrumb(LANG_PLACES_ITEM_TAB_EVENTS, $this->href_to('events', $item['id']));
	$this->addBreadcrumb($event['title']);

	$this->setPageTitle($event['title']);

	$is_own_place = $item['user_id'] == $user->id;
	$is_own_entry = $event['user_id'] == $user->id;

	$is_can_edit = cmsUser::isAllowed($ctype['name'], 'edit_events', 'all') ||
				   (cmsUser::isAllowed($ctype['name'], 'edit_events', 'own_places') && $is_own_place) ||
				   (cmsUser::isAllowed($ctype['name'], 'edit_events', 'own') && $is_own_entry);

	$is_can_delete = cmsUser::isAllowed($ctype['name'], 'delete_events', 'all') ||
				   (cmsUser::isAllowed($ctype['name'], 'delete_events', 'own_places') && $is_own_place) ||
				   (cmsUser::isAllowed($ctype['name'], 'delete_events', 'own') && $is_own_entry);

	if ($is_can_edit){
		$this->addToolButton(array(
			'class' => 'edit',
			'title' => LANG_PLACES_EVENTS_EDIT,
			'href'  => $this->href_to('edit_event', $event['id'])
		));
	}

	if ($is_can_delete){
		$this->addToolButton(array(
			'class' => 'delete',
			'title' => LANG_PLACES_EVENTS_DELETE,
			'href'  => $this->href_to('delete_event', $event['id']),
			'onclick' => "if(!confirm('".LANG_PLACES_EVENTS_DELETE_CONFIRM."')){ return false; }"
		));
	}

    if (!empty($event['seo_title'])){ $this->setPageTitle($event['seo_title']); }
    if (!empty($event['seo_keys'])){ $this->setPageKeywords($event['seo_keys']); }
    if (!empty($event['seo_desc'])){ $this->setPageDescription($event['seo_desc']); }

?>

<h1><?php html($event['title']); ?></h1>

<div class="maps-news-entry-full">	
	<div class="body">			
		<div class="content clearfix">
			<?php if ($event['image']) { ?>
				<div class="cont_item_img_left">
					<a href="<?php echo html_image_src($event['image'], 'big', true); ?>" class="ajax-modal">			
						<?php echo html_image($event['image'], 'normal'); ?>			
					</a>
				</div>		
			<?php } ?>				
			<?php echo $event['content']; ?>
			<hr class="margin-t5 margin-b5">
		<div class="event-date">
			<strong><?php echo LANG_PLACES_EVENTS_START; ?>:	</strong>			
			<span><?php echo html_date_time($event['date_start']); ?></span>
			<br>
			<?php if ($event['date_end']) { ?>
				<strong><?php echo LANG_PLACES_EVENTS_END; ?>:		</strong>		
				<span><?php echo html_date($event['date_end']); ?></span>
				<br>				
			<?php } ?>

			<?php if ($event['price']) { ?>
				<strong><?php echo LANG_PLACES_EVENTS_COST; ?>:		</strong>		
				<span><?php echo LANG_FROM . ' ' . $event['price'] . ' ' . $this->controller->options['events_currency']; ?></span>				
			<?php } ?>
		</div>	

		</div>		

	</div>				
</div>

<div class="news-target news-target margin-b15 margin-t5">		
	<?php $url = href_to_abs($ctype['name'], $item['slug'] . '.html#marker-' . $event['marker_id'] );	?>
	<div class="maps-balloon media">
		<?php if (!empty($item['photo'])){ ?>
            <a class="media-left image-64" href="<?php echo $url; ?>">
                <?php echo html_image($item['photo'], 'small'); ?>
            </a>
		<?php } ?>
		<div class="media-body">
            <div class="d-flex">
                <a class="margin-r5" href="<?php echo $url; ?>"><?php html($item['title']); ?></a>
                <div class="maps_list_addrs addrs">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <?php html($marker['address']); ?>
                </div>
            </div>
			<?php if ($marker['contacts']){ ?>
				<div class="contacts margin-t5">
					<?php $this->renderChild('contacts', array('contacts'=>$marker['contacts'])); ?>						
				</div>		
			<?php } ?>
		</div>
	</div>
</div>

<ul class="info_bar margin-t10">
	<li class="date">
		<span class="glyphicon glyphicon-calendar"></span> 
		<?php echo html_date_time($event['date_pub']); ?>
	</li>
	<li> <span class="glyphicon glyphicon-user margin-r5"></span>
		<a href="<?php echo href_to('users', $event['user_id']); ?>">
			<?php echo $event['user']['nickname']; ?>
		</a> 		
	</li>
    <?php if (!empty($this->options['share_link'])){ ?>
    <li class="bar_item bi_share">
        <div class="share dark-share">
           <?php echo $this->options['share_link']; ?>
        </div>
    </li>
    <?php } ?>
</ul>
<?php if ($comments_widget) { echo $comments_widget; } ?>
