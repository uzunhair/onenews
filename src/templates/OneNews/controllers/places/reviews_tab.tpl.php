<?php 

	$user = cmsUser::getInstance();
	$this->addJS('templates/default/js/maps/reviews.js');
		
?>

<div id="maps-reviews-list" class="maps-entries-list">

	<?php if ($reviews) { ?>

		<div class="filter-panel gui-panel form-inline">
			<div class="form-group">
				<label class="mr-10"><?php echo LANG_SORTING; ?></label>
				<?php echo html_select('sort', $sortings, $sort); ?>
			</div>
		</div>	
	
		<?php $this->renderChild('review', array(
			'reviews' => $reviews,
			'item' => false,
			'ctype' => $ctype,
			'criteria' => false,
			'is_profile' => true,
			'is_crop' => false,
			'is_ratings' => $this->controller->options['reviews_rating']
		)); ?>			
	
		<script><?php echo $this->getLangJS('LANG_PLACES_REVIEW_DELETE_CONFIRM'); ?></script>
	
	<?php } ?>
	
	<?php if (!$reviews) { ?>
		<p>
			<?php echo LANG_PLACES_REVIEWS_PROFILE_NONE; ?> 
		</p>
	<?php } ?>
	
</div>

<?php if($reviews && ($total > $perpage)) { ?>
	<?php 	
		$query = array();		
		if ($sort != 'date-desc') { $query['sort'] = $sort; }
		echo html_pagebar($page, $perpage, $total, false, $query); 
	?>
<?php } ?>
