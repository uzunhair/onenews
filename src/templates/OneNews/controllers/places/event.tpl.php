<?php 
	$user = cmsUser::getInstance();
	if (!isset($is_item_link)) { $is_item_link = false; }
?>

<?php foreach($events as $event){ ?>

	<?php $url = $this->href_to('events', array($event['item_id'], $event['id'])); ?>

	<div class="media media-white maps-news-entry">
        <a class="media-left" href="<?php echo $url; ?>">
            <?php if ($event['image']) { ?>
                <?php echo html_image($event['image']); ?>
            <?php } else { ?>
                <img src="/upload/f_project/no_img_64.jpg" alt="">
            <?php } ?>
        </a>
		<div class="media-body">
			<div class="title">
				<a class="font-s16" href="<?php echo $url; ?>"><?php html($event['title']); ?></a>
			</div>
			<div class="event-teaser">
				<?php html($event['teaser']); ?>
			</div>	
			<ul class="list-inline list-event-date">
				<li>
					<?php echo LANG_PLACES_EVENTS_START; ?>:
                </li>
                <li>
					<span class="glyphicon glyphicon-calendar margin-r5"></span>
					<span><?php echo html_date_time($event['date_start']); ?></span>
				</li>

				<?php if ($event['is_long'] && $event['date_end']) { ?>
					<li>
						<?php echo LANG_PLACES_EVENTS_END; ?>:
                    </li>
                    <li>
						<span aria-hidden="true" class="glyphicon glyphicon-calendar margin-r5"></span>
						<span><?php echo html_date($event['date_end']); ?></span>
					</li>				
				<?php } ?>

				<?php if ($event['price']) { ?>
					<li>
						<?php echo LANG_PLACES_EVENTS_COST; ?>:	
						<span class="glyphicon glyphicon-ruble"></span>			
						<span><?php echo LANG_FROM . ' ' . $event['price'] . ' ' . $this->controller->options['events_currency']; ?></span>	
						</li>			
				<?php } ?>
			</ul>	
			<ul class="list-inline list-event-date">						
				<?php if ($is_item_link){ ?>
					<?php $item_url = href_to($ctype['name'], $event['item_slug'] . '.html'); ?>
					<li class="addr">
						<span class="glyphicon glyphicon-map-marker margin-r5"></span>
						<a href="<?php echo $item_url; ?>"><?php echo $event['item_title']; ?></a>
					</li>						
				<?php } ?>                
                <?php if (count($event['addrs']) && $event['marker_id']) { ?>	
                <li>
                	<span class="glyphicon glyphicon-map-marker margin-r5"></span>
                    <span class="addr margin-r5"><?php echo $event['addrs'][$event['marker_id']]; ?></span>
                  </li>
                <?php } ?>		
				<li>
					<span class="glyphicon glyphicon-user margin-r5"></span> <a href="<?php echo href_to('users', $event['user_id']); ?>" class="user"><?php echo $event['user']['nickname']; ?></a>
				</li>		
			</ul>
		</div>				
	</div>

<?php } ?>		
