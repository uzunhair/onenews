<div id="maps-coordinates-form">
	
	<div class="address">
		<input placeholder="<?php echo LANG_PLACES_COORD_FIND; ?>" value="" class="input">
	</div>
	
	<div id="map-canvas"></div>
	
	<div class="buttons">
		<input type="button" value="<?php echo LANG_SAVE; ?> test" class="btn btn-primary">
	</div>
	
</div>

<script>
	
	icms.map.createPositionMap("map-canvas", {
		center: [<?php echo nf($lat, 4); ?>, <?php echo nf($lng, 4); ?>], 
		map_type: 'map',
		zoom: <?php if ($lat) { ?>15<?php } else { ?>5<?php } ?>,
	}, setCoordinates);

    function setCoordinates(lat, lng){
		lat = lat.toFixed(6);
		lng = lng.toFixed(6);
		var value = lat+':'+lng;
		var text = lat+', '+lng;
		$('#f_<?php echo $field_id; ?> input.coordinates').val(value);
		$('#f_<?php echo $field_id; ?> span.coordinates-text').show().html(text);
	}

	$('#maps-coordinates-form .address .input').on('keyup', function(e){		
		if (e.keyCode == 13){
			$(this).prop('disabled', true);
            var value = $(this).val();
            var address = {getString: function(){
               return value;     
            }};
			icms.map.getAddressCoordinates(address, function(lat, lng){
				icms.map.setPositionMapCenter([lat, lng]);
                setCoordinates(lat, lng);
				$('#maps-coordinates-form .address .input').prop('disabled', false).focus();
			}, function(error){
				if (error) { alert(error); }
				$('#maps-coordinates-form .address .input').prop('disabled', false).focus();
				return false;
			})
		}
	});
	
	$('#maps-coordinates-form .buttons .btn.btn-primary').on('click', function(){
		icms.modal.close();
	});
    
    if (typeof(google) != 'undefined'){
        google.maps.event.trigger(window, 'load');
    }
	
</script>
