<?php

	$pagetitle = $do=='add' ? LANG_PLACES_REVIEW_ADD : LANG_PLACES_REVIEW_EDIT;

	$this->renderChild('item_header', array(
		'item' => $item,
		'ctype' => $ctype,
		'is_only_breadcrumb' => true
	));

	$this->addBreadcrumb($pagetitle);

	$this->setPageTitle($pagetitle);

?>

<h1><?php echo $pagetitle; ?>: <span><?php echo $item['title']; ?></span></h1>

<?php
	$this->renderForm($form, $review, array(
		'action' => '',
		'method' => 'post',
        'append_html' => $captcha_html
	), $errors);
