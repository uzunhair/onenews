<?php

	$user = cmsUser::getInstance();

    $this->addJS('templates/default/js/jquery-ui.js');
	$this->addJS('templates/OneNews/js/maps/owner.js');

	$this->addCSS('templates/default/css/jquery-ui.css');

	$this->renderChild('item_header', array(
		'item' => $item,
		'ctype' => $ctype,
		'is_only_breadcrumb' => true
	));

	$this->addBreadcrumb(LANG_PLACES_OWNER);
	$this->setPageTitle(LANG_PLACES_CHANGE_OWNER, $item['title']);

?>
<h1><?php echo LANG_PLACES_CHANGE_OWNER; ?></h1>

<div id="maps-place-owner">

	<form action="" method="post">

		<div class="find">
			<div class="field form-inline">
				<input type="hidden" value="<?php echo $owner['id']; ?>" id="user-id" name="new_owner_id">
				<?php echo html_input('text', 'username', '', array('id'=>'username', 'autocomplete'=>'off', 'placeholder'=>LANG_PLACES_OWNER_SELECT)); ?>
				<?php echo html_button(LANG_SELECT, 'add', 'return icms.adminModerators.add()', array('id'=>'submit', 'class'=>'button-small')); ?>
			</div>
			<div class="loading-icon" style="display: none"></div>
		</div>

		<div class="owner old-owner margin-b15">
			<h3><?php echo LANG_PLACES_OWNER_OLD; ?></h3>
			<div class="user clearfix ">
				<div class="avatar"><?php echo html_avatar_image($owner['avatar'], 'micro'); ?></div>
				<div class="name"><a href="<?php echo href_to('users', $owner['id']); ?>"><?php echo $owner['nickname']; ?></a></div>
			</div>
		</div>

		<div class="owner gui-panel new-owner" style="display: none">
			<h3><?php echo LANG_PLACES_OWNER_NEW; ?></h3>
			<div class="user">
				<div class="avatar"></div>
				<div class="name"></div>
			</div>
		</div>

		<div class="buttons">
			<?php echo html_submit(LANG_SAVE); ?>
		</div>

	</form>

</div>

<script>
	icms.mapsOwner.autocompleteURL = '<?php echo href_to('places', 'users'); ?>';
	icms.mapsOwner.submitURL = '<?php echo $this->href_to('get_owner'); ?>';
	icms.mapsOwner.init();
</script>