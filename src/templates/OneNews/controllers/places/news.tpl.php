<?php 

	$user = cmsUser::getInstance();

	$this->addJS('templates/default/js/maps/reviews.js');
		
	$is_can_add_news = $this->controller->options['news_on'] &&
						(
							cmsUser::isAllowed($ctype['name'], 'add_news', 'all_places') || 
							(cmsUser::isAllowed($ctype['name'], 'add_news', 'own_places') && $item['user_id'] == $user->id)
						) &&
						!cmsUser::isPermittedLimitReached($ctype['name'], 'max_news', $user_news_month_count);
	
	if ($is_can_add_news) {

		$this->addToolButton(array(
			'class' => 'add',
			'title' => LANG_PLACES_NEWS_ADD,
			'href'  => $this->href_to('add_news', $item['id'])
		));
		
	}	
	
	$this->renderChild('item_header', array(
		'item' => $item,
		'ctype' => $ctype,
		'menu_items' => $menu_items,
		'is_only_breadcrumb' => false
	));

	$this->addBreadcrumb(LANG_PLACES_ITEM_TAB_NEWS);
	$this->setPageTitle(LANG_PLACES_ITEM_TAB_NEWS, $item['title']);
		
?>

<div id="maps-news-list" class="maps-entries-list">

	<div class="filter-panel gui-panel form-inline">
		<?php if (count($item['addrs']) > 1) { ?>
		<div class="form-group">
			<label class="mr-10"><?php echo LANG_PLACES_ADDR; ?></label>
			<?php echo html_select('marker_id', array(0=>LANG_ALL) + $item['addrs'], $marker_id); ?>
		</div>
		<?php } ?>
		<div class="form-group">
			<label class="mr-10"><?php echo LANG_SORTING; ?></label>
			<?php echo html_select('sort', $sortings, $sort); ?>
		</div>
	</div>
	
	<?php if ($entries) { ?>
		
		<?php $this->renderChild('news_entry', array(
			'entries' => $entries,
			'item' => $item,
			'ctype' => $ctype,
		)); ?>			
	
	<?php } ?>
	
	<?php if (!$entries) { ?>
		<p>
			<?php echo LANG_PLACES_NEWS_NONE; ?> 
			<?php if ($is_can_add_news) { ?>
				<a href="<?php echo $this->href_to('add_news', $item['id']); ?>"><?php echo LANG_PLACES_NEWS_ADD; ?></a>
			<?php } ?>
		</p>
	<?php } ?>
	
</div>

<?php if($entries && ($total > $perpage)) { ?>
	<?php 	
		$query = array();		
		if ($marker_id) { $query['addr_id'] = $marker_id; }
		if ($sort != 'date-desc') { $query['sort'] = $sort; }
		echo html_pagebar($page, $perpage, $total, false, $query); 
	?>
<?php } ?>
