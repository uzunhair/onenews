<?php 

	$pagetitle = $h1title = $title;

	if ($cat_id > 1){
		$pagetitle = $title . ': ' . $category['title'];
		$h1title = $title . ': <span>' . $category['title'] . '</span>';
	}

	$this->addBreadcrumb($ctype['title'], href_to($ctype['name']));
	$this->addBreadcrumb($title, $this->href_to('news'));
	
	$this->setPageTitle($pagetitle);
	
	if (isset($category['path']) && $category['path']){
        foreach($category['path'] as $c){
            $this->addBreadcrumb($c['title'], $this->href_to('news'). '?cat_id=' . $c['id']);
        }
    }
	
	$city = $this->controller->options['is_city_filter'] ? cmsUser::sessionGet('maps-city') : false; 	
	$city_name = $city ? $city['name'] : LANG_PLACES_CITY_SELECT;
		
?>
<div class="white_del">
<h1>
    <?php echo $h1title; ?>
    <?php if ($this->controller->options['is_city_filter']) { ?>
        <div id="maps-list-city-selector">
            <a class="ajax-modal" href="<?php echo href_to('places', 'city_select'); ?>" title="<?php echo LANG_PLACES_CITY_SELECT; ?>"><?php echo $city_name; ?></a>
        </div>
    <?php } ?>    
</h1>

<?php if ($subcats && $ctype['is_cats'] && !empty($ctype['options']['is_show_cats'])){ ?>
    <div class="gui-panel clearfix content_categories<?php if (count($subcats)>8){ ?> categories_small<?php } ?>">
        <ul class="cont_cat flex">
            <?php foreach($subcats as $c){ ?>
                <li>
                    <a href="<?php echo $this->href_to('news') . '?cat_id=' . $c['id']; ?>"><?php echo $c['title']; ?></a>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>

<div id="maps-news-list" class="maps-entries-list">

	<?php if ($entries) { ?>
		
		<?php $this->renderChild('news_entry', array(
			'entries' => $entries,
			'ctype' => $ctype,
			'is_item_link' => true,
		)); ?>			
	
	<?php } ?>
	
	<?php if (!$entries) { ?>
		<p>
			<?php echo LANG_PLACES_NEWS_NONE; ?> 
		</p>
	<?php } ?>
	
</div>

<?php if($entries && ($total > $perpage)) { ?>
	<?php 	
		$query = ($cat_id > 1) ? array('cat_id'=>$cat_id) : false;
		echo html_pagebar($page, $perpage, $total, false, $query); 
	?>
<?php } ?>
</div>