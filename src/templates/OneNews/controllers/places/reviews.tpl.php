<?php 

	$user = cmsUser::getInstance();

	$this->addJS('templates/default/js/maps/reviews.js');

    if ($user->id){
        $is_can_add = $this->controller->options['reviews_on'] &&
                      cmsUser::isAllowed($ctype['name'], 'add_reviews') &&
                      !cmsUser::isPermittedLimitReached($ctype['name'], 'max_reviews', $user_reviews_count) &&
                      ($user->id != $item['user_id']);
    } else {
        $is_can_add = !empty($this->controller->options['reviews_guest']);
    }

	if ($is_can_add) {

		$add_href = $this->href_to('add_review', $item['id']);
		if ($marker_id) { $add_href .= "?addr_id={$marker_id}"; }

		$this->addToolButton(array(
			'class' => 'add',
			'title' => LANG_PLACES_REVIEW_ADD,
			'href'  => $add_href
		));

	}

	$this->renderChild('item_header', array(
		'item' => $item,
		'ctype' => $ctype,
		'menu_items' => $menu_items,
		'is_only_breadcrumb' => false
	));


	$this->addBreadcrumb(LANG_PLACES_ITEM_TAB_REVIEWS);
	$this->setPageTitle(LANG_PLACES_ITEM_TAB_REVIEWS, $item['title']);

?>

<div id="maps-reviews-list" class="maps-entries-list">

	<div class="filter-panel gui-panel form-inline">
		<?php if (count($item['addrs']) > 1) { ?>
		<div class="form-group">
			<label class="mr-10"><?php echo LANG_PLACES_ADDR; ?></label>
			<?php echo html_select('marker_id', array(0=>LANG_ALL) + $item['addrs'], $marker_id); ?>
		</div>
		<?php } ?>
		<div class="form-group">
			<label class="mr-10"><?php echo LANG_SORTING; ?></label>
			<?php echo html_select('sort', $sortings, $sort); ?>
		</div>
	</div>

	<?php if ($user_reviews_count) { ?>
		<div>
			<?php if (!$my){ ?>
				<span class="margin-r5"><?php printf(LANG_PLACES_REVIEWS_USER, html_spellcount($user_reviews_count, LANG_PLACES_REVIEW_SPELLCOUNT)); ?></span>
				<a href="<?php echo $this->href_to('reviews', $item['id']); ?>?my=1"><?php echo LANG_PLACES_REVIEWS_SHOW_MY; ?></a>
			<?php } else { ?>
				<span class="margin-r5"><?php echo LANG_PLACES_REVIEWS_ONLY_USER; ?></span>
				<a href="<?php echo $this->href_to('reviews', $item['id']); ?>"><?php echo LANG_PLACES_REVIEWS_SHOW_ALL; ?></a>
			<?php } ?>
		</div>
	<?php } ?>

	<?php if ($reviews) { ?>

		<?php $this->renderChild('review', array(
			'reviews' => $reviews,
			'item' => $item,
			'ctype' => $ctype,
			'criteria' => $criteria,
			'is_ratings' => $this->controller->options['reviews_rating']
		)); ?>

		<script><?php echo $this->getLangJS('LANG_PLACES_REVIEW_DELETE_CONFIRM'); ?></script>

	<?php } ?>


	<?php if (!$reviews) { ?>
		<p>
			<?php echo LANG_PLACES_REVIEW_NONE; ?>
			<?php if ($is_can_add) { ?>
				<a href="<?php echo $this->href_to('add_review', $item['id']); ?>"><?php echo LANG_PLACES_REVIEW_ADD; ?></a>
			<?php } ?>
		</p>
	<?php } ?>

</div>

<?php if($reviews && ($total > $perpage)) { ?>
	<?php
		$query = array();
		if ($my) { $query['my'] = $my; }
		if ($marker_id) { $query['addr_id'] = $marker_id; }
		if ($sort != 'date-desc') { $query['sort'] = $sort; }
		echo html_pagebar($page, $perpage, $total, false, $query);
	?>
<?php } ?>
