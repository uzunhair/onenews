<?php 

	$pagetitle = $do=='add' ? LANG_PLACES_NEWS_ADD : LANG_PLACES_NEWS_EDIT;

	$this->renderChild('item_header', array(
		'item' => $item,
		'ctype' => $ctype,
		'is_only_breadcrumb' => true
	));

	$this->addBreadcrumb($pagetitle);
	
	$this->setPageTitle($pagetitle);
	
?>

<h1><?php echo $pagetitle; ?></h1>

<?php 
	$this->renderForm($form, $entry, array(
		'action' => '',
		'method' => 'post'
	), $errors);
	