<?php
	$user = cmsUser::getInstance();
	if (!isset($is_crop)) { $is_crop = true; }
	if (!isset($is_answer_link)) { $is_answer_link = true; }
	if (!isset($is_answer)) { $is_answer = true; }
	if (!isset($is_addr)) { $is_addr = true; }
	if (!isset($is_profile)) { $is_profile = false; }
	if (!isset($is_ratings)) { $is_ratings = false; }
?>
<div class="maps-review">
<?php foreach($reviews as $review){ ?>

<?php

	$is_can_edit =	(cmsUser::isAllowed($ctype['name'], 'edit_reviews', 'all') ||
					(cmsUser::isAllowed($ctype['name'], 'edit_reviews', 'own') && $review['user_id'] == $user->id));
	$is_can_delete =	(cmsUser::isAllowed($ctype['name'], 'delete_reviews', 'all') ||
						(cmsUser::isAllowed($ctype['name'], 'delete_reviews', 'own') && $review['user_id'] == $user->id));
	$is_can_answer =	(cmsUser::isAllowed($ctype['name'], 'answer_reviews', 'all') ||
						(cmsUser::isAllowed($ctype['name'], 'answer_reviews', 'own_places') && $item['user_id'] == $user->id));

?>

<div class="media">
	<a class="pull-left avatar-wheel" href="<?php echo href_to('users', $review['user_id']); ?>">
		<?php echo html_avatar_image($review['user']['avatar'], 'micro'); ?>
	</a>
	<div class="comment-media-body">

		<div class="comment-head">
			<div class="clearfix">
				<a href="<?php echo href_to('users', $review['user_id']); ?>">
					<?php echo $review['user']['nickname']; ?>
				</a>							
				<span class="text-muted ml-10 mr-10 font-s12">
					<?php echo html_date_time($review['date_pub']); ?>
				</span>
				<span class="anchor">
					<a href="<?php echo $this->href_to('review', $review['id']); ?>">#</a> 
				</span>	
				
				<?php if ($review['date_last_mod']){ ?>
					<div class="pull-right text-muted date_pub ml-5">
						<?php printf(LANG_PLACES_REVIEW_CHANGED, html_date_time($review['date_last_mod'])); ?>
					</div>
				<?php } ?>
			<?php if (($is_can_answer && $is_answer_link) || $is_can_edit || $is_can_delete){ ?>
			<div class="pull-right">
				<?php if ($is_can_answer && $is_answer_link){ ?>
					<?php $reply = $review['is_answered'] ? LANG_PLACES_REVIEW_REPLY_CHANGE : LANG_PLACES_REVIEW_REPLY; ?>
					<a class="answer" data-toggle="tooltip" data-placement="bottom" href="<?php echo $this->href_to('review', $review['id']); ?>#reply" title="<?php echo $reply; ?>"><span class="glyphicon glyphicon-share-alt"></span></a>
				<?php } ?>
				<?php if ($is_can_edit){ ?>
					<a class="edit" data-toggle="tooltip" data-placement="bottom" href="<?php echo $this->href_to('edit_review', $review['id']); ?>" title="<?php echo LANG_EDIT; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
				<?php } ?>
				<?php if ($is_can_delete){ ?>
					<a class="delete"  data-toggle="tooltip" data-placement="bottom" href="<?php echo $this->href_to('delete_review', $review['id']); ?>" title="<?php echo LANG_DELETE; ?>"><span class="glyphicon glyphicon-remove"></span></a>
				<?php } ?>
			</div>
		<?php } ?>	
			</div>
		</div>
		<div class="comment-text img-responsive-div">
		<?php if ($is_addr && (count($review['addrs']) > 1 || $is_profile)) { ?>		
		<div class="addr">
			<?php if ($is_profile){ ?>
				<span><a href="<?php echo href_to($ctype['name'], $review['item_slug'] . '.html'); ?>"><?php echo $review['item_title']; ?></a> &rarr; </span>
			<?php } ?>
			<span><?php echo $review['addrs'][$review['marker_id']]; ?></span>
		</div>
		<?php } ?>
		<?php if ($is_ratings){ ?>
			<?php if (!$is_profile && $review['score_avg'] > 0) { ?>
				<div class="ratings ratings-review">
					<?php for($r=1; $r<=3; $r++) { ?>
						<?php if ($review['score'.$r] == 0) { continue; } ?>
						<div class="score">
							<div class="legend"><?php echo $criteria[$r]; ?></div>
							<?php $this->renderChild('stars_glyphicon', array('value' => $review['score'.$r], 'show_value'=>false)); ?>

						</div>
					<?php } ?>
				</div>
			<?php } ?>			
			<?php if ($is_profile && $review['score_avg'] > 0) { ?>
				<div class="ratings">
					<div class="score">
						<div class="legend"><?php echo LANG_PLACES_RATING_SCORE; ?></div>
						<?php $this->renderChild('stars', array('value' => $review['score_avg'], 'show_value'=>true)); ?>	
					</div>
				</div>
			<?php } ?>			
		<?php } ?>			
		<?php 
			$max_len = 600;
			$is_crop_now = $is_crop && (mb_strlen($review['content_html']) > $max_len);						
		?>		
		<div class="content<?php if ($is_crop_now) { ?> cropped<?php } ?>">
			<div class="text">
				<?php echo $review['content_html']; ?>
				<?php if ($review['answer'] && ($is_answer_link || !$is_can_answer)){ ?>
				<div class="answer">
					<div class="title"><?php echo LANG_PLACES_REVIEW_ANSWER; ?> <span class="date"><?php echo html_date_time($review['date_answered']); ?></span></div>
					<div class="body"><?php echo $review['answer_html']; ?></div>
				</div>
				<?php } ?>
				<?php if ($is_crop_now) { ?><div class="cropper"></div><?php } ?>
			</div>
			<?php if ($is_crop_now) { ?>
				<div class="expand">
					<a href="#expand" class="ajaxlink"><?php echo LANG_PLACES_REVIEW_EXPAND; ?></a>
				</div>
			<?php } ?>
		</div>		
		</div>
	</div>				
</div>

<?php } ?>		
</div>