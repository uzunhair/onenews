<?php

    $this->setPageTitle($item['title']);

    if (!empty($ctype['seo_keys'])){ $this->setPageKeywords($ctype['seo_keys']); }
    if (!empty($ctype['seo_desc'])){ $this->setPageDescription($ctype['seo_desc']); }
    if (!empty($item['seo_keys'])){ $this->setPageKeywords($item['seo_keys']); }
    if (!empty($item['seo_desc'])){ $this->setPageDescription($item['seo_desc']); }

    $base_url = $ctype['name'];

    if ($ctype['options']['list_on']){
        $list_header = empty($ctype['labels']['list']) ? $ctype['title'] : $ctype['labels']['list'];
        $this->addBreadcrumb($list_header, href_to($base_url));
    }

    if (isset($item['category'])){
        foreach($item['category']['path'] as $c){
            $this->addBreadcrumb($c['title'], href_to($base_url, $c['slug']));
        }
    }

    $this->addBreadcrumb($item['title'], $this->href_to($item['slug'] . '.html'));    

	if ($is_only_breadcrumb) { return; }
	
	if (isset($menu_items)) { $this->addMenuItems('item-menu', $menu_items); }
	
?>

<h1>
	<?php if ($item['parent_id']){ ?>
		<div class="parent_title">
			<a href="<?php echo href_to($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a> &rarr;
		</div>
	<?php } ?>
	<?php html($item['title']); ?>
	<?php if ($item['is_private']) { ?>
		<span class="is_private" title="<?php html(LANG_PRIVACY_PRIVATE); ?>"></span>
	<?php } ?>
</h1>

<?php if (isset($menu_items)){ ?>
	<div id="maps-item-menu">
		<div class="tabs-menu">
			<?php $this->menu('item-menu', true, 'nav nav-tabs'); ?>
		</div>
	</div>
<?php } ?>
