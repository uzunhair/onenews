<?php
	$user = cmsUser::getInstance();
	if (!isset($is_item_link)) { $is_item_link = false; }
?>

<?php foreach($entries as $entry){ ?>

	<?php $url = $this->href_to('news', array($entry['item_id'], $entry['id'])); ?>

	<div class="media maps-news-entry">
		<a class="media-left image-64" href="<?php echo $url; ?>">
			<?php if ($entry['image']) { ?>
				<?php echo html_image($entry['image']); ?>
			<?php } else { ?>
				<img src="/upload/f_project/no_img_64.jpg" alt="">
			<?php } ?>
		</a>
		<div class="media-body">
			<div class="title">
			<a class="font-s16" href="<?php echo $url; ?>"><?php html($entry['title']); ?></a>
			</div>
			<div class="teaser">
				<?php html($entry['teaser']); ?>
			</div>
			<ul class="list-inline maps_list_addrs">
				<li class="date"><span class="glyphicon glyphicon-calendar"></span> <?php echo html_date_time($entry['date_pub']); ?></li>
				<?php if (!$is_item_link){ ?>
					<?php if (count($entry['addrs']) && $entry['marker_id']) { ?>
						<li class="addr"><span class="glyphicon glyphicon-map-marker margin-r5"></span> <?php echo $entry['addrs'][$entry['marker_id']]; ?></li>
					<?php } ?>
				<?php } else { ?>
					<?php $item_url = href_to($ctype['name'], $entry['item_slug'] . '.html'); ?>
					<li class="addr">
						<a href="<?php echo $item_url; ?>"><span class="glyphicon glyphicon-map-marker margin-r5"></span> <?php echo $entry['item_title']; ?></a>
					</li>
				<?php } ?>
				<li>
					<span class="glyphicon glyphicon-user"></span> <a href="<?php echo href_to('users', $entry['user_id']); ?>" class="user"><?php echo $entry['user']['nickname']; ?></a>
				</li>
			</ul>
		</div>
	</div>

<?php } ?>
