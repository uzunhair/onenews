<?php 

	$user = cmsUser::getInstance();

	$this->addJS('templates/default/js/maps/events.js');
		
	$is_can_add_events = $this->controller->options['events_on'] &&
						(
							cmsUser::isAllowed($ctype['name'], 'add_events', 'all_places') || 
							(cmsUser::isAllowed($ctype['name'], 'add_events', 'own_places') && $item['user_id'] == $user->id)
						) &&
						!cmsUser::isPermittedLimitReached($ctype['name'], 'max_events', $user_events_month_count);
	
	if ($is_can_add_events) {

		$this->addToolButton(array(
			'class' => 'add',
			'title' => LANG_PLACES_EVENTS_ADD,
			'href'  => $this->href_to('add_event', $item['id'])
		));
		
	}	
	
	$this->renderChild('item_header', array(
		'item' => $item,
		'ctype' => $ctype,
		'menu_items' => $menu_items,
		'is_only_breadcrumb' => false
	));

	$this->addBreadcrumb(LANG_PLACES_ITEM_TAB_EVENTS);
	$this->setPageTitle(LANG_PLACES_ITEM_TAB_EVENTS, $item['title']);
		
?>

<div id="maps-events-list" class="maps-entries-list">

	<div class="filter-panel gui-panel form-inline">
		<?php if (count($item['addrs']) > 1) { ?>
            <div class="form-group addr">
            	<label class="mr-10"><?php echo LANG_PLACES_ADDR; ?></label>
                 <?php echo html_select('marker_id', array(0=>LANG_ALL) + $item['addrs'], $marker_id); ?>
            </div>
		<?php } ?>
		<div class="form-group sort">
			<label class="mr-10"><?php echo LANG_PLACES_EVENTS_SHOW; ?></label>
			<?php echo html_select('period', $periods, $period); ?>
		</div>
	</div>
	
	<?php if ($events) { ?>
		
		<?php $this->renderChild('event', array(
			'events' => $events,
			'item' => $item,
			'ctype' => $ctype,
		)); ?>			
	
	<?php } ?>
	
	<?php if (!$events) { ?>
		<p>
			<?php echo LANG_PLACES_EVENTS_NONE; ?> 
			<?php if ($is_can_add_events) { ?>
				<a href="<?php echo $this->href_to('add_event', $item['id']); ?>"><?php echo LANG_PLACES_EVENTS_ADD; ?></a>
			<?php } ?>
		</p>
	<?php } ?>
	
</div>

<?php if($events && ($total > $perpage)) { ?>
	<?php 	
		$query = array();		
		if ($marker_id) { $query['addr_id'] = $marker_id; }
		if ($period != 'near') { $query['period'] = $period; }
		echo html_pagebar($page, $perpage, $total, false, $query); 
	?>
<?php } ?>
