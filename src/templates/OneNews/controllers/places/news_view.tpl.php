<?php

	$user = cmsUser::getInstance();

	$this->renderChild('item_header', array(
		'item' => $item,
		'ctype' => $ctype,
		'is_only_breadcrumb' => true
	));

	$this->addBreadcrumb(LANG_PLACES_ITEM_TAB_NEWS, $this->href_to('news', $item['id']));
	$this->addBreadcrumb($entry['title']);

	$this->setPageTitle($entry['title']);

	$is_own_place = $item['user_id'] == $user->id;
	$is_own_entry = $entry['user_id'] == $user->id;

	$is_can_edit = cmsUser::isAllowed($ctype['name'], 'edit_news', 'all') ||
				   (cmsUser::isAllowed($ctype['name'], 'edit_news', 'own_places') && $is_own_place) ||
				   (cmsUser::isAllowed($ctype['name'], 'edit_news', 'own') && $is_own_entry);

	$is_can_delete = cmsUser::isAllowed($ctype['name'], 'delete_news', 'all') ||
				   (cmsUser::isAllowed($ctype['name'], 'delete_news', 'own_places') && $is_own_place) ||
				   (cmsUser::isAllowed($ctype['name'], 'delete_news', 'own') && $is_own_entry);

	if ($is_can_edit){
		$this->addToolButton(array(
			'class' => 'edit',
			'title' => LANG_PLACES_NEWS_EDIT,
			'href'  => $this->href_to('edit_news', $entry['id'])
		));
	}

	if ($is_can_delete){
		$this->addToolButton(array(
			'class' => 'delete',
			'title' => LANG_PLACES_NEWS_DELETE,
			'href'  => $this->href_to('delete_news', $entry['id']),
			'onclick' => "if(!confirm('".LANG_PLACES_NEWS_DELETE_CONFIRM."')){ return false; }"
		));
	}

    if (!empty($entry['seo_title'])){ $this->setPageTitle($entry['seo_title']); }
    if (!empty($entry['seo_keys'])){ $this->setPageKeywords($entry['seo_keys']); }
    if (!empty($entry['seo_desc'])){ $this->setPageDescription($entry['seo_desc']); }

?>

<h1><?php html($entry['title']); ?></h1>

<div class="maps-news-entry-full">
	<div class="body">
		<div class="content clearfix">
			<?php if ($entry['image']) { ?>
				<div class="cont_item_img_left">
					<a href="<?php echo html_image_src($entry['image'], 'big', true); ?>" class="ajax-modal">			
						<?php echo html_image($entry['image'], 'normal'); ?>			
					</a>
				</div>
			<?php } ?>
			<?php echo $entry['content']; ?>
		</div>		
	</div>				
</div>

<div class="news-target news-target margin-b15 margin-t5">	
	<?php $url = href_to_abs($ctype['name'], $item['slug'] . '.html#marker-' . $entry['marker_id'] );	?>
	<div class="maps-balloon media">
		<?php if (!empty($item['photo'])){ ?>
            <a class="media-left image-64" href="<?php echo $url; ?>"><?php echo html_image($item['photo'], 'small'); ?></a>
		<?php } ?>
		<div class="media-body">
			<div class="title">
				<a class="font-s16" href="<?php echo $url; ?>"><?php html($item['title']); ?></a>
			</div>
			<div class="maps_list_addrs addrs">
				<span class="glyphicon glyphicon-map-marker"></span> 
				<?php html($marker['address']); ?>
			</div>
			<?php if ($marker['contacts']){ ?>
				<div class="contacts">						
					<?php $this->renderChild('contacts', array('contacts'=>$marker['contacts'])); ?>						
				</div>		
			<?php } ?>
		</div>
	</div>
</div>
<ul class="info_bar margin-t10">		
	<li class="date">
		<span class="glyphicon glyphicon-calendar"></span> 
		<?php echo html_date_time($entry['date_pub']); ?>
	</li>
	<li> <span class="glyphicon glyphicon-user"></span>
		<a href="<?php echo href_to('users', $entry['user_id']); ?>">
			<?php echo $entry['user']['nickname']; ?>
		</a> 		
	</li>
    <?php if (!empty($this->options['share_link'])){ ?>
    <li class="bar_item bi_share">
        <div class="share dark-share">
           <?php echo $this->options['share_link']; ?>
        </div>
    </li>
    <?php } ?>
</ul>

<?php if ($comments_widget) { echo $comments_widget; } ?>
