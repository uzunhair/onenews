<?php 

    $spellcount = html_spellcount(count($items), $ctype['labels']['one'], $ctype['labels']['two'], $ctype['labels']['many']); 
    
    $cats = array();
    
    foreach($items as $item) {
        
        if (!isset($cats[$item['category_title']])){
            $cats[$item['category_title']] = array(
                'slug' => $item['category_slug'],
                'childs' => array($item)
            );            
        } else {            
            $cats[$item['category_title']]['childs'][] = $item;
        }
        
    }
    
?>
<div class="maps-balloon-list">
	
    <div class="title"><?php printf(LANG_PLACES_BALLOON_LIST, $spellcount); ?>:</div>
    
    <div class="items">
        <ul class="list">
            <?php foreach($cats as $title=>$cat) { ?>
                <?php $cat_url = href_to_abs($ctype['name'], $cat['slug']); ?>
                <li class="category">
                    <a href="<?php echo $cat_url; ?>"><?php echo $title; ?></a>
                    <ul>
                        <?php foreach($cat['childs'] as $item) { ?>
                            <?php $url = href_to_abs($ctype['name'], $item['slug'] . '.html#' . $item['marker_id'] ); ?>
                            <li>
                                <a class="item" href="<?php echo $url; ?>"><?php echo $item['title']; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
	
</div>
