<?php
	$this->addJS('templates/default/js/maps/systems/'.$options['system'].'.js');
	$this->addJS('templates/default/js/maps/form.js');

	$center = $options['pm_center'];

	$is_one_city = ($options['mode']=='one') && isset($options['city_id']);
	$city = array('id'=>'', 'name'=>'', 'region'=>'', 'country'=>'', 'lat'=>'', 'lng'=>'');

	if ($is_one_city){
		$city = $options['city'];
		$center = array($city['lat'], $city['lng']);
	}

?>
<fieldset class="margin-b10">
	<script><?php echo $this->getLangJS('LANG_EDIT', 'LANG_DELETE', 'LANG_PLACES_ADDR_DELETE_CONFIRM'); ?></script>
	<legend><?php echo LANG_PLACES_MARKERS_LIST; ?></legend>
	<div id="maps-markers-list">

		<div id="list">
			<?php if ($markers) { ?>
				<?php foreach($markers as $marker) { ?>
					<?php unset($marker['addr_hash']); ?>
					<?php unset($marker['item_id']); ?>
					<div class="field marker-field" id="marker-<?php echo $marker['id']; ?>">
						<?php if ($marker['address']){ ?>
							<div class="addr hh">
								<span class="glyphicon glyphicon-map-marker"></span>  <?php html($marker['address']); ?>
							</div>
						<?php } ?>
						<div class="coords text-muted font-s12">
							<span class="glyphicon glyphicon-globe"></span>
							<?php html(nf($marker['lat'],4) . ', ' . nf($marker['lng'],4)); ?></div>
						<a class="edit ajaxlink" href="#edit"><?php echo LANG_EDIT; ?></a>
						<a class="delete ajaxlink" href="#delete"><?php echo LANG_DELETE; ?></a>
						<input class="json" type="hidden" name="markers[]" value="<?php echo rawurlencode(json_encode($marker)); ?>">
					</div>
				<?php } ?>
			<?php } ?>
		</div>
		
		<div id="button-add">
			<span class="glyphicon glyphicon-plus-sign"></span>
			<a id="add-address" class="ajaxlink" href="#add-address">
				<?php echo LANG_PLACES_MARKERS_ADD; ?></a>
		</div>

	</div>
</fieldset>

<div id="maps-marker-form-window" style="display:none">
	<?php
		$this->renderChild('marker_form', array(
			'cities'=>$cities,
			'options'=>$options,
			'is_one_city'=>$is_one_city,
			'city'=>$city
		));
	?>
</div>

<script>
	icms.mapsForm.initMarkersList({
		mode: '<?php echo $options['mode']; ?>',
		center: [<?php echo nf($center[0],4); ?>, <?php echo nf($center[1],4); ?>],
		counter: <?php echo $new_markers_counter; ?>,
        max: <?php if ($max) { echo $max; } else { ?>false<?php } ?>
	});
</script>
