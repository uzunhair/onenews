<?php 	
	$rating = $value;
	$value *= 10; $step = 0.5 * 10;
	$value = (round($value)%$step === 0) ? round($value) : round(($value+$step/2)/$step)*$step;
	$value = $value / 10;		
	
	if (!isset($show_value)) { $show_value = true; }
?>
<div class="maps-star-rating<?php if (!empty($class)) { ?> <?php echo $class; ?><?php } ?>">
	<ul class="stars">
		<?php for($s=1; $s<=5; $s++) { ?>
			<?php 			
				if ($value >= 1) { $class = 'full'; $value -= 1; } else 
				if ($value >= 0.5) { $class = 'half'; $value -= 0.5; } else 
				{ $class = 'empty'; $value -= 0.5; }
			?>
			<li class="star <?php echo $class; ?>"></li>
		<?php } ?>
	</ul>
	<?php if ($show_value) { ?>
		<span class="value"><?php echo $rating; ?></span>
	<?php } ?>
</div>
