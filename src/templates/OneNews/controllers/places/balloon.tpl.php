<?php
    $cat_url = href_to_abs($ctype_name, $item['category_slug']);
    $url = href_to_abs($ctype_name, $item['slug'] . '.html' );
    if (count($item['addrs'])>1){ $url .= "#{$marker_id}"; }
?>
<div class="maps-balloon media">
	<?php if (!empty($item['photo'])){ ?>
		<div class="media-left">
			<a href="<?php echo $url; ?>"><?php echo html_image($item['photo'], 'small'); ?></a>
		</div>
	<?php } ?>
				
	<div class="media-body">

        <div class="category">
            <a href="<?php echo $cat_url; ?>" target="_top"><?php echo $item['category_title']; ?></a>
        </div>
        
		<div class="title">
			<a href="<?php echo $url; ?>" target="_top"><?php html($item['title']); ?></a>
		</div>

		<?php if (!empty($item['addrs'][$marker_id])){ ?>
			<div class="maps_list_addrs addrs">
				<span class="glyphicon glyphicon-map-marker"></span> <?php html($item['addrs'][$marker_id]); ?>
			</div>
		<?php } ?>
		
		<div class="contacts">
			<?php if ($item['contacts']){ ?>
				<?php $this->renderChild('contacts', array('contacts'=>$item['contacts'])); ?>
			<?php } ?>
		</div>
                <?php if (!empty($marker['bhours'])){ ?>
                    <div class="bhours">
                        <?php
                            $dow = date('N');
                            $day = $marker['bhours'][$dow];
                            $text = $day['off'] ? LANG_PLACES_DAY_OFF : sprintf(LANG_PLACES_BHOURS_WORKING, $day['start'].'-'.$day['end']);
                        ?>
                        <?php echo $text; ?>
                    </div>
                <?php } ?>		

		<?php if ($options['reviews_on']){ ?>
			<div class="reviews">
				<?php if ($options['reviews_rating']){ ?>
					<?php $this->renderChild('stars', array('value' => $item['marker_rating'], 'show_value'=>false, 'class'=>'small')); ?>
				<?php } ?>
				<a href="<?php echo href_to('places', 'reviews', array($item['id'])) . "?addr_id={$marker_id}"; ?>" target="_top"><?php echo html_spellcount($item['marker_reviews'], LANG_PLACES_REVIEW_SPELLCOUNT); ?></a>
			</div>
		<?php } ?>
		
	</div>
</div>