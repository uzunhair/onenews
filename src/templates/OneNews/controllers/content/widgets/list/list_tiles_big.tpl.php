<?php if ($items){ ?>
<div class="position-r">
    <div id="wookmark-gallery<?php echo $widget->id; ?>" class="wd_cont_list tiles-big">
        <?php foreach($items as $item) { ?>

            <?php
                $url        = href_to($ctype['name'], $item['slug']) . '.html';
                $is_private = $item['is_private'] && $hide_except_title && !$item['user']['is_friend'];
                $image      = (($image_field && !empty($item[$image_field])) ? $item[$image_field] : '');
                if ($is_private) {
                    if($image_field && !empty($item[$image_field])){
                        $image = default_images('private', 'micro');
                    }
                    $url = '';
                }
            ?>

            <div class="item">
                <?php if ($image) { ?>
                    <div class="image">
                        <?php if ($url) { ?>
                            <a href="<?php echo $url; ?>">
                                <img src="<?php echo html_image_src($item[$image_field], 'normal', true); ?>" alt="" class="img-responsive">
                            </a>
                        <?php } else { ?>
                            <div>
                                <img src="<?php echo html_image_src($item[$image_field], 'normal', true); ?>" alt="" class="img-responsive">
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <div class="info">
                    <?php if ($is_show_details) { ?>
                        <div class="details">
                            <span class="author">
                                <a href="<?php echo href_to('users', $item['user']['id']); ?>"><?php html($item['user']['nickname']); ?></a>
                                <?php if ($item['parent_id']){ ?>
                                    <?php echo LANG_WROTE_IN_GROUP; ?>
                                    <a href="<?php echo rel_to_href($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
                                <?php } ?>
                            </span>
                            <span class="date">
                                <?php html(string_date_age_max($item['date_pub'], true)); ?>
                            </span>
                            <?php if($ctype['is_comments']){ ?>
                                <span class="comments">
                                    <?php if ($url) { ?>
                                        <a href="<?php echo $url . '#comments'; ?>" title="<?php echo LANG_COMMENTS; ?>">
                                            <span class="glyphicon glyphicon-comment"></span>
                                            <?php echo intval($item['comments']); ?>
                                        </a>
                                    <?php } else { ?>
                                        <span class="glyphicon glyphicon-comment"></span> <?php echo intval($item['comments']); ?>
                                    <?php } ?>
                                </span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="title">
                        <?php if ($url) { ?>
                            <a class="tiles-big-a" href="<?php echo $url; ?>"><?php html($item['title']); ?></a>
                        <?php } else { ?>
                            <span class="tiles-big-a"><?php html($item['title']); ?></span>
                        <?php } ?>
                        <?php if ($item['is_private']) { ?>
                            <span class="is_private" title="<?php html(LANG_PRIVACY_PRIVATE); ?>"></span>
                        <?php } ?>
                    </div>
                    <?php if ($teaser_field && !empty($item[$teaser_field])) { ?>
                        <div class="teaser">
                            <?php if (!$is_private) { ?>
                                <?php echo string_short($item[$teaser_field], $teaser_len); ?>
                            <?php } else { ?>
                                <!--noindex--><div class="private_field_hint"><?php echo LANG_PRIVACY_PRIVATE_HINT; ?></div><!--/noindex-->
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

        <?php } ?>
    </div>
</div>
<?php $this->addJS("templates/{$this->name}/theme_js/imagesloaded.pkgd.min.js"); ?>
<?php $this->addJS("templates/{$this->name}/theme_js/wookmark.js"); ?>
<?php $this->addJS("templates/{$this->name}/theme_js/jquery.dotdotdot.js"); ?>

    <script type="text/javascript">
        (function () {
            function getWindowWidth() {
                return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
            }

            var wookmark;
            imagesLoaded('#wookmark-gallery<?php echo $widget->id; ?>', function () {
                wookmark = new Wookmark('#wookmark-gallery<?php echo $widget->id; ?>', {
                    itemWidth: 210, // Optional, the width of a grid item
                    offset: 15,
                    align: 'left',
                    flexibleWidth: function () {
                        return getWindowWidth() < 1024 ? '100%' : '50%';
                    }
                });
            });
            $('.tiles-big-a').dotdotdot({
                watch: 'window'
            });
        })(jQuery);
    </script>

<?php } ?>