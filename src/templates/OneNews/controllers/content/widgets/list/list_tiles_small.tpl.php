<?php if ($items){ ?>
<div class="position-r">
    <div id="wookmark-gallery<?php echo $widget->id; ?>"  class="wd_content_list tiles-small">
        <?php foreach($items as $item) { ?>

            <?php
                $url        = href_to($ctype['name'], $item['slug']) . '.html';
                $is_private = $item['is_private'] && $hide_except_title && !$item['user']['is_friend'];
                $image      = (($image_field && !empty($item[$image_field])) ? $item[$image_field] : '');
                if ($is_private) {
                    if($image_field && !empty($item[$image_field])){
                        $image = default_images('private', 'micro');
                    }
                    $url = '';
                }
            ?>

            <div class="item">
                <div class="image">
                    <?php if ($url) { ?>
                        <a href="<?php echo $url; ?>" title="<?php html($item['title']); ?>">
                            <?php echo html_image($image, 'small', $item['title']); ?>
                        </a>
                    <?php } else { ?>
                        <div title="<?php html($item['title']); ?>"><?php echo html_image($image, 'small', $item['title']); ?></div>
                    <?php } ?>
                </div>
            </div>

        <?php } ?>
    </div>
</div>
<?php $this->addJS("templates/{$this->name}/theme_js/imagesloaded.pkgd.min.js"); ?>
<?php $this->addJS("templates/{$this->name}/theme_js/wookmark.js"); ?>

    <script type="text/javascript">
        (function () {
            function getWindowWidth() {
                return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
            }

            var wookmark;
            imagesLoaded('#wookmark-gallery<?php echo $widget->id; ?>', function () {
                wookmark = new Wookmark('#wookmark-gallery<?php echo $widget->id; ?>', {
                    itemWidth: 64,
                    offset: 15,
                    align: 'center',
                    flexibleWidth: function () {
                        return getWindowWidth() < 1024 ? '100%' : '50%';
                    }
                });
            });
        })(jQuery);
    </script>
<?php } ?>