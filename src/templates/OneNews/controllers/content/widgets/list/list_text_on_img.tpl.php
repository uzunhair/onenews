<?php if ($items){ ?>

    <div class="wd_content_list col_prefix text_on_img">
        <?php foreach($items as $item) { ?>

            <?php
                $url        = href_to($ctype['name'], $item['slug']) . '.html';
                $is_private = $item['is_private'] && $hide_except_title && !$item['user']['is_friend'];
                $image      = (($image_field && !empty($item[$image_field])) ? $item[$image_field] : '');
                if ($is_private) {
                    if($image_field && !empty($item[$image_field])){
                        $image = default_images('private', 'micro');
                    }
                    $url = '';
                }
            ?>

            <div class="item">
                <?php if ($image_field && !empty($item[$image_field])) { ?>
                    <div class="image">
                        <a style="background-image:url('<?php echo html_image_src($item[$image_field], 'big', true); ?>')" href="<?php echo $url; ?>"></a>
						
                <div class="info_on_img">
                    <div class="title">
                        <?php if ($url) { ?>
                            <a href="<?php echo $url; ?>"><?php html($item['title']); ?></a>
                        <?php } else { ?>
                            <?php html($item['title']); ?>
                        <?php } ?>
                        <?php if ($item['is_private']) { ?>
                            <span class="is_private" title="<?php html(LANG_PRIVACY_HINT); ?>"></span>
                        <?php } ?>
                    </div>
					    <?php if ($is_show_details) { ?>
                        <ul class="list-info">
                            <li>
                                <a href="<?php echo href_to('users', $item['user']['id']); ?>"><?php html($item['user']['nickname']); ?></a>
                                <?php if ($item['parent_id']){ ?>
                                    <?php echo LANG_WROTE_IN_GROUP; ?>
                                    <a href="<?php echo href_to($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
                                <?php } ?>
                            </li>
                            <li>
                                <?php html(string_date_age_max($item['date_pub'], true)); ?>
                            </li>
                            <?php if($ctype['is_comments']){ ?>
                                <li class="comments">
                                    <?php if ($url) { ?>
                                        <a href="<?php echo $url . '#comments'; ?>" title="<?php echo LANG_COMMENTS; ?>">
                                            <span class="glyphicon glyphicon-comment"></span>
                                            <?php echo intval($item['comments']); ?>
                                        </a>
                                    <?php } else { ?>
                                        <span class="glyphicon glyphicon-comment"></span> <?php echo intval($item['comments']); ?>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
				</div>
				<?php } ?>
            </div>

        <?php } ?>
    </div>

<?php } ?>