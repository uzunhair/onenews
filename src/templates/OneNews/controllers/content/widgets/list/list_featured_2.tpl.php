<?php if ($items){ ?>

    <div class="wd_content_list featured featured_2">
        <?php foreach($items as $item) { ?>

            <?php
                $url        = href_to($ctype['name'], $item['slug']) . '.html';
                $is_private = $item['is_private'] && $hide_except_title && !$item['user']['is_friend'];
                $image      = (($image_field && !empty($item[$image_field])) ? $item[$image_field] : '');
                if ($is_private) {
                    if($image_field && !empty($item[$image_field])){
                        $image = default_images('private', 'micro');
                    }
                    $url = '';
                }
            ?>
            <?php $is_first = !isset($is_first); ?>
            <?php $size = $is_first ? 'normal' : 'small'; ?>

            <div class="item <?php if ($is_first) { ?>item-first<?php } ?>">
                <?php if ($image_field) { ?>
                    <div class="image">
                    <?php if ($is_first) { ?>
                        <?php if (!empty($item[$image_field])) { ?>
                            <a style="background-image:url('<?php echo html_image_src($item[$image_field], 'normal', true); ?>')" href="<?php echo $url; ?>">
                                <img src="<?php echo html_image_src($item[$image_field], 'normal', true); ?>" alt=""  class="visible-xs-block">
                            </a>
                            <?php } else { ?>
                            <a style="background-image:url('/upload/f_project/no_img_256.jpg')" href="<?php echo $url; ?>"></a>   
                        <?php } ?>
                    <?php } else { ?>
                            <a href="<?php echo $url; ?>">
                                <?php if (!empty($item[$image_field])) { ?>
                                    <?php echo html_image($item[$image_field], 'small', $item['title']); ?>
                                <?php } else { ?>
                                    <img src="/upload/f_project/no_img_64.jpg" alt="">
                                <?php } ?>
                            </a>
                    <?php } ?>
                    </div>
                <?php } ?>
                <div class="info">
                    <div class="title">
                        <?php if ($url) { ?>
                            <a href="<?php echo $url; ?>"><?php html($item['title']); ?></a>
                        <?php } else { ?>
                            <?php html($item['title']); ?>
                        <?php } ?>
                        <?php if ($item['is_private']) { ?>
                            <span class="is_private" title="<?php html(LANG_PRIVACY_HINT); ?>"></span>
                        <?php } ?>
                    </div>
					<?php if ($is_first && $is_show_details) { ?>
                        <ul class="list-info">
                            <li>
                                <?php html(string_date_age_max($item['date_pub'], true)); ?>
                            </li>
							<li>
                                <a href="<?php echo href_to('users', $item['user']['id']); ?>"><?php html($item['user']['nickname']); ?></a>
                                <?php if ($item['parent_id']){ ?>
                                    <?php echo LANG_WROTE_IN_GROUP; ?>
                                    <a href="<?php echo href_to($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
                                <?php } ?>
                            </li>
                            <?php if($ctype['is_comments']){ ?>
                                <li class="comments">
                                    <?php if ($url) { ?>
                                        <a href="<?php echo $url . '#comments'; ?>" title="<?php echo LANG_COMMENTS; ?>">
                                            <span class="glyphicon glyphicon-comment"></span>
                                            <?php echo intval($item['comments']); ?>
                                        </a>
                                    <?php } else { ?>
                                        <span class="glyphicon glyphicon-comment"></span> <?php echo intval($item['comments']); ?>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
					<?php if ($is_first) { ?>
							<div class="teaser"><?php echo $item['teaser']; ?></div>
					<?php } else { ?>
						<?php if ($teaser_field && !empty($item[$teaser_field])) { ?>
							<div class="teaser"><?php echo $item[$teaser_field]; ?></div>
						<?php } ?>
					<?php } ?>
                    
                    <?php if (!$is_first && $is_show_details) { ?>
                        <ul class="list-info">
                            <li>
                                <a href="<?php echo href_to('users', $item['user']['id']); ?>"><?php html($item['user']['nickname']); ?></a>
                                <?php if ($item['parent_id']){ ?>
                                    <?php echo LANG_WROTE_IN_GROUP; ?>
                                    <a href="<?php echo href_to($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
                                <?php } ?>
                            </li>
                            <li>
                                <?php html(string_date_age_max($item['date_pub'], true)); ?>
                            </li>
                            <?php if($ctype['is_comments']){ ?>
                                <li class="comments">
                                    <?php if ($url) { ?>
                                        <a href="<?php echo $url . '#comments'; ?>" title="<?php echo LANG_COMMENTS; ?>">
                                            <span class="glyphicon glyphicon-comment"></span>
                                            <?php echo intval($item['comments']); ?>
                                        </a>
                                    <?php } else { ?>
                                        <span class="glyphicon glyphicon-comment"></span> <?php echo intval($item['comments']); ?>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>

        <?php } ?>
    </div>

<?php } ?>