<?php if ($items){ ?>

<?php $this->addJS("templates/{$this->name}/theme_js/owl.carousel.min.js"); ?>
<?php $this->addCSS("templates/{$this->name}/theme_css/owl.carousel.css"); ?>

<!-- Set up your HTML -->
<div class="owl-carousel sl sl_style1">
    <?php $x=""; $y=""; ?>
    <?php foreach($items as $item) { ?>
    <?php   $url = href_to($ctype['name'], $item['slug']) . '.html';
            $image = html_image_src($item[$image_field], 'big', true); ?>

    <?php if ($x==0){?>
        <div>
    <?php } ?>

      <div class="sl_block sl_1">
         <a class="sl_img" href="<?php echo $url; ?>" <?php if(!empty($image)){?>style="background-image:url('<?php echo $image; ?>')"<?php }?>></a>
         <div class="sl_info">
            <div class="sl_cat">
                <ul class="list-inline margin-b0">
                    <li>
                        <span class="glyphicon glyphicon-calendar"></span>
                        <?php $date = date_create($item['date_pub']); echo date_format($date, 'd.m.Y'); ?>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-comment"></span>
                        <?php if ($item['is_private']) { ?>
                            <?php echo intval($item['comments']); ?>
                        <?php } else { ?>
                            <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>#comments" title="<?php echo LANG_COMMENTS; ?>">
                                <?php echo intval($item['comments']); ?>
                            </a>
                        <?php } ?>
                    </li>
                    <li title="<?php echo LANG_HITS; ?>">
                        <span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span>
                        <?php echo $item['hits_count']; ?>
                    </li>
                </ul>
            </div>
         <?php if ($teaser_field && !empty($item[$teaser_field])) { ?>
            <div class="sl_desc">
               <a class="sl_desc_hover" href="<?php echo $url; ?>"><?php echo $item[$teaser_field]; ?></a>
            </div>
         <?php } ?>

         </div>
      </div>

   <?php if ($x==3){ ?>
      </div>
   <?php } ?>

   <?php $y++; if ($y==count($items) && $x !=3){ ?>
      </div>
   <?php } ?>

   <?php if ($x==3){$x = 0;} else { $x++;} ?>

   <?php } //foreach end ?>

</div> <!-- owl-carousel end -->

<script>
$(document).ready(function(){
  $(".sl_style1").owlCarousel({
    items:1,
    loop:true,
    autoplay:<?php echo $autoplay; ?>,
    autoplayTimeout: <?php echo $autoplaytimeout; ?>,
    autoplayHoverPause: <?php echo $autoplayHoverPause; ?>,
    smartSpeed: 1000
  });
});
</script>
<?php } ?>
