<div class="widget_content_filter">
	<div class="filter-container">
		<form action="<?php echo $page_url; ?>" method="get">
			<?php echo html_input('hidden', 'page', 1); ?>
			<div class="fields">
				<?php foreach($fields as $name => $field){ ?>
					<?php $value = isset($filters[$name]) ? $filters[$name] : null; ?>
					<?php $output = $field['handler']->setItem(array('ctype_name' => $ctype_name, 'id' => null, 'category' => $category))->getFilterInput($value); ?>
					<?php if (!$output){ continue; } ?>
                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">
                        <?php if($field['type']=='checkbox') { ?>
                          <label class="btn btn-default filter-checkbox">
                            <?php echo $output; ?> <?php echo $field['title']; ?>
                          </label>
                        <?php } elseif (($field['type']=='city') || ($field['type'] == 'list' && !($field['options']['is_filter_multi'])) ) { ?>
                                <?php echo $output; ?>
                                <script>
                                        $('.f_<?php echo $field['name']; ?> select').chosen({no_results_text: '<?php echo LANG_LIST_EMPTY; ?>', placeholder_text_single: '<?php echo $field['title']; ?>', disable_search_threshold: 8, width: '100%', allow_single_deselect: true, search_placeholder: '<?php echo LANG_BEGIN_TYPING; ?>'});
                                </script>
                        <?php } else { ?>
                            <div class="dropdown">
                                <button id="f_<?php echo $field['name']; ?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default">
                                    <?php echo $field['title']; ?>
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu keep_open" aria-labelledby="f_<?php echo $field['name']; ?>">
                                    <?php echo $output; ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div> 
				<?php } ?>
				<?php if (!empty($props_fields)){ ?>
					<?php foreach($props as $prop){ ?>
						<?php
							$field = $props_fields[$prop['id']];
							$field->setName("p{$prop['id']}");
							if ($prop['type'] == 'list' && !empty($prop['options']['is_filter_multi'])){ $field->setOption('filter_multiple', true); }
							if ($prop['type'] == 'number' && !empty($prop['options']['is_filter_range'])){ $field->setOption('filter_range', true); }
							$value = isset($filters["p{$prop['id']}"]) ? $filters["p{$prop['id']}"] : null;
						?>
                        <div class="field ft_<?php echo $prop['type']; ?> f_prop_<?php echo $prop['id']; ?>">

                        <?php if($prop['type']=='checkbox') { ?>
                          <label class="btn btn-default filter-checkbox">
                            <?php echo $field->getFilterInput($value); ?> <?php echo $prop['title']; ?>
                          </label>
                        <?php } elseif ($prop['type'] == 'list' && !($prop['options']['is_filter_multi'])) { ?>
                                <?php echo $field->setItem(array('ctype_name' => $ctype_name, 'id' => null, 'category' => $category))->getFilterInput($value); ?>
                                <script>
                                        $('.f_prop_<?php echo $prop['id']; ?> select').chosen({no_results_text: '<?php echo LANG_LIST_EMPTY; ?>', placeholder_text_single: '<?php echo $prop['title']; ?>', disable_search_threshold: 8, width: '100%', allow_single_deselect: true, search_placeholder: '<?php echo LANG_BEGIN_TYPING; ?>'});
                                </script>
                        <?php } else { ?>
                            <div class="dropdown">
                                <button id="f_<?php echo $prop['name']; ?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default">
                                    <?php echo $prop['title']; ?>
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu keep_open" aria-labelledby="f_<?php echo $prop['name']; ?>">
                                    <?php echo $field->setItem(array('ctype_name' => $ctype_name, 'id' => null, 'category' => $category))->getFilterInput($value); ?>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
					<?php } ?>
				<?php } ?>
			</div>
			<div class="buttons">
				<?php echo html_submit(LANG_FILTER_APPLY); ?>
				<?php if (sizeof($filters)){ ?>
                    <a href="<?php echo $page_url; ?>"><?php echo LANG_CANCEL; ?></a>
				<?php } ?>
			</div>
		</form>
	</div>
</div>