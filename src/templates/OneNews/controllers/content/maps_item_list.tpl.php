<div class="category_places">
    <?php if ($page_header && !$request->isInternal() && !$is_frontpage){  ?>
        <?php if (!empty($ctype['options']['is_rss']) && $this->controller->isControllerEnabled('rss')){ ?>
            <div class="content_list_rss_icon">
                <a href="<?php echo href_to('rss', 'feed', $ctype['name']) . $rss_query; ?>">RSS</a>
            </div>
        <?php } ?>
        <h1 class="cont_head"><?php echo $page_header; ?></h1>
    <?php } ?>

    <?php if ($datasets && !$is_hide_items){ ?>
        <div class="content_datasets">
            <ul class="nav nav-tabs">
                <?php if( $ctype['options']['list_show_filter'] ){ ?>
                    <li class="pull-right">
                        <a href="javascript:toggleFilter()" title="<?php echo LANG_SHOW_FILTER; ?>">
                            <span class="glyphicon glyphicon-search"></span>
                        </a>
                    </li>
                <?php } ?>
                <?php $ds_counter = 0; ?>
                <?php foreach($datasets as $set){ ?>
                    <?php $ds_selected = ($dataset == $set['name'] || (!$dataset && $ds_counter==0)); ?>
                    <li <?php if ($ds_selected){ ?>class="active"<?php } ?>>
                        <?php if ($ds_counter > 0) { $ds_url = sprintf(rel_to_href($base_ds_url), $set['name']); } ?>
                        <?php if ($ds_counter == 0) { $ds_url = href_to($base_url, isset($category['slug']) ? $category['slug'] : ''); } ?>

                        <?php if ($ds_selected){ ?>
                            <a href="#"><?php echo $set['title']; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $ds_url; ?>"><?php echo $set['title']; ?></a>
                        <?php } ?>
                    </li>
                    <?php $ds_counter++; ?>
                <?php } ?>
            </ul>
        </div>
        <?php if (!empty($current_dataset['description'])){ ?>
            <div class="content_datasets_description">
                <?php echo $current_dataset['description']; ?>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if (!empty($category['description'])){?>
        <div class="category_description mb-3"><?php echo $category['description']; ?></div>
    <?php } ?>

    <?php if ($subcats && $ctype['is_cats'] && !empty($ctype['options']['is_show_cats'])){ ?>
        <div class="gui-panel clearfix content_categories<?php if (count($subcats)>8){ ?> categories_small<?php } ?>">
            <ul class="list-null places-cat-nav">
                <?php foreach($subcats as $c){ ?>
                    <li class="<?php echo str_replace('/', '-', $c['slug']);?>">
                        <a href="<?php echo href_to($base_url . ($dataset ? '-'.$dataset : ''), $c['slug']); ?>">
                            <?php if($ctype['name']=="places" && $c['icon']){ ?>
                                <img alt="<?php echo $c['title']; ?>" src="/upload/markers/<?php echo html($c['icon']); ?>" class="display-b" />
                            <?php } else { ?>
                                <img alt="" src="/upload/markers/default.png" class="display-b" />
                            <?php }?>
                            <span class="places-cat-title">
                                <?php echo $c['title']; ?>
                            </span>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>


    <?php echo $items_list_html; ?>
</div>