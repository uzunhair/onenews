<?php $core = cmsCore::getInstance(); ?>
<?php
// The theme work Atid

$is_right = $this->hasWidgetsOn('right-top', 'right-center', 'right-bottom');
$is_left  = $this->hasWidgetsOn('left-top', 'left-center', 'left-bottom');
   
   $push_class = "";
   $pull_class = "";
    if ($is_right && $is_left) {
     $body_class = "col-md-6 col-xs-12 two-aside";
        if ($this->options['aside_pos']=='one_big_one') {
            $push_class = 'col-md-push-3';
            $pull_class = 'col-md-pull-6';
        }
    } 
    else if ($is_left)      {
     //включена только левая колонка
     $body_class = "col-md-9 col-xs-12 one-aside";
        if ($this->options['aside_pos']=='one_big_one') {
            $push_class = 'col-md-push-3';
            $pull_class = 'col-md-pull-9';
        }
    }
    else if ($is_right)     {
     //включена только правая колонка
     $body_class = "col-md-9 col-xs-12 one-aside";
    }
    else                    {
        $body_class = "col-xs-12 no-aside";
    }
 ?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php $this->title(); ?></title>

    <?php $this->addMainCSS("templates/{$this->name}/addon/bootstrap/css/bootstrap.css"); ?>
    <?php $this->addMainCSS("templates/{$this->name}/css/bootstrap-{$this->options['theme_color']}.css"); ?>
    <?php $this->addMainCSS("templates/{$this->name}/css/theme-layout.css"); ?>
    <?php $this->addMainCSS("templates/{$this->name}/css/theme-gui.css"); ?>
    <?php $this->addMainCSS("templates/{$this->name}/css/theme-widgets.css"); ?>
    <?php $this->addMainCSS("templates/{$this->name}/css/theme-content.css"); ?>
    <?php $this->addMainCSS("templates/{$this->name}/css/theme-modal.css"); ?>
    <?php $this->addMainJS("templates/{$this->name}/js/jquery.js"); ?>
    <?php $this->addMainJS("templates/{$this->name}/js/jquery-modal.js"); ?>
    <?php $this->addMainJS("templates/{$this->name}/js/core.js"); ?>
    <?php $this->addMainJS("templates/{$this->name}/js/modal.js"); ?>
    <?php if (cmsUser::isLogged()){ ?>
        <?php $this->addMainJS("templates/{$this->name}/js/messages.js"); ?>
    <?php } ?>

    <?php $this->addMainJS("templates/{$this->name}/addon/bootstrap/js/bootstrap.min.js"); ?>
    <?php $this->addMainJS("templates/{$this->name}/theme_js/atid.js"); ?>
    <?php $this->addJS("templates/{$this->name}/theme_js/flexmenu.min.js"); ?>

<?php if (!empty($this->options['my_styles'])){ ?>
    <?php $this->addCSS("templates/{$this->name}/css/{$this->options['my_styles']}"); ?>
<?php } ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php $this->head(); ?>
    <?php echo $this->options['apple_touch_icon']; ?>
</head>
<body id="<?php echo $device_type; ?>_device_type" itemscope itemtype="http://schema.org/WebPage">
<div id="wrap">
<header>   
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container head-trian">
            <div class="navbar-header">
              <!-- Кнопка открытия верхнего меню -->
              <button type="button" class="navbar-toggle collapsed navbar-toggle-user" data-toggle="collapse" data-target="#navbar-collapse-2">
                <span class="glyphicon glyphicon-user"></span>
              </button>
              <!-- Кнопка открытия нижнего меню -->
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

                <?php if($core->uri) { ?>
                    <a href="<?php echo href_to_home(); ?>" class="logo-mobile visible-xs">
                        <img alt="" src="/templates/<?php echo $this->name; ?>/images/logo.png" class="img-responsive">
                    </a>
                <?php } else { ?>
                    <div class="logo-mobile visible-xs">
                        <img alt="" src="/templates/<?php echo $this->name; ?>/images/logo.png" class="img-responsive">
                    </div>
                <?php } ?>

            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-2">
        	   <?php $this->widgets('header', false, 'wrapper_plain'); ?>
            </div>
        </div>
    </nav>
</header> 

    <div id="layout">
    	<div class="container head-block">
        	<div class="head-two hidden-xs">
        		<div class="clearfix">
                    <?php if($core->uri) { ?>
                        <a id="logo" href="<?php echo href_to_home(); ?>">
                            <img alt="" src="/templates/<?php echo $this->name; ?>/images/logo.png">
                        </a>
                    <?php } else { ?>
                        <div id="logo">
                            <img alt="" src="/templates/<?php echo $this->name; ?>/images/logo.png">
                        </div>
                    <?php } ?>

                    <?php if($this->hasWidgetsOn('top_banner')) {?>
        			<div class="banner">
                        <?php $this->widgets('top_banner', false, 'wrapper_plain'); ?>
        			</div>
                    <?php }?>
        		</div>
        	</div>
            <?php if (!$config->is_site_on){ ?>
                <div id="site_off_notice" class="alert alert-danger" role="alert">
                    <button type="button" class="close ml-5" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php if (cmsUser::isAdmin()){ ?>
                        <?php printf(ERR_SITE_OFFLINE_FULL, href_to('admin', 'settings', 'siteon')); ?>
                    <?php } else { ?>
                        <?php echo ERR_SITE_OFFLINE; ?>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if($this->hasWidgetsOn('top')) { ?>
                <nav class="navbar navbar-inverse navbar-static-top navbar-go-top margin-b0" itemscope itemtype="http://schema.org/SiteNavigationElement" >
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <?php $this->widgets('top', false, 'wrapper_plain'); ?>
                    </div>
                </nav>
            <?php } ?>
            <?php if($this->hasWidgetsOn('carousel')) { ?>
            <div class="carousel_block">
                <?php $this->widgets('carousel'); ?>
            </div>
            <?php }?>
    	</div>

        <div class="container">
		<div id="body" class="row">
            <?php
            $messages = cmsUser::getSessionMessages();
            if ($messages){ ?>
                <div class="sess_messages">
                    <?php foreach($messages as $message){ ?>
                        <div class="<?php echo $message['class']; ?>"><?php echo $message['text']; ?></div>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if ($config->show_breadcrumbs && $core->uri && $this->isBreadcrumbs()){ ?>
                <div id="breadcrumbs">
                    <?php $this->breadcrumbs(array('strip_last'=>false)); ?>
                    <?php if($this->hasWidgetsOn('breadcrumbs')) { ?>
                        <?php $this->widgets('breadcrumbs', false, 'wrapper_breadcrumbs'); ?>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="section <?php echo $body_class. ' ' . $push_class; ?>">
                <?php $this->widgets('body-top'); ?>

                <?php if ($this->isBody()){ ?>
                    <article>
                        <?php $this->body(); ?>
                    </article>
                <?php } ?>

                <?php $this->widgets('body-bottom'); ?>
            </div>
			
            <?php if($this->hasWidgetsOn('left-top', 'left-center', 'left-bottom')) { ?>
            <aside id="left_aside" class="col-xs-12 col-md-3 <?php echo $pull_class; ?> aside">
                <?php $this->widgets('left-top', true, 'wrapper_sidebar'); ?>

                <?php $this->widgets('left-center', true, 'wrapper_sidebar'); ?>

                <?php $this->widgets('left-bottom', true, 'wrapper_sidebar'); ?>
            </aside>
			<?php } ?>
			
            <?php if ($this->hasWidgetsOn('right-top') || $this->hasWidgetsOn('right-center') || $this->hasWidgetsOn('right-bottom')) { ?>	
            <aside id="right_aside" class="col-xs-12 col-md-3 aside">
                <?php $this->widgets('right-top', true, 'wrapper_sidebar'); ?>

                <?php $this->widgets('right-center', true, 'wrapper_sidebar'); ?>

                <?php $this->widgets('right-bottom', true, 'wrapper_sidebar'); ?>
            </aside>
            <?php } ?>

        </div>

        <?php if ($config->debug && cmsUser::isAdmin()){ ?>
            <div id="debug_block">
                <?php $this->renderAsset('ui/debug', array('core' => $core)); ?>
            </div>
        <?php } ?>
    </div>
</div>
</div>
<footer class="main-footer">

	<div class="footer-box">
        <?php if($this->hasWidgetsOn('footer_nav')) { ?>
		<nav class="navbar navbar-default navbar-static-top" role="navigation">
			<div class="container">
				<?php $this->widgets('footer_nav', false, 'wrapper_plain'); ?>
			</div>
		</nav>
        <?php } ?>
        <?php if ($this->hasWidgetsOn('subfooter')) { ?>
        <div class="container">
        	<div id="subfooter" class="row">
        		<?php $this->widgets('subfooter', true, 'wrapper_subfooter'); ?>
        	</div>
        </div>
        <?php } ?>

	</div><!-- End "footer-box" -->

	<div id="copyright">
		<div class="container">
			<div class="navbar-text">
				<a href="<?php echo $this->options['owner_url'] ? $this->options['owner_url'] : href_to_home(); ?>">
                    <strong class="text-upper"><?php html($this->options['owner_name'] ? $this->options['owner_name'] : cmsConfig::get('sitename')); ?></strong>
                </a> &copy; <?php echo $this->options['owner_year'] ? $this->options['owner_year'] : date('Y'); ?>
			</div> 

			<?php $this->widgets('footer', false, 'wrapper_footer_nav'); ?>

            <?php if (!empty($this->options['counter'])){ ?>
                <div class="navbar-metric navbar-right text-center" <?php if (!empty($this->options['counter_padding'])){ ?>style="padding:<?php echo $this->options['counter_padding']; ?>;" <?php } ?>>
                    <?php echo $this->options['counter']; ?>
                </div>
            <?php } ?>
			<div class="navbar-text navbar-right">
			    <span class="item">
                    <?php echo LANG_POWERED_BY_INSTANTCMS; ?>
                </span>
				<a href="#">Bootstrap3</a>
                <?php if ($config->debug && cmsUser::isAdmin()){ ?>
                    <span class="item">
                            <a href="#debug_block" title="<?php echo LANG_DEBUG; ?>" class="ajax-modal"><?php echo LANG_DEBUG; ?></a>
                        </span>
                    <span class="item">
                            Time: <?php echo cmsDebugging::getTime('cms', 4); ?> s
                        </span>
                    <span class="item">
                            Mem: <?php echo round(memory_get_usage(true)/1024/1024, 2); ?> Mb
                        </span>
                <?php } ?>
			</div>
		</div>
	</div><!-- End "copyright" -->
</footer>

<?php if ($this->hasWidgetsOn('active_menu')) { ?>
<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
    <div class="container">
        <div class="navbar-header">
          <!-- Кнопка открытия нижнего меню -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-active-menu">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-active-menu">
           <?php $this->widgets('active_menu', true, 'wrapper_plain'); ?>
        </div>
    </div>
</nav>
<style>body{padding-bottom:50px;}</style>
<?php }?>

    <script type="text/javascript">
        $('ul.flex').flexMenu();
        
        $('ul.cont_cat.flex').flexMenu({
            linkTextAll : 'Показать категории'
        });

        $("body").on("click",function(event){
            if ($(event.target).prop("tagName") === "A")
            return;
            $(".flexMenu-popup").hide();
            $(".flexMenu-viewMore").removeClass("active");
        });
    </script>
</body>
</html>
