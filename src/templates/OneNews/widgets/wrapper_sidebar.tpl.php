<div class="widget<?php if ($widget['class_wrap']) { ?> <?php echo $widget['class_wrap'];  } ?>">
    <?php if ($widget['title'] && $is_titles){ ?>
        <div class="wd_title<?php if ($widget['class_title']) { ?> <?php echo $widget['class_title'];  } ?>">
            <?php echo $widget['title']; ?>
            <?php if (!empty($widget['links'])) { ?>
                <div class="links pos-r">
                    <?php $links = string_parse_list($widget['links']); ?>
                    <a aria-expanded="false" role="button" aria-haspopup="true" data-toggle="dropdown"
                       data-target="#" id="widget-links-a<?php echo $widget['id']; ?>">
                        <span class="glyphicon glyphicon-link"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <?php foreach($links as $link){ ?>
                        <li>
                            <a href="<?php echo (strpos($link['value'], 'http') === 0) ? $link['value'] : href_to($link['value']); ?>">
                                <?php echo $link['id']; ?>
                            </a>
                        </li>
                    <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        </div>
    <?php } ?>

    <div class="wd_body<?php if ($widget['class']) { ?> <?php echo $widget['class'];  } ?>">
        <?php echo $widget['body']; ?>
    </div>
</div>