<div class="dropdown dropdown-breadcrumbs">
    <button id="drop-breadcrumbs-btn" type="button" class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
    </button>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="drop-breadcrumbs-btn">
        <?php echo $widget['body']; ?>
    </div>
</div>