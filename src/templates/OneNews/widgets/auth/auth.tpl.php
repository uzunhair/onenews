<div class="widget_auth">
    <form action="<?php echo href_to('auth', 'login'); ?>" method="POST">

        <?php echo html_input('hidden', 'is_back', 1); ?>
        
        <div class="field margin-b10">
            <label class="sr-only"><?php echo LANG_EMAIL; ?>:</label>
            <?php echo html_input('text', 'login_email', '', array('required'=>true, 'tabindex' => '1')); ?>
        </div>

        <div class="field">
            <label class="sr-only"><?php echo LANG_PASSWORD; ?>:</label>
            <?php echo html_input('password', 'login_password', '', array('required'=>true, 'tabindex' => '2')); ?>
        </div>
        <div class="checkbox">
            <label for="remember">
              <input type="checkbox" id="remember" name="remember" value="1" tabindex="3"> <?php echo LANG_REMEMBER_ME; ?>
            </label>
        </div>
        <div>
            <?php echo html_submit(LANG_LOG_IN, 'submit', array('tabindex' => '4', 'class'=>'btn btn-primary pull-left')); ?>
            <div class="pull-left font-s12 ml-10" style="margin-top: -2px;">
            <a href="<?php echo href_to('auth', 'restore'); ?>"><?php echo LANG_FORGOT_PASS; ?></a>
            <br>    
			<a href="<?php echo href_to('auth', 'register'); ?>"><?php echo LANG_REGISTRATION; ?></a>
            </div>
        </div>
    </form>
</div>
