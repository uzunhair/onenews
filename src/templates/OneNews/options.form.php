<?php

class formOneNewsTemplateOptions extends cmsForm {

    public function init() {

        return array(

            // array(
            //     'type' => 'fieldset',
            //     'title' => LANG_PAGE_LOGO,
            //     'childs' => array(
            //         new fieldImage('logo', array(
            //             'options' => array(
            //                 'sizes' => array('small', 'original')
            //             )
            //         )),
            //     )
            // ),

            array(
                'type' => 'fieldset',
                'title' => LANG_ONE_NEWS_THEME_COPYRIGHT,
                'childs' => array(

                    new fieldString('owner_name', array(
                        'title' => LANG_TITLE
                    )),

                    new fieldString('owner_url', array(
                        'title' => LANG_ONE_NEWS_THEME_COPYRIGHT_URL,
                        'hint' => LANG_ONE_NEWS_THEME_COPYRIGHT_URL_HINT
                    )),

                    new fieldString('owner_year', array(
                        'title' => LANG_ONE_NEWS_THEME_COPYRIGHT_YEAR,
                        'hint' => LANG_ONE_NEWS_THEME_COPYRIGHT_YEAR_HINT
                    )),

                )
            ),

            array(
                'type' => 'fieldset',
                'title' => LANG_ONE_NEWS_THEME_LAYOUT_COLUMNS,
                'childs' => array(

                    new fieldList('aside_pos', array(
                        'title' => LANG_ONE_NEWS_THEME_LAYOUT_SIDEBAR_POS,
                        'default' => 'big_one_one',
                        'items' => array(
                            'big_one_one' => LANG_ONE_NEWS_THEME_LAYOUT_COMPONENT_LEFT_RIGHT,
                            'one_big_one' => LANG_ONE_NEWS_THEME_LAYOUT_LEFT_COMPONENT_RIGHT,
                        )
                    )),

                )
            ),

            array(
                'type' => 'fieldset',
                'title' => LANG_ONE_NEWS_THEME_NEWS,
                'childs' => array(

                    new fieldList('news_img', array(
                        'title' => LANG_ONE_NEWS_THEME_NEWS_POS,
                        'default' => 'center',
                        'items' => array(
                            'center' => LANG_ONE_NEWS_THEME_NEWS_CENTER,
                            'left' => LANG_ONE_NEWS_THEME_LAYOUT_LEFT,
                            'right' => LANG_ONE_NEWS_THEME_LAYOUT_RIGHT,
                        )
                    )),

                    new fieldList('info_bar', array(
                        'title' => LANG_ONE_NEWS_THEME_INFO,
                        'default' => 'content',
                        'items' => array(
                            'content' => LANG_ONE_NEWS_THEME_INFO_CONTENT,
                            'title' => LANG_ONE_NEWS_THEME_INFO_TITLE,
                        )
                    )),

                )
            ),

            array(
                'type' => 'fieldset',
                'title' => LANG_ONE_NEWS_THEME_PHOTOS_CONTROLLER,
                'childs' => array(
                    new fieldList('photo_go', array(
                        'title' => LANG_ONE_NEWS_THEME_PHOTOS_GO_PHOTO_BLOCK,
                        'default' => 'photo_go_true',
                        'items' => array(
                            'photo_go_true' => LANG_YES,
                            'photo_go_false' => LANG_NO,
                        )
                    )),
                )
            ),

            array(
                'type' => 'fieldset',
                'title' => LANG_ONE_NEWS_THEME_OTHER_TEXT,
                'childs' => array(

                    new fieldList('theme_color', array(
                        'title' => LANG_ONE_NEWS_THEME_COLOR,
                        'default' => 'theme1',
                        'items' => array(
                            'theme1' => LANG_ONE_NEWS_THEME_COLOR_1,
                            'theme2' => LANG_ONE_NEWS_THEME_COLOR_2,
                            'theme3' => LANG_ONE_NEWS_THEME_COLOR_3
                        )
                    )),

                    new fieldText('share_link', array(
                        'title' => LANG_ONE_NEWS_THEME_SHARE,
                        'hint' => LANG_ONE_NEWS_THEME_SHARE_INFO,
                    )),

                    new fieldText('counter', array(
                        'title' => LANG_ONE_NEWS_THEME_COUNT,
                        'hint' => LANG_ONE_NEWS_THEME_COUNT_INFO,
                    )),
                    new fieldString('counter_padding', array(
                        'title' => LANG_ONE_NEWS_THEME_COUNT_PADDING,
                        'hint' => LANG_ONE_NEWS_THEME_COUNT_PADDING_INFO,
                    )),
                    new fieldString('my_styles', array(
                        'title' => LANG_ONE_NEWS_THEME_MY_STYLES,
                        'hint' => LANG_ONE_NEWS_THEME_MY_STYLES_HINT,
                    )),
                    new fieldText('apple_touch_icon', array(
                        'title' => LANG_ONE_NEWS_THEME_APPLE_ICON,
                    )),
                )
            ),

        );

    }

}
