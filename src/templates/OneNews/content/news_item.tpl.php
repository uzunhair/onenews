<?php $user = cmsUser::getInstance(); ?>
<div itemscope itemtype="http://schema.org/Article">
<?php if ($fields['title']['is_in_item']){ ?>
    <h1 class="content_title">
        <span itemprop="headline">
            <?php html($item['title']); ?>
        </span>
        <?php if ($item['is_private']) { ?>
            <span class="is_private" title="<?php html(LANG_PRIVACY_HINT); ?>"></span>
        <?php } ?>
    </h1>
    <?php if ($item['parent_id'] && !empty($ctype['is_in_groups'])){ ?>
        <h2 class="parent_title item_<?php echo $item['parent_type']; ?>_title">
            <a href="<?php echo rel_to_href($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
        </h2>
    <?php } ?>
    <?php unset($fields['title']); ?>
<?php } ?>

    <?php
        $show_bar = !empty($item['rating_widget']) ||
                    $fields['date_pub']['is_in_item'] ||
                    $fields['user']['is_in_item'] ||
            ($ctype['is_comments'] && $item['is_comments_on']) ||
                    !empty($ctype['options']['hits_on']) ||
                    !$item['is_pub'] ||
                    !$item['is_approved'];
    ?>
<?php if ($this->options['info_bar']=='title'){ ?>
    <?php if ($show_bar){ ?>
        <ul class="info_bar info_bar_news">
            <?php if (!empty($item['rating_widget'])){ ?>
                <li class="bar_item bi_rating">
                    <?php echo $item['rating_widget']; ?>
                </li>
            <?php } ?>
            <?php if ($fields['date_pub']['is_in_item']){ ?>
                <li style="clear: both;" class="bar_item bi_date_pub" title="<?php html( $fields['date_pub']['title'] ); ?>">
                    <span class="glyphicon glyphicon-calendar"></span>
                    <span>
                        <meta itemprop="datePublished" content="<?php echo date('c', strtotime($item['date_pub'])); ?>">
                        <?php echo $fields['date_pub']['html']; ?>
                    </span>
                </li>
            <?php } ?>
            <?php if (!$item['is_pub']){ ?>
                <li class="bar_item bi_not_pub">
                    <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                </li>
            <?php } ?>
            <?php if (!empty($ctype['options']['hits_on'])){ ?>
                <li class="bar_item bi_hits" title="<?php echo LANG_HITS; ?>">
                    <span class="glyphicon glyphicon-eye-open"></span>
                    <?php echo $item['hits_count']; ?>
                </li>
            <?php } ?>
            <?php if ($fields['user']['is_in_item']){ ?>
                <li class="bar_item bi_user" title="<?php html( $fields['user']['title'] ); ?>">
                    <span class="glyphicon glyphicon-user"></span>
                    <span itemprop="author">
                        <?php echo $fields['user']['html']; ?>
                    </span>
                </li>
                <?php if (!empty($item['folder_title'])){ ?>
                    <li class="bar_item bi_folder">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <a href="<?php echo href_to('users', $item['user']['id'], array('content', $ctype['name'], $item['folder_id'])); ?>"><?php echo $item['folder_title']; ?></a>
                    </li>
                <?php } ?>
            <?php } ?>
            <?php if (!empty($this->options['share_link'])){ ?>
            <li class="bar_item bi_share">
                <div class="share dark-share">
                   <?php echo $this->options['share_link']; ?>
                </div>
            </li>
            <?php } ?>
            <?php if (!$item['is_approved']){ ?>
                <li class="bar_item bi_not_approved">
                    <?php echo LANG_CONTENT_NOT_APPROVED; ?>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>
<?php } ?>
<div class="news_item clearfix">
<div class="clearfix">

    <?php if (($fields['photo']['is_in_item']) && (!empty($item['photo']))) { ?>
        <?php if ($this->options['news_img'] == 'center') { ?>
            <div class="cont_item_img_center">
                <img src="<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_full'], true); ?>"
                     alt="<?php html($item['title']); ?>" title="<?php html($item['title']); ?>" itemprop="image"
                     class="img-responsive">
            </div>
        <?php } else { ?>
            <div class="cont_item_img_<?php html($this->options['news_img']); ?>">
                <img src="<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_full'], true); ?>"
                     alt="<?php html($item['title']); ?>" title="<?php html($item['title']); ?>" itemprop="image"
                     class="img-responsive">
            </div>
        <?php } ?>
        <?php $item['photo'] = ""; ?>
    <?php } ?>

        <?php if (!empty($fields)) { ?>

            <?php $fields_fieldsets = cmsForm::mapFieldsToFieldsets($fields, function($field, $user) use ($item) {
                if (!$field['is_in_item'] || $field['is_system']) { return false; }
                if ((empty($item[$field['name']]) || empty($field['html'])) && $item[$field['name']] !== '0') { return false; }
                if ($field['groups_read'] && !$user->isInGroups($field['groups_read'])) { return false; }
                return true;
            } ); ?>

            <?php foreach ($fields_fieldsets as $fieldset_id => $fieldset) { ?>

                <?php $is_fields_group = !empty($ctype['options']['is_show_fields_group']) && $fieldset['title']; ?>

                <?php if ($is_fields_group) { ?>
                    <div class="fields_group fields_group_<?php echo $ctype['name']; ?>_<?php echo $fieldset_id ?>">
                    <h3 class="group_title"><?php html($fieldset['title']); ?></h3>
                <?php } ?>

                <?php if (!empty($fieldset['fields'])) { ?>
                    <?php foreach ($fieldset['fields'] as $name => $field) { ?>

                        <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?> <?php echo $field['options']['wrap_type']; ?>_field" <?php if($field['options']['wrap_width']){ ?> style="width: <?php echo $field['options']['wrap_width']; ?>;"<?php } ?>>
                            <?php if ($field['options']['label_in_item'] != 'none') { ?>
                                <div class="title_<?php echo $field['options']['label_in_item']; ?>"><?php html($field['title']); ?>: </div>
                            <?php } ?>
                            <div class="value margin-b10" itemprop="articleBody"><?php echo $field['html']; ?></div>
                        </div>

                    <?php } ?>
                <?php } ?>

                <?php if ($is_fields_group) { ?></div><?php } ?>

            <?php } ?>

        <?php } ?>


    <?php
        $hooks_html = cmsEventsManager::hookAll("content_{$ctype['name']}_item_html", $item);
        if ($hooks_html) { echo html_each($hooks_html); }
    ?>
</div>

    <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) &&  $item['tags']){?>
        <div class="tags_bar margin-b5">
		<span class="glyphicon glyphicon-tags color-blue2 padding-r5"></span>
            <?php echo html_tags_bar($item['tags']); ?>
        </div>
    <?php } ?>


    <?php if ($ctype['item_append_html']){ ?>
        <div class="append_html"><?php echo $ctype['item_append_html']; ?></div>
    <?php } ?>

<?php if ($this->options['info_bar']=='content'){ ?>
    <?php if ($show_bar){ ?>
        <ul class="info_bar margin-t10">
            <?php if (!empty($item['rating_widget'])){ ?>
                <li class="bar_item bi_rating">
                    <?php echo $item['rating_widget']; ?>
                </li>
            <?php } ?>
            <?php if ($fields['date_pub']['is_in_item']){ ?>
                <li style="clear: both;" class="bar_item bi_date_pub" title="<?php html( $fields['date_pub']['title'] ); ?>">
                    <span class="glyphicon glyphicon-calendar"></span>
                    <span>
                        <meta itemprop="datePublished" content="<?php echo date('c', strtotime($item['date_pub'])); ?>">
                        <?php echo $fields['date_pub']['html']; ?>
                    </span>
                </li>
            <?php } ?>
            <?php if (!$item['is_pub']){ ?>
                <li class="bar_item bi_not_pub">
                    <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                </li>
            <?php } ?>
            <?php if (!empty($ctype['options']['hits_on'])){ ?>
                <li class="bar_item bi_hits" title="<?php echo LANG_HITS; ?>">
                    <span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span>
                    <?php echo $item['hits_count']; ?>
                </li>
            <?php } ?>
            <?php if ($fields['user']['is_in_item']){ ?>
                <li class="bar_item bi_user" title="<?php html( $fields['user']['title'] ); ?>">
                    <span class="glyphicon glyphicon-user"></span>
                    <span itemprop="author">
                        <?php echo $fields['user']['html']; ?>
                    </span>
                </li>
                <?php if (!empty($item['folder_title'])){ ?>
                    <li class="bar_item bi_folder">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <a href="<?php echo href_to('users', $item['user']['id'], array('content', $ctype['name'], $item['folder_id'])); ?>"><?php echo $item['folder_title']; ?></a>
                    </li>
                <?php } ?>
            <?php } ?>


            <?php if (!empty($this->options['share_link'])){ ?>
            <li class="bar_item bi_share">
                <div class="share dark-share">
                   <?php echo $this->options['share_link']; ?>
                </div>
            </li>
            <?php } ?>

            <?php if (!$item['is_approved']){ ?>
                <li class="bar_item bi_not_approved">
                    <?php echo $item['is_draft'] ? LANG_CONTENT_DRAFT_NOTICE : LANG_CONTENT_NOT_APPROVED; ?>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>
<?php } ?>

</div>
</div>
