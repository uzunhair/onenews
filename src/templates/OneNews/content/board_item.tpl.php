<?php   
    $user = cmsUser::getInstance();
    $autor=cmsCore::getModel('users')->getUser($item['user_id']);
?>

<?php
    $show_bar = !empty($item['rating_widget']) ||
    $fields['date_pub']['is_in_item'] ||
    $fields['user']['is_in_item'] ||
    !empty($ctype['options']['hits_on']) ||
    !$item['is_pub'] ||
    !$item['is_approved'];
?>

<?php $this->addCSS("templates/{$this->name}/css/board.css"); ?>

<?php if (isset($fields['photos']) && $fields['photos']['is_in_item'] && !empty($item['photos'])){ ?>
    <?php   $this->addCSS("templates/{$this->name}/theme_css/lightSlider.css");
            $this->addJS("templates/{$this->name}/theme_js/jquery.lightSlider.min.js"); ?>
<?php } ?>

<?php if ($item['photo']) { ?>
    <?php $this->addCSS("templates/{$this->name}/theme_css/lightGallery.css"); ?>
    <?php $this->addJS("templates/{$this->name}/theme_js/lightGallery.min.js"); ?>
<?php } ?>

<div class="clearfix">

<?php if (!empty($fields['photos']) || !empty($item['photo'])) { ?>

    <?php if (isset($fields['photos']) && $fields['photos']['is_in_item'] && !empty($item['photos'])){ ?>
    <div class="board-photo board-Gallery">
        <ul id="imageGallery">
            <?php if ($item['photo']){ ?>
            <li data-thumb="<?php echo html_image_src($item['photo'], 'small', true); ?>" data-src="<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_full'], true); ?>">
                <?php echo html_image($item['photo'], $fields['photo']['options']['size_full'], $item['title']); ?>
            </li>
            <?php } ?>
            <?php
            $images = cmsModel::yamlToArray($item['photos']); 
              foreach($images as $paths){
                echo '<li data-thumb="/upload/' . $paths['small'].'" data-src="/upload/' . $paths['big'].'">';
                echo  '<img alt="" src="/upload/' . $paths['big'].'">';
                echo '</li>';

            } ?>
        </ul>

<script type ="text/javascript">
    $(document).ready(function() {
        if($(window).width() < 767)
            {
            $('#imageGallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:6,
                slideMargin:0,
                verticalHeight:246,
                adaptiveHeight:false,
                lang: {
                    allPhotos: 'All'
                },
                prevHtml: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextHtml: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onSliderLoad: function(plugin) {
                plugin.lightGallery();
                } 
            });
        } else {

            $('#imageGallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:4,
                vertical:true,
                verticalHeight:216,
                vThumbWidth:50,
                slideMargin:0,
                thumbMargin:4,
                prevHtml: '<span class="glyphicon glyphicon-chevron-left"></span>',
                nextHtml: '<span class="glyphicon glyphicon-chevron-right"></span>',
                onSliderLoad: function(plugin) {
                plugin.lightGallery();
                }
            });
        }      
    });
</script>
    </div>

    <?php } else if ($item['photo']) { ?>
        <div class="board-photo">
            <ul id="imageGallery" class="list-null">
                <li class="board-photo-big position-r" data-src="<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_full'], true); ?>">
                    <img src="<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_full'], true); ?>" alt="" class="img-responsive ">
                </li>
            </ul>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#imageGallery").lightGallery();
            });
        </script>
    <?php } ?>

    <?php
        unset($item['photo']); // отключаем вывод главной картинки
        unset($fields['photos']);
        $item['photo'] = "";
        $item['photos'] = ""; ?>
<?php } ?>

<div class="board-basic-info">

    <?php if ($show_bar){ ?>
        <?php if (!$item['is_pub']){ ?>
            <p class="padding-all5 bg-info overflow-h">
                <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
            </p>
        <?php } ?>                  
        <?php if (!$item['is_approved']){ ?>
            <p class="padding-all5 bg-danger overflow-h">
                <?php echo $item['is_draft'] ? LANG_CONTENT_DRAFT_NOTICE : LANG_CONTENT_NOT_APPROVED; ?>
            </p>
        <?php } ?>
    <?php } ?>

    <div class="media margin-b15">
        <?php if ($fields['user']['is_in_item']){ ?>
        <div class="media-left media-middle">
            <a class="board-avatar-circle" href="<?php echo href_to('users', $item['user']['id']); ?>" title="<?php echo $item['user']['nickname']; ?>">
                <?php echo html_avatar_image($autor['avatar'], 'micro', $autor['nickname']); ?>
            </a> 
        </div>
        <div class="media-body media-middle">
            <?php echo $fields['user']['html']; ?>
        </div>
        <?php } ?>
        <div class="media-right media-middle nowrap font-s12">
            <?php if (!empty($ctype['options']['hits_on'])){ ?>
                <span class="bar_item bi_hits" title="<?php echo LANG_HITS; ?>">
                    <span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span>
                    <?php echo $item['hits_count']; ?>
                </span>
            <?php } ?> 

            <?php if ($fields['date_pub']['is_in_item']){ ?>
                <span class="bar_item bi_date_pub" title="<?php html( $fields['date_pub']['title'] ); ?>">
                    &nbsp;
                    <span aria-hidden="true" class="glyphicon glyphicon-calendar"></span>
                    <?php echo $fields['date_pub']['html']; ?>
                </span>
            <?php } ?> 
        </div>
    </div>
                
    <?php if ($fields['title']['is_in_item']){ ?>
        <h1 class="content_title cont_<?php echo $ctype['name']; ?>_title">
            <?php if ($item['parent_id'] && !empty($ctype['is_in_groups'])){ ?>
                <div class="parent_title">
                    <a href="<?php echo href_to($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a> &rarr;
                </div>
            <?php } ?>
            <?php html($item['title']); ?>
            <?php if ($item['is_private']) { ?>
                <span class="is_private" title="<?php html(LANG_PRIVACY_HINT); ?>"></span>
            <?php } ?>
        </h1>
        <?php unset($fields['title']); ?>
    <?php } ?>

    <?php if (!empty($fields)) { ?>
        <?php $fields_fieldsets = cmsForm::mapFieldsToFieldsets($fields, function($field, $user) use ($item) {
            if (!$field['is_in_item'] || $field['is_system']) { return false; }
            if ((empty($item[$field['name']]) || empty($field['html'])) && $item[$field['name']] !== '0') { return false; }
            if ($field['groups_read'] && !$user->isInGroups($field['groups_read'])) { return false; }
            return true;
        } ); ?>

        <?php foreach ($fields_fieldsets as $fieldset_id => $fieldset) { ?>

            <?php $is_fields_group = !empty($ctype['options']['is_show_fields_group']) && $fieldset['title']; ?>

            <?php if ($is_fields_group) { ?>
                <div class="fields_group fields_group_<?php echo $ctype['name']; ?>_<?php echo $fieldset_id ?>">
                <h3 class="group_title"><?php html($fieldset['title']); ?></h3>
            <?php } ?>

            <?php if (!empty($fieldset['fields'])) { ?>
                <?php foreach ($fieldset['fields'] as $name => $field) { ?>

                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?> <?php echo $field['options']['wrap_type']; ?>_field" <?php if($field['options']['wrap_width']){ ?> style="width: <?php echo $field['options']['wrap_width']; ?>;"<?php } ?>>
                        <?php if ($field['options']['label_in_item'] != 'none') { ?>
                            <div class="title_<?php echo $field['options']['label_in_item']; ?>"><?php html($field['title']); ?>: </div>
                        <?php } ?>
                        <div class="value"><?php echo $field['html']; ?></div>
                    </div>

                <?php } ?>
            <?php } ?>

            <?php if ($is_fields_group) { ?></div><?php } ?>

        <?php } ?>

    <?php } ?>

</div>
</div>
    <?php
        $hooks_html = cmsEventsManager::hookAll("content_{$ctype['name']}_item_html", $item);
        if ($hooks_html) { echo html_each($hooks_html); }
    ?>

    <?php if ($ctype['item_append_html']){ ?>
        <div class="append_html"><?php echo $ctype['item_append_html']; ?></div>
    <?php } ?>

    <?php 
        $check_props = ($props && array_filter((array)$props_values));
        if ($check_props)     {
            $check_tags     = false;
            $check_comments = false;
        }
        else if (!empty($item['comments_widget'])) {
            $check_tags     = false;
            $check_comments = true;
        }
    ?>

<div role="tabpanel">
  <ul class="nav nav-tabs margin-b10 margin-t20" role="tablist">
    <?php if ($check_props) { ?>
        <li role="presentation" class="active">
            <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Характеристики</a>
        </li>
    <?php } ?>

    <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) &&  $item['tags']){?>
        <li<?php if ($check_tags == true) { ?> class="active"<?php } ?>>
            <a href="#tags-bar" aria-controls="tags-bar" role="tab" data-toggle="tab">Теги</a>
        </li>
    <?php } ?>
    <?php if ($fields['user']['is_in_item']){ ?>
    <?php if (!empty($item['folder_title'])){ ?>
    <li class="pull-right bi_folder">
        <a href="<?php echo href_to('users', $item['user']['id'], array('content', $ctype['name'], $item['folder_id'])); ?>">
            <span class="glyphicon glyphicon-folder-open"></span> 
            &nbsp;
            <?php echo $item['folder_title']; ?>
        </a>
    </li>
    <?php } ?>
    <?php } ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <?php if ($props && array_filter((array)$props_values)) { ?>
        <div role="tabpanel" class="tab-pane fade in active" id="home">
            <div class="content_item <?php echo $ctype['name']; ?>_item">
                <?php
                    $props_fields = $this->controller->getPropsFields($props);
                    $props_fieldsets = cmsForm::mapFieldsToFieldsets($props);
                ?>
                    <div class="content_item_props <?php echo $ctype['name']; ?>_item_props clearfix">
                        <?php foreach($props_fieldsets as $fieldset){ ?>
                            <?php if ($fieldset['title']){ ?>
                                <div class="props-item">
                                    <div class="props-head">
                                        <?php html($fieldset['title']); ?>
                                    </div>
                            <?php } ?>

                            <?php if ($fieldset['fields']){ ?>
                                <?php foreach($fieldset['fields'] as $prop){ ?>
                                    <?php if (isset($props_values[$prop['id']])) { ?>
                                    <?php $prop_field = $props_fields[$prop['id']]; ?>
                                        <div class="props-info">
                                            <div class="props-info2">
                                                <div class="title">
                                                    <?php html($prop['title']); ?>
                                                </div>
                                                <div class="value">
                                                    <?php echo $prop_field->setItem($item)->parse($props_values[$prop['id']]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>

                            <?php if ($fieldset['title']){ ?>
                                </div>
                            <?php } ?>

                        <?php } ?>
                    </div>
            </div>
        </div>
    <?php } ?>
      <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) &&  $item['tags']){?>
        <div role="tabpanel" class="tab-pane fade <?php if ($check_tags == true) { echo "in active";} ?>" id="tags-bar">
            <div class="font-s16">
                <span class="glyphicon glyphicon-tags font-s12" style="padding-right: 4px;"></span>
                <?php echo html_tags_bar($item['tags']); ?>
            </div>
        </div>
      <?php } ?>


  </div>
</div>