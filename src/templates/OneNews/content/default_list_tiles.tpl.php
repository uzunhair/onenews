<?php
    if( $ctype['options']['list_show_filter'] ) {
        $this->renderAsset('ui/filter-panel', array(
            'css_prefix'   => $ctype['name'],
            'page_url'     => $page_url,
            'fields'       => $fields,
            'props_fields' => $props_fields,
            'props'        => $props,
            'filters'      => $filters,
            'ext_hidden_params' => $ext_hidden_params,
            'is_expanded'  => $ctype['options']['list_expand_filter']
        ));
    }
?>

<?php if ($items){ ?>
    <div class="cont_list <?php echo $ctype['name']; ?>_list position-r">
        <div id="wookmark-gallery">
            <?php foreach($items as $item){ ?>
                <?php $stop = 0; ?>
                <div class="tiles <?php echo $ctype['name']; ?>_list_item<?php if (!empty($item['is_vip'])){ ?> is_vip<?php } ?>">

                    <?php if (!empty($item['fields']['photo'])) { ?>
                        <?php $preset = $fields['photo']['options']['size_teaser']; ?>
                        <div class="tiles_photo<?php if (!empty($item['is_private_item'])) { ?> tiles_photo_private<?php } ?>">
                            <?php if (!empty($item['is_private_item'])) { ?>
                                <?php echo html_image(default_images('private', $preset), $preset, $item['title']); ?>
                            <?php } else { ?>
                                <a href="<?php echo href_to($ctype['name'], $item['slug'] . '.html'); ?>">
                                    <?php if (!empty($item['photo'])) { ?>
                                        <?php echo html_image($item['photo'], $preset, $item['title']); ?>
                                    <?php } else { ?>
                                        <img src="/upload/f_project/no_img_big.jpg" alt="">
                                    <?php } ?>
                                </a>
                            <?php } ?>
                            <?php unset($item['fields']['photo']); ?>
                        </div>
                    <?php } ?>

                    <div class="tiles_fields">
                        <?php if ($fields['date_pub']['is_in_list']) { ?>
                            <div class="tiles_date" title="<?php echo $fields['date_pub']['title']; ?>">
                                <?php echo $fields['date_pub']['handler']->parse($item['date_pub']); ?>
                                <?php if ($item['is_private']) { ?>
                                    <span class="glyphicon glyphicon-eye-close pull-right" title="<?php html(LANG_PRIVACY_HINT); ?>"></span>
                                <?php } ?>
                            </div>
                        <?php } ?>

                <?php foreach($item['fields'] as $field){ ?>

                    <?php if ($stop === 2) { break; } ?>

                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?> field_<?php echo $field['label_pos']; ?>">

                        <?php if ($field['label_pos'] != 'none'){ ?>
                            <div class="title_<?php echo $field['label_pos']; ?>">
                                <?php echo $field['title'] . ($field['label_pos']=='left' ? ': ' : ''); ?>
                            </div>
                        <?php } ?>

                        <?php if ($field['name'] == 'title' && $ctype['options']['item_on']){ ?>
                            <h2 class="value">
                                <?php if (!empty($this->menus['list_actions_menu'])){ ?>
                                    <div class="list_actions_menu controller_actions_menu dropdown_menu">
                                        <input tabindex="-1" type="checkbox" id="menu_label_<?php echo $item['id']; ?>">
                                        <label for="menu_label_<?php echo $item['id']; ?>" class="group_menu_title"></label>
                                        <ul class="list_actions menu">
                                            <?php foreach($this->menus['list_actions_menu'] as $menu){ ?>
                                                <li>
                                                    <a class="<?php echo isset($menu['options']['class']) ? $menu['options']['class'] : ''; ?>" href="<?php echo string_replace_keys_values($menu['url'], $item); ?>" title="<?php html($menu['title']); ?>">
                                                        <?php echo $menu['title']; ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                                <?php if ($item['parent_id']){ ?>
                                    <a class="parent_title" href="<?php echo rel_to_href($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
                                    &rarr;
                                <?php } ?>

                                <?php if (!empty($item['is_private_item'])) { $stop++; ?>
                                    <?php html($item[$field['name']]); ?> <span class="is_private" title="<?php html($item['private_item_hint']); ?>"></span>
                                <?php } else { ?>
                                    <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>"><?php html($item[$field['name']]); ?></a>
                                    <?php if ($item['is_private']) { ?>
                                        <span class="is_private" title="<?php echo LANG_PRIVACY_HINT; ?>"></span>
                                    <?php } ?>
                                <?php } ?>
                            </h2>
                        <?php } else { ?>
                            <div class="value">
                                <?php if (!empty($item['is_private_item'])) { ?>
                                    <div class="private_field_hint"><?php echo $item['private_item_hint']; ?></div>
                                <?php } else { ?>
                                    <?php echo $field['html']; ?>
                                <?php } ?>
                            </div>
                        <?php } ?>

                    </div>

                <?php } ?>

                <?php
                $show_bar = !empty($item['rating_widget']) ||
                    $fields['date_pub']['is_in_list'] ||
                    ($ctype['is_comments'] && $item['is_comments_on']) ||
                    $fields['user']['is_in_list'] ||
                    !$item['is_approved'];
                ?>

                <?php if ($show_bar){ ?>
                    <ul class="list-inline tiles_info_bar clearfix">
                        <?php if ($fields['user']['is_in_list']){ ?>
                            <li class="bar_item pull-left" title="<?php echo $fields['user']['title']; ?>">
                                <span class="glyphicon glyphicon-user"></span>
                                <?php echo $fields['user']['handler']->parse( $item['user'] ); ?>
                            </li>
                        <?php } ?>
                        <?php if (!empty($item['rating_widget'])){ ?>
                            <li class="pull-right">
                                <span class="glyphicon glyphicon-star"></span>
                                <span>
                                    <?php echo intval($item['rating']); ?>
                                </span>
                            </li>
                        <?php } ?>
                        <?php if (!$item['is_pub']){ ?>
                            <li class="bar_item bi_not_pub">
                                <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                            </li>
                        <?php } ?>
                        <?php if (!empty($ctype['options']['hits_on'])){ ?>
                            <li class="bar_item bi_hits pull-right" title="<?php echo LANG_HITS; ?>">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                <?php echo $item['hits_count']; ?>
                            </li>
                        <?php } ?>
                        <?php if ($ctype['is_comments'] && $item['is_comments_on']){ ?>
                            <li class="pull-right">
                                <span class="glyphicon glyphicon-comment font-s12"></span>
                                <?php if (!empty($item['is_private_item'])) { ?>
                                    <?php echo intval($item['comments']); ?>
                                <?php } else { ?>
                                    <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>#comments" title="<?php echo LANG_COMMENTS; ?>">
                                        <?php echo intval($item['comments']); ?>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                        <?php if (!$item['is_approved']){ ?>
                            <li class="bar_item bi_not_approved">
                                <span class="glyphicon glyphicon-warning-sign"></span>
                                <?php echo !empty($item['is_draft']) ? LANG_CONTENT_DRAFT_NOTICE : (empty($item['is_new_item']) ? LANG_CONTENT_EDITED.'. ' : '').LANG_CONTENT_NOT_APPROVED; ?>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                </div> <!-- tiles_fields -->
            </div>

        <?php } ?>
</div>
    </div>
<?php $this->addJS("templates/{$this->name}/theme_js/imagesloaded.pkgd.min.js"); ?>
<?php $this->addJS("templates/{$this->name}/theme_js/wookmark.js"); ?>
<?php $this->addJS("templates/{$this->name}/theme_js/jquery.dotdotdot.js"); ?>

    <script type="text/javascript">
        (function () {
            function getWindowWidth() {
                return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
            }

            var wookmark;
            imagesLoaded('#wookmark-gallery', function () {
                wookmark = new Wookmark('#wookmark-gallery', {
                    itemWidth: 210,
                    offset: 15,
                    align: 'left',
                    flexibleWidth: function () {
                        return getWindowWidth() < 1024 ? '100%' : '50%';
                    }
                });
                $('.height-tiles').dotdotdot({
                    watch: 'window'
                });
            });
        })(jQuery);
    </script>

    <?php if ($perpage < $total) { ?>
        <?php echo html_pagebar($page, $perpage, $total, $page_url, array_merge($filters, $ext_hidden_params)); ?>
    <?php } ?>

<?php } else {

    if(!empty($ctype['labels']['many'])){
        echo sprintf(LANG_TARGET_LIST_EMPTY, $ctype['labels']['many']);
    } else {
        echo LANG_LIST_EMPTY;
    }

}
