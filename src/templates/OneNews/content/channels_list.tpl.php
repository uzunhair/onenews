<?php
    if( $ctype['options']['list_show_filter'] ) {
        $this->renderAsset('ui/filter-panel', array(
            'css_prefix'        => $ctype['name'],
            'page_url'          => $page_url,
            'fields'            => $fields,
            'props_fields'      => $props_fields,
            'props'             => $props,
            'filters'           => $filters,
            'ext_hidden_params' => (isset($ext_hidden_params) ? $ext_hidden_params : null),
            'is_expanded'       => $ctype['options']['list_expand_filter']
        ));
    }
?>

<?php if ($items){ ?>

    <div class="content_list tiled <?php echo $ctype['name']; ?>_list">

        <?php foreach($items as $item){ ?>

            <?php $item['ctype'] = $ctype; ?>

            <div class="full video_channels_list content_list_item">
                <div class="float_left video_tumb_block">
                    <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>" style="background-image: url(<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_teaser'], true); ?>)">
                        <?php if($item['movie_count']) { ?>
                        <span class="vitems_count">
                            <b><?php echo $item['movie_count']; ?></b> <?php echo html_spellcount_only($item['movie_count'], LANG_MOVIE_1, LANG_MOVIE_2, LANG_MOVIE_10); ?>
                        </span>
                        <?php } ?>
                    </a>
                </div>
                <div class="overflow channel_text_block">
                    <div class="channel_text">
                        <!--noindex-->
                        <div class="float-right">
                            <?php if($user->is_logged) { ?>

                                <?php if($item['user_id'] == $user->id) { ?>

                                    <a href="<?php echo ($item['subscribers_count'] ? href_to('channels', $item['slug'], 'subscribers') : ''); ?>" class="unsubscribe float_left"><i class="fa fa-check-square-o"></i> <span><?php echo LANG_VIDEO_SUBSCRIBERS; ?></span></a>

                                <?php } else { ?>

                                    <a href="#subscribe" class="subscriber float_left" data-link0="<?php echo href_to('video', 'subscribe', array($item['id'], 1)); ?>" data-link1="<?php echo href_to('video', 'subscribe', array($item['id'], 0)); ?>" data-text0="<?php echo LANG_VIDEO_SUBSCRIBE; ?>" data-text1="<?php echo LANG_VIDEO_UNSUBSCRIBE; ?>" data-issubscribe="<?php echo (in_array($item['id'], $ctype['user_subscribe']) ? 1 : 0) ?>"><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> <span></span></a>

                                <?php } ?>

                            <?php } else { ?>

                                    <a href="<?php echo href_to('auth', 'login'); ?>" rel="nofollow" class="subscribe float_left ajax-modal"><?php echo LANG_VIDEO_SUBSCRIBE; ?></a>

                            <?php } ?>
                            <span class="count-subscribers float_left"><?php echo $item['subscribers_count']; ?></span>
                        </div>
                        <!--/noindex-->
                        <h2 class="ft_caption h3">
                            <?php if ($item['parent_id']){ ?>
                                <a class="parent_title" href="<?php echo href_to($item['parent_url']); ?>">
                                    <?php echo htmlspecialchars($item['parent_title']); ?>
                                </a> &rarr;
                            <?php } ?>
                            <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>" title="<?php html($item['title']);?>">
                                <?php html($item['title']);?>
                            </a>
                        </h2>
                    </div>
                    <div class="video_content">
                        <div class="fields">
                        <?php foreach($fields as $field){ ?>

                            <?php if (empty($item[$field['name']]) ||
                                    $field['is_system'] ||
                                    !$field['is_in_list'] ||
                                    $field['name'] == 'title') { continue; } ?>
                            <?php if ($field['groups_read'] && !$user->isInGroups($field['groups_read'])) { continue; } ?>

                            <?php
                                if (!isset($field['options']['label_in_list'])) {
                                    $label_pos = 'none';
                                } else {
                                    $label_pos = $field['options']['label_in_list'];
                                }
                            ?>

                            <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">

                                <?php if ($label_pos != 'none'){ ?>
                                    <div class="title_<?php echo $label_pos; ?>"><?php echo $field['title'] . ($label_pos=='left' ? ': ' : ''); ?></div>
                                <?php } ?>

                                <div class="value">
                                    <?php echo $field['handler']->setItem($item)->parseTeaser($item[$field['name']]); ?>
                                </div>

                            </div>

                        <?php } ?>
                        </div>
                        <?php if (!empty($item['cached_latest'])){ ?>
                            <div class="latest_channel_movies overflow">
                                <?php foreach($item['cached_latest'] as $latest){ ?>
                                <div class="latest_channel_movie float_left">
                                    <a class="poster_img" href="<?php echo href_to('video', $latest['slug'].'.html'); ?>" title="<?php html($latest['title']);?>" style="background-image: url(<?php echo get_video_url('images_small', $latest['tumb_path']); ?>)">
                                        <span><?php echo duration_format($latest['duration']); ?></span>
                                    </a>
                                </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

        <?php } ?>

    </div>

    <?php if ($perpage < $total) { ?>
        <?php echo html_pagebar($page, $perpage, $total, $page_url, $filters); ?>
    <?php } ?>

<?php } else {

    if(!empty($ctype['labels']['many'])){
        echo sprintf(LANG_TARGET_LIST_EMPTY, $ctype['labels']['many']);
    } else {
        echo LANG_LIST_EMPTY;
    }

}
