<?php $user = cmsUser::getInstance(); ?>
<div class="white_del">
    <?php $this->renderChild('../channels/channel_header', array('channel' => $item, 'fields' => $fields)); ?>

    <?php unset($fields['title']); ?>
    <div class="channel-body">

        <?php
        $hooks_html = cmsEventsManager::hookAll("content_{$ctype['name']}_item_html", $item);
        if ($hooks_html) { echo html_each($hooks_html); }

        if ($ctype['item_append_html']){ ?>
            <div class="append_html"><?php echo $ctype['item_append_html']; ?></div>
        <?php } ?>
    </div>
</div>