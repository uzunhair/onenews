<?php
if( $ctype['options']['list_show_filter'] ) {
    $this->renderAsset('ui/filter-panel', array(
        'css_prefix'   => $ctype['name'],
        'page_url'     => $page_url,
        'fields'       => $fields,
        'props_fields' => $props_fields,
        'props'        => $props,
        'filters'      => $filters,
        'ext_hidden_params' => (isset($ext_hidden_params) ? $ext_hidden_params : null),
        'is_expanded'  => $ctype['options']['list_expand_filter']
    ));
}
$vconfig = cmsController::loadOptions('video');
$channel = modelVideo::getCachedResult('channel');
?>

<?php if ($items){ ?>

    <?php if(!empty($vconfig['movie_list_style']) && $vconfig['movie_list_style'] == 'table') { ?>

        <div class="field_movie_list overflow sortable_list">

        <?php foreach($items as $item){ ?>

            <?php
                $movie_link = href_to_abs($ctype['name'], $item['slug'].'.html');
                if(!empty($item['playlist_id'])){
                    $link_params = array(
                        'playlist' => $item['playlist_id']
                    );
                    $movie_link .= '?'.http_build_query($link_params, '', '&');
                }
                $is_private = $item['is_private'] && !empty($hide_except_title) && empty($item['user']['is_friend']);
                $image      = (!empty($item['photo']) ? $item['photo'] : '');
                if ($is_private && function_exists('default_images')) { // проверка для совместимости с 2.3.0
                    $image      = default_images('private', 'normal');
                    $movie_link = '#';
                }

            ?>

            <div class="iwrap <?php if($item['custom_img'] && !$is_private){ ?> start_rotation<?php } ?>" <?php if($item['custom_img'] && !$is_private){ ?> data-custom_img="<?php html(json_encode($item['custom_img'])); ?>"<?php } ?> data-movie_id="<?php echo $item['id']; ?>">
            <div class="position-r overflow">
                <div class="iwrap-thumbnail overflow">
                    <div class="player_wrap background_image <?php if($is_private) { ?>private_player_wrap<?php } ?> <?php if(!$item['is_family_friendly']){ ?> adult-stub<?php } ?>" <?php if ($image){ ?>style="background-image: url(<?php echo html_image_src($image, 'normal', true); ?>)"<?php } ?>>
                        <?php if ($image){ ?>
                            <img alt="<?php html($item['title']); ?>" src="<?php echo html_image_src($image, 'normal', true, false); ?>" />
                        <?php } ?>
                    </div>
                    <?php if(!$item['is_family_friendly']){ ?>
                        <span class="adult-icon">18+</span>
                    <?php } ?>
                    <a class="ioverlay <?php if($is_private) { ?>movie_is_private<?php } ?>" href="<?php echo $movie_link; ?>">
                    <span class="iplay">
                        <?php if (!empty($item['_cost']) && $user->id != $item['user_id']){ ?>
                            <i class="fa fa-dollar"></i> <?php echo $user->is_logged ? LANG_VIDEO_PAY : LANG_VIDEO_PAY_FROM; ?> <span><?php echo html_spellcount($item['_cost'], $item['_cost_currency']); ?></span>
                        <?php } else { ?>
                            <i class="fa fa-play-circle-o"></i> <?php echo LANG_PLAY_VIDEO; ?>
                        <?php } ?>
                    </span>
                </a>
                    <div class="iwrap-button">
                        <?php if(!empty($item['duration'])) { ?>
                            <div class="iduration <?php if($user->is_logged) { ?>onhover_hide<?php } ?>"><i class="fa fa-clock-o"></i> <?php echo duration_format($item['duration']); ?></div>
                        <?php } ?>
                        <?php if(!empty($item['is_hd'])) { ?>
                            <div class="video_is_hd" title="<?php echo LANG_VIDEO_HD; ?>">HD</div>
                        <?php } ?>
                    </div>
                    <?php if($user->is_logged) { ?>
                        <div class="addto_playlist" data-href="<?php echo href_to('video', 'list_playlists', array($item['id'])); ?>"><i class="fa fa-plus-circle"></i> <?php echo LANG_VIDEO_ADDTO; ?></div>
                    <?php } ?>

                </div>
                <a class="ifield_title" title="<?php html($item['title']); ?>" href="<?php echo $movie_link; ?>"><span><?php html($item['title']); ?></span></a>
                <?php if(!empty($item['content']) && !$is_private) { ?>
                    <div class="video_content_data sr-only"><?php echo html_clean($item['content'], 240); ?></div>
                <?php } ?>
            </div>
            </div>
        <?php } ?>
        </div>

    <?php } else { ?>

        <div class="content_list video_list sortable_list">

            <?php foreach($items as $item){ ?>

                <?php
                    $item['ctype'] = $ctype;
                    $stop = 0;
                    $movie_link = href_to_abs($ctype['name'], $item['slug'].'.html');
                    if(!empty($item['playlist_id'])){
                        $link_params = array(
                            'playlist' => $item['playlist_id']
                        );
                        $movie_link .= '?'.http_build_query($link_params, '', '&');
                    }
                    $is_private = $item['is_private'] && !empty($hide_except_title) && empty($item['user']['is_friend']);
                    $image      = (!empty($item['photo']) ? $item['photo'] : '');
                    if ($is_private && function_exists('default_images')) { // проверка для совместимости с 2.3.0
                        $image      = default_images('private', 'normal');
                        $movie_link = '#';
                    }
                ?>

                <div class="full video_list_item content_list_item<?php if (!empty($item['is_vip'])){ ?> is_vip<?php } ?>" data-movie_id="<?php echo $item['id']; ?>">
                    <div class="float_left video_tumb_block <?php if($item['custom_img'] && !$is_private){ ?> start_rotation<?php } ?>" <?php if($item['custom_img'] && !$is_private){ ?> data-custom_img="<?php html(json_encode($item['custom_img'])); ?>"<?php } ?>>
                        <a class="background_image<?php if($is_private) { ?> private_player_wrap movie_is_private<?php } ?>" href="<?php echo $movie_link; ?>" <?php if (!empty($image)){ ?>style="background-image: url(<?php echo html_image_src($image, 'normal', true); ?>)"<?php } ?>>
                            <i class="fa fa-play-circle-o"></i>
                            <img alt="<?php html($item['title']); ?>" src="<?php echo html_image_src($image, 'normal', true, false); ?>" />
                        </a>
                        <?php if($item['duration']){ ?>
                            <div class="duration"><i class="fa fa-clock-o"></i> <span title="<?php echo LANG_VIDEO_DURATION; ?>"><?php echo duration_format($item['duration']); ?></span></div>
                        <?php } ?>
                    </div>
                    <div class="overflow video_text_block">
                        <div class="video_text">
                            <h3>
                                <a itemprop="url" class="<?php if($is_private) { ?>movie_is_private<?php } ?>" href="<?php echo $movie_link; ?>"><span itemprop="name"><?php html($item['title']); ?></span></a><?php if($item['is_hd']) { ?> <span class="is_hd" title="<?php echo LANG_VIDEO_HD; ?>">HD</span><meta itemprop="videoQuality" content="HD"><?php } ?>
                            </h3>
                            <?php if ($item['parent_id']){ ?>
                                <h4 class="video_parent_title">
                                    <i class="fa fa-group"></i> <a href="<?php echo href_to($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
                                </h4>
                            <?php } ?>
                            <?php if (!empty($item['_cost']) && $user->id != $item['user_id']){ ?>
                            <div class="video_cost">
                                <i class="fa fa-dollar"></i> <?php echo $user->is_logged ? '' : LANG_FROM; ?> <span><?php echo html_spellcount($item['_cost'], $item['_cost_currency']); ?></span>
                            </div>
                            <?php } ?>
                            <div class="video_content">
                                <div>
                                    <?php if (!empty($item['editors_choice'])) { ?>
                                        <div class="editors_choice" title="<?php echo $fields['editors_choice']['title']; ?>"><i class="fa fa-check-square-o"></i></div>
                                    <?php } ?>
                                <?php foreach($fields as $field){ ?>
                                    <?php if ($stop === 2) { break; } ?>
                                    <?php if (empty($item[$field['name']]) || $field['is_system']) { continue; } ?>
                                    <?php if (!$field['is_in_list'] || $field['name'] == 'title') { continue; } ?>
                                    <?php if ($field['groups_read'] && !$user->isInGroups($field['groups_read'])) { continue; } ?>

                                    <?php
                                        if (!isset($field['options']['label_in_list'])) {
                                            $label_pos = 'none';
                                        } else {
                                            $label_pos = $field['options']['label_in_list'];
                                        }
                                    ?>

                                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">
                                        <?php if ($label_pos != 'none'){ ?>
                                            <div class="title_<?php echo $label_pos; ?>"><?php echo $field['title'] . ($label_pos=='left' ? ': ' : ''); ?></div>
                                        <?php } ?>
                                        <div class="value">
                                            <?php echo $field['handler']->setItem($item)->parseTeaser($item[$field['name']]); ?>
                                        </div>
                                    </div>

                                <?php } ?>
                                <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) && $item['tags']){ ?>
                                    <div class="tags_bar"><i class="fa fa-tags"></i> <?php echo html_tags_bar($item['tags']); ?></div>
                                <?php } ?>
                                <ul class="info_bar">
                                    <?php if ($ctype['is_rating']){ ?>
                                        <li class="bar_item">
                                            <i class="fa fa-star"></i> <?php echo $item['rating']; ?>
                                        </li>
                                    <?php } ?>
                                    <?php if ($fields['date_pub']['is_in_list']){ ?>
                                        <li class="bar_item bi_date_pub" title="<?php echo $fields['date_pub']['title']; ?>">
                                            <i class="fa fa-calendar"></i> <?php echo $fields['date_pub']['handler']->parse($item['date_pub']); ?>
                                        </li>
                                    <?php } ?>
                                    <?php if (!$item['is_pub']){ ?>
                                        <li class="bar_item bi_not_pub">
                                            <i class="fa fa-calendar"></i> <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                                        </li>
                                    <?php } ?>
                                    <?php if (!empty($ctype['options']['hits_on'])){ ?>
                                        <li class="bar_item" title="<?php echo LANG_HITS; ?>">
                                            <i class="fa fa-eye"></i> <?php echo $item['hits_count']; ?>
                                        </li>
                                    <?php } ?>
                                    <?php if ($fields['user']['is_in_list']){ ?>
                                        <li class="bar_item" title="<?php echo $fields['user']['title']; ?>">
                                            <i class="fa fa-user"></i> <?php echo $fields['user']['handler']->parse( $item['user'] ); ?>
                                        </li>
                                        <?php if (!empty($item['folder_title'])){ ?>
                                            <li class="bar_item">
                                                <i class="fa fa-folder"></i> <a href="<?php echo href_to('users', $item['user']['id'], array('content', $ctype['name'], $item['folder_id'])); ?>"><?php echo $item['folder_title']; ?></a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ($ctype['is_comments'] && $item['is_comments_on']){ ?>
                                        <li class="bar_item">
                                            <?php if (!empty($item['is_private_item'])) { ?>
                                                <i class="fa fa-comments"></i> <?php echo intval($item['comments']); ?>
                                            <?php } else { ?>
                                                <i class="fa fa-comments"></i> <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>#comments" title="<?php echo LANG_COMMENTS; ?>"><?php echo intval($item['comments']); ?></a>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                                    <?php if (!$item['is_approved']){ ?>
                                        <li class="bar_item bi_not_approved">
                                            <i class="fa fa-warning"></i> <?php echo LANG_CONTENT_NOT_APPROVED; ?>
                                        </li>
                                    <?php } ?>
                                    <?php if($user->is_logged) { ?>
                                        <li class="bar_item addto_playlist" data-href="<?php echo href_to('video', 'list_playlists', array($item['id'])); ?>"><i class="fa fa-plus-circle"></i> <?php echo LANG_VIDEO_ADDTO; ?></li>
                                    <?php } ?>
                                </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>

    <?php } ?>

    <?php if ($perpage < $total) { ?>
        <?php echo html_pagebar($page, $perpage, $total, $page_url, $filters); ?>
    <?php } ?>
<script type="text/javascript">
    <?php echo $this->getLangJS('LANG_PRIVACY_PRIVATE_HINT'); ?>
</script>
<?php } else {

    if(!empty($ctype['labels']['many'])){
        echo sprintf(LANG_TARGET_LIST_EMPTY, $ctype['labels']['many']);
    } else {
        echo LANG_LIST_EMPTY;
    }

}