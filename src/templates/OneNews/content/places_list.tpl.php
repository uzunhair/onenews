<?php

$options = $ctype['places-options'];

$this->addCSS($this->getStylesFileName('places'));
$this->addJS('templates/default/js/maps/systems/'.$options['system'].'.js');
$this->addJS('templates/default/js/maps/list.js');

$city = $options['is_city_filter'] ? cmsUser::sessionGet('maps-city') : false;
$city_name = $city ? $city['name'] : LANG_PLACES_CITY_SELECT;
$city_full = $city ? implode(', ', array($city['country'], $city['region'], $city['name'])) : false;
$center = empty($city['lat']) ? explode(':', $options['center']) : array($city['lat'], $city['lng']);

$map_filter = $filters;
if (!empty($category_id)) { $map_filter['category_id'] = $category_id;  }
if (!empty($dataset)) { $map_filter['dataset'] = $dataset;  }
if ($city) { $map_filter['city_id'] = $city['id']; }

if (!empty($filters['user_id'])){
    $options['is_city_filter'] = false;
    $options['zoom'] = $options['min_zoom'];
    unset($map_filter['city_id']);
}

$core = cmsCore::getInstance();
if ($core->uri_controller == 'tags' && $core->uri_action == 'search'){
    $map_filter['tag'] = $core->request->get('q');
}

$map_filter = http_build_query($map_filter);

if( $ctype['options']['list_show_filter'] ) {
    $this->renderAsset('ui/filter-panel', array(
        'css_prefix' => $ctype['name'],
        'page_url' => $page_url,
        'fields' => $fields,
        'props_fields' => $props_fields,
        'props' => $props,
        'filters' => $filters,
        'is_expanded' => $ctype['options']['list_expand_filter']
    ));
}

if ($user->is_admin){
    $this->addToolButton(array(
        'class' => 'settings',
        'title' => LANG_PLACES_SETTINGS,
        'href'  => href_to('admin', 'controllers', array('edit', 'places'))
    ));
}

?>

<?php $this->addJS("templates/{$this->name}/theme_js/jquery.dotdotdot.js"); ?>
    <script type="text/javascript">
        $(function() {
            $('.value_text').dotdotdot({
                watch: 'window'
            });
            $('.maps_list_title').dotdotdot({
                watch: 'window'
            });
        });
    </script>

    <div id="maps-map-block" class="maps-map-component">
        <?php if ($options['is_city_filter']) { ?>
            <div id="maps-city-selector">
                <a class="ajaxlink ajax-modal" href="<?php echo href_to('places', 'city_select'); ?>" title="<?php echo LANG_PLACES_CITY_SELECT; ?>"><?php echo $city_name; ?></a>
            </div>
        <?php } ?>
        <div id="map-canvas" style="height: <?php echo $options['height']; ?>px"></div>
    </div>

    <script>
        icms.mapsList.init('map-canvas', {
            zoom: <?php echo $options['zoom'] + 1; ?>,
            min_zoom: <?php echo $options['min_zoom'] + 1; ?>,
            max_zoom: <?php echo $options['max_zoom'] + 1; ?>,
            center: [<?php echo nf($center[0], 4); ?>, <?php echo nf($center[1], 4); ?>],
            city_addr: <?php echo $city_full && empty($city['lat']) ? "'{$city_full}'" : 'false'; ?>,
            city_id: <?php echo $city ? $city['id'] : 'false'; ?>,
            map_type: '<?php echo $options['map_type']; ?>',
            map_type_select: <?php echo $options['map_type_select'] ? 'true' : 'false'; ?>,
            scroll_zoom: <?php echo $options['scroll_zoom'] ? 'true' : 'false'; ?>,
            bounds: <?php echo $options['mk_bounds'] ? 'true' : 'false'; ?>,
            filter: '<?php echo $map_filter; ?>',
            load_url: '<?php echo href_to('places', 'markers'); ?>',
            icons_url: '<?php echo cmsConfig::get('upload_host') . '/markers'; ?>',
            balloon_url: '<?php echo href_to('places', 'balloon'); ?>',
            city_save_url: '<?php echo href_to('places', 'city_save'); ?>',
            delay: '<?php echo $options['mk_delay']; ?>'
        });
    </script>

<?php if ($items){ ?>

    <div class="content_list featured <?php echo $ctype['name']; ?>_list maps-list">

        <?php foreach($items as $item){ ?>

            <?php $item['ctype'] = $ctype; ?>

            <div class="media maps_list_item content_list_item <?php echo $ctype['name']; ?>_list_item<?php if (!empty($item['is_vip'])){ ?> is_vip<?php } ?>">

                <?php if (!empty($item['fields']['photo'])){ ?>
                    <a class="maps-list-photo" href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>"
                       style="background-image:url('<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_teaser'], true); ?>')">
                        <span class="img-responsive-div">
                            <?php echo html_image($item['photo'], $fields['photo']['options']['size_teaser']); ?>
                        </span>
                    </a>
                <?php } else { ?>
                    <a class="maps-list-photo maps-list-no-img" href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>" style="background-image:url('/upload/f_project/no_img_256.jpg')">
                        <span>
                            <img src="/upload/f_project/no_img_256.jpg" alt="">
                        </span>
                    </a>
                <?php } ?>
                <?php unset($item['photo']); ?>

                <div class="maps_list_fields fields">
                    <div class="maps_list_fields_child">
                        <?php if (isset($fields['title']) && $fields['title']['is_in_list'] && !empty($item['title'])){ ?>
                            <div class="ft_caption">
                                <a class="maps_list_title" href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>" title="<?php echo html($item['title']); ?>">
                                    <?php echo html($item['title']); ?>
                                    <?php unset($item['title']); ?>
                                </a>
                            </div>
                        <?php } ?>

                        <?php if (!empty($item['addrs'])){ ?>
                            <?php $item['addrs'] = cmsModel::yamlToArray($item['addrs']); ?>
                            <div class="maps_list_addrs addrs">
                                <ul class="list-inline margin-t5 margin-b5">
                                    <?php foreach($item['addrs'] as $addr){ ?>
                                        <li>
                                            <span class="glyphicon glyphicon-map-marker"></span>
                                            <?php html($addr); ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>

                        <?php foreach($fields as $field){ ?>

                            <?php if (empty($item[$field['name']])) { continue; } ?>
                            <?php if ($field['is_system']) { continue; } ?>
                            <?php if (!$field['is_in_list']) { continue; } ?>

                            <?php if ($field['groups_read'] && !$user->isInGroups($field['groups_read'])) { continue; } ?>

                            <?php
                            if (!isset($field['options']['label_in_list'])) {
                                $label_pos = 'none';
                            } else {
                                $label_pos = $field['options']['label_in_list'];
                            }
                            ?>

                            <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">

                                <?php if ($label_pos != 'none'){ ?>
                                    <div class="title_<?php echo $label_pos; ?>"><?php echo $field['title'] . ($label_pos=='left' ? ': ' : ''); ?></div>
                                <?php } ?>

                                <div class="value value_<?php echo $field['type']; ?>">
                                    <?php if ($field['name'] == 'title' && $ctype['options']['item_on']){ ?>

                                        <?php if ($item['parent_id']){ ?>
                                            <a class="parent_title" href="<?php echo href_to($item['parent_url']); ?>"><?php echo htmlspecialchars($item['parent_title']); ?></a>
                                            &rarr;
                                        <?php } ?>

                                        <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>"><?php echo htmlspecialchars($item[$field['name']]); ?></a>

                                    <?php } else { ?>
                                        <?php echo $field['handler']->setItem($item)->parseTeaser($item[$field['name']]); ?>
                                    <?php } ?>
                                </div>

                            </div>

                        <?php } ?>

                    </div>

                    <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) &&  $item['tags']){?>
                        <div class="tags_bar">
                            <?php echo html_tags_bar($item['tags']); ?>
                        </div>
                    <?php } ?>

                <?php
                $show_bar = $ctype['is_rating'] ||
                    $fields['date_pub']['is_in_item'] ||
                    $fields['user']['is_in_item'] ||
                    ($ctype['is_comments'] && $item['is_comments_on']) ||
                    !empty($ctype['options']['hits_on']) ||
                    !$item['is_pub'] ||
                    !$item['is_approved'];
                ?>

                <?php if ($show_bar){ ?>
                    <div class="maps_list_info">
                        <?php if ($fields['user']['is_in_list']){ ?>
                            <?php $autor=cmsCore::getModel('users')->getUser($item['user_id']);?>
                            <div class="bar_item maps_list_avatar">
                                <a class="maps-avatar-circle" href="<?php echo href_to('users', $item['user']['id']) ?>" title="<?php echo $item['user']['nickname']; ?>">
                                    <?php echo html_avatar_image($autor['avatar'], 'micro', $autor['nickname']); ?>
                                </a>
                            </div>
                            <div class="bar_item bi_user font-bold" title="<?php echo $fields['user']['title']; ?>">
                                <?php echo $fields['user']['handler']->parse( $item['user'] ); ?>
                            </div>
                            <?php if (!empty($item['folder_title'])){ ?>
                                <div class="bar_item bi_folder">
                                    <a href="<?php echo href_to('users', $item['user']['id'], array('content', $ctype['name'], $item['folder_id'])); ?>"><?php echo $item['folder_title']; ?></a>
                                </div>
                            <?php } ?>
                        <?php } ?>

                        <?php if (!$item['is_pub']){ ?>
                            <div class="bar_item bi_not_pub">
                                <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                            </div>
                        <?php } ?>

                        <?php if ($options['reviews_on'] && $options['reviews_rating']){ ?>
                            <div class="bar_item bi_rating">
                                <?php $this->renderChild('../places/stars', array('value' => $item['rating_avg'], 'class'=>'small', 'show_value'=>false)); ?>
                            </div>
                        <?php } ?>
                        <?php if ($fields['date_pub']['is_in_list']){ ?>
                            <div class="bar_item bi_date_pub text-muted" title="<?php echo $fields['date_pub']['title']; ?>">
                                <?php echo $fields['date_pub']['handler']->parse( $item['date_pub'] ); ?>
                            </div>
                        <?php } ?>
                        <?php if (!$item['is_pub']){ ?>
                            <div class="bar_item bi_not_pub">
                                <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                            </div>
                        <?php } ?>
                        <?php if (!empty($ctype['options']['hits_on'])){ ?>
                            <li class="bar_item bi_hits" title="<?php echo LANG_HITS; ?>">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                <?php echo $item['hits_count']; ?>
                            </li>
                        <?php } ?>
                        <?php if ($ctype['is_comments']){ ?>
                            <div class="bar_item nowrap maps_list_comments">
                                <span class="glyphicon glyphicon-comment font-s12" aria-hidden="true"></span> <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>#comments" title="<?php echo LANG_COMMENTS; ?>"><?php echo intval($item['comments']); ?></a>
                            </div>
                        <?php } ?>
                        <?php if ($options['reviews_on']){ ?>
                            <div class="bar_item bi_comments">
                                <a class="font-bold" href="<?php echo href_to('places', 'reviews', array($item['id'])); ?>"><?php echo html_spellcount($item['reviews_count'], LANG_PLACES_REVIEW_SPELLCOUNT); ?></a>
                            </div>
                        <?php } ?>
                        <?php if (!$item['is_approved']){ ?>
                            <div class="bar_item bi_not_approved">
                                <?php echo LANG_CONTENT_NOT_APPROVED; ?>
                            </div>
                        <?php } ?> 
                    </div>
                <?php } ?>
                </div>
            </div>

        <?php } ?>

    </div>

    <?php if ($perpage < $total) { ?>
        <?php echo html_pagebar($page, $perpage, $total, $page_url, $filters); ?>
    <?php } ?>

<?php } else {
    echo '<div class="margin-t10">';

    if(!empty($ctype['labels']['many'])){
        echo sprintf(LANG_TARGET_LIST_EMPTY, $ctype['labels']['many']);
    } else {
        echo LANG_LIST_EMPTY;
    }

    echo '</div>';

}