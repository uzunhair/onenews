<?php
    if( $ctype['options']['list_show_filter'] ) {
        $this->renderAsset('ui/filter-panel', array(
            'css_prefix'   => $ctype['name'],
            'page_url'     => $page_url,
            'fields'       => $fields,
            'props_fields' => $props_fields,
            'props'        => $props,
            'filters'      => $filters,
            'is_expanded'  => $ctype['options']['list_expand_filter']
        ));
    }
?>

<?php if ($items){ ?>
    <div class="content_list list_blogs <?php echo $ctype['name']; ?>_list">
        <?php foreach($items as $item){ ?>

            <?php $stop = 0; ?>

		<div class="cont_blogs_item <?php echo $ctype['name']; ?>_list_item<?php if (!empty($item['is_vip'])){ ?> is_vip<?php } ?>">

            <div class="blogs-items-photo img-responsive-div">

            <?php if ((isset($fields['picture']) && $fields['picture']['is_in_list'] && !empty($item['picture'])) || (isset($fields['photo']) && $fields['photo']['is_in_list'] && !empty($item['photo']))){ ?>
                    <?php if (!empty($item['is_private_item'])) { ?>
                        <?php echo html_image(default_images('private', $fields['picture']['options']['size_teaser']), $fields['picture']['options']['size_teaser'], $item['title']); ?>
                    <?php } else { ?>
                        <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>">
                            <?php echo html_image($item['picture'], $fields['picture']['options']['size_teaser'], $item['title']); ?>
                        </a>
                    <?php } ?>
                    <?php unset($item['fields']['picture']); ?>
            <?php } ?>

                <div class="blogs-photo-info text-center <?php if (isset($fields['picture']) && $fields['picture']['is_in_list'] && !empty($item['picture'])){ ?> blogs-photo-absolut<?php } ?>">
                    <?php $autor = cmsCore::getModel('users')->getUser($item['user_id']);?>
                    <?php unset($item['picture']); ?>
                    <a class="avatar-wheel display-i-b" href="<?php echo href_to('users', $item['user']['id']) ?>" title="<?php echo $item['user']['nickname']; ?>">
                        <?php echo html_avatar_image($autor['avatar'], 'micro', $autor['nickname']); ?>
                    </a>
                    <?php if ($item['parent_id']){ ?>   
                        <div class="blogs-cat font-s16">
                            <a class="parent_title" href="<?php echo href_to($item['parent_url']); ?>">
                                <?php echo htmlspecialchars($item['parent_title']); ?>
                                <?php unset($item['parent_title']); ?>
                            </a>
                        </div>
                    <?php } ?>
                        <div class="font-s22">
                            <a class="font-bold" href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>">
                                <?php echo htmlspecialchars($item['title']); ?>
                            </a>
                        </div>
                </div>      
            </div>
            <div>
                <?php
                    $show_bar = !empty($item['rating_widget']) ||
                                $fields['date_pub']['is_in_list'] ||
                        ($ctype['is_comments'] && $item['is_comments_on']) ||
                                $fields['user']['is_in_list'] ||
                                !empty($ctype['options']['hits_on']) || 
                                !$item['is_pub'] || 
                                !$item['is_approved'];
                ?>

                <?php if ($show_bar){ ?>
                    <ul class="info_bar">
                        <?php if (!empty($item['rating_widget'])){ ?>
                            <li class="bar_item bi_rating">
                                <?php echo $item['rating_widget']; ?>
                            </li>
                        <?php } ?>
                        <?php if ($fields['date_pub']['is_in_list']){ ?>
                            <li class="bar_item bi_date_pub" title="<?php echo $fields['date_pub']['title']; ?>">
                               <span aria-hidden="true" class="glyphicon glyphicon-calendar"></span>
                                <?php echo $fields['date_pub']['handler']->parse( $item['date_pub'] ); ?>
                            </li>
                        <?php } ?>
                        <?php if (!$item['is_pub']){ ?>
                            <li class="bar_item bi_not_pub">
                                <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                            </li>
                        <?php } ?>
                        <?php if (!$item['is_pub']){ ?>
                            <li class="bar_item bi_not_pub">
                                <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                            </li>
                        <?php } ?>                      
                        <?php if ($fields['user']['is_in_list']){ ?>
                            <li class="bar_item bi_user" title="<?php echo $fields['user']['title']; ?>">
                                <span class="glyphicon glyphicon-user"></span>
                                <?php echo $fields['user']['handler']->parse( $item['user'] ); ?>
                            </li>
                            <?php if (!empty($item['folder_title'])){ ?>
                                <li class="bar_item bi_folder">
                                    <span class="glyphicon glyphicon-folder-open"></span>  
                                    &nbsp;
                                    <a href="<?php echo href_to('users', $item['user']['id'], array('content', $ctype['name'], $item['folder_id'])); ?>"><?php echo $item['folder_title']; ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                        <?php if (!empty($ctype['options']['hits_on'])){ ?>
                            <li class="bar_item bi_hits" title="<?php echo LANG_HITS; ?>">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                <?php echo $item['hits_count']; ?>
                            </li>
                        <?php } ?>
                        <?php if ($ctype['is_comments'] && $item['is_comments_on']){ ?>
                            <li class="bar_item bi_comments">
                                <span class="glyphicon glyphicon-comment"></span>
                                <?php if (!empty($item['is_private_item'])) { ?>
                                    <?php echo intval($item['comments']); ?>
                                <?php } else { ?>
                                    <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>#comments" title="<?php echo LANG_COMMENTS; ?>">
                                        <?php echo intval($item['comments']); ?>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                        <?php if (!$item['is_approved']){ ?>
                            <li class="bar_item bi_not_approved <?php if (empty($item['is_new_item'])){ ?>is_edit_item<?php } ?>">
                                <?php echo !empty($item['is_draft']) ? LANG_CONTENT_DRAFT_NOTICE : (empty($item['is_new_item']) ? LANG_CONTENT_EDITED.'. ' : '').LANG_CONTENT_NOT_APPROVED; ?>
                            </li>
                        <?php } ?>                                              
                    </ul>
                <?php } ?>
            </div>
                <div class="cont_list_fields blog-list-fields margin-b5">

                <?php foreach($item['fields'] as $field){ ?>

                    <?php if ($stop === 2) { break; } ?>

                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">

                        <?php if ($field['label_pos'] != 'none'){ ?>
                            <div class="title_<?php echo $field['label_pos']; ?>">
                                <?php echo $field['title'] . ($field['label_pos']=='left' ? ': ' : ''); ?>
                            </div>
                        <?php } ?>

                            <?php if ($field['name'] != 'title'){ ?>
                                <div class="value">
                                    <?php if (!empty($item['is_private_item'])) { ?>
                                        <div class="private_field_hint"><?php echo $item['private_item_hint']; ?></div>
                                    <?php } else { ?>
                                        <?php echo $field['html']; ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>

                    </div>

                <?php } ?>

                <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) &&  $item['tags']){?>
                    <div class="tags_bar margint-t10">
					<span class="glyphicon glyphicon-tags" style="padding-right: 5px;"></span>
                        <?php echo html_tags_bar($item['tags']); ?>
                    </div>
                <?php } ?>
                </div>
            </div>

        <?php } ?>

    </div>

    <?php if ($perpage < $total) { ?>
        <?php echo html_pagebar($page, $perpage, $total, $page_url, array_merge($filters, $ext_hidden_params)); ?>
    <?php } ?>

<?php } else {

    if(!empty($ctype['labels']['many'])){
        echo sprintf(LANG_TARGET_LIST_EMPTY, $ctype['labels']['many']);
    } else {
        echo LANG_LIST_EMPTY;
    }

}
