<?php
	$user = cmsUser::getInstance();
	$options = $item['places-options'];
	$this->addCSS($this->getStylesFileName('places'));
	$this->addJS('templates/default/js/maps/systems/'.$options['system'].'.js');
	$this->addJS('templates/default/js/maps/item.js');
	$this->setMenuItems('item-menu', $item['menu-items']);

	$is_can_review = $options['reviews_on'] &&
					 cmsUser::isAllowed($ctype['name'], 'add_reviews') &&
					 !cmsUser::isPermittedLimitReached($ctype['name'], 'max_reviews', $item['user_reviews_count']) &&
					 ($user->id != $item['user_id']);

	$is_can_add_news = $options['news_on'] && $item['category']['is_news'] &&
						(
							cmsUser::isAllowed($ctype['name'], 'add_news', 'all_places') ||
							(cmsUser::isAllowed($ctype['name'], 'add_news', 'own_places') && $item['user_id'] == $user->id)
						) &&
						!cmsUser::isPermittedLimitReached($ctype['name'], 'max_news', $item['user_news_month_count']);

	$is_can_add_events = $options['events_on'] && $item['category']['is_events'] &&
						(
							cmsUser::isAllowed($ctype['name'], 'add_events', 'all_places') ||
							(cmsUser::isAllowed($ctype['name'], 'add_events', 'own_places') && $item['user_id'] == $user->id)
						) &&
						!cmsUser::isPermittedLimitReached($ctype['name'], 'max_events', $item['user_events_month_count']);

	if ($is_can_review) {
		$this->addToolButton(array(
			'class' => 'add',
			'title' => LANG_PLACES_REVIEW_ADD,
			'href'  => href_to('places', 'add_review', $item['id'])
		));
	}

	if ($is_can_add_news) {
		$this->addToolButton(array(
			'class' => 'add',
			'title' => LANG_PLACES_NEWS_ADD,
			'href'  => href_to('places', 'add_news', $item['id'])
		));
	}


	if ($is_can_add_events) {
		$this->addToolButton(array(
			'class' => 'add',
			'title' => LANG_PLACES_EVENTS_ADD,
			'href'  => href_to('places', 'add_event', $item['id'])
		));
	}

	if ($user->is_admin){
		$this->addToolButton(array(
			'class' => 'user_add',
			'title' => LANG_PLACES_CHANGE_OWNER,
			'href'  => href_to('places', 'owner', $item['id'])
		));
	}

    $markers = $item['markers'];
    $first_marker = count($markers) ? array_shift($markers) : array('id'=>false);
    unset($markers);

?>
<div itemscope itemtype="http://schema.org/Organization">
<?php if ($fields['title']['is_in_item']){ ?>
    <h1 class="cont_head">
        <?php if ($item['parent_id'] && !empty($ctype['is_in_groups'])){ ?>
            <div class="parent_title">
                <a href="<?php echo href_to($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a> &rarr;
            </div>
        <?php } ?>
        <span itemprop="name"><?php html($item['title']); ?></span>
        <?php if ($item['is_private']) { ?>
            <span class="is_private" title="<?php html(LANG_PRIVACY_HINT); ?>"></span>
        <?php } ?>
    </h1>
    <?php unset($fields['title']); ?>
<?php } ?>

<?php if ($item['menu-items'] && count($item['menu-items']) > 1){ ?>
    <div id="maps-item-menu">
        <div class="tabs-menu">
            <?php $this->menu('item-menu', true, 'nav nav-tabs'); ?>
        </div>
    </div>
<?php } ?>

<?php if ($options['reviews_on'] && $options['reviews_rating'] && $item['category']['is_reviews']) { ?>
    <div id="maps-item-rating" class="clearfix font-s16">
        <div class="col-sm-3 col-xs-6 score total">
            <div class="legend"><?php echo LANG_RATING; ?></div>
            <?php $this->renderChild('../places/stars_glyphicon', array('value' => $item['rating_avg'], 'class'=>'white')); ?>
        </div>
        <?php for($r=1; $r<=3; $r++) { ?>
            <?php if (empty($item['rating-criteria'][$r])) { continue; } ?>
            <div class="col-sm-3 col-xs-6 score rating-<?php echo $r; ?>">
                <div class="legend"><?php echo $item['rating-criteria'][$r]; ?></div>
                <?php $this->renderChild('../places/stars_glyphicon', array('value' => $item['rating'.$r], 'class'=>'white')); ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>

<div class="content_item <?php echo $ctype['name']; ?>_item">

<div class="clearfix">
<?php if (isset($fields['photos']) && $fields['photos']['is_in_item'] && !empty($item['photos'])){ ?>
    <?php
        $this->addCSS("templates/{$this->name}/theme_css/lightSlider.css");
        $this->addJS("templates/{$this->name}/theme_js/jquery.lightSlider.js");
    ?>
<?php } ?>

<?php if ($item['photo']) { ?>
    <?php $this->addCSS("templates/{$this->name}/theme_css/lightGallery.css"); ?>
    <?php $this->addJS("templates/{$this->name}/theme_js/lightGallery.min.js"); ?>
<?php } ?>

<?php if (!empty($fields['photos']) && !empty($item['photo'])) { ?>


    <?php if (isset($fields['photos']) && $fields['photos']['is_in_item'] && !empty($item['photos'])){ ?>
    <div class="board-photo board-Gallery">
        <ul id="imageGallery">
            <?php if ($item['photo']){ ?>
            <li data-thumb="<?php echo html_image_src($item['photo'], 'small', true); ?>" data-src="<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_full'], true); ?>">
                <?php echo html_image($item['photo'], $fields['photo']['options']['size_full']); ?>
            </li>
            <?php } ?>

            <?php
            $images = cmsModel::yamlToArray($item['photos']);
              foreach($images as $paths){
                echo '<li data-thumb="/upload/' . $paths['small'].'" data-src="/upload/' . $paths['big'].'">';
                echo  '<img alt="" src="/upload/' . $paths['big'].'">';
                echo '</li>';

            } ?>
        </ul>

        <script type ="text/javascript">
            $(document).ready(function() {
                if($(window).width() < 767)
                    {
                    $('#imageGallery').lightSlider({
                        gallery:true,
                        item:1,
                        thumbItem:6,
                        slideMargin:0,
                        verticalHeight:246,
                        adaptiveHeight:false,
                        prevHtml: '<span class="glyphicon glyphicon-chevron-left"></span>',
                        nextHtml: '<span class="glyphicon glyphicon-chevron-right"></span>',
                        onSliderLoad: function(plugin) {
                        plugin.lightGallery();
                        }
                    });
                } else {

                    $('#imageGallery').lightSlider({
                        gallery:true,
                        item:1,
                        thumbItem:4,
                        vertical:true,
                        verticalHeight:216,
                        vThumbWidth:50,
                        slideMargin:0,
                        thumbMargin:4,
                        prevHtml: '<span class="glyphicon glyphicon-chevron-left"></span>',
                        nextHtml: '<span class="glyphicon glyphicon-chevron-right"></span>',
                        onSliderLoad: function(plugin) {
                        plugin.lightGallery();
                        }
                    });
                }
            });
        </script>
    </div>

    <?php } else if ($item['photo']) { ?>
        <div class="board-photo">
            <ul id="imageGallery" class="list-null">
                <li class="board-photo-big position-r" data-src="<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_full'], true); ?>">
                    <img src="<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_full'], true); ?>" alt="" class="img-responsive ">
                </li>
            </ul>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#imageGallery").lightGallery();
            });
        </script>

    <?php } ?>

    <?php unset($item['photo']); // отключаем вывод главной картинки ?>
    <?php unset($fields['photos']); ?>
<?php } ?>

    <div itemprop="description">
        <?php if (!empty($fields)) { ?>

            <?php $fields_fieldsets = cmsForm::mapFieldsToFieldsets($fields, function($field, $user) use ($item) {
                if (!$field['is_in_item'] || $field['is_system']) { return false; }
                if ((empty($item[$field['name']]) || empty($field['html'])) && $item[$field['name']] !== '0') { return false; }
                if ($field['groups_read'] && !$user->isInGroups($field['groups_read'])) { return false; }
                return true;
            } ); ?>

            <?php foreach ($fields_fieldsets as $fieldset_id => $fieldset) { ?>

                <?php $is_fields_group = !empty($ctype['options']['is_show_fields_group']) && $fieldset['title']; ?>

                <?php if ($is_fields_group) { ?>
                    <div class="fields_group fields_group_<?php echo $ctype['name']; ?>_<?php echo $fieldset_id ?>">
                    <h3 class="group_title"><?php html($fieldset['title']); ?></h3>
                <?php } ?>

                <?php if (!empty($fieldset['fields'])) { ?>
                    <?php foreach ($fieldset['fields'] as $name => $field) { ?>

                        <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?> <?php echo $field['options']['wrap_type']; ?>_field" <?php if($field['options']['wrap_width']){ ?> style="width: <?php echo $field['options']['wrap_width']; ?>;"<?php } ?>>
                            <?php if ($field['options']['label_in_item'] != 'none') { ?>
                                <div class="title_<?php echo $field['options']['label_in_item']; ?>"><?php html($field['title']); ?>: </div>
                            <?php } ?>
                            <div class="value"><?php echo $field['html']; ?></div>
                        </div>

                    <?php } ?>
                <?php } ?>

                <?php if ($is_fields_group) { ?></div><?php } ?>

            <?php } ?>

        <?php } ?>
    </div>
</div> <!-- End row -->

    <?php if ($props && $props_values) { ?>
        <?php
            $props_fields = $this->controller->getPropsFields($props);
            $props_fieldsets = cmsForm::mapFieldsToFieldsets($props);
        ?>
        <div class="content_item_props <?php echo $ctype['name']; ?>_item_props clearfix">
            <?php foreach($props_fieldsets as $fieldset){ ?>
                <?php if ($fieldset['title']){ ?>
                    <div class="props-item">
                        <div class="props-head margin-b10">
                            <?php html($fieldset['title']); ?>
                        </div>
                <?php } ?>

                <?php if ($fieldset['fields']){ ?>
                    <?php foreach($fieldset['fields'] as $prop){ ?>
                        <?php if (isset($props_values[$prop['id']])) { ?>
                        <?php $prop_field = $props_fields[$prop['id']]; ?>
                            <div class="props-info">
                                <div class="margin-b10 clearfix">
                                    <div class="title">
                                        <?php html($prop['title']); ?>
                                    </div>
                                    <div class="value">
                                        <?php echo $prop_field->parse($props_values[$prop['id']]); ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>

                <?php if ($fieldset['title']){ ?>
                    </div>
                <?php } ?>

            <?php } ?>
        </div>
    <?php } ?>

    <?php if ($item['markers']) { ?>

        <div class="gui-panel maps-item-contacts row">
            <div class="col-sm-8 col-sm-push-4 maps-mini-map">
                <div id="map-canvas"></div>
                <?php if ($options['is_embed']){ ?>
                    <div class="embed-link">
                        <?php $embed_url = href_to($ctype['name'], 'embed_code', $item['id']); ?>
                        <a data-href="<?php echo $embed_url; ?>" href="<?php if (isset($first_marker['id'])){ echo $embed_url .'/' . $first_marker['id']; } ?>" class="ajax-modal" title="<?php echo LANG_PLACES_EMBED; ?>"><?php echo LANG_PLACES_EMBED; ?></a>
                    </div>
                <?php } ?>
            </div>

            <div class="col-sm-4 col-sm-pull-8">
                <div class="row maps-addrs-list">
                <h2 class="maps-addrs-title"><?php echo LANG_PLACES_ITEM_ADDR_BLOCK; ?></h2>
                <div class="position-r">
                <ul id="maps-addresses" class="list-null maps-addrs-addresses">
                    <?php foreach($item['markers'] as $marker) { ?>
                        <li class="marker-<?php echo $marker['id']; ?>" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                            <div class="clearfix">
                                <a href="#<?php echo $marker['id']; ?>" class="ajaxlink addr font-bold" data-lat="<?php echo nf($marker['lat'], 4); ?>" data-lng="<?php echo nf($marker['lng'], 4); ?>" data-id="<?php echo $marker['id']; ?>">
                                    <span>
                                        Адрес:
                                    </span>
                                    <span itemprop="addressLocality"><?php echo $marker['addr_city']; ?></span>,
                                    <span itemprop="streetAddress"><?php echo $marker['addr_street']; ?> <?php echo $marker['addr_house']; ?></span>
                                   <?php if($marker['addr_room']) { ?>- <?php echo $marker['addr_room']; ?> <?php } ?>
                                </a>
                            <?php if (empty($options['no_bh']) && !empty($marker['bhours'])){ ?>
                                <div class="bhours">
                                    <?php
                                        $dow = date('N');
                                        $day = $marker['bhours'][$dow];
                                        $text = $day['off'] ? LANG_PLACES_DAY_OFF : sprintf(LANG_PLACES_BHOURS_WORKING, $day['start'].'-'.$day['end']);
                                    ?>
                                    <a href="#bhours" data-id="<?php echo $marker['id']; ?>">
                                        <span>
                                            График работы:
                                        </span>
                                        <?php echo $text; ?>
                                    </a>
                                </div>
                            <?php } ?>
                                <?php if ($options['reviews_on']){ ?>
                                    <div class="reviews">
                                        <?php if ($options['reviews_rating']){ ?>
                                            <?php $this->renderChild('../places/stars', array('value' => $marker['rating_avg'], 'show_value'=>false, 'class'=>'small')); ?>
                                        <?php } ?>
                                        <a href="<?php echo href_to('places', 'reviews', array($item['id'])) . "?addr_id={$marker['id']}"; ?>">
                                            <?php echo html_spellcount($marker['reviews_count'], LANG_PLACES_REVIEW_SPELLCOUNT); ?>
                                        </a>
                                    </div>
                                <?php } ?>
                                <div class="maps-addrs-contacts contacts">
                                    <?php if ($marker['contacts']){ ?>
                                        <?php $this->renderChild('../places/contacts', array('contacts'=>$marker['contacts'])); ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                </div>
                <?php if (count($item['markers'])>1) {?>
                    <?php $scount = html_spellcount(count($item['markers'])-1, LANG_PLACES_ITEM_ADDR_SPELLCOUNT); ?>
                    <a href="#show-more" class="show-more-addrs ajaxlink"><?php printf(LANG_PLACES_ITEM_ADDR_MORE, $scount); ?></a>
                <?php } ?>
                </div>
            </div>

            <script>
                icms.mapsItem.init('map-canvas', {
                    url: '<?php echo href_to('places'); ?>',
                    zoom: <?php echo $options['mm_zoom'] + 1; ?>,
                    min_zoom: <?php echo $options['mm_min_zoom'] + 1; ?>,
                    max_zoom: <?php echo $options['mm_max_zoom'] + 1; ?>,
                    icon: '<?php echo $first_marker['icon'] ? $first_marker['icon'] : 'default.png'; ?>',
                    icons_url: '/<?php echo ltrim(cmsConfig::get('upload_host'), '/') . '/markers'; ?>',
                    map_type: '<?php echo $options['mm_map_type']; ?>',
                    title: '<?php echo $item['title']; ?>',
                    <?php if (!empty($options['key'])) { ?>
                    api_key: '<?php echo $options['key']; ?>'
                    <?php } ?>
                });
            </script>

        </div>

    <?php } ?>

    <?php
        $hooks_html = cmsEventsManager::hookAll("content_{$ctype['name']}_item_html", $item);
        if ($hooks_html) { echo html_each($hooks_html); }
    ?>

    <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) &&  $item['tags']){?>
        <div class="tags_bar margin-b5">
            <span class="glyphicon glyphicon-tags color-blue2 padding-r5"></span>
            <?php echo html_tags_bar($item['tags']); ?>
        </div>
    <?php } ?>

    <?php
        $show_bar = $ctype['is_rating'] ||
                    $fields['date_pub']['is_in_item'] ||
                    $fields['user']['is_in_item'] ||
            ($ctype['is_comments'] && $item['is_comments_on']) ||
					!empty($ctype['options']['hits_on']) ||
					!$item['is_pub'] ||
                    !$item['is_approved'];
    ?>

    <?php if ($ctype['item_append_html']){ ?>
        <div class="append_html"><?php echo $ctype['item_append_html']; ?></div>
    <?php } ?>

    <?php if ($show_bar){ ?>
        <ul class="info_bar margin-t10">
            <?php if ($ctype['is_rating']){ ?>
                <li class="bar_item bi_rating">
                    <?php echo $item['rating_widget']; ?>
                </li>
            <?php } ?>
            <?php if ($fields['date_pub']['is_in_item']){ ?>
                <li class="bar_item bi_date_pub" title="<?php html( $fields['date_pub']['title'] ); ?>">
                    <span class="glyphicon glyphicon-calendar"></span>
                    <?php echo $fields['date_pub']['html']; ?>
                </li>
            <?php } ?>
            <?php if (!$item['is_pub']){ ?>
                <li class="bar_item bi_not_pub">
                    <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                </li>
            <?php } ?>
            <?php if (!empty($ctype['options']['hits_on'])){ ?>
                <li class="bar_item bi_hits" title="<?php echo LANG_HITS; ?>">
                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                    <?php echo $item['hits_count']; ?>
                </li>
            <?php } ?>
            <?php if ($fields['user']['is_in_item']){ ?>
                <li class="bar_item bi_user" title="<?php html( $fields['user']['title'] ); ?>">
                    <?php echo $fields['user']['html']; ?>
                </li>
                <?php if (!empty($item['folder_title'])){ ?>
                    <li class="bar_item bi_folder">
                        <a href="<?php echo href_to('users', $item['user']['id'], array('content', $ctype['name'], $item['folder_id'])); ?>"><?php echo $item['folder_title']; ?></a>
                    </li>
                <?php } ?>
            <?php } ?>
            <?php if (!empty($this->options['share_link'])){ ?>
            <li class="bar_item bi_share">
                <div class="share dark-share">
                   <?php echo $this->options['share_link']; ?>
                </div>
            </li>
            <?php } ?>
            <?php if (!$item['is_approved']){ ?>
                <li class="bar_item bi_not_approved">
                    <?php echo LANG_CONTENT_NOT_APPROVED; ?>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>

</div>
<div>