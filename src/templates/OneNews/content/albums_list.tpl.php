<?php
$this->addCSS($this->getStylesFileName('photos'));

if( $ctype['options']['list_show_filter'] ) {
    $this->renderAsset('ui/filter-panel', array(
        'css_prefix'   => $ctype['name'],
        'page_url'     => $page_url,
        'fields'       => $fields,
        'props_fields' => $props_fields,
        'props'        => $props,
        'filters'      => $filters,
        'ext_hidden_params' => $ext_hidden_params,
        'is_expanded'  => $ctype['options']['list_expand_filter']
    ));
}
?>

<?php if ($items){ ?>
<div class="white-del-standart">
<div class="position-r">
    <div id="wookmark-gallery" class="content_list tiled <?php echo $ctype['name']; ?>_list">
    <?php foreach($items as $item){ ?>
        <?php $stop = 0; ?>
    <div class="wookmark_item">
        <div class="photo">
            <?php if (!empty($item['is_private_item'])) { ?>
                <?php echo html_image(default_images('private', $ctype['photos_options']['preset_small']), $ctype['photos_options']['preset_small'], $item['title']); ?>
            <?php } else { ?>
                <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>">
                    <?php if (!empty($item['cover_image']) && !empty($fields['cover_image']['is_in_list'])){ ?>
                        <?php echo html_image($item['cover_image'], $ctype['photos_options']['preset_small'], $item['title']); ?>
                        <?php unset($item['cover_image']); ?>
                    <?php } else { ?>
                        <img src="/upload/f_project/no_img_big.jpg" alt="">
                    <?php } ?>
                    <span class="photos_count">
                        <?php echo html_spellcount($item['photos_count'], LANG_PHOTOS_PHOTO_SPELLCOUNT); ?>
                        <?php if ($item['is_public'] && !empty($fields['is_public']['is_in_list'])) { ?>
                            / <span><?php echo LANG_PHOTOS_PUBLIC_ALBUM; ?></span>
                        <?php } ?>
                    </span>
                </a>
            <?php } ?>
            <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) &&  $item['tags']){?>
                <div class="photo-note">
                    <div class="tags_bar">
                        <?php echo html_tags_bar($item['tags']); ?>
                    </div>
                </div>
            <?php } ?>
        </div>

    <div class="photo-info">
        <div class="fields padding-all10">
            <div class="photos_album_title_wrap">
                <div class="photos_album_title">
                    <?php if ($item['parent_id']) { ?>
                        <?php echo htmlspecialchars($item['parent_title']); ?> &rarr;
                    <?php } ?>
                    <?php if (!empty($item['is_private_item'])) { ?>
                        <?php html($item['title']); ?> <span class="is_private" title="<?php html($item['private_item_hint']); ?>"></span>
                    <?php } else { ?>
                        <?php html($item['title']); ?>
                        <?php if ($item['is_private']) { ?>
                            <span class="glyphicon glyphicon-eye-close font-s12" title="<?php html(LANG_PRIVACY_HINT); ?>"></span>
                        <?php } ?>
                    <?php } ?>
                </div>
                <?php if (!empty($item['fields']['content'])) { ?>
                    <div class="photos_album_description_wrap">
                        <div class="photos_album_description">
                            <?php echo $item['fields']['content']['html']; ?>
                        </div>
                    </div>
                <?php } ?>
                <?php unset($item['fields']['cover_image'], $item['fields']['title'], $item['fields']['content'], $item['fields']['is_public']); ?>
            </div>

                <?php foreach($item['fields'] as $field){ ?>

                    <?php if ($stop === 2) { break; } ?>

                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">

                        <?php if ($field['label_pos'] != 'none'){ ?>
                            <div class="title_<?php echo $field['label_pos']; ?>">
                                <?php echo $field['title'] . ($field['label_pos']=='left' ? ': ' : ''); ?>
                            </div>
                        <?php } ?>

                        <div class="value">
                            <?php if (!empty($item['is_private_item'])) { $stop++; ?>
                                <div class="private_field_hint"><?php echo $item['private_item_hint']; ?></div>
                            <?php } else { ?>
                                 <?php echo $field['html']; ?>
                            <?php } ?>
                        </div>

                    </div>

                <?php } ?>

                </div>

        <?php $show_bar = $ctype['is_rating'] ||
            $fields['date_pub']['is_in_list'] ||
            $fields['user']['is_in_list'] ||
            ($ctype['is_comments'] && $item['is_comments_on']) ||
            !$item['is_approved'];
        ?>
        <?php if ($show_bar){ ?>
        <div class="info-bar-user media">
            <?php if ($fields['user']['is_in_list']){ ?>
            <div class="media-left media-middle bar-user-title">
                <?php $autor = cmsCore::getModel('users')->getUser($item['user_id']);?>
                <a class="avatar-circle pull-left" href="<?php echo href_to('users', $item['user']['id']) ?>" title="<?php echo $item['user']['nickname']; ?>">
                    <?php echo html_avatar_image($autor['avatar'], 'micro', $autor['nickname']); ?>
                </a> 
            </div>
            <div class="media-body media-middle">
                <a href="<?php echo href_to('users', $item['user']['id']) ?>" class="bar-user-title-a height-20">
                    <?php echo ($item['user']['nickname']); ?>
                </a>
            </div>
            <?php } ?>
            <div class="media-right media-middle nowrap">
                <?php if ($ctype['is_comments'] && $item['is_comments_on']){ ?>
                    <span class="bar-user-item">
                        <?php if (!empty($item['is_private_item'])) { ?>
                            <span class="glyphicon glyphicon-comment font-s12"></span>
                            <?php echo intval($item['comments']); ?>
                        <?php } else { ?>
                            <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>#comments" title="<?php echo LANG_COMMENTS; ?>">
                                 <span class="glyphicon glyphicon-comment font-s12"></span>
                                <?php echo intval($item['comments']); ?>
                            </a>
                        <?php } ?>
                    </span>
                    &nbsp;
                <?php } ?>
                <?php if ($ctype['is_rating']){ ?>
                    <span class="bar-user-item">
                        <span class="glyphicon glyphicon-star"></span>
                        <span>
                            <?php echo $item['rating_widget']; ?>
                        </span>
                    </span>
                <?php } ?>
            </div>

                <?php if (!$item['is_approved']){ ?>
                    <div class="bar_item bi_not_approved <?php if (empty($item['is_new_item'])){ ?>is_edit_item<?php } ?>">
                        <?php echo !empty($item['is_draft']) ? LANG_CONTENT_DRAFT_NOTICE : (empty($item['is_new_item']) ? LANG_CONTENT_EDITED.'. ' : '').LANG_CONTENT_NOT_APPROVED; ?>
                    </div>
                <?php } ?>

            </div>
            <?php } ?>
        </div>
    </div>
    
        <?php } ?>

    </div>
</div>
</div>
<?php $this->addJS("templates/{$this->name}/theme_js/imagesloaded.pkgd.min.js"); ?>
<?php $this->addJS("templates/{$this->name}/theme_js/wookmark.js"); ?>

  <script type="text/javascript">
    (function() {
      function getWindowWidth() {
        return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
      }
      var wookmark;
      imagesLoaded('#wookmark-gallery', function () {
        wookmark = new Wookmark('#wookmark-gallery', {
          itemWidth: 210,
          offset: 15,
          align: 'left',
        flexibleWidth: function () {
            return getWindowWidth() < 1024 ? '100%' : '50%';
          }
        });
      });
    })(jQuery);
  </script>

    <?php if ($perpage < $total) { ?>
        <?php echo html_pagebar($page, $perpage, $total, $page_url, array_merge($filters, $ext_hidden_params)); ?>
    <?php } ?>

    <?php $this->addJS("templates/{$this->name}/theme_js/jquery.dotdotdot.js"); ?>
    <script type="text/javascript">
        $(function() {
            $('.height-20').dotdotdot({
                watch: 'window'
            });
        });
    </script>

<?php  } else {

    if(!empty($ctype['labels']['many'])){
        echo sprintf(LANG_TARGET_LIST_EMPTY, $ctype['labels']['many']);
    } else {
        echo LANG_LIST_EMPTY;
    }

}
