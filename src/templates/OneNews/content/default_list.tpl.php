<?php
    if( $ctype['options']['list_show_filter'] ) {
        $this->renderAsset('ui/filter-panel', array(
            'css_prefix'   => $ctype['name'],
            'page_url'     => $page_url,
            'fields'       => $fields,
            'props_fields' => $props_fields,
            'props'        => $props,
            'filters'      => $filters,
            'ext_hidden_params' => $ext_hidden_params,
            'is_expanded'  => $ctype['options']['list_expand_filter']
        ));
    }
?>

<?php if ($items){ ?>

    <div class="content_list default_list <?php echo $ctype['name']; ?>_list">

        <?php foreach($items as $item){ ?>

            <?php $stop = 0; ?>
			<div class="media cont_list_item <?php echo $ctype['name']; ?>_list_item<?php if (!empty($item['is_vip'])){ ?> is_vip<?php } ?>">

            <?php if (!empty($item['fields']['photo'])){ ?>
                <?php if (!empty($item['is_private_item'])) { ?>
                    <div class="cont_list_photo img-responsive-div cont_list_private">
                        <?php echo html_image(default_images('private', $fields['photo']['options']['size_teaser']), $fields['photo']['options']['size_teaser'], $item['title']);
                        unset($item['fields']['photo']); ?>
                    </div>
                <?php } else { ?>
                    <div class="cont_list_photo img-responsive-div <?php if (!empty($item['is_private_item'])) { ?>cont_list_private<?php } ?>">
                        <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>"
                           <?php if (!empty($item['photo']) && !empty($fields['photo']['is_in_list'])){ ?>style="background-image: url(<?php echo html_image_src($item['photo'], $fields['photo']['options']['size_teaser'], true); ?>);"
                            <?php } else {?>
                                style="background-image: url('/upload/f_project/no_img_256.jpg');"
                            <?php } ?>>
                            <?php if (!empty($item['photo']) && !empty($fields['photo']['is_in_list'])){ ?>
                                <span class="sr-only">
                                    <?php echo html_image($item['photo'], $fields['photo']['options']['size_teaser'], $item['title']);
                                    unset($item['fields']['photo']); ?>
                                </span>
                            <?php } ?>
                        </a>
                    </div>
                <?php } ?>
            <?php } ?>

                <div class="cont_list_fields">
                <?php foreach($item['fields'] as $field){ ?>

                    <?php if ($stop === 2) { break; } ?>

                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">

                        <?php if ($field['label_pos'] != 'none'){ ?>
                            <div class="title_<?php echo $field['label_pos']; ?>">
                                <?php echo $field['title'] . ($field['label_pos']=='left' ? ': ' : ''); ?>
                            </div>
                        <?php } ?>


                        <?php if ($field['name'] == 'title' && $ctype['options']['item_on']){ ?>
                            <h4 class="margin-t0 margin-b5">
                                <!--@todo Проверить данный блок, и добавить ко всем спискам где это необходимо -->
                                <?php if (!empty($this->menus['list_actions_menu'])){ ?>
                                    <div class="list_actions_menu controller_actions_menu dropdown_menu">
                                        <input tabindex="-1" type="checkbox" id="menu_label_<?php echo $item['id']; ?>">
                                        <label for="menu_label_<?php echo $item['id']; ?>" class="group_menu_title"></label>
                                        <ul class="list_actions menu">
                                            <?php foreach($this->menus['list_actions_menu'] as $menu){ ?>
                                                <li>
                                                    <a class="<?php echo isset($menu['options']['class']) ? $menu['options']['class'] : ''; ?>" href="<?php echo string_replace_keys_values($menu['url'], $item); ?>" title="<?php html($menu['title']); ?>">
                                                        <?php echo $menu['title']; ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                            <?php if ($item['parent_id']){ ?>
                                <a class="parent_title" href="<?php echo rel_to_href($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
                                &rarr;
                            <?php } ?>

                            <?php if (!empty($item['is_private_item'])) { $stop++; ?>
                                <?php html($item[$field['name']]); ?> <span class="is_private" title="<?php html($item['private_item_hint']); ?>"></span>
                            <?php } else { ?>
                                <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>"><?php html($item[$field['name']]); ?></a>
                                <?php if ($item['is_private']) { ?>
                                    <span class="is_private" title="<?php html(LANG_PRIVACY_HINT); ?>"></span>
                                <?php } ?>
                            <?php } ?>
                            </h4>
                        <?php } else { ?>
                            <div class="value">
                                <?php if (!empty($item['is_private_item'])) { ?>
                                    <div class="private_field_hint"><?php echo $item['private_item_hint']; ?></div>
                                <?php } else { ?>
                                    <?php echo $field['html']; ?>
                                <?php } ?>
                            </div>
                        <?php } ?>

                    </div>

                <?php } ?>

                    <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) &&  $item['tags']){?>
                    <div class="tags_bar">
					<span class="glyphicon glyphicon-tags" style="padding-right: 2px;"></span>
                        <?php echo html_tags_bar($item['tags']); ?>
                    </div>
                <?php } ?>

                <?php
					$show_bar = !empty($item['rating_widget']) ||
                        $fields['date_pub']['is_in_list'] ||
                        $fields['user']['is_in_list'] ||
                        ($ctype['is_comments'] && $item['is_comments_on']) ||
                        !empty($ctype['options']['hits_on']) ||
                        !$item['is_pub'] ||
                        !$item['is_approved'];
                ?>

                <?php if ($show_bar){ ?>
                    <ul class="info_bar">
                        <?php if (!empty($item['rating_widget'])){ ?>
                            <li class="bar_item bi_rating">
                                <?php echo $item['rating_widget']; ?>
                            </li>
                        <?php } ?>
                        <?php if ($fields['date_pub']['is_in_list']){ ?>
                            <li class="bar_item bi_date_pub" title="<?php echo $fields['date_pub']['title']; ?>">
                               <span aria-hidden="true" class="glyphicon glyphicon-calendar"></span>
                                <?php echo $fields['date_pub']['handler']->parse( $item['date_pub'] ); ?>
                            </li>
                        <?php } ?>
                        <?php if (!$item['is_pub']){ ?>
                            <li class="bar_item bi_not_pub">
                                <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                            </li>
                        <?php } ?>
                        <?php if ($fields['user']['is_in_list']){ ?>
                            <li class="bar_item bi_user" title="<?php echo $fields['user']['title']; ?>">
                                <span class="glyphicon glyphicon-user padding-r5"></span>
								<?php echo $fields['user']['handler']->parse( $item['user'] ); ?>
                            </li>
                            <?php if (!empty($item['folder_title'])){ ?>
                                <li class="bar_item bi_folder">
									<span class="glyphicon glyphicon-folder-open"></span>
                                    <a href="<?php echo href_to('users', $item['user']['id'], array('content', $ctype['name'], $item['folder_id'])); ?>"><?php echo $item['folder_title']; ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                        <?php if (!empty($ctype['options']['hits_on'])){ ?>
                            <li class="bar_item bi_hits" title="<?php echo LANG_HITS; ?>">
                                <span class="glyphicon glyphicon-eye-open"></span>
                                <?php echo $item['hits_count']; ?>
                            </li>
                        <?php } ?>
                        <?php if ($ctype['is_comments'] && $item['is_comments_on']){ ?>
                            <li class="bar_item bi_comments">
                                <span class="glyphicon glyphicon-comment"></span>
                                <?php if (!empty($item['is_private_item'])) { ?>
                                    <?php echo intval($item['comments']); ?>
                                <?php } else { ?>
                                    <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>#comments" title="<?php echo LANG_COMMENTS; ?>">
                                        <?php echo intval($item['comments']); ?>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                        <?php if (!$item['is_approved']){ ?>
                            <li class="bar_item bi_not_approved <?php if (empty($item['is_new_item'])){ ?>is_edit_item<?php } ?>">
                                <?php echo !empty($item['is_draft']) ? LANG_CONTENT_DRAFT_NOTICE : (empty($item['is_new_item']) ? LANG_CONTENT_EDITED.'. ' : '').LANG_CONTENT_NOT_APPROVED; ?>
                            </li>
                        <?php } ?>												
                    </ul>
                <?php } ?>
                </div>
            </div>

        <?php } ?>

    </div>

    <?php if ($perpage < $total) { ?>
        <?php echo html_pagebar($page, $perpage, $total, $page_url, array_merge($filters, $ext_hidden_params)); ?>
    <?php } ?>

<?php } else {

    if(!empty($ctype['labels']['many'])){
        echo sprintf(LANG_TARGET_LIST_EMPTY, $ctype['labels']['many']);
    } else {
        echo LANG_LIST_EMPTY;
    }

}

