<?php

$user    = cmsUser::getInstance();
$vconfig = cmsController::loadOptions('video');

if($user->is_admin && $item['provider_obj']->is_can_download){

    $download_status = provider::getMovieQueue($item['id'], 'process_download');

    if(!$download_status){
        $this->addToolButton(array(
            'class' => 'download',
            'title' => LANG_VIDEO_UNBIND,
            'confirm' => LANG_VIDEO_UNBIND_CONFIRM,
            'href'  => href_to('video', 'unbind', $item['id'])
        ));
    } else {
        cmsUser::addSessionMessage($download_status['status'], 'info');
    }

}
if($user->is_admin && $item['provider'] == 'localhost'){

    $this->addToolButton(array(
        'class' => 'images',
        'title' => LANG_VIDEO_RECRATE_TMB,
        'confirm' => LANG_VIDEO_RECRATE_TMB_HINT,
        'href'  => href_to('video', 'recreate_thumb', $item['id'])
    ));

}
if($item['provider'] != 'localhost' && ((cmsUser::isAllowed('video', 'replace') && $user->id == $item['user_id']) || $user->is_admin)){

    $this->addToolButton(array(
        'title' => LANG_VIDEO_REPLACE,
        'href'  => href_to('video', 'replace', $item['id'])
    ));

}

// Поддержка oembed.com
if(!empty($vconfig['enable_oembed_json'])){

    $this->addHead('<link rel="alternate" type="application/json+oembed" href="'.href_to_abs('video', 'oembed').'?url='.urlencode(href_to_abs('video', $item['slug'].'.html')).'&format=json" title="'.htmlspecialchars($item['title']).'" />');

}
if(!empty($vconfig['enable_oembed_xml'])){

    $this->addHead('<link rel="alternate" type="text/xml+oembed" href="'.href_to_abs('video', 'oembed').'?url='.urlencode(href_to_abs('video', $item['slug'].'.html')).'&format=xml" title="'.htmlspecialchars($item['title']).'" />');

}
$is_access_by_link = $item['is_parent_hidden'] && !$item['is_private'];
?>


<div itemscope itemtype="http://schema.org/VideoObject" class="content_item <?php echo $ctype['name']; ?>_item">

    <div class="page_player"><?php echo $item['player_html']; ?></div>
    <?php if ($fields['title']['is_in_item']) { ?>
        <h1 class="nowrap_text video-title">
            <?php if ($item['is_hd']) { ?>
                <span class="is_hd" title="<?php echo LANG_VIDEO_HD; ?>">HD</span>
                <meta itemprop="videoQuality" content="HD">
            <?php } else { ?>
                <meta itemprop="videoQuality" content="medium">
            <?php } ?>
            <span itemprop="name"><?php html($item['title'] . (!empty($item['playlist']['title']) ? ', ' . $item['playlist']['title'] : '')); ?></span>
            <?php if ($item['is_private']) { ?>
                <span class="is_private" title="<?php html(LANG_PRIVACY_PRIVATE); ?>"></span>
            <?php } ?>
        </h1>
        <?php if ($item['parent_id'] && !empty($ctype['is_in_groups'])) { ?>
            <h3 class="video_parent_title">
                <i class="fa fa-group"></i> <a
                        href="<?php echo href_to($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
            </h3>
        <?php } ?>
        <?php unset($fields['title']); ?>
    <?php } ?>

    <?php if (!empty($item['playlist_movie'])) { ?>
        <?php $this->renderControllerChild('video', 'playlist_movie', array(
            'movies' => $item['playlist_movie'],
            'current_id' => $item['id'],
            'playlist' => $item['playlist']
        )); ?>
    <?php } ?>
    <?php if ($item['duration']) { ?>
        <meta itemprop="duration" content="<?php echo time_to_iso8601_duration($item['duration']); ?>">
    <?php } ?>
    <?php if ($item['is_family_friendly']) { ?>
        <meta itemprop="isFamilyFriendly" content="true">
    <?php } else { ?>
        <meta itemprop="isFamilyFriendly" content="false">
    <?php } ?>
    <meta itemprop="uploadDate" content="<?php echo date('c', strtotime($item['date_pub'])); ?>">

    <div class="full top10 bottom10">
        <?php if ($fields['user']['is_in_item']) { ?>
            <div class="float_left user-box">
                <?php if ($item['channel']) { ?>

                    <a title="<?php echo $item['channel']['title']; ?>"
                       href="<?php echo href_to('channels', $item['channel']['slug'] . '.html'); ?>"
                       class="float_left userav">
                        <?php echo html_avatar_image($item['channel']['photo'], 'small'); ?>
                    </a>
                    <div class="float-right subscribers">
                        <h3>
                            <a title="<?php echo $item['channel']['title']; ?>"
                               href="<?php echo href_to('channels', $item['channel']['slug'] . '.html'); ?>"><?php echo $item['channel']['title']; ?></a>
                            <span><?php echo html_spellcount($item['channel']['movie_count'], LANG_MOVIE_1, LANG_MOVIE_2, LANG_MOVIE_10); ?></span>
                        </h3>
                        <?php if ($user->is_logged) { ?>

                            <?php if ($item['channel']['user_id'] == $user->id) { ?>

                                <a href="<?php echo($item['channel']['subscribers_count'] ? href_to('channels', $item['channel']['slug'], 'subscribers') : ''); ?>"
                                   class="unsubscribe float_left"><i class="fa fa-check-square-o"></i>
                                    <span><?php echo LANG_VIDEO_SUBSCRIBERS; ?></span></a>

                            <?php } else { ?>

                                <a href="#subscribe" class="subscriber float_left"
                                   data-link0="<?php echo href_to('video', 'subscribe', array($item['channel_id'], 1)); ?>"
                                   data-link1="<?php echo href_to('video', 'subscribe', array($item['channel_id'], 0)); ?>"
                                   data-text0="<?php echo LANG_VIDEO_SUBSCRIBE; ?>"
                                   data-text1="<?php echo LANG_VIDEO_UNSUBSCRIBE; ?>"
                                   data-issubscribe="<?php echo $item['is_subscribe'] ?>"><i class="fa fa-square-o"></i><i
                                            class="fa fa-check-square-o"></i> <span></span></a>

                            <?php } ?>

                        <?php } else { ?>

                            <a href="<?php echo href_to('auth', 'login'); ?>" rel="nofollow"
                               class="subscribe float_left ajax-modal"><?php echo LANG_VIDEO_SUBSCRIBE; ?></a>

                        <?php } ?>
                        <span class="count-subscribers float_left"><?php echo $item['channel']['subscribers_count']; ?></span>
                    </div>

                <?php } else {
                    if (!$user->isPrivacyAllowed($item['user'], 'view_user_video')) {
                        $profiel_link = href_to('users', $item['user']['id']);
                    } else {
                        $profiel_link = href_to('users', $item['user']['id'], array('content', 'video'));
                    } ?>
                    <div class="media">
                        <div class="media-left">
                            <a title="<?php echo $item['user']['nickname']; ?>" href="<?php echo $profiel_link; ?>"
                               class="userav margin-null">
                                <?php echo html_avatar_image($item['user']['avatar'], 'small', $item['user']['nickname']); ?>
                            </a>
                        </div>
                        <div class="media-left media-middle">
                            <h3>
                                <a title="<?php echo $item['user']['nickname']; ?>"
                                   href="<?php echo $profiel_link; ?>"><?php echo $item['user']['nickname']; ?></a>
                                <span><?php echo html_spellcount($item['user']['movie_count'], LANG_MOVIE_1, LANG_MOVIE_2, LANG_MOVIE_10); ?></span>
                            </h3>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="like-box float-right">
            <div class="like-views">
                <?php if (!empty($ctype['options']['native_hits_on'])) { ?>
                    <?php echo html_spellcount($item['hits_count'], LANG_HITS_1, LANG_HITS_2, LANG_HITS_10); ?>
                <?php } ?>
            </div>
            <?php if (!empty($item['rating_details'])) { ?>
                <div class="watch-sparkbars"
                     title="<?php echo sprintf(LANG_LIKE_UNLIKE, $item['rating_details']['like'], $item['rating_details']['unlike']); ?>">
                    <div style="width: <?php echo $item['rating_details']['like_percent']; ?>%"
                         class="watch-sparkbar-likes"></div>
                    <div style="width: <?php echo $item['rating_details']['unlike_percent']; ?>0%"
                         class="watch-sparkbar-dislikes"></div>
                </div>
            <?php } ?>
            <?php if (!empty($item['rating_widget'])) { ?>
                <div class="like-show">
                    <?php echo $item['rating_widget']; ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="full bottom10">
        <ul class="nav-tabs">
            <li class="active">
                <a href="#" data-id="item-info"><i class="fa fa-film"></i> <?php echo LANG_VIDEO_ABOUT; ?></a>
            </li>
            <li>
                <a href="#" data-id="item-share" id="item-share-tab"><i
                            class="fa fa-share-alt"></i> <?php echo LANG_VIDEO_SHARE; ?></a>
            </li>
            <?php if ($item['provider_obj']->data['play_custom_player'] && ($item['play_file_paths'] || $item['ext'])) { ?>
                <li>
                    <a title="<?php html(sprintf(LANG_VIDEO_DOWNLOAD_TEXT, $item['title'])); ?>"
                       href="<?php echo href_to('video', 'download', array($item['id'])); ?>" data-id="item-download"><i
                                class="fa fa-download"></i> <?php echo LANG_VIDEO_DOWNLOAD; ?></a>
                </li>
            <?php } ?>
            <?php if ($user->is_logged && cmsUser::isAllowed('channels', 'add')) { ?>
                <li>
                    <a href="<?php echo href_to('video', 'list_playlists', array($item['id'])); ?>"
                       data-id="item-addto"><i class="fa fa-plus"></i> <?php echo LANG_VIDEO_ADDTO; ?></a>
                </li>
            <?php } ?>
            <?php if ($user->id != $item['user_id']) { ?>
                <li>
                    <a href="<?php echo href_to('video', 'complain', array($item['id'])); ?>" data-id="item-report"><i
                                class="fa fa-flag"></i> <?php echo LANG_VIDEO_REPORT; ?></a>
                </li>
            <?php } ?>
            <?php if (cmsUser::isAllowed('video', 'view_stat', 'all') || (cmsUser::isAllowed('video', 'view_stat', 'own_video') && $item['user_id'] == $user->id)) { ?>
                <?php if (!empty($ctype['options']['native_hits_on']) && !empty($vconfig['anti_cheat'])) { ?>
                    <li>
                        <a href="<?php echo href_to('video', 'show_hit_stats', array($item['id'])); ?>"
                           data-id="item-stat"><i class="fa fa-bar-chart"></i> <?php echo LANG_VIDEO_STATS; ?></a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>

    <div id="item-info" class="video_tab">
        <div class="video_description_block float_left">
            <div itemprop="description">
                <?php foreach ($fields as $name => $field) { ?>

                    <?php if (!$field['is_in_item'] || $field['is_system']) {
                        continue;
                    } ?>
                    <?php if ((empty($item[$field['name']]) || empty($field['html'])) && $item[$field['name']] !== '0') {
                        continue;
                    } ?>
                    <?php if ($field['groups_read'] && !$user->isInGroups($field['groups_read'])) {
                        continue;
                    } ?>

                    <?php
                    if (!isset($field['options']['label_in_item'])) {
                        $label_pos = 'none';
                    } else {
                        $label_pos = $field['options']['label_in_item'];
                    }
                    ?>

                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">

                        <?php if ($label_pos != 'none'){ ?>
                            <div class="title_<?php echo $label_pos; ?>"><?php html($field['title']); ?>: </div>
                        <?php } ?>
                        <div class="value"
                             <?php if ($field['name'] == 'content') { ?>itemprop="description"<?php } ?>><?php echo $field['html']; ?></div>
                    </div>

                <?php } ?>
            </div>
            <?php if ($props && array_filter((array)$props_values)) { ?>
                <?php
                $props_fields = $this->controller->getPropsFields($props);
                $props_fieldsets = cmsForm::mapFieldsToFieldsets($props);
                ?>
                <div class="content_item_props <?php echo $ctype['name']; ?>_item_props clearfix">
                    <?php foreach ($props_fieldsets as $fieldset) { ?>
                        <?php if ($fieldset['title']) { ?>
                            <div class="props-item">
                            <div class="props-head">
                                <?php html($fieldset['title']); ?>
                            </div>
                        <?php } ?>

                        <?php if ($fieldset['fields']) { ?>
                            <?php foreach ($fieldset['fields'] as $prop) { ?>
                                <?php if (isset($props_values[$prop['id']])) { ?>
                                    <?php $prop_field = $props_fields[$prop['id']]; ?>
                                    <div class="props-info">
                                        <div class="props-info2">
                                            <div class="title">
                                                <?php html($prop['title']); ?>
                                            </div>
                                            <div class="value">
                                                <?php echo $prop_field->setItem($item)->parse($props_values[$prop['id']]); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>

                        <?php if ($fieldset['title']) { ?>
                            </div>
                        <?php } ?>

                    <?php } ?>
                </div>
            <?php } ?>

            <?php $is_tags = $ctype['is_tags'] &&
                !empty($ctype['options']['is_tags_in_item']) &&
                $item['tags']; ?>

            <?php if ($is_tags) { ?>
                <div class="info_bar">
                    <div class="bar_item">
                        <i class="fa fa-tags"></i> <?php echo html_tags_bar($item['tags']); ?>
                    </div>
                </div>
            <?php } ?>

        </div>
        <div class="video_metadata_block float_left">

            <div class="info_bar">
                <?php if ($fields['date_pub']['is_in_item']) { ?>
                    <div class="bar_item bi_date_pub" title="<?php html($fields['date_pub']['title']); ?>">
                        <i class="fa fa-calendar"></i> <?php echo $fields['date_pub']['html']; ?>
                    </div>
                    <meta itemprop="uploadDate" content="<?php echo date('c', strtotime($item['date_pub'])); ?>">
                <?php } ?>
                <?php if (!$item['is_pub']) { ?>
                    <div class="bar_item bi_not_pub">
                        <i class="fa fa-calendar"></i> <?php echo LANG_CONTENT_NOT_IS_PUB; ?>
                    </div>
                <?php } ?>
                <?php if (!empty($item['folder_title']) || !empty($item['category']['slug'])) { ?>
                    <div class="bar_item nowrap_text">
                        <i class="fa fa-folder"></i> <?php if (!empty($item['category']['slug'])) { ?><a
                            href="<?php echo href_to('video', $item['category']['slug']); ?>"><?php echo $item['category']['title']; ?></a><?php } ?><?php if (!empty($item['folder_title'])) { ?>,
                            <a
                            href="<?php echo href_to('users', $item['user']['id'], array('content', $ctype['name'], $item['folder_id'])); ?>"><?php echo $item['folder_title']; ?></a><?php } ?>
                    </div>
                <?php } ?>
                <?php if (!$item['is_approved']) { ?>
                    <div class="bar_item bi_not_approved">
                        <i class="fa fa-warning"></i> <?php echo LANG_CONTENT_NOT_APPROVED; ?>
                    </div>
                <?php } ?>
            </div>

        </div>
        <?php if ($ctype['item_append_html']) { ?>
            <div class="append_html"><?php echo $ctype['item_append_html']; ?></div>
        <?php } ?>
    </div>
    <div id="item-share" class="video_tab">
        <div class="share_code">
            <?php if ($is_access_by_link) { ?>
                <div class="ui_message ui_warning alert alert-warning"><?php echo LANG_PRIVACY_ONLY_LINK_HINT; ?></div>
            <?php } ?>
            <label><?php echo LANG_MOVIE_LINK; ?>:</label>
            <input type="text" readonly="readonly" onclick="$(this).select();" class="input form-control"
                   value="<?php echo href_to_abs('video', $item['slug'] . '.html'); ?>"/>
            <?php $this->renderControllerChild('video', 'share_code', array('movie' => array(
                'allow_embed' => $item['allow_embed'],
                'is_playlist' => (isset($item['playlist']['id']) ? $item['playlist']['id'] : 0),
                'share_code' => $item['share_code'],
                'id' => $item['id']
            ))); ?>
        </div>
    </div>
    <div id="item-report" class="video_tab"></div>
    <div id="item-stat" class="video_tab"></div>
    <div id="item-download" class="video_tab"></div>
    <div id="item-addto" class="video_tab"></div>

    <?php $hooks_html = cmsEventsManager::hookAll("content_{$ctype['name']}_item_html", $item);
    if ($hooks_html) {
        echo html_each($hooks_html);
    } ?>
    <?php if ($item['allow_embed']) { ?>
        <link itemprop="embedUrl" href="http:<?php html(provider::getEmbedUrl($item['id'])); ?>">
    <?php } ?>
    <a itemprop="url" href="<?php echo href_to_abs('video', $item['slug'] . '.html'); ?>"></a>
</div>

<?php if (method_exists($this, 'hasMenu') && $this->hasMenu('item-menu')) { ?>
    <div id="content_item_tabs">
        <div class="tabs-menu">
            <?php $this->menu('item-menu', true, 'nav nav-filtr'); ?>
        </div>
    </div>
<?php } ?>

<!--noindex-->
<div id="more_link" class="or_title hid">
    <div class="line"></div>
    <b><span><i class="fa fa-angle-down"></i> <?php echo LANG_VIDEO_TUP; ?></span><span class="hid"><i
                    class="fa fa-angle-up"></i> <?php echo LANG_VIDEO_TDOWN; ?></span></b>
</div>
<!--/noindex-->
<script type="text/javascript">
    <?php if(!empty($vconfig['autolightoff'])) { ?>
    $('.replay').on('click', function () {
        iVideo.loff_stop = false;
    });
    <?php echo $this->getLangJS('LANG_CLOSE', 'LANG_VIDEO_TUP'); ?>
    $('.player_play').on('click', function () {
        iVideo.autoLightOffInit(<?php echo (int)$vconfig['autolightoff_sec']; ?>, <?php echo (int)!empty($vconfig['autolightoff_close_by_button']); ?>);
    });
    <?php } ?>
    $(function () {
        <?php if(!empty($vconfig['hide_big_content'])) { ?>
        f_content = $('.field.ft_html.f_content');
        h = f_content.height();
        if (h > 100) {
            $(f_content).addClass('content_height').append($('#more_link').removeClass('hid'));
        }
        $('#more_link').on('click', function () {
            $('span', this).toggleClass('hid');
            $(f_content).toggleClass('content_height');
        });
        <?php } ?>
        $('.nav-tabs li a').on('click', function(e){
            icon = $('i', this);
            icon.addClass('fa-spinner fa-pulse');
            pli = $(this).parents('ul').find('li');
            $(pli).removeClass('active');
            $(this).parent().addClass('active');
            $('.video_tab').hide();
            link = $(this).attr('href');
            tab_selector = $('#'+$(this).data('id'));
            if(link !== '#'){
                $.post(link, {}, function(data) {
                    tab_selector.html(data);
                    icon.removeClass('fa-spinner fa-pulse');
                });
            } else {
                icon.removeClass('fa-spinner fa-pulse');
            }
            tab_selector.show();
            return false;
        });
        $('.nav-tabs li.active a').trigger('click');
    });
</script>