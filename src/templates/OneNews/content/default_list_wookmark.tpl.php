<?php $user = cmsUser::getInstance(); ?>
<?php
    if( $ctype['options']['list_show_filter'] ) {
        $this->renderAsset('ui/filter-panel', array(
            'css_prefix'   => $ctype['name'],
            'page_url'     => $page_url,
            'fields'       => $fields,
            'props_fields' => $props_fields,
            'props'        => $props,
            'filters'      => $filters,
            'is_expanded'  => $ctype['options']['list_expand_filter']
        ));
    }
?>

<?php $this->addJS("templates/{$this->name}/theme_js/jquery.dotdotdot.js"); ?>
<script type="text/javascript">
    $(function () {
        $('.height-20').dotdotdot({
            watch: 'window'
        });
        $('.height-tiles').dotdotdot({
            watch: 'window'
        });
    });
</script>

<?php if ($items){ ?>
<?php $fields['price']['is_in_list'] = ""; ?>
<div class="position-r">
    <div id="wookmark-gallery" class="<?php echo $ctype['name']; ?>_list">
    <?php foreach($items as $item){ ?>
        <?php $stop = 0; ?>
        <?php $autor=cmsCore::getModel('users')->getUser($item['user_id']);?>
		<div class="wookmark_item <?php echo $ctype['name']; ?>_list_item">
        <div class="wookmark_div <?php if (!empty($item['is_vip'])){ ?> is_vip<?php } ?>">
            <?php if (!$item['is_approved']){ ?>
                <div class="padding-all5 bg-info text-center <?php if (empty($item['is_new_item'])){ ?>is_edit_item<?php } ?>">
                    <?php echo !empty($item['is_draft']) ? LANG_CONTENT_DRAFT_NOTICE : (empty($item['is_new_item']) ? LANG_CONTENT_EDITED.'. ' : '').LANG_CONTENT_NOT_APPROVED; ?>
                </div>
            <?php } ?>

            <?php if (!empty($item['fields']['photo'])){ ?>
                <div class="wookmark_img_div position-r">
                    <?php if (!empty($item['is_private_item'])) { ?>
                        <?php echo html_image(default_images('private', $fields['photo']['options']['size_teaser']), $fields['photo']['options']['size_teaser'], $item['title']); ?>
                    <?php } else { ?>
                        <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>" class="wookmark_img">
                                <?php echo html_image($item['photo'], $fields['photo']['options']['size_teaser'], $item['title']); ?>
                        </a>
                    <?php } ?>
                    <?php unset($item['fields']['photo']); ?>

                    <?php if ($fields['price']['is_in_list']){ ?>
                    <span class="wookmark_img_price">
                        <?php echo $fields['price']['handler']->parse( $item['price'] ); ?>
                        <?php unset($item['price']); ?>
                    </span>
                    <?php } ?> 
                </div>
            <?php } else if ($ctype['name'] == 'board' ){ // Если нет фото и этот стиль используется для ?>

            <div class="wookmark_only_price">
                <span class="wookmark_img_price">
                    <?php echo $fields['price']['handler']->parse( $item['price'] ); ?>
                    <?php unset($item['price']); ?>
                </span>
            </div>

            <?php } ?>

            <div class="wookmark_fields">
                <?php if ($fields['date_pub']['is_in_list']){ ?>
                    <div class="tiles_date margin-t-5" title="<?php echo $fields['date_pub']['title']; ?>">
                        <?php echo html_date_time($item['date_pub']); ?> 
                    </div>
                <?php } ?>
                <?php foreach($item['fields'] as $field){ ?>

                    <?php if ($stop === 2) { break; } ?>

                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?> field_<?php echo $field['label_pos']; ?>">

                        <?php if ($field['label_pos'] != 'none'){ ?>
                            <div class="title_<?php echo $field['label_pos']; ?>">
                                <?php echo $field['title'] . ($field['label_pos']=='left' ? ': ' : ''); ?>
                            </div>
                        <?php } ?>

                        <?php if ($field['name'] == 'title' && $ctype['options']['item_on']){ ?>
                            <h2 class="value">
                                <?php if (!empty($this->menus['list_actions_menu'])){ ?>
                                    <div class="list_actions_menu controller_actions_menu dropdown_menu">
                                        <input tabindex="-1" type="checkbox" id="menu_label_<?php echo $item['id']; ?>">
                                        <label for="menu_label_<?php echo $item['id']; ?>" class="group_menu_title"></label>
                                        <ul class="list_actions menu">
                                            <?php foreach($this->menus['list_actions_menu'] as $menu){ ?>
                                                <li>
                                                    <a class="<?php echo isset($menu['options']['class']) ? $menu['options']['class'] : ''; ?>" href="<?php echo string_replace_keys_values($menu['url'], $item); ?>" title="<?php html($menu['title']); ?>">
                                                        <?php echo $menu['title']; ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                                <?php if ($item['parent_id']){ ?>
                                    <a class="parent_title" href="<?php echo rel_to_href($item['parent_url']); ?>"><?php html($item['parent_title']); ?></a>
                                    &rarr;
                                <?php } ?>

                                <?php if (!empty($item['is_private_item'])) { $stop++; ?>
                                    <?php html($item[$field['name']]); ?> <span class="is_private" title="<?php html($item['private_item_hint']); ?>"></span>
                                <?php } else { ?>
                                    <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>"><?php html($item[$field['name']]); ?></a>
                                    <?php if ($item['is_private']) { ?>
                                        <span class="is_private" title="<?php echo LANG_PRIVACY_HINT; ?>"></span>
                                    <?php } ?>
                                <?php } ?>
                            </h2>
                        <?php } else { ?>
                            <div class="value">
                                <?php if (!empty($item['is_private_item'])) { ?>
                                    <div class="private_field_hint"><?php echo $item['private_item_hint']; ?></div>
                                <?php } else { ?>
                                    <?php echo $field['html']; ?>
                                <?php } ?>
                            </div>
                        <?php } ?>

                    </div>

                <?php } ?>



                <?php if ($ctype['is_tags'] && !empty($ctype['options']['is_tags_in_list']) &&  $item['tags']){?>
                    <div class="tags_bar">
					<span class="glyphicon glyphicon-tags" style="padding-right: 2px;"></span>
                        <?php echo html_tags_bar($item['tags']); ?>
                    </div>
                <?php } ?>

                <?php
					$show_bar = !empty($item['rating_widget']) ||
								$fields['date_pub']['is_in_list'] ||
								$fields['user']['is_in_list'] ||
                        ($ctype['is_comments'] && $item['is_comments_on']) ||
								!empty($ctype['options']['hits_on']) || 
								!$item['is_pub'] || 
								!$item['is_approved'];
                ?>

            </div> 
        <?php if ($show_bar){ ?>
        <div class="info-bar-style1 media">
            <?php if ($fields['user']['is_in_list']){ ?>
                
            <div class="media-left media-middle bar-user-title">
                <a class="avatar-circle pull-left" href="<?php echo href_to('users', $item['user']['id']) ?>" title="<?php echo $item['user']['nickname']; ?>">
                    <?php echo html_avatar_image($autor['avatar'], 'micro', $autor['nickname']); ?>
                </a>
            </div>
            <div class="media-body media-middle">
                <a href="/users/<?php echo $item['user']['id']; ?>" class="bar-user-title-a height-20">
                    <?php echo ($item['user']['nickname']); ?>
                </a>
            </div>

            <?php } ?>

            <div class="media-right media-middle nowrap font-s12">
                <?php if (!empty($ctype['options']['hits_on'])){ ?>
                    <span class="bar-user-item bar_item bi_hits" title="<?php echo LANG_HITS; ?>">
                        <span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span>
                        <span><?php echo $item['hits_count']; ?></span>
                    </span>
                    &nbsp;
                <?php } ?>
                <?php if ($ctype['is_comments'] && $item['is_comments_on']){ ?>
                    <span class="bar-user-item">
                        <span class="glyphicon glyphicon-comment font-s12"></span> 
                        <?php if (!empty($item['is_private_item'])) { ?>
                            <?php echo intval($item['comments']); ?>
                        <?php } else { ?>
                            <a href="<?php echo href_to($ctype['name'], $item['slug'].'.html'); ?>#comments" title="<?php echo LANG_COMMENTS; ?>">
                                <?php echo intval($item['comments']); ?>
                            </a>
                        <?php } ?>
                    </span>
                    &nbsp;
                <?php } ?>
                <?php if (!empty($item['rating_widget'])){ ?>
                    <span class="bar-user-item">
                        <span class="glyphicon glyphicon-star"></span>
                        <span>    
                            <?php echo intval($item['rating']); ?>
                        </span>
                    </span>
                <?php } ?>
            </div>

            </div>
            <?php } ?>  

            </div> <!-- End wookmark_item -->
            </div> <!-- End wookmark_div -->
    <?php } ?>
    </div> <!-- End gallery -->
</div>
    <?php if ($perpage < $total) { ?>
        <?php echo html_pagebar($page, $perpage, $total, $page_url, array_merge($filters, $ext_hidden_params)); ?>
    <?php } ?>

    <?php $this->addJS("templates/{$this->name}/theme_js/imagesloaded.pkgd.min.js"); ?>
    <?php $this->addJS("templates/{$this->name}/theme_js/wookmark.js"); ?>

    <script type="text/javascript">
        (function () {
            function getWindowWidth() {
                return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
            }

            var wookmark;
            imagesLoaded('#wookmark-gallery', function () {
                wookmark = new Wookmark('#wookmark-gallery', {
                    itemWidth: 210,
                    offset: 15,
                    align: 'left',
                    flexibleWidth: function () {
                        return getWindowWidth() < 1024 ? '100%' : '50%';
                    }
                });
            });
        })(jQuery);
    </script>

<?php } else {

    if(!empty($ctype['labels']['many'])){
        echo sprintf(LANG_TARGET_LIST_EMPTY, $ctype['labels']['many']);
    } else {
        echo LANG_LIST_EMPTY;
    }

}
