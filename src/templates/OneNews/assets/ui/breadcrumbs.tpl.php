<?php $listed = array(); ?>
<ul class="list-null flex clearfix">
    <li class="site-home">
        <a href="<?php echo $options['home_url']; ?>" title="<?php echo LANG_HOME; ?>"><span class="glyphicon glyphicon-home"></span></a>
    </li>

    <?php if ($breadcrumbs) { ?>

        <li class="sep"></li>

        <?php foreach($breadcrumbs as $id=>$item){ ?>

            <?php if (in_array($item['href'], $listed)){ continue; } ?>

            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                <?php if (!isset($item['is_last'])){ ?>
                    <a href="<?php echo $item['href']; ?>" itemprop="url">
                        <span itemprop="title"><?php html($item['title']); ?></span>
                    </a>
                <?php } else { ?>
                <a href="<?php echo $item['href']; ?>" itemprop="url">
                    <span itemprop="title"><?php html($item['title']); ?></span>
                </a>
                <?php } ?>
            </li>

            <?php if ($id < sizeof($breadcrumbs)-1){ ?>
                <li class="sep"></li>
            <?php } ?>

            <?php $listed[] = $item['href']; ?>

        <?php } ?>

    <?php } ?>
</ul>