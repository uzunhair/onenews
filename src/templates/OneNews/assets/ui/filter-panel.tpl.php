<?php $user = cmsUser::getInstance(); ?>

    <script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '.dropdown-menu', function (e) {
            $(this).hasClass('keep_open') && e.stopPropagation(); // This replace if conditional.
        }); 
    });
    </script>
    <?php
        $this->addJSFromContext('templates/OneNews/js/jquery-chosen.js');
        $this->addCSSFromContext('templates/OneNews/css/jquery-chosen.css');
    ?>
<?php if (!isset($is_expanded)){ $is_expanded = false; } unset($filters['user_id']); ?>
<?php $form_url = is_array($page_url) ? $page_url['base'] : $page_url; $form_url_sep = strpos($form_url, '?') === false ? '?' : '&'; ?>
<div class="filter-panel gui-panel <?php echo $css_prefix;?>-filter clearfix">
    <div class="filter-link" <?php if($filters || $is_expanded){ ?>style="display:none"<?php } ?>>
        <a href="javascript:toggleFilter()"><span><span class="glyphicon glyphicon-search"></span> <?php echo LANG_SHOW_FILTER; ?></span></a>
    </div>
    <div class="filter-container" <?php if(!$filters && !$is_expanded){ ?>style="display:none"<?php } ?>>
		<div class="filter-close">
            <a href="javascript:toggleFilter();"><span><?php echo LANG_CLOSE; ?></span></a>
        </div>
        <form action="<?php echo $form_url; ?>" method="get">
            <?php echo html_input('hidden', 'page', 1); ?>
            <?php if(!empty($ext_hidden_params)){ ?>
                <?php foreach($ext_hidden_params as $fname => $fvalue){ ?>
                    <?php echo html_input('hidden', $fname, $fvalue); ?>
                    <?php if($filters){ $filters[$fname] = $fvalue; } ?>
                <?php } ?>
            <?php } ?>
            <div class="fields clearfix">
                <?php $fields_count = 0; ?>
                <?php foreach($fields as $name => $field){ ?>
                    <?php if (!$field['is_in_filter']){ continue; } ?>
                    <?php if (!empty($field['filter_view']) && !$user->isInGroups($field['filter_view'])) { continue; } ?>
                    <?php $value = isset($filters[$name]) ? $filters[$name] : null; ?>
                    <?php $output = $field['handler']->setItem(array('ctype_name' => $css_prefix, 'id' => null))->getFilterInput($value); ?>
                    <?php if (!$output){ continue; } ?>
                    <?php $fields_count++; ?>
                    <div class="field ft_<?php echo $field['type']; ?> f_<?php echo $field['name']; ?>">
                        <?php if($field['type']=='checkbox') { ?>
                          <label class="btn btn-default filter-checkbox">
                            <?php echo $output; ?> <?php echo $field['title']; ?>
                          </label>
                        <?php } elseif (($field['type']=='city') || ($field['type'] == 'list' && !($field['options']['filter_multiple'])) ) { ?>
                                <?php echo $output; ?>
                                <script>
                                        $('.f_<?php echo $field['name']; ?> select').chosen({no_results_text: '<?php echo LANG_LIST_EMPTY; ?>', placeholder_text_single: '<?php echo $field['title']; ?>', disable_search_threshold: 8, width: '100%', allow_single_deselect: true, search_placeholder: '<?php echo LANG_BEGIN_TYPING; ?>'});
                                </script>
                        <?php } else { ?>
                            <div class="dropdown">
                                <button id="f_<?php echo $field['name']; ?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default">
                                    <?php echo $field['title']; ?>
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu keep_open" aria-labelledby="f_<?php echo $field['name']; ?>">
                                    <?php echo $output; ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>    
                <?php } ?>
                <?php if (!empty($props_fields)){ ?>
                    <?php foreach($props as $prop){ ?>
                        <?php
                            if (!$prop['is_in_filter']){ continue; }
                            $fields_count++;
                            $field = $props_fields[$prop['id']];
                            $field->setName("p{$prop['id']}");
                            if ($prop['type'] == 'list' && !empty($prop['options']['is_filter_multi'])){ $field->setOption('filter_multiple', true); }
                            if ($prop['type'] == 'number' && !empty($prop['options']['is_filter_range'])){ $field->setOption('filter_range', true); }
                            $value = isset($filters["p{$prop['id']}"]) ? $filters["p{$prop['id']}"] : null;
                        ?>
                        <div class="field ft_<?php echo $prop['type']; ?> f_prop_<?php echo $prop['id']; ?>">
                        <?php if($prop['type']=='checkbox') { ?>
                          <label class="btn btn-default filter-checkbox">
                            <?php echo $field->getFilterInput($value); ?> <?php echo $prop['title']; ?>
                          </label>
                        <?php } elseif ($prop['type'] == 'list' && !($prop['options']['is_filter_multi'])) { ?>
                                <?php echo $field->getFilterInput($value); ?>
                                <script>
                                        $('.f_prop_<?php echo $prop['id']; ?> select').chosen({no_results_text: '<?php echo LANG_LIST_EMPTY; ?>', placeholder_text_single: '<?php echo $prop['title']; ?>', disable_search_threshold: 8, width: '100%', allow_single_deselect: true, search_placeholder: '<?php echo LANG_BEGIN_TYPING; ?>'});
                                </script>
                        <?php } else { ?>
                            <div class="dropdown">
                                <button id="f_<?php echo $prop['name']; ?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default">
                                    <?php echo $prop['title']; ?>
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu keep_open" aria-labelledby="f_<?php echo $prop['name']; ?>">
                                    <?php echo $field->getFilterInput($value); ?>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <?php if ($fields_count) { ?>
                <div class="buttons">
                    <?php echo html_submit(LANG_FILTER_APPLY); ?>
                    <?php if (sizeof($filters)){ ?>
                        <div class="link">
                            <a href="<?php echo ((is_array($page_url) && !empty($page_url['cancel'])) ? $page_url['cancel'] : $form_url); ?>"><?php echo LANG_CANCEL; ?></a>
                        </div>
                        <div class="link">
                            # <a href="<?php echo $form_url.$form_url_sep.http_build_query($filters); ?>"><?php echo LANG_FILTER_URL; ?></a>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </form>
    </div>
</div>
<?php if (!$fields_count) { ?>
<script type="text/javascript">
    $(function (){
        $('.filter-panel.groups-filter').hide();
    });
</script>
<?php } ?>
