<?php
    $config = cmsConfig::getInstance();
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo ERR_PAGE_NOT_FOUND; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link type="text/css" rel="stylesheet" href="<?php echo $config->root; ?>templates/<?php echo $config->template; ?>/css/bootstrap.min.css"); ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $config->root; ?>templates/<?php echo $config->template; ?>/css/theme-errors.css"); ?>
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>
<body>
<div id="error404" class="container">
    <div class="display-center">
        <div class="jumbotron">
            <h1 class="text-muted">404</h1>
            <p class="lead text-center"><?php echo ERR_PAGE_NOT_FOUND; ?></p>
                <form action="<?php echo href_to('search'); ?>" method="get">
                    <div class="form-inline">
                        <?php echo html_input('text', 'q', '', array('placeholder' => ERR_SEARCH_QUERY_INPUT)); ?>
                        <button type="submit" name="submit" class="btn btn-default"><?php echo ERR_SEARCH_TITLE; ?></button>
                    </div>
                </form>
            <hr class="my-2">
            <p>
                <a class="btn btn-primary btn-lg text-center col-xs-12" href="<?php echo $config->root; ?>"><?php echo LANG_BACK_TO_HOME; ?></a>
            </p>
        </div>
    </div>
</div>
</body>

