C:\Documents and Settings\777\Application Data\Sublime Text 2

Системные файлы шаблона
 htmlhelper 

- main.tpl.php  	    -	Тело шаблона
- options.form.php 	  -	Настройка шаблона
- options.css.php  	  -	Настройка css шаблона

 assets
  errors
  -error.tpl.php 		   Ошибка
  -notfound.tpl.php 	 Ошибка 404
  -offline.tpl.php 		 Сайт отключен


|---admin.tpl.php
|---main.tpl.php    				    Главный файл шаблона
|---options.css.php 				    Css Опции
|---options.form.php            Файл для записи настроек
|---scheme.html                 Схема шаблона
|---assets
|   |---errors
|   |   |---error.tpl.php       Ошибка
|   |   |---notfound.tpl.php    404 ошибка
|   |   `---offline.tpl.php     Сайт отключен
|   |---fields
|   |   |---age.tpl.php         Возраст
|   |   |---caption.tpl.php      
|   |   |---checkbox.tpl.php    Чекбокс
|   |   |---city.tpl.php        Город
|   |   |---color.tpl.php       Цвет
|   |   |---date.tpl.php        Дата
|   |   |---date_range.tpl.php
|   |   |---file.tpl.php 
|   |   |---hidden.tpl.php
|   |   |---html.tpl.php        
|   |   |---image.tpl.php
|   |   |---images.tpl.php
|   |   |---list.tpl.php
|   |   |---listgroups.tpl.php
|   |   |---listmultiple.tpl.php
|   |   |---number.tpl.php
|   |   |---string.tpl.php
|   |   |---text.tpl.php
|   |   |---url.tpl.php
|   |   |---user.tpl.php
|   |   `---users.tpl.php
|   `---ui
|       |---breadcrumbs.tpl.php             Хлебные крошки
|       |---filter-panel.tpl.php            Фильтр
|       |---form.tpl.php                    Формы
|       |---grid-data.tpl.php
|       |---grid-perms.tpl.php
|       `---menu.tpl.php                    Меню
|---content                                 Шаблоны для списка типов контента
|   |---default_item.tpl.php
|   |---default_list.tpl.php
|   |---default_list_featured.tpl.php
|   |---default_list_table.tpl.php
|   `---default_list_tiles.tpl.php
|---controllers                           Контроллеры
|   |---activity
|   |   |---index.tpl.php
|   |   |---list.tpl.php
|   |   |---profile_tab.tpl.php
|   |   |---styles.css
|   |   |---backend
|   |   |   |---options.tpl.php
|   |   |   `---perms.tpl.php
|   |   `---widgets
|   |       `---list
|   |           `---list.tpl.php
|   |---admin
|   |   |---content.tpl.php
|   |   |---content_cats_add.tpl.php
|   |   |---content_cats_order.tpl.php
|   |   |---content_filter.tpl.php
|   |   |---content_item_move.tpl.php
|   |   |---controllers.tpl.php
|   |   |---controllers_edit.tpl.php
|   |   |---credits.tpl.php
|   |   |---ctypes.tpl.php
|   |   |---ctypes_basic.tpl.php
|   |   |---ctypes_dataset.tpl.php
|   |   |---ctypes_datasets.tpl.php
|   |   |---ctypes_field.tpl.php
|   |   |---ctypes_fields.tpl.php
|   |   |---ctypes_field_options.tpl.php
|   |   |---ctypes_labels.tpl.php
|   |   |---ctypes_moderator.tpl.php
|   |   |---ctypes_moderators.tpl.php
|   |   |---ctypes_perms.tpl.php
|   |   |---ctypes_prop.tpl.php
|   |   |---ctypes_props.tpl.php
|   |   |---index.tpl.php
|   |   |---install_finish.tpl.php
|   |   |---install_ftp.tpl.php
|   |   |---install_package_info.tpl.php
|   |   |---install_upload.tpl.php
|   |   |---menu.tpl.php
|   |   |---menu_form.tpl.php
|   |   |---menu_item.tpl.php
|   |   |---settings.tpl.php
|   |   |---settings_scheduler.tpl.php
|   |   |---settings_scheduler_task.tpl.php
|   |   |---settings_theme.tpl.php
|   |   |---styles.css
|   |   |---update.tpl.php
|   |   |---user.tpl.php
|   |   |---users.tpl.php
|   |   |---users_filter.tpl.php
|   |   |---users_group.tpl.php
|   |   |---users_group_perms.tpl.php
|   |   |---widgets.tpl.php
|   |   |---widgets_page.tpl.php
|   |   |---widgets_settings.tpl.php
|   |   `---admin
|   |---auth
|   |   |---index.tpl.php
|   |   |---login.tpl.php
|   |   |---registration.tpl.php
|   |   |---reset.tpl.php
|   |   |---restore.tpl.php
|   |   `---backend
|   |       `---options.tpl.php
|   |---comments
|   |   |---comment.tpl.php
|   |   |---index.tpl.php
|   |   |---list.tpl.php
|   |   |---list_index.tpl.php
|   |   |---profile_tab.tpl.php
|   |   |---backend
|   |   |   |---options.tpl.php
|   |   |   `---perms.tpl.php
|   |   `---widgets
|   |       `---list
|   |           `---list.tpl.php
|   |---content
|   |   |---category_form.tpl.php
|   |   |---category_view.tpl.php
|   |   |---folder_form.tpl.php
|   |   |---item_form.tpl.php
|   |   |---item_props_fields.tpl.php
|   |   |---item_view.tpl.php
|   |   |---profile_tab.tpl.php
|   |   `---widgets
|   |       |---categories
|   |       |   `---categories.tpl.php
|   |       |---list
|   |       |   |---list1.tpl.php
|   |       |   |---list_basic.tpl.php
|   |       |   |---list_compact.tpl.php
|   |       |   |---list_featured.tpl.php
|   |       |   |---list_tiles_big.tpl.php
|   |       |   `---list_tiles_small.tpl.php
|   |       `---slider
|   |           `---slider.tpl.php
|   |---controllers
|   |---frontpage
|   |   `---index.tpl.php
|   |---geo
|   |   `---widget.tpl.php
|   |---groups
|   |   |---group_activity.tpl.php
|   |   |---group_closed.tpl.php
|   |   |---group_content.tpl.php
|   |   |---group_delete.tpl.php
|   |   |---group_edit.tpl.php
|   |   |---group_edit_header.tpl.php
|   |   |---group_edit_staff.tpl.php
|   |   |---group_edit_staff_item.tpl.php
|   |   |---group_header.tpl.php
|   |   |---group_members.tpl.php
|   |   |---group_view.tpl.php
|   |   |---index.tpl.php
|   |   |---invite.tpl.php
|   |   |---invite_friends.tpl.php
|   |   |---list.tpl.php
|   |   |---profile_tab.tpl.php
|   |   |---styles.css
|   |   `---backend
|   |       |---options.tpl.php
|   |       `---perms.tpl.php
|   |---images
|   |   |---styles.css
|   |   |---upload_multi.tpl.php
|   |   `---upload_single.tpl.php
|   |---markitup
|   |   |---widget.tpl.php
|   |   `---backend
|   |       `---options.tpl.php
|   |---messages
|   |   |---contact.tpl.php
|   |   |---index.tpl.php
|   |   |---message.tpl.php
|   |   |---notices.tpl.php
|   |   `---backend
|   |       `---options.tpl.php
|   |---moderation
|   |   |---empty.tpl.php
|   |   |---index.tpl.php
|   |   `---styles.css
|   |---photos
|   |   |---album.tpl.php
|   |   |---styles.css
|   |   |---upload.tpl.php
|   |   |---view.tpl.php
|   |   `---widget.tpl.php
|   |---rating
|   |   |---info.tpl.php
|   |   |---info_list.tpl.php
|   |   |---widget.tpl.php
|   |   `---backend
|   |       `---options.tpl.php
|   |---recaptcha
|   |   |---captcha.tpl.php
|   |   `---backend
|   |       `---options.tpl.php
|   |---rss
|   |   |---feed.tpl.php
|   |   `---backend
|   |       |---edit.tpl.php
|   |       `---index.tpl.php
|   |---search
|   |   |---index.tpl.php
|   |   |---styles.css
|   |   |---backend
|   |   |   `---options.tpl.php
|   |   `---widgets
|   |       `---search
|   |           `---search.tpl.php
|   |---sitemap
|   |   |---sitemap.tpl.php
|   |   |---sitemap_index.tpl.php
|   |   `---backend
|   |       `---options.tpl.php
|   |---tags
|   |   |---search.tpl.php
|   |   |---styles.css
|   |   |---backend
|   |   |   |---tag.tpl.php
|   |   |   `---tags.tpl.php
|   |   `---widgets
|   |       `---cloud
|   |           `---cloud.tpl.php
|   |---users
|   |   |---friend_add.tpl.php
|   |   |---friend_delete.tpl.php
|   |   |---index.tpl.php
|   |   |---list.tpl.php
|   |   |---profile_closed.tpl.php
|   |   |---profile_content.tpl.php
|   |   |---profile_edit.tpl.php
|   |   |---profile_edit_header.tpl.php
|   |   |---profile_edit_notices.tpl.php
|   |   |---profile_edit_password.tpl.php
|   |   |---profile_edit_privacy.tpl.php
|   |   |---profile_edit_theme.tpl.php
|   |   |---profile_friends.tpl.php
|   |   |---profile_header.tpl.php
|   |   |---profile_invites.tpl.php
|   |   |---profile_invites_results.tpl.php
|   |   |---profile_karma.tpl.php
|   |   |---profile_tab.tpl.php
|   |   |---profile_view.tpl.php
|   |   |---styles.css
|   |   |---backend
|   |   |   |---field.tpl.php
|   |   |   |---fields.tpl.php
|   |   |   |---field_options.tpl.php
|   |   |   |---index.tpl.php
|   |   |   |---migration.tpl.php
|   |   |   |---migrations.tpl.php
|   |   |   |---options.tpl.php
|   |   |   |---perms.tpl.php
|   |   |   |---tab.tpl.php
|   |   |   `---tabs.tpl.php
|   |   `---widgets
|   |       |---avatar
|   |       |   `---avatar.tpl.php
|   |       |---list
|   |       |   `---list.tpl.php
|   |       `---online
|   |           `---online.tpl.php
|   `---wall
|       |---entry.tpl.php
|       `---list.tpl.php
|---css
|   |---colorpicker.css
|   |---datatree.css
|   |---jquery-ui.css
|   |---theme-content.css
|   |---theme-errors.css
|   |---theme-gui.css
|   |---theme-layout.css
|   |---theme-modal.css
|   |---theme-text.css
|   `---theme-widgets.css
|---js
|   |---admin-content.js
|   |---admin-moderators.js
|   |---admin-props.js
|   |---admin-users.js
|   |---admin-widgets.js
|   |---colorpicker.js
|   |---comments.js
|   |---content.js
|   |---core.js
|   |---datagrid-drag.js
|   |---datagrid-pagination.js
|   |---datagrid.js
|   |---datatree.js
|   |---files.js
|   |---fileuploader.js
|   |---geo.js
|   |---groups.js
|   |---images-upload.js
|   |---jquery-cookie.js
|   |---jquery-modal.js
|   |---jquery-scroll.js
|   |---jquery-ui.js
|   |---jquery-upload.js
|   |---jquery.js
|   |---messages.js
|   |---modal.js
|   |---photos.js
|   |---rating.js
|   |---slider.js
|   |---users.js
|   `---wall.js
|---profiles
|   |---options.form.php
|   `---styler.php
`---widgets
    |---wrapper.tpl.php
    |---wrapper_plain.tpl.php
    |---wrapper_tabbed.tpl.php
    |---auth
    |   `---auth.tpl.php
    |---html
    |   `---html.tpl.php
    |---menu
    |   `---menu.tpl.php
    `---text
        `---text.tpl.php
