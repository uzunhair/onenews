<?php

class formTemplateProfileOptions extends cmsForm {

    public function init() {

        return array(

            array(
                'type' => 'fieldset',
                'title' => LANG_ONE_NEWS_THEME_BG,
                'childs' => array(

                    new fieldImage('bg_img', array(
                        'title' => LANG_ONE_NEWS_THEME_BG_IMAGE,
                        'hint' => LANG_ONE_NEWS_THEME_BG_IMAGE_HINT,
                        'options' => array(
                            'sizes' => array('small', 'big', 'original')
                        )
                    ))
                )
            ),

            array(
                'type' => 'fieldset',
                'title' => LANG_ONE_NEWS_THEME_BG_POS,
                'childs' => array(

                    new fieldList('bg_pos_x', array(
                        'title' => LANG_ONE_NEWS_THEME_BG_POS_X,
                        'default' => 'center',
                        'items' => array(
                            'left' => LANG_ONE_NEWS_THEME_BG_POS_X_LEFT,
                            'center' => LANG_ONE_NEWS_THEME_BG_POS_X_CENTER,
                            'right' => LANG_ONE_NEWS_THEME_BG_POS_X_RIGHT,
                        )
                    )),

                    new fieldList('bg_pos_y', array(
                        'title' => LANG_ONE_NEWS_THEME_BG_POS_Y,
                        'default' => 'center',
                        'items' => array(
                            'top' => LANG_ONE_NEWS_THEME_BG_POS_Y_TOP,
                            'center' => LANG_ONE_NEWS_THEME_BG_POS_Y_CENTER,
                            'bottom' => LANG_ONE_NEWS_THEME_BG_POS_Y_BOTTOM,
                        )
                    ))

                )
            ),

            array(
                'type' => 'fieldset',
                'title' => LANG_ONE_NEWS_THEME_TOP_MARGIN,
                'childs' => array(

                    new fieldNumber('height_top', array(
                        'default' => 200,
                        'rules' => array(
                           array('min', 43),
                           array('max', 300),
                        )
                    )),

                )
            ),

        );

    }

}
