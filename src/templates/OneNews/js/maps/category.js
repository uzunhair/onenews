var icms = icms || {};

icms.mapsCategory = (function ($) {
	
	this.criteriaURL = null;
	this.cache = {};
	
    this.onDocumentReady = function() {	
		
		$('#maps-category-marker .value a').on('click', function(){
			icms.modal.open('#markers-gallery-window');
		});
		
		$('#markers-gallery a').on('click', function(){
			var url = $(this).parent('li').css('background-image');
			$('#f_icon input#icon').val($(this).data('icon'));
			$('#maps-category-marker .value .icon').css('background-image', url);
			icms.modal.close();
		});
		
		this.criteriaURL = $('#maps-category-marker').data('criteria-url');
		
		$('select#parent_id').on('change', function(){
			
			$('input#criteria_1').attr('placeholder', '');
			$('input#criteria_2').attr('placeholder', '');
			$('input#criteria_3').attr('placeholder', '');
			
			var parent_id = $(this).val();
			
			if (icms.mapsCategory.cache[parent_id]){
				var data = icms.mapsCategory.cache[parent_id];
				icms.mapsCategory.setCriteriaPlaceholders(data);
				return;
			}
			
			$.post(icms.mapsCategory.criteriaURL, {parent_id: parent_id}, function(result){
				
				if (!result) { return; }
				
				icms.mapsCategory.setCriteriaPlaceholders(result);
				
				icms.mapsCategory.cache[parent_id] = result;
				
			}, 'json');
			
		}).trigger('change');
				
    }
	
	this.setCriteriaPlaceholders = function(data){
		$('input#criteria_1').attr('placeholder', data[1]);
		$('input#criteria_2').attr('placeholder', data[2]);
		$('input#criteria_3').attr('placeholder', data[3]);		
	}

    //====================================================================//		
	
	return this;

}).call(icms.mapsCategory || {},jQuery);
