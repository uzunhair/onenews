var icms = icms || {};

icms.mapsEvents = (function ($) {

    this.onDocumentReady = function() {	
		
		$('.filter-panel .addr select').on('change', function(){
			icms.mapsEvents.submit();
		});
				
		$('.filter-panel .sort select').on('change', function(){
			icms.mapsEvents.submit();
		});
				
    }
	
	this.submit = function(){
		
		var marker_id = $('.filter-panel .addr select').val();
		var sort = $('.filter-panel .sort select').val();
		
		var query = [];
		
		if (marker_id > 0){ query.push('addr_id=' + marker_id); }
		if (sort != 'near') { query.push('period=' + sort); }
		
		query = '?' + query.join('&');
		
		window.location.href = query;
		
	}
	
    //====================================================================//		
	
	return this;

}).call(icms.mapsEvents || {},jQuery);
