var icms = icms || {};

icms.mapsForm = (function ($) {

	this.options = {};

    this.onDocumentReady = function() {

		$('#maps-marker-form #buttons .button-submit').on('click', function(){
			icms.mapsForm.submitMarker();
		});

		$('#maps-marker-form #buttons .button').on('click', function(){
			icms.modal.close();
		});

		$('#maps-marker-form #find-coords').on('click', function(){

			icms.mapsForm.findCoordsByAddress();

			return false;

		});

		$('#maps-markers-list #add-address').on('click', function(){
			icms.mapsForm.addMarker();
			return false;
		})

		$('#maps-marker-form #select-city').on('click', function(){

			if ($('#maps-marker-form #city-selection #city-exists .list .item').length > 0){
				$('#maps-marker-form #city-selection #city-search').hide();
				$('#maps-marker-form #city-selection #city-exists').show();
			} else {
				$('#maps-marker-form #city-selection #city-search').show();
				$('#maps-marker-form #city-selection #city-exists').hide();
			}


            var $pane = $('#maps-marker-form #city-selection');

            if ($pane.is(':visible')){
                $('#maps-marker-form #map-canvas').show();
                $('#maps-marker-form #b-hours').hide();
                $pane.hide();
            } else {
                $('#maps-marker-form #map-canvas').hide();
                $('#maps-marker-form #b-hours').hide();
                $pane.show();
            }

			return false;

		});

		$('#maps-marker-form #city-search .button').on('click', function(){
			icms.mapsForm.searchCityByName($(this));
			return false;
		});

		$('#maps-marker-form #city-exists .other a').on('click', function(){
			$('#maps-marker-form #city-exists').hide();
			$('#maps-marker-form #city-search').show();
			$('#maps-marker-form #city-search input#city-search-name').focus();
		});

		$('#maps-marker-form #city-exists .list .item').each(function(){
			$(this).on('click', function(){
				icms.mapsForm.selectCity({
					id: $(this).data('id'),
					name: $('.name', this).html(),
					region: $('.region', this).html(),
					country: $('.country', this).html(),
					lat: $(this).data('lat'),
					lng: $(this).data('lng'),
				});
				return false;
			});
		});

        $('#maps-marker-form #bh-edit').on('click', function(e){

            e.preventDefault();

            var $pane = $('#maps-marker-form #b-hours');

            if ($pane.is(':visible')){
                $('#maps-marker-form #map-canvas').show();
                $('#maps-marker-form #city-selection').hide();
                $pane.hide();
            } else {
                $('#maps-marker-form #map-canvas').hide();
                $('#maps-marker-form #city-selection').hide();
                $pane.show();
            }
			return false;

		});

        $('#maps-marker-form #b-hours select.bh-start').change(function(){
            var $select = $(this);
            var time = $select.val();
            var day = $select.parents('tr').data('day');
            $('#maps-marker-form #b-hours .days tr').each(function(){
                var this_day = $(this).data('day');
                if (this_day <= day) { return; }
                $(this).find('.bh-start').val(time);
            });
        });

        $('#maps-marker-form #b-hours select.bh-end').change(function(){
            var $select = $(this);
            var time = $select.val();
            var day = $select.parents('tr').data('day');
            $('#maps-marker-form #b-hours .days tr').each(function(){
                var this_day = $(this).data('day');
                if (this_day <= day) { return; }
                $(this).find('.bh-end').val(time);
            });
        });

        $('#maps-marker-form #b-hours .bh-off').click(function(){
            var $off = $(this);
            var off = $off.prop('checked');
            $off.parents('tr').find('.cell-hours .wrap').toggleClass('hidden', off);
        });

    };

    //====================================================================//

	this.addMarker = function(){

		if ($('#maps-marker-form #city-selection #city-exists .list .item').length > 0){
			$('#maps-marker-form #city-selection #city-search').hide();
			$('#maps-marker-form #city-selection #city-exists').show();
		} else {
			$('#maps-marker-form #city-selection #city-search').show();
			$('#maps-marker-form #city-selection #city-exists').hide();
		}

		icms.modal.openAjax('#maps-marker-form-window', {}, function(){

            console.log('open');

			$('#maps-marker-form input:text').val('');
			$('#maps-marker-form #input-marker-id').val('new' + icms.mapsForm.options.counter);

            $('#maps-marker-form .bh-off').prop('checked', false);
            $('#maps-marker-form .bh-start').val('0:00');
            $('#maps-marker-form .bh-end').val('0:00');
            $('#maps-marker-form .cell-hours .wrap').removeClass('hidden');

			icms.mapsForm.options.counter++;

			if (!icms.map.map){
				icms.map.createPositionMap("map-canvas", {
					center: icms.mapsForm.options.center,
					map_type: 'map',
					zoom: 15,
				}, function(lat, lng){
					$('#input-lat').val( lat.toFixed(4) );
					$('#input-lng').val( lng.toFixed(4) );
				});
			} else {
				icms.map.setPositionMapCenter(icms.mapsForm.options.center);
			}

		});

		icms.modal.setCallback('close', function(){

            console.log('close');

			$('#maps-marker-form input:text').val('');
			$('#maps-marker-form #select-city').removeClass('icon');

			if (icms.mapsForm.options.mode == 'all'){
				$('#maps-marker-form #city-name').html('');
			}

			$('#maps-marker-form #city-selection').hide();
			$('#maps-marker-form #b-hours').hide();

			if ($('#maps-marker-form #city-selection #city-exists .list .item').length > 0){
				$('#maps-marker-form #city-selection #city-search').hide();
				$('#maps-marker-form #city-selection #city-exists').show();
			} else {
				$('#maps-marker-form #city-selection #city-search').show();
				$('#maps-marker-form #city-selection #city-exists').hide();
			}

			$('#maps-marker-form #map-canvas').show();

		});

	}

	this.editMarker = function(json){

		icms.modal.openAjax('#maps-marker-form-window', {}, function(){

			var marker = JSON.parse(decodeURIComponent(json));

			$('#maps-marker-form input:text').val('');

			$('#input-marker-id').val( marker.id );

			if (marker.addr_city_id){
				$('#input-addr-country').val( marker.addr_country );
				$('#input-addr-region').val( marker.addr_region );
				$('#input-addr-city-id').val( marker.addr_city_id );
				$('#input-addr-city').val( marker.addr_city );
				$('#maps-marker-form #select-city').addClass('icon');
				$('#maps-marker-form #city-name').html(marker.addr_city);
			}

			$('#input-addr-street').val( marker.addr_street );
			$('#input-addr-house').val( marker.addr_house );
			$('#input-addr-room').val( marker.addr_room );

			$('#input-lat').val( marker.lat );
			$('#input-lng').val( marker.lng );

			$('#input-contacts-phone').val( marker.contacts.phone );
			$('#input-contacts-url').val( marker.contacts.url );
			$('#input-contacts-skype').val( marker.contacts.skype );
			$('#input-contacts-icq').val( marker.contacts.icq );
			$('#input-contacts-email').val( marker.contacts.email );

            $('#maps-marker-form #b-hours .day').each(function(){
                var $day = $(this);
                var day = $day.data('day');
                var start = '0:00', end = '0:00', off = false;
                if (day in marker.bhours){
                    start = marker.bhours[day].start;
                    end = marker.bhours[day].end;
                    off = marker.bhours[day].off == true;
                }
                $day.find('.bh-start').val(start);
                $day.find('.bh-end').val(end);
                $day.find('.bh-off').prop('checked', off);
                $day.find('.cell-hours .wrap').toggleClass('hidden', off);
            });

			if (!icms.map.map){
				icms.map.createPositionMap("map-canvas", {
					center: [Number(marker.lat), Number(marker.lng)],
					map_type: 'map',
				}, function(lat, lng){
					$('#input-lat').val( lat.toFixed(4) );
					$('#input-lng').val( lng.toFixed(4) );
				});
			} else {
				icms.map.setPositionMapCenter([marker.lat, marker.lng]);
			}

		});

		icms.modal.setCallback('close', function(){
			$('#maps-marker-form input:text').val('');
			$('#maps-marker-form #select-city').removeClass('icon');
			if (icms.mapsForm.options.mode == 'all'){
				$('#maps-marker-form #city-name').html('');
			}
			$('#maps-marker-form #city-selection').hide();
			$('#maps-marker-form #b-hours').hide();
			$('#maps-marker-form #map-canvas').show();
		});

	}

    //====================================================================//

	this.submitMarker = function(){

		var errors = false;

		var lat = $('#input-lat').val();
		var lng = $('#input-lng').val();

		$('#input-lat').removeClass('error');
		$('#input-lng').removeClass('error');

		if (!lat) { $('#input-lat').addClass('error'); errors = true; }
		if (!lng) { $('#input-lng').addClass('error'); errors = true; }

		if (errors) { return; }

        var bhours = {};

        $('#maps-marker-form #b-hours .day').each(function(){
            var $day = $(this);
            var start = $day.find('.bh-start').val();
            var end = $day.find('.bh-end').val();
            var off = $day.find('.bh-off').prop('checked');
            bhours[$day.data('day')] = {
                start: start,
                end: end,
                off: off
            };
        });

		var markerData = {
			id: $('#input-marker-id').val(),
			addr_country: $('#input-addr-country').val(),
			addr_region: $('#input-addr-region').val(),
			addr_city: $('#input-addr-city').val(),
			addr_city_id: $('#input-addr-city-id').val(),
			addr_city_lat: $('#input-addr-city-lat').val(),
			addr_city_lng: $('#input-addr-city-lng').val(),
			addr_street: $('#input-addr-street').val(),
			addr_house: $('#input-addr-house').val(),
			addr_room: $('#input-addr-room').val(),
			lat: lat,
			lng: lng,
			contacts: {
				phone: $('#input-contacts-phone').val(),
				url: $('#input-contacts-url').val(),
				skype: $('#input-contacts-skype').val(),
				icq: $('#input-contacts-icq').val(),
				email: $('#input-contacts-email').val(),
			},
            bhours: bhours
		};

		this.addMarkerToList(markerData);

		icms.modal.close();

	}

	this.addMarkerToList = function(marker){

		var json = encodeURIComponent(JSON.stringify(marker));

		var address = [];

		if (marker.addr_city) { address.push(marker.addr_city); }
		if (marker.addr_street) { address.push(marker.addr_street); }

		address = address.join(', ');

		if (address && marker.addr_house) { address += ', ' + marker.addr_house; }
		if (address && marker.addr_room) { address += ' - ' + marker.addr_room; }

		var is_edit = ($('#marker-'+marker.id).length > 0);

		if (is_edit){
			var dom = $('#marker-'+marker.id).html('');
		} else {
			var dom = $('<div class="field marker-field" id="marker-'+marker.id+'"></div>');
		}

		if (address) { dom.append('<div class="addr">'+address+'</div>'); }

		dom.append('<div class="coords">'+marker.lat+', '+marker.lng+'</div>')
			.append('<a class="edit ajaxlink" href="#edit">'+LANG_EDIT+'</a>')
			.append('<a class="delete ajaxlink"  href="#delete">'+LANG_DELETE+'</a>')
			.append('<input class="json" type="hidden" name="markers[]" value="'+json+'">');

		$('.edit', dom).on('click', function(){
			var json = $(this).parent('.field').find('.json').val();
			icms.mapsForm.editMarker(json);
		});

		$('.delete', dom).on('click', function(){
			if (confirm(LANG_PLACES_ADDR_DELETE_CONFIRM)){
                $(this).parent('.field').fadeOut(300, function(){
					$(this).remove();
                    if (icms.mapsForm.options.max){
                        var markersCount = $('.marker-field').length;
                        $('#maps-markers-list #button-add').toggle(markersCount < icms.mapsForm.options.max);
                    }
				});
			}
		});

		if (!is_edit){
			$('#maps-markers-list #list').append(dom);
            if (icms.mapsForm.options.max){
                var markersCount = $('.marker-field').length;
                $('#maps-markers-list #button-add').toggle(markersCount < icms.mapsForm.options.max);
            }
		}

	}

	this.initMarkersList = function(options){

		this.options = options;

		$('#maps-markers-list .marker-field').each(function(){

			$('.edit', $(this)).on('click', function(){
				var json = $(this).parent('.field').find('.json').val();
				icms.mapsForm.editMarker(json);
			});

			$('.delete', $(this)).on('click', function(){
				if (confirm(LANG_PLACES_ADDR_DELETE_CONFIRM)){
					$(this).parent('.field').fadeOut(300, function(){
						$(this).remove();
                        if (icms.mapsForm.options.max){
                            var markersCount = $('.marker-field').length;
                            $('#maps-markers-list #button-add').toggle(markersCount < icms.mapsForm.options.max);
                        }
					});
				}
			});

		});

	}

    //====================================================================//

	this.searchCityByName = function(button){

		var name = $('input#city-search-name').val();

		if (!name) { return; }

		var url = button.data('url');

		$('#maps-marker-form #city-search .loading').show();
		$('#maps-marker-form #city-search #city-results .list').html('').hide();
		button.prop('disabled', true);

		$.post(url, {name: name}, function(results){

			if (results){

				for(var key in results){

					var city = results[key];

					var dom = $('<div class="item" data-id="'+city.id+'" id="city-'+city.id+'"></div>')
							.append('<div class="name">'+city.name+'</div>')
							.append('<div class="details"><span class="country">'+city.country+'</span>, <span class="region">'+city.region+'</span></div>');

					$(dom).on('click', function(){
						icms.mapsForm.selectCity({
							id: $(this).data('id'),
							name: $('.name', this).html(),
							region: $('.region', this).html(),
							country: $('.country', this).html()
						}, true);
						return false;
					});

					$('#maps-marker-form #city-search #city-results .list').append(dom);

				}

			}

			$('#maps-marker-form #city-search .loading').hide();
			$('#maps-marker-form #city-search #city-results .list').show();
			button.prop('disabled', false);

		}, 'json');

	}

	this.selectCity = function (city, detectLatLng){

		if (typeof(detectLatLng) == 'undefined') { detectLatLng = false; }

		if ($('#maps-marker-form #city-selection #city-exists .list #city-'+city.id).length == 0){
			var dom = $('<div class="item" data-id="'+city.id+'" id="city-'+city.id+'"></div>')
					.append('<div class="name">'+city.name+'</div>')
					.append('<div class="details"><span class="country">'+city.country+'</span>, <span class="region">'+city.region+'</span></div>');
			$(dom).on('click', function(){
				icms.mapsForm.selectCity({
					id: city.id,
					name: city.name,
					region: city.region,
					country: city.country,
					lat: $(this).data('lat'),
					lng: $(this).data('lng'),
				});
				return false;
			});
			$('#maps-marker-form #city-selection #city-exists .list').append(dom);
		}

		$('#maps-marker-form #city-name').html(city.name);
		$('#maps-marker-form #input-addr-city').val(city.name);
		$('#maps-marker-form #input-addr-city-id').val(city.id);
		$('#maps-marker-form #input-addr-region').val(city.region);
		$('#maps-marker-form #input-addr-country').val(city.country);
		$('#maps-marker-form #map-canvas').toggle();
		$('#maps-marker-form #city-selection').toggle();
		$('#maps-marker-form a#select-city').addClass('icon');

		if (!detectLatLng){

			$('#maps-marker-form #input-addr-city-lat').val(city.lat);
			$('#maps-marker-form #input-addr-city-lng').val(city.lng);
			this.findCoordsByAddress();

		} else {

			$('#maps-marker-form #input-addr-city-lat').val('');
			$('#maps-marker-form #input-addr-city-lng').val('');

			var city_address = this.createAddress({
				country: city.country,
				region: city.region,
				city: city.name,
			});

			icms.map.getAddressCoordinates(city_address, function(lat, lng){
				lat = lat.toFixed(4);
				lng = lng.toFixed(4);
				$('#maps-marker-form #input-addr-city-lat').val(lat);
				$('#maps-marker-form #input-addr-city-lng').val(lng);
				$('#maps-marker-form #city-exists #city-'+city.id)
						.attr('data-lat', lat)
						.attr('data-lng', lng);
				icms.mapsForm.findCoordsByAddress();
			}, function(error){ return false; });

		}

	}

    //====================================================================//

	this.findCoordsByAddress = function(){

		var address = this.createAddress({});
		var is_addr = false;

		if ($('#input-addr-country').val()) { address.country = $('#input-addr-country').val(); is_addr=true; }
		if ($('#input-addr-region').val()) { address.region = $('#input-addr-region').val(); is_addr=true; }
		if ($('#input-addr-city').val()) { address.city = $('#input-addr-city').val(); is_addr=true; }
		if ($('#input-addr-street').val()) { address.street = $('#input-addr-street').val(); is_addr=true; }
		if ($('#input-addr-house').val()) { address.house = $('#input-addr-house').val(); is_addr=true; }

		if (!is_addr) { return false; }

		$('#maps-marker-form #find-coords').addClass('loading');

		icms.map.getAddressCoordinates(address, function(lat, lng){
			lat = lat.toFixed(4);
			lng = lng.toFixed(4);
			$('#input-lat').val( lat );
			$('#input-lng').val( lng );
			icms.map.setPositionMapCenter([lat, lng]);
			$('#maps-marker-form #find-coords').removeClass('loading');
		}, function(error){
			if (error) { alert(error); }
			$('#maps-marker-form #find-coords').removeClass('loading');
		});

	}

	this.createAddress = function(addressObject){

		addressObject.getString = function() {
			var segments = [];
			for (var key in this){
				if (typeof(this[key]) !== 'string') { continue; }
				segments.push(this[key]);
			}
			return segments.join(', ');
		};

		return addressObject;

	}

    //====================================================================//

	return this;

}).call(icms.mapsForm || {},jQuery);
