var icms = icms || {};

icms.mapsOwner = (function ($) {

	this.cache = [];
	this.autocompleteURL = null;
	this.submitURL = null;

    this.init = function() {

        $( "#username" ).autocomplete({
            minLength: 2,
            delay: 500,
            source: function( request, response ) {

                var term = request.term;

                if ( term in icms.mapsOwner.cache ) {
                    response( icms.mapsOwner.cache[ term ] );
                    return;
                }

                $.getJSON(icms.mapsOwner.autocompleteURL, request, function( data, status, xhr ) {
                    console.log(data);
                    icms.mapsOwner.cache[ term ] = data;
                    response( data );
                });

            }
        });

		$('#maps-place-owner .find .button').on('click', function(){

			var name = $('#maps-place-owner .find .input').val();

			if (name.length==0) { return false; }

			$('#maps-place-owner .find .field').hide();
			$('#maps-place-owner .find .loading-icon').show();

			$.post(icms.mapsOwner.submitURL, {name: name}, function(result){

				$('#maps-place-owner .find .input').val('');

				$('#maps-place-owner .find .field').show();
				$('#maps-place-owner .find .loading-icon').hide();

				if (result.error){
					alert(result.message);
					return false;
				}

				$('#maps-place-owner .old-owner').hide();

				$('#maps-place-owner #user-id').val(result.id);
				$('#maps-place-owner .new-owner .avatar').html(result.avatar);
				$('#maps-place-owner .new-owner .name').html('<a href="'+result.url+'">'+result.name+'</a>');
				$('#maps-place-owner .new-owner').show();

			}, 'json');

		});

    }

    //====================================================================//

	return this;

}).call(icms.mapsOwner || {},jQuery);
