var icms = icms || {};

icms.mapsReviews = (function ($) {

    this.onDocumentReady = function() {	
		
		$('.filter-panel .addr select').on('change', function(){
			icms.mapsReviews.submit();
		});
				
		$('.filter-panel .sort select').on('change', function(){
			icms.mapsReviews.submit();
		});
				
		$('#maps-reviews-list .item .delete').on('click', function(){
			if (!confirm(LANG_PLACES_REVIEW_DELETE_CONFIRM)){ return false; }
		});
				
		$('#maps-reviews-list .item .expand a').on('click', function(){
			
			var content = $(this).parent('.expand').parent('.content');
			content.removeClass('cropped');
			$('.cropper', content).remove();
			$(this).remove();
			return false;
			
		});
				
    }
	
	this.submit = function(){
		
		var marker_id = $('.filter-panel .addr select').val();
		var sort = $('.filter-panel .sort select').val();
		
		var query = [];
		
		if (marker_id > 0){ query.push('addr_id=' + marker_id); }
		if (sort != 'date-desc') { query.push('sort=' + sort); }
		
		query = '?' + query.join('&');
		
		window.location.href = query;
		
	}
	
    //====================================================================//		
	
	return this;

}).call(icms.mapsReviews || {},jQuery);
