var icms = icms || {};

icms.mapsEmbed = (function ($) {
		
    this.markers = [];
    this.bounds = {};
	
	this.init = function(elementId, options) {
		
        this.markers = options.markers;
        this.bounds = options.bounds;
        
		icms.map.createMap(elementId, options, function(){
			icms.mapsEmbed.onMapReady();
		}, function(){});
		
	}
    
    this.onMapReady = function(){
        
        for(var idx in this.markers){
            var m = this.markers[idx];
            icms.map.addMarker(m.id, m.lat, m.lng, m.icon);
        }
        
        if (this.markers.length > 1){
            if (icms.map.panToMarkers() == false){
                icms.map.panToBounds(this.bounds);
            }
        }
        
    }
        
    //====================================================================//		
	
	return this;

}).call(icms.mapsEmbed || {},jQuery);
