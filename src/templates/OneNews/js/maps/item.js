var icms = icms || {};

icms.mapsItem = (function ($) {

    this.mapTop = 0;

    this.init = function(elementId, options) {

		var match = location.hash.match(/^#([0-9]+)$/);

		if (match){
			var marker_id = match[1];
			$('.maps-addrs-list li.marker-'+marker_id).prependTo('.maps-addrs-list ul.addresses');
			$('.maps-addrs-list ul.addresses > li').hide().eq(0).show();
			$('.maps-item-contacts a.show-more-addrs').show();
            icms.mapsItem.setEmbedHref(marker_id);
		}

		$('.maps-item-contacts a.addr').on('click', function(){

			var lat = $(this).data('lat');
			var lng = $(this).data('lng');

			icms.map.setItemMapCenter([lat, lng]);
			icms.map.setItemMapMarkerContent($(this).html());

			$('.maps-item-contacts a.addr').removeClass('selected');
			$(this).addClass('selected');

            icms.mapsItem.setEmbedHref($(this).data('id'));

		});

        $('.maps-item-contacts .bhours a').on('click', function(e){
            e.preventDefault();
            var id = $(this).data('id');
            icms.modal.openAjax(options.url + '/bhours', {id: id});
		});

		$('.maps-item-contacts a.show-more-addrs').on('click', function(){
			$(this).hide();
			$('.maps-addrs-list ul.addresses li').show();
			return false;
		});

		var firstLink = $('.maps-item-contacts a.addr').eq(0).addClass('selected');

		options.center = [firstLink.data('lat'), firstLink.data('lng')];
		options.address = firstLink.html();

        this.mapTop = $('.maps-mini-map').offset().top;

		icms.map.createItemMap(elementId, options);

        $('.maps-mini-map').css('max-width', '100%');

        $(window).scroll(function() {
            if ($('.maps-item-contacts').width() <= 640){ return; }
            var scroll = $(window).scrollTop();
            var margin = scroll - icms.mapsItem.mapTop + 10;
            var mapHeight = $('.maps-mini-map').height();
            var maxMargin = $('.maps-item-contacts').height() - mapHeight - 10;
            if (margin > maxMargin) { margin = maxMargin; }
            if (margin < 0) { margin = 0; }
            if (scroll > icms.mapsItem.mapTop) {
                $('.maps-mini-map').css('margin-top', margin+'px');
            }
        });

    }

    this.setEmbedHref = function (marker_id){
        if (!$('.embed-link a').length){ return ;}
        var embed_url = $('.embed-link a').data('href') + '/' + marker_id;
        $('.embed-link a').attr('href', embed_url);
    }

    //====================================================================//

	return this;

}).call(icms.mapsItem || {},jQuery);
