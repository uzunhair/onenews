var icms = icms || {};

icms.mapsImport = (function ($) {

    this.onDocumentReady = function() {	
		
		$('#f_is_cats_create select').on('change', function(){
			
			var is_cats_create = $(this).val();
			
			if (is_cats_create == 1) { 
				$('#f_is_cats_nested').show(); 
				$('#f_is_cats_nested select').trigger('change');
			} else {
				$('#f_is_cats_nested').hide(); 
				$('#f_category_separator').hide(); 				
			}
			
		}).trigger('change');
		
		$('#f_is_cats_nested select').on('change', function(){
			
			var is_cats_create = $('#f_is_cats_create select').val();
			var is_cats_nested = $(this).val();
			
            console.log(is_cats_nested);
            
			if (is_cats_create ==1 && is_cats_nested ==1) { 
				$('#f_category_separator').show(); 
			} else {
				$('#f_category_separator').hide(); 				
			}
			
		}).trigger('change');
		
		$('.scheme-fields a').on('click', function(){
			
			var field = $(this).parent('.scheme-fields').find('select').val();
			var scheme = $('.scheme-template input').val().trim();

			if (scheme){
				scheme += ', ' + field;
			} else {
				scheme = field;
			}
			
			$('.scheme-template input').val(scheme);
			
			return false;
			
		});
				
    }
	
    //====================================================================//		
	
	return this;

}).call(icms.mapsImport || {},jQuery);
