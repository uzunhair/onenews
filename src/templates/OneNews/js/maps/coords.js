var icms = icms || {};

icms.mapsCoords = (function ($) {
    
    this.rowsCount = 0;
    this.currentRow = 0;
    this.rowsExist = 0;
    
    this.options = {}
    this.results = {};

    this.init = function(options) {	
		
		$('#maps-find-coords .button').on('click', function(){
			icms.mapsCoords.start();
            $(this).prop('disabled', true).css('opacity', '0.5');
            $('#maps-find-coords .buttons').addClass('loading').html(LANG_PLACES_COORDS_SEARCHING);
		});
        
        this.rowsExist = options.rowsExist;
        this.options = options;
        
    }
    
    this.start = function(){

        this.rowsCount = $('#maps-find-coords #datagrid tbody tr').length;
        this.currentRow = 0;
        this.results = {};
        
        if (this.rowsCount > 0){
            this.next();
        }
        
    }
	
	this.next = function(){
        
        var row = $('#maps-find-coords #datagrid tbody tr').eq(this.currentRow);
        var id = row.attr('id');
        var address_text = $('td', row).eq(2).html();
        
        var address = {};
        address.getString = function(){
            return address_text;
        }        
        
        icms.map.getAddressCoordinates(address, function(lat, lng){
            icms.mapsCoords.setLatLng(id, lat, lng);
        }, function(lat, lng){
            icms.mapsCoords.setLatLng(id, 0, 0);
        });
		
	}
    
    this.setLatLng = function(id, lat, lng){
        
        var row = $('#maps-find-coords #datagrid tbody tr#'+id);
        
        $('.input-lat', row).html(lat);
        $('.input-lng', row).html(lng);
        
        this.results[id] = {lat: lat, lng: lng};
        
        this.currentRow++;
        
        if (this.currentRow != this.rowsCount){
            setTimeout(function(){
                icms.mapsCoords.next();
            }, 500);
        } else {
            this.saveResults();
        }
        
    }
    
    this.saveResults = function(){
        
        $('#maps-find-coords .buttons').html(LANG_PLACES_COORDS_SAVING);
        
        $.post(this.options.saveURL, {results: this.results}, function(serverAnswer){
            
            icms.mapsCoords.resultsSaved(serverAnswer);
            
        }, 'json');
        
    }
    
    this.resultsSaved = function(serverAnswer){
        
        if (serverAnswer.error) { return; }
        
        this.rowsExist = serverAnswer.count;
        
        this.setCounter();
        
        if (this.rowsExist > 0){                        
            
            $('#maps-find-coords .buttons').html(LANG_PLACES_COORDS_SEARCHING);
            
            icms.datagrid.loadRows(function(){
               icms.mapsCoords.start(); 
            });
            
        } else {
            
            $('h2 .count').remove();
            $('#maps-find-coords .search').remove();            
            $('#maps-find-coords .none').show();            
            
        }
        
    }
    
    this.setCounter = function(){
        
        $('h2 .count').fadeOut(300, function(){
            $('h2 .count').html(icms.mapsCoords.rowsExist).fadeIn(300);
        });
        
    }
	
    //====================================================================//		
	
	return this;

}).call(icms.mapsCoords || {},jQuery);
