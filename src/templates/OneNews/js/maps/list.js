var icms = icms || {};

icms.mapsList = (function ($) {
	
	this.loadUrl = null;
	this.filter = null;
	this.reloadIntervalId = null;
	this.reloadDelay = 0;
	this.useBounds = false;
	this.cityAddr = null;
	this.cityId = null;
	this.cityCoordsSaveURL = null;
	
    this.onDocumentReady = function() {	
		
		var sW = $('#maps-city-selector').width();
		var mW = $('#maps-map-block').width();
		
		$('#maps-city-selector').css('left', mW/2-sW/2 + 'px').show();

    }
	
	this.init = function(elementId, options) {
		
		this.useBounds = options.bounds;
		this.loadUrl = options.load_url;
		this.filter = options.filter;
		
		this.cityAddr = options.city_addr;
		this.cityId = options.city_id;
		this.cityCoordsSaveURL = options.city_save_url;
        this.reloadDelay = options.delay;
		
		icms.map.createMap(elementId, options, function(){
			icms.mapsList.onMapReady();
		}, function(){
			icms.mapsList.onMapChanged();
		});
		
	}
	
	this.onMapReady = function(){
		
		if (this.cityAddr && this.cityId) {
            
            var cityAddr = this.cityAddr;
			
			icms.map.getAddressCoordinates({getString: function() { return cityAddr; }}, function(lat, lng){
				icms.map.setCenter([lat, lng]);
				icms.mapsList.saveCityCoords(lat, lng);
			}, function(){
				return false;
			});
			
		}
		
		this.loadMarkers();
		
	}
	
	this.onMapChanged = function(){

		if (!this.useBounds){ return; }

		clearTimeout(this.reloadIntervalId);

		this.reloadIntervalId = setTimeout(function() {
			icms.mapsList.loadMarkers();
		}, this.reloadDelay);
			
	}
	
	this.saveCityCoords = function(lat, lng){
		
		if (!this.cityId){ return; }
		
		$.post(this.cityCoordsSaveURL, {id: this.cityId, lat: lat, lng: lng});
		
	}
	
	this.loadMarkers = function() {
		
		var params = {
			bounds: icms.map.getBounds(), 
			filter: this.filter
		}			
		
		$.post(this.loadUrl, params, function(result){
			
			if (result.error) { return; }
			
			icms.map.clearMap();
			
			if (result.markers.length > 0){
				
				for(var i in result.markers){
					
					var m = result.markers[i];
					
					if (!m.id) { continue; }
					if (!m.icon) { m.icon = 'default.png'; }
										
					icms.map.addMarker(m.id, m.lat, m.lng, m.icon);
					
				}
				
			}
			
		}, 'json');
		
	}

    //====================================================================//		
	
	return this;

}).call(icms.mapsList || {},jQuery);
