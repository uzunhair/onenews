var icms = icms || {};

icms.map = (function ($) {

	this.iconSize = [27, 26];
	this.iconOffset = [-6, -26];

	this.options = null;
	this.map = null;
	this.placemark = null;
	this.clusterer = null;

	this.callbackChange = null;

	//====================================================================//

	this.createMap = function(elementId, options, callbackReady, callbackChange){

		this.callbackChange = callbackChange;

		ymaps.ready(function(){
			icms.map.createMapOnReady(elementId, options);
			callbackReady();
		});

	}

	this.createMapOnReady = function(elementId, options){

		this.options = options;

		this.map = new ymaps.Map(elementId, {
				center: options.center,
				zoom: options.zoom,
				type: this.getMapType(options.map_type),
				behaviors: ['default'],
				controls: ['zoomControl', 'fullscreenControl']
			}, {
				minZoom: options.min_zoom,
				maxZoom: options.max_zoom
		});

		if (!options.scroll_zoom) { this.map.behaviors.disable(['scrollZoom']); }

		if (options.map_type_select){
			this.map.controls.add('typeSelector');
		}

		this.map.events.add('boundschange', function (e) {
			icms.map.callbackChange();
		});

		this.clusterer = new ymaps.Clusterer({
            maxZoom: options.max_zoom - 1
        });

		this.map.geoObjects.add(this.clusterer);

	}

	this.getBounds = function(){

		return this.map.getBounds();

	}

	this.clearMap = function(){

		this.clusterer.removeAll();

	}

	this.setCenter = function(coords){

		this.map.setCenter(coords);

	}

	this.setZoom = function(zoom){

		this.map.setZoom(zoom);

	}

	//====================================================================//

	this.addMarker = function(id, lat, lng, icon){

		var placemark = new ymaps.Placemark([lat, lng], {}, {
			iconLayout: 'default#image',
			iconImageHref: this.options.icons_url + '/' + icon,
			iconImageSize: this.iconSize,
			iconImageOffset: this.iconOffset,

		});

		placemark.events.add('click', function(e){
		   icms.map.openMarker(id, placemark);
		});

		this.clusterer.add(placemark);

	}

	this.openMarker = function(id, marker){

		if (marker.isBalloonReady){ return; }

		marker.properties.set({
			balloonContent: '<div class="balloon-loading"></div>'
		});

		$.post(this.options.balloon_url, {id: id}, function(result){
			marker.properties.set({
				balloonContent: result
			});
			marker.isBalloonReady = true;
		}, 'html');

	}

    this.panToMarkers = function(){

        var bounds = this.clusterer.getBounds();
        this.map.setBounds(bounds);

    }

	//====================================================================//

	this.createPositionMap = function(elementId, options, dragEndCallback){

		ymaps.ready(function(){
			icms.map.createPositionMapOnReady(elementId, options, dragEndCallback);
		});

	}

    this.createPositionMapOnReady = function(elementId, options, dragEndCallback){

        var is_drag = typeof(dragEndCallback) != 'undefined';
        var zoom = typeof(options.zoom) != 'undefined' ? options.zoom : 16;

        console.log(options.map_type);

		this.map = new ymaps.Map(elementId, {
				center: options.center,
				zoom: zoom,
				type: this.getMapType(options.map_type),
				behaviors: ['default'],
				controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
			}
		);

		this.placemark = new ymaps.Placemark(options.center, {}, {
			draggable: is_drag,
			preset: 'twirl#lightblueDotIcon'
		});

        if (is_drag){
            this.placemark.events.add("dragend", function (event) {
                var coords = icms.map.placemark.geometry.getCoordinates();
                dragEndCallback(coords[0], coords[1]);
            });
        }

		this.map.geoObjects.add(this.placemark);

    }

	this.setPositionMapCenter = function(coords){

		this.placemark.geometry.setCoordinates(coords);
		this.setCenter(coords);
		this.setZoom(16);

	}

	//====================================================================//

	this.createItemMap = function(elementId, options){

		ymaps.ready(function(){
			icms.map.createItemMapOnReady(elementId, options);
		});

	}

	this.createItemMapOnReady = function(elementId, options){

		this.map = new ymaps.Map(elementId, {
				center: options.center,
				zoom: options.zoom,
				type: this.getMapType(options.map_type),
				behaviors: ['default'],
				controls: ['zoomControl', 'fullscreenControl']
			}, {
				minZoom: options.min_zoom,
				maxZoom: options.max_zoom
		});

		this.placemark = new ymaps.Placemark(options.center, {}, {
			iconLayout: 'default#image',
			iconImageHref: options.icons_url + '/' + options.icon,
			iconImageSize: this.iconSize,
			iconImageOffset: this.iconOffset
		});

		this.map.geoObjects.add(this.placemark);

		this.options = options;

		this.setItemMapMarkerContent(options.address);

	}

	this.setItemMapCenter = function(coords){

		this.placemark.geometry.setCoordinates(coords);
		this.setCenter(coords);
		this.setZoom( this.options.zoom );

	}

	this.setItemMapMarkerContent = function(address){

		this.placemark.properties.set({
			balloonContentHeader: this.options.title,
			balloonContentBody: address,
		});

	}

    //====================================================================//

	this.getAddressCoordinates = function(address, callbackSuccess, callbackError){

        var addr = ('getString' in address) ? address.getString() : address;

		ymaps.geocode(addr, {results: 1}).then(function (res) {
			var firstGeoObject = res.geoObjects.get(0);
			if (firstGeoObject){
				var coords = firstGeoObject.geometry.getCoordinates();
				callbackSuccess(coords[0], coords[1]);
			} else {
				callbackError();
			}
		}, function (err) {
			callbackError(err);
		})

	}

    //====================================================================//

	this.getMapType = function(map_type){

		var type;

		switch (map_type){
			case 'map': type = "yandex#map";break;
			case 'hybrid': type = "yandex#hybrid";break;
			case 'satellite': type = "yandex#satellite";break;
			case 'nmap': type = "yandex#publicMap";break;
			case 'nhybrid': type = "yandex#publicMapHybrid";break;
			default: type = "yandex#map";break;
		}

		return type;

	}

	return this;

}).call(icms.map || {},jQuery);
