var icms = icms || {};

icms.map = (function ($) {

	this.iconSize = [27, 26];
	this.iconOffset = [8, 26];
	this.popupOffset = [0, 0];

	this.options = null;
	this.map = null;
	this.placemark = null;
	this.markers = null;
	this.maxPopupWidth = null;

	this.clustererPluginURL = '//2gis.github.io/mapsapi/vendors/Leaflet.markerCluster/leaflet.markercluster-src.js';

	this.callbackChange = null;

	//====================================================================//

	this.createMap = function(elementId, options, callbackReady, callbackChange){

		this.callbackChange = callbackChange;

		DG.then(function(){
			return DG.plugin(icms.map.clustererPluginURL);
		}).then(function() {
			icms.map.createMapOnReady(elementId, options);
			callbackReady();
		});

	}

	this.createMapOnReady = function(elementId, options){

		this.options = options;

		this.maxPopupWidth = $('#'+elementId).width() - 50;

		this.map = DG.map(elementId, {
			center: options.center,
			zoom: options.zoom,
			minZoom: options.min_zoom,
			maxZoom: options.max_zoom,
			scrollWheelZoom: options.scroll_zoom,
		});

		this.map.on('moveend zoomend', function(e) {
			icms.map.callbackChange();
		});

		this.markers = DG.markerClusterGroup();
		this.map.addLayer(this.markers);

		this.balloon = DG.popup();

	}

	this.getBounds = function(){

		var bounds = this.map.getBounds();

		return [
			[bounds.getSouthWest().lat, bounds.getSouthWest().lng],
			[bounds.getNorthEast().lat, bounds.getNorthEast().lng]
		];

	}

	this.clearMap = function(){

		this.markers.clearLayers();

	}

	this.setCenter = function(coords){

		this.map.setView(coords);

	}

	this.setZoom = function(zoom){

		this.map.setZoom(zoom);

	}

	//====================================================================//

	this.addMarker = function(id, lat, lng, icon){

		var marker = DG.marker([lat, lng], {
			icon: DG.icon({
				iconUrl: this.options.icons_url + '/' + icon,
				iconSize: this.iconSize,
				iconAnchor: this.iconOffset,
				popupAnchor: this.popupOffset,
			}),
			riseOnHover: true
		});

		marker.bindPopup('<div class="balloon-loading"></div>');

		marker.on('click', function(e){
			icms.map.openMarker(id, marker);
		});

		this.markers.addLayer(marker);

	}

	this.openMarker = function(id, marker){

		if (marker.isBalloonReady){
			marker.openPopup();
			return;
		}

		marker.setPopupContent('<div class="balloon-loading"></div>');
		marker.openPopup();

		$.post(this.options.balloon_url, {id: id}, function(result){
			marker.unbindPopup();
			marker.bindPopup(result, {maxWidth: icms.map.maxPopupWidth});
			marker.openPopup();
			marker.isBalloonReady = true;
		}, 'html');

	}

    this.panToMarkers = function(){

        var bounds = this.markers.getBounds();
        this.map.fitBounds(bounds);

    }

	//====================================================================//

	this.createPositionMap = function(elementId, options, dragEndCallback){

        DG.then(function() {
			icms.map.createPositionMapOnReady(elementId, options, dragEndCallback);
		});

	}

    this.createPositionMapOnReady = function(elementId, options, dragEndCallback){

        var is_drag = typeof(dragEndCallback) != 'undefined';
        var zoom = typeof(options.zoom) != 'undefined' ? options.zoom : 16;

		this.map = DG.map(elementId, {
			center: options.center,
			zoom: zoom,
			fullscreenControl: false
		});

		this.placemark = DG.marker(options.center, {
			draggable: is_drag
		}).addTo(this.map);

        if (is_drag) {

            this.placemark.on('dragend', function(e){
                var coords = icms.map.placemark.getLatLng();
                dragEndCallback(coords.lat, coords.lng);
            });

            // Geocoder workaround: use Yandex.Maps
            this.includeYandexGeocoder();

        }

    }

	this.setPositionMapCenter = function(coords){

		this.placemark.setLatLng(coords);
		this.setCenter(coords);
		this.setZoom(16);

	}

	//====================================================================//

	this.createItemMap = function(elementId, options){

		DG.then(function(){
			icms.map.createItemMapOnReady(elementId, options);
		});

	}

	this.createItemMapOnReady = function(elementId, options){

		this.map = DG.map(elementId, {
			center: options.center,
			zoom: options.zoom,
			minZoom: options.min_zoom,
			maxZoom: options.max_zoom,
			fullscreenControl: false
		});

		this.placemark = DG.marker(options.center, {
			icon: DG.icon({
				iconUrl: options.icons_url + '/' + options.icon,
				iconSize: this.iconSize,
				iconAnchor: this.iconOffset,
				popupAnchor: this.popupOffset,
			}),
		});

		this.placemark.addTo(this.map);

		this.options = options;

		this.setItemMapMarkerContent(options.address);

	}

	this.setItemMapCenter = function(coords){

		this.placemark.setLatLng(coords);
		this.setCenter(coords);
		this.setZoom( this.options.zoom );

	}

	this.setItemMapMarkerContent = function(address){

		var content = '<div><strong>' + this.options.title + '</strong>' +
						'<div>' + address + '</div>';

		this.placemark.closePopup();
		this.placemark.unbindPopup();
		this.placemark.bindPopup(content);

	}

    //====================================================================//

	this.includeYandexGeocoder = function (){

		var ymapsScript = $('<script type="text/javascript" src="//api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU">');
		$("head").append(ymapsScript);

	}

	this.getAddressCoordinates = function(address, callbackSuccess, callbackError){

		ymaps.geocode(address.getString(), {results: 1}).then(function (res) {
			var firstGeoObject = res.geoObjects.get(0);
			if (firstGeoObject){
				var coords = firstGeoObject.geometry.getCoordinates();
				callbackSuccess(coords[0], coords[1]);
			} else {
				callbackError();
			}
		}, function (err) {
			callbackError(err);
		});

	}

    //====================================================================//

	return this;

}).call(icms.map || {},jQuery);
