/*
 * JavaScript for Bootstrap's docs (http://getbootstrap.com)
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under the Creative Commons Attribution 3.0 Unported License. For
 * details, see http://creativecommons.org/licenses/by/3.0/.
 */
$(document).ready(function () {
  $(function () {
        $("[rel='tooltip']").tooltip();
  });

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

$('#navbar-collapse-1').on('show.bs.collapse', function () {
  var box = $('#navbar-collapse-2');
    if (box.hasClass('collapse in')) {
     box.collapse('hide');
    }
 });

 $('#navbar-collapse-2').on('show.bs.collapse', function () {
  var box = $('#navbar-collapse-1');
    if (box.hasClass('collapse in')) {
     box.collapse('hide');
    }
 });

$('.video-info').on('click', function() {
    window.location.href = $(this).prev().attr('href');
});

});