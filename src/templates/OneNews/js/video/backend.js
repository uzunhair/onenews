var icms = icms || {};

icms.iVideoBackend = (function ($) {

    this.loaderStart = function (obj){
        $(obj).hide().after('<div class="loading-icon"></div>');
    };
    this.loaderStop = function (obj){
        $(obj).show().next().remove();
    };

    this.showCheckRegexp = function (out_text){
        if((/\.(gif|jpg|jpeg|png)(?:\?([^#]*))?(?:#(.*))?$/i).test(out_text)){
            var i = new Image();
            i.src = out_text;
            i.onload = function() {
               $.nmData($(i), {autoSizable: true, anim: {def: 'show'}});
            };
        } else {
            $.nmData($('<div class="modal_padding">'+out_text+'</div>'), {autoSizable: true, sizes: {minH: 70}, anim: {def: 'show'}});
        }
        return;
    };

    this.onDocumentReady = function() {

        $(document).on('click', '.img-modal', function (){
            $.nmManual(this, {autoSizable: true, anim: {def: 'show'}});
            $.nmTop().callbacks.afterReposition = function(){
                $('.nyroModalCont').css('overflow', 'hidden');
            };
            return false;
        });

        $('.check_regexp').on('click', function (){
            icms.iVideoBackend.loaderStart(this);
            var a = this;
            $.post($(this).attr('href'), { rxp: $(this).parents('.field').find('input, textarea').val() }, function(result){
                icms.iVideoBackend.loaderStop(a);
                if(result.errors){
                    return icms.iVideoBackend.showCheckRegexp(result.errors);
                }
                if(result.html){
                    $.nmData($(result.html), {autoSizable: true, anim: {def: 'show'}}); return;
                }
            }, 'json');
            return false;
        });
        $('.auto_copy').on('click', function (){
            $(this).parents('.input-prefix-suffix').find('input').val($(this).data('path'));
            return false;
        });

        $('#ffmpeg_data_is_watermark').on('click', function (){
            if($(this).is(':checked')){
                $('#f_ffmpeg_data_watermark_position_id, #f_wm_image, #f_ffmpeg_data_wm_margin').show().addClass('parent_field');
            } else {
                $('#f_ffmpeg_data_watermark_position_id, #f_wm_image, #f_ffmpeg_data_wm_margin').hide().removeClass('parent_field');
            }
        }).triggerHandler('click');

        $('#is_original_date_pub').on('click', function (){
            if($(this).is(':checked')){
                $('#f_is_import_comments, #f_import_comments_count, #f_comments_order').show().addClass('parent_field');
            } else {
                $('#f_is_import_comments, #f_import_comments_count, #f_comments_order').hide().removeClass('parent_field');
            }
        }).triggerHandler('click');

        $('#conversion_type').on('change', function (){
            if($(this).val() == 2){
                $('#f_concurrent_conversion').show().addClass('parent_field');
            } else {
                $('#f_concurrent_conversion').hide().removeClass('parent_field');
            }
        }).trigger('change');

        $('#screenshots_count_unit').on('change', function (){
            if($(this).val() == 0){
                $('#f_screenshots_count_fixed, #f_screenshots_count_dynamic').hide().removeClass('parent_field');
            } else if($(this).val() == 1) {
                $('#f_screenshots_count_fixed').show().addClass('parent_field');
                $('#f_screenshots_count_dynamic').hide().removeClass('parent_field');
            } else {
                $('#f_screenshots_count_dynamic').show().addClass('parent_field');
                $('#f_screenshots_count_fixed').hide().removeClass('parent_field');
            }
        }).trigger('change');

        $('#is_timeline_screenshots').on('click', function (){
            if($(this).is(':checked')){
                $('#f_timeline_screenshots_step_4, #f_timeline_screenshots_step_20, #f_timeline_screenshots_step_21, #f_timeline_screenshots_size').show().addClass('parent_field');
            } else {
                $('#f_timeline_screenshots_step_4, #f_timeline_screenshots_step_20, #f_timeline_screenshots_step_21, #f_timeline_screenshots_size').hide().removeClass('parent_field');
            }
        }).triggerHandler('click');

        $('#after_download_action').on('change', function (){
            if($(this).val() == 2){
                $('#f_conversion_after_download_min_resolution').show().addClass('parent_field');
            } else {
                $('#f_conversion_after_download_min_resolution').hide().removeClass('parent_field');
            }
        }).trigger('change');

        $('#allow_embed').on('click', function (){
            if($(this).prop('checked')){
                $('#f_allow_embed_domain, #f_free_embed_access, #f_denied_embed_domain').show().addClass('parent_field');
            } else {
                $('#f_allow_embed_domain, #f_free_embed_access, #f_denied_embed_domain').hide().removeClass('parent_field');
            }
        }).triggerHandler('click');

        $('#autolightoff').on('click', function (){
            if($(this).prop('checked')){
                $('#f_autolightoff_sec, #f_autolightoff_close_by_button').show().addClass('parent_field');
            } else {
                $('#f_autolightoff_sec, #f_autolightoff_close_by_button').hide().removeClass('parent_field');
            }
        }).triggerHandler('click');

        $('#play_remote').on('click', function (){
            if($(this).prop('checked')){
                $('#f_link_lifetime').show().addClass('parent_field');
            } else {
                $('#f_link_lifetime').hide().removeClass('parent_field');
            }
        }).triggerHandler('click');

        $('#relaying').on('change', function (){
            if($(this).val() == 0){
                $('#f_link_lifetime').hide().removeClass('parent_field');
            } else {
                $('#f_link_lifetime').show().addClass('parent_field');
            }
        }).trigger('change');

        $('#countries input, #cats input, #groups_view input, #download_view input').on('click', function (){
            v = $(this).val();
            p = $(this).parents('.input_checkbox_list');
            if(v==0){
                $('input', p).not('input[value="0"]').prop('checked', false);
            } else {
                $('input[value="0"]', p).prop('checked', false);
            }
        });

        $('#new_movie_notify_hour').width(55).after($('#f_new_movie_notify_right_away > label').css({display: 'inline-block', color: '#666'})).after($('#new_movie_notify_minute').width(55)).after('<span id="mnsep"> : </span>');
        $('#new_movie_notify_right_away').on('click', function (){
            if($(this).prop('checked')){
                $('#new_movie_notify_hour, #new_movie_notify_minute, #mnsep').hide();
            } else {
                $('#new_movie_notify_hour, #new_movie_notify_minute, #mnsep').show();
            }
        }).triggerHandler('click');

        $(document).on('click', '.export', function (){
            window.onbeforeunload = null;
        });

        $('#check_dir_no_write').on('click', function (){
            if(!confirm(LANG_ADMIN_CHECK_DIR_NO_WRITE_CONFIRM)){ return false; }
            $(this).addClass('loading').html($(this).data('toggle'));
            $.post($(this).attr('href'), {}, function(result){
                $('#fid_5').html(result);
            });
            return false;
        });

    };

    this.resoreSubmitAjax = function (){
        if(this.oldSubmitAjax){
            icms.forms.submitAjax = this.oldSubmitAjax;
        }
    };

    this.initCheckLinkSubmitAjax = function() {
        this.resoreSubmitAjax();
        this.oldSubmitAjax = icms.forms.submitAjax;
        icms.forms.submitAjax = function(form){

            $(form).find(':submit').prop('disabled', true);

            var form_data = this.toJSON($(form));

            var url = $(form).attr('action');

            $.post(url, form_data, function(result){

                if (typeof(result.errors) != 'object'){
                    return icms.iVideoBackend.showCheckRegexp(result.errors);
                }

                if (typeof(result.errors)=='object'){

                    $(form).find(':submit').prop('disabled', false);

                    $('.field_error', form).removeClass('field_error');
                    $('.error_text', form).remove();

                    for(var field_id in result.errors){
                        var id = field_id.replace(':', '_');
                        $('#f_'+id, form).addClass('field_error');
                        $('#f_'+id, form).prepend('<div class="error_text">' + result.errors[field_id] + '</div>');
                    }

                    icms.modal.resize();

                    return;

                }

            }, 'json');

            return false;

        };
    };

	return this;

}).call(icms.iVideoBackend || {},jQuery);

function formSuccess (form){
    window.location.href = window.location.pathname;
}