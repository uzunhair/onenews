function spellCount (num, one, two, many){
    if (num%10==1 && num%100!=11){
        str = one;
    } else if(num%10>=2 && num%10<=4 && (num%100<10 || num%100>=20)){
        str = two;
    } else {
        str = many;
    }
    return num+' '+str;
}
function toggleFilter(){
    filter = $('.filter-panel');
    if($('.filter-container', filter).is(':visible')){
        $('.filter-link', filter).show();
        $('.filter-container', filter).hide();
    } else {
        $('.filter-link', filter).hide();
        $('.filter-container', filter).show();
    }
}
var detectAdBlock = {
    enabled: false,
    class: 'adsbox pub_300x250 pub_300x250m pub_728x90 text-ad textAd text_ad text_ads text-ads text-ad-links',
    style: 'width: 1px !important; height: 1px !important; position: absolute !important; left: -10000px !important; top: -1000px !important;',
    is_enabled: function (){
        return this.enabled;
    },
    init: function (){
		var ad_test_block = document.createElement('div');
        ad_test_block.setAttribute('class', this.class);
        ad_test_block.setAttribute('style', this.style);
		var bait = window.document.body.appendChild(ad_test_block);
        window.setTimeout(function() {
            if(window.document.body.getAttribute('abp') !== null
            || bait.offsetParent === null
            || bait.offsetHeight == 0
            || bait.offsetLeft == 0
            || bait.offsetTop == 0
            || bait.offsetWidth == 0
            || bait.clientHeight == 0
            || bait.clientWidth == 0) {
                detectAdBlock.enabled = true;
            }
            if(window.getComputedStyle !== undefined) {
                var ad_test_block_temp = window.getComputedStyle(bait, null);
                if(ad_test_block_temp && (ad_test_block_temp.getPropertyValue('display') == 'none' || ad_test_block_temp.getPropertyValue('visibility') == 'hidden')) {
                    detectAdBlock.enabled = true;
                }
            }
            window.document.body.removeChild(bait);
            if(detectAdBlock.enabled === true){
                $(document).trigger('adblock_enabled');
            }
        }, 100);
    }
};
var iVideo = {
    adblock_enable: false,
    loff: false,
    loff_stop: false,
    loff_timer: null,
    movie_id: 0,
    close_by_button: 0,
    after_close_lightoff_url: '',
    autoLightOffInit: function (sec, close_by_button){
        iVideo.close_by_button = close_by_button || 0;
        if(iVideo.close_by_button === 0){
            $(document).on('mousemove keydown scroll', function (){
                clearTimeout(iVideo.loff_timer);
                if(iVideo.loff_stop){ return; }
                if(iVideo.loff === true){
                    iVideo.lightOn();
                }
                iVideo.loff_timer = setTimeout(iVideo.lightOff, (sec*1000));
            });
            $('body').trigger('mousemove');
        } else {
            iVideo.loff_timer = setTimeout(iVideo.lightOff, (sec*1000));
            $(document).one('click', '#lightsoff-close', function (){
                clearTimeout(iVideo.loff_timer);
                if(iVideo.loff_stop){ return; }
                if(iVideo.loff === true){
                    iVideo.lightOn();
                }
                if(iVideo.after_close_lightoff_url != ''){
                    window.location.href = iVideo.after_close_lightoff_url;
                }
                return false;
            });
        }
    },
    autoLightOffDestroy: function (){
        this.loff_stop = true;
        this.lightOn();
    },
    lightOff: function (){
        if(iVideo.loff_stop){ return; }
        $('#player_wrap').css({visibility: 'visible', position: 'relative', 'z-index': 1999});
        if(iVideo.close_by_button){
            action_button = '<div id="lightsoff-title"/><div class="lightsoff-actions"><i title="'+LANG_VIDEO_TUP+'" id="lightsoff-maximize" class="fa fa-expand" /><i title="'+LANG_CLOSE+'" id="lightsoff-close" class="fa fa-close" /></div>';
        } else { action_button = ''; }
        $('body').prepend('<div class="lightsoff-background" id="lightsoff-background">'+action_button+'</div>');
        $('#lightsoff-background').css({ height: $(document).height() }).fadeIn();
        $('#lightsoff-background').show();
        $('#lightsoff-title').html($('h1').text());
        $('.lightsoff-actions > i').addClass('preanim');
        setTimeout(function (){ $('.lightsoff-actions > i').removeClass('preanim'); }, 2000);
        iVideo.loff = true;
        if (screenfull.enabled && iVideo.close_by_button) {
            iVideo.bindFullScreen();
            $(document).on(screenfull.raw.fullscreenchange, function (){
                if(!screenfull.isFullscreen){
                    iVideo.bindFullScreen();
                }
            });
        } else {
            $('#lightsoff-maximize').hide();
        }
    },
    bindFullScreen: function (){
        $(document).one('click', '#lightsoff-maximize', function (){
            screenfull.request($('#player_wrap')[0]);
            return false;
        });
    },
    lightOn: function (){
        $('#lightsoff-background').fadeOut(function() {
            $('.lightsoff-background').remove();
            $('#player_wrap').css({'z-index' : ''});
        });
        iVideo.loff = false;
    },
    _play: function (link, headers, skip_ad, autoplay){
        if(iVideo.adblock_enable && detectAdBlock.is_enabled()){
            $('#player_container').html($('#adblock_info').removeClass('hid'));
            return;
        }
        this.setMovieId(link);
        headers = headers || {};
        skip_ad = skip_ad || 0;
        if(typeof autoplay === 'undefined'){
            autoplay = 1;
        }
        $('.replay').addClass('hid');
        $(document).off('advideoend');
        $('#player_container').fadeOut('fast', function() {
            $(this).fadeIn().html('<div class="player_play_loading"></div>');
            $.ajax({
                type: 'GET',
                url:  link+'?autopay='+autoplay+'&skip_ads='+skip_ad,
                headers: headers,
                success: function(data,status,xhr){
                    $('#player_container').append(data);
                    $('.player_play_loading').delay(400).fadeOut();
                    $('#movie_embed_title, #video_share').addClass('movie_embed_title_autohide');
                    $(document).trigger('ivideo_player_load');
                }
            });
        });
    },
    setMovieId: function (link_or_id){
        if($.isNumeric(link_or_id)){
            this.movie_id = link_or_id;
        } else {
            arr = link_or_id.split ('/');
            this.movie_id = arr[3];
        }
        return this;
    },
    play: function (link, headers, skip_ad, autoplay){
        this._play(link, headers, skip_ad, autoplay);
    },
    subscribeInit: function (){

        this.setSubscribe();

        $('.subscriber').on('click', function () {

            subscriber = this;

            $.get($(this).attr('href'), function(data){

                $(subscriber).data('issubscribe', data.is_subscribe);
                iVideo.setSubscribe(subscriber);
                $(subscriber).parent().find('.count-subscribers').html(data.count);
            }, 'json');

            return false;

        });

    },
    unSubscribeCallback: function (subscriberObj){
        return;
    },
    setSubscribe: function (subscriberObj){

        set = function (obj){
            is_subscribe = $(obj).data('issubscribe');
            $('span', obj).html($(obj).data('text'+is_subscribe));
            $(obj).attr('href', $(obj).data('link'+is_subscribe));
            if(is_subscribe == 0){
                $(obj).removeClass('unsubscribe').addClass('subscribe');
                iVideo.unSubscribeCallback(obj);
            } else {
                $(obj).removeClass('subscribe').addClass('unsubscribe');
            }
        };

        if(subscriberObj){
            set(subscriberObj); return;
        }

        $('.subscriber').each(function(indx){
            set(this);
        });

    },
    showListPlayLists: function (link, callback){
        $.post(link, {}, function(data) {
            $('#addto_playlist_wrap').html(data);
            if(callback){
                callback();
            }
        });
        return this;
    },
    removeListPlayLists: function (){
        $('#addto_playlist_wrap, #ploverlay').remove();
        return this;
    },
    initAjaxPagebar: function (list_selector){
        if($('.pagebar').length < 1){
            return;
        }
        var link = $('.pagebar a').first().attr('href');
        urlparts = link.split('?');
        if (urlparts.length >= 2) {
            link = urlparts[0];
        }
        t = $('.pagebar .pagebar_notice').html().split(' ');
        total = +t[3];
        show_more_block = $('#show_more_block');
        perpage         = +$(show_more_block).data('perpage');
        total_pages = Math.ceil(total / perpage);
        $('.pagebar').remove();
        $(show_more_block).show().on('click', function (){
            $('.fa', show_more_block).show();
            next_page = +$(show_more_block).data('page');
            $.post(link, {page: next_page, is_ajax: 1}, function(data){
                $('.fa', show_more_block).hide();
                nextnext_page = next_page+1;
                $(show_more_block).data('page', nextnext_page);
                $(list_selector).append($(data).not('.pagebar').children());
                if(next_page>=total_pages){
                    $(show_more_block).remove(); return;
                }
            });
        });
    },
    allInit: function (){
        if($('aside div.widget.menu_options').length){
            sidebar_wd_count = 0;
            $('aside > div').each(function(indx){
                var ln = $(this).find(' > *').length;
                if(ln >= 1){
                    sidebar_wd_count += ln;
                }
            });
            if(sidebar_wd_count == 1){
                if($('#breadcrumbs').length >= 1){
                    $('#body section').width('100%');
                    $('#breadcrumbs').prepend($('aside div.widget.menu_options').addClass('linked_menu'));
                } else {
                    $('#body section').width('100%').prepend($('aside div.widget.menu_options').addClass('linked_menu'));
                }
            }
        }
        $('.video_cat_desc').on('click', function() {
            window.location.href = $(this).prev().attr('href');
        });
		$('.channel_sorting_panel .sort select').on('change', function(){
            window.location.href = '?sort=' + $(this).val();
		});
        $('body').on('click', '.addto_playlist', function () {
            $('html').off('click', iVideo.removeListPlayLists);
            addto_playlist = this;
            $('#addto_playlist_wrap, #ploverlay').remove();
            icon = $('i', this);
            icon.addClass('fa-spinner fa-pulse');
            link = $(this).data('href');
            $('body').append('<div id="addto_playlist_wrap"/><div id="ploverlay" />');
            ploverlay_class = $(this).data('target');
            if(ploverlay_class){
                $('#ploverlay').addClass(ploverlay_class+'_ploverlay');
            }
            pos = $(icon).offset();
            $('#addto_playlist_wrap').css({top: pos.top, left: pos.left});
            iVideo.showListPlayLists(link, function (){
                icon.removeClass('fa-spinner fa-pulse');
                $('#ploverlay').one('click', iVideo.removeListPlayLists);
            });
            ivPlayLists.successAddPlaylist = function (){
                $.colorbox.close();
                iVideo.showListPlayLists(link);
            };
        });
        $('body').on('click', '.movie_is_private', function (){
            alert(LANG_PRIVACY_PRIVATE_HINT);
            return false;
        });

        this.subscribeInit();

    }
};
var ivPlayLists = {
    params: {
        root_url: '',
        movie_id: 0,
        skip_ads: 0
    },
    old_after: false,
    setAfter:function (newAfter){
        if(newAfter !== false){
            this.old_after = this.after;
            this.after = newAfter;
        }
        return this;
    },
    restoreAfter: function (){
        if(this.old_after !== false){
            this.after = this.old_after;
            this.old_after = false;
        }
        return this;
    },
    after: function (){
        $(document).off('advideoend');
        if(iVideo.close_by_button === 0){
            iVideo.autoLightOffDestroy();
        }
        ivPlayLists.afterPlay();
        return this;
    },
    showReplay: function (){
        $('.replay').removeClass('hid');
    },
    skipAds: function (){
        $(document).off('advideoend');
        this.params.skip_ads = 1; return this;
    },
    successAddPlaylist: function (){
        $.colorbox.close();
        $('.video_item .nav-tabs li.active a').trigger('click');
    },
    afterPlay: function (){
        if(this.params.movie_id == 0){
            this.params.movie_id = iVideo.movie_id;
        }
        $('#player_container').append('<div class="player_play_loading"></div>');
        $.get(this.params.root_url+'/after_play/'+this.params.movie_id+'?skip_ads='+this.params.skip_ads, function(data){
            $('.player_play_loading').remove();
            if(data){
                $('#player_container').html(data);
            } else {
                ivPlayLists.showReplay();
                if(ivPlayLists.runAfterPlay !== false){
                    ivPlayLists.runAfterPlay();
                }
            }
            $('#item-share-tab').trigger('click');
        });
        this.params.skip_ads = 0;
    },
    runAfterPlay: false,
    init: function (params){
        for (var prop in params){
            this.params[prop] = params[prop];
        }
        after_funct = function (){
            iPlayerWrap.restoreContext();
            ivPlayLists.after();
        };
        $(document).off('ivideoend', after_funct).on('ivideoend', after_funct);
    }
};
var imgRotation = {

    source: '',
    period: 1000,
    idx: 1,
    data: {},
    img: {},
    timeout: null,

    start: function (){

        var new_src = this.data.path+this.data.images[imgRotation.idx];
        var i       = new Image();

        i.onload = function(){
            if(!i.width){
                imgRotation.start(); return;
            }
            $(imgRotation.img).css({'background-image': 'url(' + new_src + ')'});
            imgRotation.timeout = setTimeout('imgRotation.start()', imgRotation.period);
        };

        i.src = new_src;

        imgRotation.idx++;
        if(imgRotation.idx > Object.keys(this.data.images).length){
            imgRotation.idx = 1;
        }

    },

    stop: function (){
        clearTimeout(+this.timeout);
        $(this.img).css({'background-image': 'url(' + this.source + ')'});
        imgRotation.idx = 1;
    },

    initList: function (){
        imgRotation.stop();
        imgRotation.img    = $('.background_image', this);
        imgRotation.data   = $(this).data('custom_img');
        imgRotation.source = $(imgRotation.img).css('background-image');
        imgRotation.start();
    },

    initSingle: function (){
        imgRotation.stop();
        imgRotation.img    = $(this);
        imgRotation.data   = $(this).data('custom_img');
        imgRotation.source = $(imgRotation.img).css('background-image');
        imgRotation.start();
    },

    init: function (func, selector){
        $(document).off('mouseenter', selector, imgRotation[func]).on('mouseenter', selector, imgRotation[func]).on('mouseleave', selector, function (){
            imgRotation.stop();
        });
    }

};
$(function() {
    detectAdBlock.init();
    iVideo.allInit();
    imgRotation.init('initList', '.start_rotation');
});
/*!
* screenfull
* v3.0.0 - 2015-11-24
* (c) Sindre Sorhus; MIT License
*/
!function(){"use strict";var a="undefined"!=typeof module&&module.exports,b="undefined"!=typeof Element&&"ALLOW_KEYBOARD_INPUT"in Element,c=function(){for(var a,b,c=[["requestFullscreen","exitFullscreen","fullscreenElement","fullscreenEnabled","fullscreenchange","fullscreenerror"],["webkitRequestFullscreen","webkitExitFullscreen","webkitFullscreenElement","webkitFullscreenEnabled","webkitfullscreenchange","webkitfullscreenerror"],["webkitRequestFullScreen","webkitCancelFullScreen","webkitCurrentFullScreenElement","webkitCancelFullScreen","webkitfullscreenchange","webkitfullscreenerror"],["mozRequestFullScreen","mozCancelFullScreen","mozFullScreenElement","mozFullScreenEnabled","mozfullscreenchange","mozfullscreenerror"],["msRequestFullscreen","msExitFullscreen","msFullscreenElement","msFullscreenEnabled","MSFullscreenChange","MSFullscreenError"]],d=0,e=c.length,f={};e>d;d++)if(a=c[d],a&&a[1]in document){for(d=0,b=a.length;b>d;d++)f[c[0][d]]=a[d];return f}return!1}(),d={request:function(a){var d=c.requestFullscreen;a=a||document.documentElement,/5\.1[\.\d]* Safari/.test(navigator.userAgent)?a[d]():a[d](b&&Element.ALLOW_KEYBOARD_INPUT)},exit:function(){document[c.exitFullscreen]()},toggle:function(a){this.isFullscreen?this.exit():this.request(a)},raw:c};return c?(Object.defineProperties(d,{isFullscreen:{get:function(){return Boolean(document[c.fullscreenElement])}},element:{enumerable:!0,get:function(){return document[c.fullscreenElement]}},enabled:{enumerable:!0,get:function(){return Boolean(document[c.fullscreenEnabled])}}}),void(a?module.exports=d:window.screenfull=d)):void(a?module.exports=!1:window.screenfull=!1)}();