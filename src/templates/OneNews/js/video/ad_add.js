$(function() {
    overlay = $('#f_overlay_start_time, #f_overlay_step_time, #f_overlay_pos, #f_overlay_height, #f_overlay_width, #f_overlay_bg_color');
    $('#type').on('change', function (){
        v = $(this).val();
        $('#f_data_html, #f_data_text, #f_videofile, #f_video_link, #f_max_click_count').hide();
        $('#f_show_seconds').show();
        if(v==1){
            $('#f_data_html').show();
            $('#f_show_seconds').hide();
        } else if(v==2) {
            $('#f_data_text').show();
        } else {
            $('#f_videofile, #f_video_link, #f_max_click_count').show();
            $('#f_show_seconds').hide();
        }
    }).trigger('change');
    $('#position').on('change', function (){
        v = $(this).val();
        $(overlay).hide();
        if(v==1){
            $('#f_position_id').show();
            $('#type option[value="3"], #f_show_seconds, #f_skip_seconds, #f_player_wrap_class').hide();
            if($('#f_videofile').is(':visible')){
                $('#type').val('1').trigger('change'); return;
            }
        } else if(v==4) {
            $('#f_position_id, #f_player_wrap_class').hide();
            $('#type option[value="3"]').hide();
            $('#f_show_seconds, #f_skip_seconds, #type option[value="1"]').show();
            $(overlay).show();
            if($('#f_videofile').is(':visible')){
                $('#type').val('1').trigger('change'); return;
            }
        } else if(v==5) {
            $('#f_player_wrap_class').show();
            $('#type option[value="3"], #type option[value="1"], #f_position_id').hide();
            $('#type').val('2').trigger('change');
        } else {
            $('#f_position_id, #f_player_wrap_class').hide();
            $('#type option[value="3"], #type option[value="1"]').show();
            $('#f_show_seconds, #f_skip_seconds').show();
        }
    }).trigger('change');
    var kurs = $('#billing_type_data').data('type') ? $('#billing_type_data').data($('#billing_type_data').data('type')) : 0;
    $('#billing_type').on('change', function (){
        v = $(this).val();
        $('#f_billing_type_hits, #f_billing_type_clicks, #f_billing_type_days').hide();
        $('#f_billing_type_'+v).show();
        kurs = $('#billing_type_data').data(v);
    }).trigger('change');
    $('#billing_type_hits, #billing_type_clicks, #billing_type_days').on('input', function (){
        res = $(this).val();
        if (isNaN(res) || !res){
            o = '';
        } else {
            o = $('#billing_type_data').data('restext')+': '+res * kurs+' '+$('#billing_type_data').data('currency');
        }
        $(this).next().html(o); return;
    });
});