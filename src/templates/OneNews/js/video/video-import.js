iParser = {
    addLinkObj: {},
    cat_id: 0,
    load_parse_form_url: '',
    add_parse_movie_url: '',
    current: {},
    onLoadData: false,
    oldSubmitAjax: false,
    oldmodalresize: false,
    startLoading: function (){
        $(this.current).addClass('selected_provider');
        $(this.current).find('.fa').removeClass('fa-check-square-o').addClass('fa-spinner fa-pulse');
        return this;
    },
    stopLoading: function (){
        $(this.current).find('.fa').removeClass('fa-spinner fa-pulse').addClass('fa-check-square-o');
        return this;
    },
    loadForm: function (current){
        this.current = current;
        $('.import_provider').removeClass('selected_provider');
        this.startLoading();
        $.post(this.load_parse_form_url, {id: current.data('id'), params: current.data('params')}, function(result){
            iParser.stopLoading().initFormSubmitAjax();
            $('#import_wrapform_data > div').html(result);
            $('#type').trigger('change');
            $('#import_wrap_data').html('');
            icms.forms.form_changed = false;
        });
    },
    resoreSubmitAjax: function (){
        if(this.oldSubmitAjax){
            icms.forms.submitAjax = this.oldSubmitAjax;
        }
        if(this.oldmodalresize){
            icms.modal.resize = this.oldmodalresize;
        }
        return this;
    },
    showMore: function (linkObj) {
        $(linkObj).find('.fa').show();
        $('#next').val($(linkObj).data('next'));
        $('#import_wrapform_data > div > form').data('append', true).submit();
        return false;
    },
    initFormSubmitAjax: function (){
        this.resoreSubmitAjax().oldSubmitAjax = icms.forms.submitAjax;
        this.oldmodalresize = icms.modal.resize;
        icms.forms.submitAjax = function(form){

            id_append_html = $(form).data('append'); $(form).data('append', false);

            $(form).find(':submit').prop('disabled', true).parent().append(' <i class="fa fa-spinner fa-pulse fa-lg"></i>');
            iParser.startLoading();

            var form_data = this.toJSON($(form));

            $('#next').val(0);

            var url = $(form).attr('action');

            $.post(url, form_data, function(result){

                iParser.stopLoading(); $('#show_more_block').remove();
                $(form).find(':submit').prop('disabled', false).next().remove();

                if (!result.errors){
                    if(id_append_html){
                        $('#import_wrap_data').append(result.result);
                    } else {
                        inserted = $('#import_wrap_data').html(result.result);
                        if(iParser.onLoadData !== false){ iParser.onLoadData(inserted, result); }
                    }
                    $('#f_captcha_key').hide().find('img').attr('src', '');
                    $('#captcha_sid').val('');
                    $('#captcha_key').val('');
                    return;
                }

                if (typeof(result.errors)=='object'){

                    $(form).find(':submit').prop('disabled', false);

                    $('.field_error', form).removeClass('field_error');
                    $('.error_text', form).remove();

                    for(var field_id in result.errors){
                        var id = field_id.replace(':', '_');
                        $('#f_'+id, form).addClass('field_error');
                        $('#f_'+id, form).prepend('<div class="error_text">' + result.errors[field_id] + '</div>');
                    }

                    return;

                }

            }, 'json');

            return false;

        };
    },
    init: function (load_parse_form_url, add_parse_movie_url, cat_id){
        this.cat_id = cat_id || 0;
        this.load_parse_form_url = load_parse_form_url;
        this.add_parse_movie_url = add_parse_movie_url;
        $('.import_provider').on('click', function (){
            iParser.loadForm($(this));
        });
        if(window.location.hash) {
            $('#select_provider_'+window.location.hash.substring(1)).trigger('click');
        }
        $('#import_wrapform_data').on('change', '#type', function (){
            $('.tab').not('#tab-from').not('#tab-all_types').hide();
            $('#tab-'+$(this).val()).show();
        });
        $('#import_wrap_data').on('click', '.video_tumb_block > a', function (){
            if($(this).data('code')){
                icms.modal.openHtml($(this).data('code'));
            }
            return false;
        });
        $('#import_wrap_data').on('click', '.success_parse_block', function (){
            return false;
        });
        $('#import_wrap_data').on('click', '.parse_action', function (){
            var icon = $(this).find('.fa');
            var current_class = $(icon).attr('class');
            $(icon).removeClass(current_class).addClass('fa fa-spinner fa-pulse');
            iParser.addLinkObj = $(this);
            current_params = $(this).parents('.video_parse_list').data('video');
            current_params.show_form = true;
            current_params.category_id = iParser.cat_id;
            $.post(iParser.add_parse_movie_url, current_params, function(result){
                $.nmData(result, {
                    callbacks: {
                        afterClose: function() {
                            iParser.initFormSubmitAjax();
                        },
                        afterReposition: function (){
                            $(icon).removeClass('fa-spinner fa-pulse').addClass(current_class);
                        },
                        initFilters : function (nm) {
                            nm.opener.attr('title', iParser.addLinkObj.attr('title')); nm.filters.push('title');
                        }
                    }, anim: {def: 'show'}
                });
            });
            return false;
        });
        $('#import_wrapform_data > span').on('click', function (){
            $('#import_wrapform_data').toggleClass('hide_form');
            $('#import_wrapform_data > span i').toggleClass('fa-toggle-down fa-toggle-right');
            return false;
        });
    }
};