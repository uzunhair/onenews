var ivideos = {
    data: {},
    fields_links: {},
    max_count: {},
    allow_providers: {},
    base_link: window.location.pathname,
    setValue: function (field_id){
        $('#'+field_id).val(JSON.stringify(this.data[field_id]));
    },
    add: function (field_id, params){
        key = this.data[field_id].push(params)-1;
        $('.field_movie_list').append('<div class="iwrap overflow" id="'+field_id+'_'+key+'" data-id="'+key+'" data-field_id="'+field_id+'"><div class="player_wrap" style="background-image: url('+params.photo+')"></div><div class="ioverlay"><div class="iedit">'+LANG_EDIT+'</div><div class="idelete">'+LANG_DELETE+'</div></div><div class="ifield_title" title="'+params.title+'"><span>'+params.title+'</span></div></div>');
        this.setValue(field_id);
        this.checkMax(field_id);
    },
    delete: function (field_id, id){
        $('#'+field_id+'_'+id).remove();
        this.data[field_id].splice(id, 1);
        this.setValue(field_id);
        this.checkMax(field_id);
        $('#list_for_field_'+field_id+' .iwrap').each(function(indx){
            $(this).attr('id', field_id+'_'+indx);
            $(this).data('id', indx);
        });
    },
    edit: function (field_id, id) {
        $.post(this.fields_links[field_id]+'/'+id, {allow_providers: this.allow_providers[field_id], item: this.data[field_id][id]}, function(result){
            $.nmData(result);
        });
    },
    addForm: function (field_id) {
        $.nmManual(this.fields_links[field_id], {autoSizable: true, ajax:{data: {allow_providers: this.allow_providers[field_id]}, type: "POST"}});
    },
    update: function (field_id, params, id){
        this.data[field_id][id] = params;
        this.setValue(field_id);
        $('#'+field_id+'_'+id+' .ifield_title > span').html(params.title);
    },
    colorboxResize: function (resize){
        width  = '90%';
        height = '90%';
        if($(window).width() > 960) { width = '860'; }
        if($(window).height() > 700) { height = '630'; }
        $.colorbox.settings.height = height;
        $.colorbox.settings.width  = width;
        if(resize) {
          $.colorbox.resize({
            height: height,
            width: width
          });
        }
    },
    play: function (link, title, wrap) {
        $.colorbox({
            href:link,
            transition:'none',
            title: title,
            scrolling: false,
            onComplete: function (){
                $('#cboxPrevious, #cboxNext').show();
                $('#cboxLoadingOverlay').hide();
                $('#cboxNext').off('click').on('click', function (){
                    next = $(wrap).next();
                    if($(next).length > 0){
                        $(next).find('.iplay').trigger('click');
                    } else {
                        $(wrap).parent().next().find('.iplay').trigger('click');
                    }
                });
                $('#cboxPrevious').off('click').on('click', function (){
                    $(wrap).prev().find('.iplay').trigger('click');
                });
            },
            onLoad: function (){
                ivideos.colorboxResize();
                $(window).on('resize', function(){
                    ivideos.colorboxResize(true);
                });
            },
            onClosed: function (){
                window.history.pushState({link: ivideos.base_link}, $('title').html(), ivideos.base_link);
                stopLoad();
                $(window).off('resize', function(){
                    ivideos.colorboxResize(true);
                });
            }
        });
    },
    checkMax: function (field_id){
        if(!this.max_count[field_id]){
            return this;
        }
        current_count = +$('#list_for_field_'+field_id+' > .iwrap').length;
        if(current_count >= this.max_count[field_id]){
            $('#video-parse-'+field_id).hide();
        } else {
            $('#video-parse-'+field_id).show();
        }
        return this;
    },
    init: function (field_id, link, values, max_count, allow_providers){
        this.fields_links[field_id] = link;
        this.data[field_id] = values;
        this.max_count[field_id] = +max_count;
        this.allow_providers[field_id] = allow_providers;
        this.checkMax(field_id);
    }
};
$(function(){
    var timer_id;
    $('.field_movie_list').on('click', '.idelete', function (){
        if(confirm(LANG_DELETE_MOVIE)){
            wrap = $(this).parents('.iwrap');
            ivideos.delete($(wrap).data('field_id'), $(wrap).data('id')); return false;
        }
    });
    $('.field_movie_list').on('click', '.iedit', function (){
        wrap = $(this).parents('.iwrap');
        ivideos.edit($(wrap).data('field_id'), $(wrap).data('id')); return false;
    });
    $('.add_video_parse_link').on('click', function (){
        ivideos.addForm($(this).data('field_id')); return false;
    });
    $('.field_movie_list').on('click', '.iplay', function (){
        wrap  = $(this).parents('.iwrap');
        link  = $('.ifield_title a', wrap).attr('href');
        title = $('.ifield_title span', wrap).html();
        window.history.pushState({link: link}, title, link);
        ivideos.play($(wrap).data('play_link'), title, wrap); return false;
    });
    $(document).off('ivideoend').on('ivideoend', function (){
        $('#cboxNext', document).trigger('click');
    });
    $(document).on('click', '#video_descr_button', function (){
        $(this).toggleClass('video_descr_button_sel');
        $('.player_field_content_wrap').fadeToggle();
        $('.lang_desc').toggle();
    });
    $(window).on('popstate', function(e){
        window.location.href = e.originalEvent.state.link;
    });
    icms.events.on('loadfieldtypeoptions', function (){
        $('#item_list_style, #items_list_style').trigger('change');
    });
    $('.tab').on('change', '#item_list_style', function (){
        if($(this).val() === 'Slider'){
            $('#f_is_autoplay_item_slider').show();
        } else {
            $('#f_is_autoplay_item_slider').hide();
        }
    });
    $('.tab').on('change', '#items_list_style', function (){
        if($(this).val() === 'Slider'){
            $('#f_is_autoplay_list_slider').show();
        } else {
            $('#f_is_autoplay_list_slider').hide();
        }
    })
    $('.tab').on('click', '#allow_providers input', function (){
        v = $(this).val();
        p = $(this).parents('.input_checkbox_list');
        if(v=='all'){
            $('input', p).not('input[value="all"]').prop('checked', false);
        } else {
            $('input[value="all"]', p).prop('checked', false);
        }
    });
});
function stopLoad(){
    if (window.stop !== undefined) {
        window.stop();
    } else if (document.execCommand !== undefined) {
        document.execCommand("Stop", false);
    }
    return false;
}
/*!
* screenfull
* v3.0.0 - 2015-11-24
* (c) Sindre Sorhus; MIT License
*/
!function(){"use strict";var a="undefined"!=typeof module&&module.exports,b="undefined"!=typeof Element&&"ALLOW_KEYBOARD_INPUT"in Element,c=function(){for(var a,b,c=[["requestFullscreen","exitFullscreen","fullscreenElement","fullscreenEnabled","fullscreenchange","fullscreenerror"],["webkitRequestFullscreen","webkitExitFullscreen","webkitFullscreenElement","webkitFullscreenEnabled","webkitfullscreenchange","webkitfullscreenerror"],["webkitRequestFullScreen","webkitCancelFullScreen","webkitCurrentFullScreenElement","webkitCancelFullScreen","webkitfullscreenchange","webkitfullscreenerror"],["mozRequestFullScreen","mozCancelFullScreen","mozFullScreenElement","mozFullScreenEnabled","mozfullscreenchange","mozfullscreenerror"],["msRequestFullscreen","msExitFullscreen","msFullscreenElement","msFullscreenEnabled","MSFullscreenChange","MSFullscreenError"]],d=0,e=c.length,f={};e>d;d++)if(a=c[d],a&&a[1]in document){for(d=0,b=a.length;b>d;d++)f[c[0][d]]=a[d];return f}return!1}(),d={request:function(a){var d=c.requestFullscreen;a=a||document.documentElement,/5\.1[\.\d]* Safari/.test(navigator.userAgent)?a[d]():a[d](b&&Element.ALLOW_KEYBOARD_INPUT)},exit:function(){document[c.exitFullscreen]()},toggle:function(a){this.isFullscreen?this.exit():this.request(a)},raw:c};return c?(Object.defineProperties(d,{isFullscreen:{get:function(){return Boolean(document[c.fullscreenElement])}},element:{enumerable:!0,get:function(){return document[c.fullscreenElement]}},enabled:{enumerable:!0,get:function(){return Boolean(document[c.fullscreenEnabled])}}}),void(a?module.exports=d:window.screenfull=d)):void(a?module.exports=!1:window.screenfull=!1)}();